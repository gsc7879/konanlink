package com.konantech.konanlink.data;

import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.dignara.lib.utils.PreferenceUtils;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.api.model.device.LimitInfo;
import com.konantech.konanlink.constant.IndexingConst;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;

import java.util.*;

@com.dignara.lib.annotation.DataBank
public class DataBank implements com.dignara.lib.fragment.DataBank {
    private static DataBank uniqueDataBank;

    public static final int VERSION_CODE = 0;
    public static final int VERSION_NAME = 1;
    public static final int DEVICE_NAME  = 2;

    private static final String PREP_NAME_KEY         = "apiKey";
    private static final String PREP_USERS_EMAIL      = "usersEmail";
    private static final String PREP_FOLDERS_INDEXING = "indexingFolders";
    private static final String PREP_READ_ANNOUNCE    = "readAnnounce";
    private static final String PREP_PASSWORD         = "password";
    private static final String PREP_HIDE_HINT_SEARCH = "isHideSearchHint";
    private static final String PREP_HIDE_DRAWER_MENU = "isHideDrawerMenu";
    private static final String PREP_HIDE_NEW_NOTI    = "isHideNewNoti";

    public static Map<Integer, Object> INFO = new Hashtable<>();

    private Context                        mContext;
    private String                         mApiKey;
    private String                         mUserEmail;
    private String                         mDeviceKey;
    private String                         mPassword;
    private long                           mLastAnnounceTime;
    private long                           mReadAnnounceTime;
    private ModelMapper                    mModelMapper;
    private AccountInfo                    mAccountInfo;
    private LimitInfo                      mDeviceLimitInfo;
    private ArrayList<DeviceInfo>          mDeviceList;
    private ArrayList<String>              mIndexingFolders;
    private HashMap<String, List<String>>  mExtensionTypeMap;
    private List<DeviceInfo>               mHostList;
    private Map<String, DeviceInfo>        mDeviceKeyMap;
    private Map<Integer, List<DeviceInfo>> mDeviceIndexingCategoryMap;

    public synchronized static DataBank getInstance() {
        if (uniqueDataBank == null) {
            uniqueDataBank = new DataBank();
        }
        return uniqueDataBank;
    }

    public void clearPreference() {
        PreferenceUtils.clearPreference(mContext);
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void initData(Context context) {
        mApiKey           = null;
        mUserEmail        = null;
        mDeviceKey        = null;
        mIndexingFolders  = null;
        mAccountInfo      = null;
        mPassword         = null;
        mLastAnnounceTime = 0;
        mReadAnnounceTime = 0;

        if (mHostList != null) {
            mHostList.clear();
            mHostList = null;
        } else {
            mHostList = new ArrayList<>();
        }

        if (mDeviceKeyMap != null) {
            mDeviceKeyMap.clear();
            mDeviceKeyMap = null;
        } else {
            mDeviceKeyMap = new Hashtable<>();
        }

        if (mExtensionTypeMap != null) {
            mExtensionTypeMap.clear();
            mExtensionTypeMap = null;
        } else {
            mExtensionTypeMap = new HashMap<>();
        }

        if (mDeviceIndexingCategoryMap != null) {
            mDeviceIndexingCategoryMap.clear();
            mDeviceIndexingCategoryMap = null;
        } else {
            mDeviceIndexingCategoryMap = new LinkedHashMap<>();
        }
        setContext(context);
    }

    @Override
    public void onSaveInstanceState(Bundle instanceState) {
        instanceState.putString("apiKey", mApiKey);
        instanceState.putString("userEmail", mUserEmail);
        instanceState.putString("password", mPassword);
        instanceState.putString("deviceKey", mDeviceKey);
        instanceState.putLong("lastAnnounceTime", mLastAnnounceTime);
        instanceState.putLong("lastReadAnnounceTime", mReadAnnounceTime);
        instanceState.putSerializable("accountInfo", mAccountInfo);
        instanceState.putSerializable("deviceLimitInfo", mDeviceLimitInfo);
        instanceState.putParcelableArrayList("deviceList", mDeviceList);
        instanceState.putSerializable("extensionTypeMap", mExtensionTypeMap);
        instanceState.putStringArrayList("indexingFolders", mIndexingFolders);
    }

    @Override
    public void onLoadInstanceState(Bundle instanceState) {
        mApiKey           = instanceState.getString("apiKey");
        mUserEmail        = instanceState.getString("userEmail");
        mPassword         = instanceState.getString("password");
        mDeviceKey        = instanceState.getString("deviceKey");
        mLastAnnounceTime = instanceState.getLong("lastAnnounceTime");
        mReadAnnounceTime = instanceState.getLong("lastReadAnnounceTime");
        mAccountInfo      = (AccountInfo) instanceState.getSerializable("accountInfo");
        mDeviceLimitInfo  = (LimitInfo) instanceState.getSerializable("deviceLimitInfo");
        mDeviceList       = instanceState.getParcelableArrayList("deviceList");
        mExtensionTypeMap = (HashMap<String, List<String>>) instanceState.getSerializable("extensionTypeMap");
        mIndexingFolders  = instanceState.getStringArrayList("indexingFolders");

        setDeviceInfoMap(mDeviceList);
    }

    public ModelMapper getModelMapper() {
        if (mModelMapper == null) {
            mModelMapper = new ModelMapper();
            mModelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        }
        return mModelMapper;
    }

    public void updateDeviceInfo(DeviceInfo deviceInfo) {
        DeviceInfo beforeDeviceInfo = mDeviceKeyMap.get(deviceInfo.getKey());
        getModelMapper().map(deviceInfo, beforeDeviceInfo);
        beforeDeviceInfo.setConnector(null);
        beforeDeviceInfo.initHostInfo();
    }

    public void initPreferences(Context context) {
        PreferenceUtils.clearPreference(context);
    }

    public void setExtensionTypeMap(HashMap<String, List<String>> extensionTypeMap) {
        mExtensionTypeMap = extensionTypeMap;
    }

    public String getExtensionType(String fileExtension) {
        for (String type : mExtensionTypeMap.keySet()) {
            for (String extension : mExtensionTypeMap.get(type)) {
                if (StringUtils.equalsIgnoreCase(fileExtension, extension)) {
                    return type;
                }
            }
        }
        return "";
    }

    public String getDeviceKey() {
        return getDeviceKey(mContext);
    }

    public String getDeviceKey(Context context) {
        if (StringUtils.isBlank(mDeviceKey)) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            mDeviceKey = telephonyManager.getDeviceId();
            if (StringUtils.isBlank(mDeviceKey)){
                mDeviceKey = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        return mDeviceKey;
    }


    public void setUserData(String apiKey, String usersEmail) {
        mApiKey    = apiKey;
        mUserEmail = usersEmail;
        PreferenceUtils.putPreference(mContext, PREP_NAME_KEY   , mApiKey);
        PreferenceUtils.putPreference(mContext, PREP_USERS_EMAIL, mUserEmail);
    }

    public boolean existApiKey() {
        return StringUtils.isNotBlank(getApiKey());
    }

    public void setHideSearchHint(boolean isHideSearchHint) {
        PreferenceUtils.putPreference(mContext, PREP_HIDE_HINT_SEARCH, isHideSearchHint);
    }

    public boolean isHideSearchHint() {
        return PreferenceUtils.getPreference(mContext).getBoolean(PREP_HIDE_HINT_SEARCH, false);
    }

    public void setHideDrawerMenu(boolean isHideDrawerMenu) {
        PreferenceUtils.putPreference(mContext, PREP_HIDE_DRAWER_MENU, isHideDrawerMenu);
    }

    public boolean isHideDrawerMenu() {
        return PreferenceUtils.getPreference(mContext).getBoolean(PREP_HIDE_DRAWER_MENU, false);
    }

    public void setHideNewNoti(boolean isHideNewNoti) {
        setHideNewNoti(mContext, isHideNewNoti);
    }

    public boolean isHideNewNoti() {
        return isHideNewNoti(mContext);
    }

    public void setHideNewNoti(Context context, boolean isHideNewNoti) {
        PreferenceUtils.putPreference(context, PREP_HIDE_NEW_NOTI, isHideNewNoti);
    }

    public boolean isHideNewNoti(Context context) {
        return PreferenceUtils.getPreference(context).getBoolean(PREP_HIDE_NEW_NOTI, true);
    }



    public DeviceInfo getDeviceInfo() {
        return mDeviceKeyMap.get(getDeviceKey());
    }

    public String getApiKey() {
        if (StringUtils.isBlank(mApiKey)) {
            mApiKey = PreferenceUtils.getPreference(mContext).getString(PREP_NAME_KEY, "");
        }
        return mApiKey;
    }

    public String getPassword() {
        if (StringUtils.isBlank(mPassword)) {
            mPassword = PreferenceUtils.getPreference(mContext).getString(PREP_PASSWORD, "");
        }
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
        PreferenceUtils.putPreference(mContext, PREP_PASSWORD, mPassword);
    }

    public boolean isMatchPassword(String password) {
        return StringUtils.equals(getPassword(), password);
    }

    public boolean isUsePassword() {
        return StringUtils.isNotBlank(getPassword());
    }

    public ArrayList<String> getIndexingFolders(boolean isForce) {
        if (isForce || mIndexingFolders == null) {
            String[] indexingFolders = StringUtils.split(PreferenceUtils.getPreference(mContext).getString(PREP_FOLDERS_INDEXING, ""), ",");
            mIndexingFolders = new ArrayList<>();
            for (String indexingFolder : indexingFolders) {
                mIndexingFolders.add(indexingFolder);
            }
        }
        return mIndexingFolders;
    }

    public List<String> saveIndexingFolders(String indexingFolders) {
        PreferenceUtils.putPreference(mContext, PREP_FOLDERS_INDEXING, indexingFolders);
        mIndexingFolders = getIndexingFolders(true);
        return mIndexingFolders;
    }

    public String getUsersEmail() {
        if (mUserEmail == null) {
            mUserEmail = PreferenceUtils.getPreference(mContext).getString(PREP_USERS_EMAIL, "");
        }
        return mUserEmail;
    }

    public long getReadAnnounceTime() {
        return getReadAnnounceTime(mContext);
    }

    public long getReadAnnounceTime(Context context) {
        if (mReadAnnounceTime == 0) {
            mReadAnnounceTime = PreferenceUtils.getPreference(context).getLong(PREP_READ_ANNOUNCE, 0L);
        }
        return mReadAnnounceTime;
    }

    public void saveReadAnnounceTime(long readAnnounceTime) {
        saveReadAnnounceTime(mContext, readAnnounceTime) ;
    }

    public void saveReadAnnounceTime(Context context, long readAnnounceTime) {
        mReadAnnounceTime = readAnnounceTime;
        PreferenceUtils.putPreference(context, PREP_READ_ANNOUNCE, mReadAnnounceTime);
    }


    public void setLastAnnounceDate(Context context, long lastAnnounceTime) {
        mLastAnnounceTime = lastAnnounceTime;
        if (getReadAnnounceTime(context) == 0) {
            saveReadAnnounceTime(context, lastAnnounceTime);
        }
    }

    public void setLastAnnounceDate(long lastAnnounceTime) {
        setLastAnnounceDate(mContext, lastAnnounceTime);
    }

    public boolean isNewAnnounce() {
        return isNewAnnounce(mContext);
    }

    public boolean isNewAnnounce(Context context) {
        return mLastAnnounceTime > getReadAnnounceTime(context);
    }

    public boolean isHostSetup() {
        return mHostList.size() > 0;
    }

    public List<DeviceInfo> getHostList() {
        return mHostList;
    }

    public void setLimitInfo(LimitInfo deviceLimitInfo) {
        mDeviceLimitInfo = deviceLimitInfo;
    }

    public void setDeviceInfoMap(List<DeviceInfo> deviceList) {
        mDeviceList = (ArrayList<DeviceInfo>) deviceList;

        if (mHostList == null) {
            mHostList = new ArrayList<>();
        } else {
            mHostList.clear();
        }

        if (mDeviceKeyMap == null) {
            mDeviceKeyMap = new Hashtable<>();
        } else {
            mDeviceKeyMap.clear();
        }

        if (mDeviceIndexingCategoryMap == null) {
            mDeviceIndexingCategoryMap = new LinkedHashMap<>();
        } else {
            mDeviceIndexingCategoryMap.clear();
        }

        for (DeviceInfo deviceInfo : deviceList) {
            setDeviceInfo(deviceInfo);
        }
    }

    private void setDeviceInfo(DeviceInfo deviceInfo) {
        String deviceKey = deviceInfo.getKey();

        mDeviceKeyMap.put(deviceKey, deviceInfo);

        if (deviceInfo.isAgent()) {
            mHostList.add(deviceInfo);
        }

        int category = deviceInfo.getCategory();
        if (deviceInfo.isMyDevice() || deviceInfo.getIndexingStatus() != IndexingConst.STATUS_NOT) {
            if (!mDeviceIndexingCategoryMap.containsKey(category)) {
                mDeviceIndexingCategoryMap.put(category, new ArrayList<DeviceInfo>());
            }
            mDeviceIndexingCategoryMap.get(category).add(deviceInfo);
        }
    }


    public boolean isKorea() {
        return StringUtils.equals(mContext.getResources().getConfiguration().locale.getCountry(), Locale.KOREA.getCountry());
    }

    public Map<Integer, List<DeviceInfo>> getDeviceIndexingCategoryMap() {
        return mDeviceIndexingCategoryMap;
    }

    public boolean isExistCategoryDevice(int category) {
        return mExtensionTypeMap.containsKey(category);
    }

    public DeviceInfo getDeviceInfo(String deviceKey) {
        return mDeviceKeyMap == null ? null : mDeviceKeyMap.get(deviceKey);
    }

    public LimitInfo getDeviceLimitInfo() {
        return mDeviceLimitInfo;
    }

    public AccountInfo getAccountInfo() {
        return mAccountInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        mAccountInfo = accountInfo;
    }

    public boolean isAddedDevice() {
        return getDeviceInfo(getDeviceKey()) != null;
    }

    public boolean isAddedSearchAbleDevice() {
        return mDeviceIndexingCategoryMap != null && mDeviceIndexingCategoryMap.size() > 1;
    }
}

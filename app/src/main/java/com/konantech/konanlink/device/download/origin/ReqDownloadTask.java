package com.konantech.konanlink.device.download.origin;

import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.FileConst;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public abstract class ReqDownloadTask extends WorkTask {
    protected File mDownloadFile;

    public ReqDownloadTask(int callBackId, File downloadFile, OnTaskListener taskListener) {
        super(callBackId, taskListener);
        mDownloadFile = downloadFile;

        try {
            FileUtils.forceMkdir(mDownloadFile.getParentFile());
            if (!FileConst.FILE_NOMEDIA.exists()) {
                FileConst.FILE_NOMEDIA.mkdirs();
                FileConst.FILE_NOMEDIA.createNewFile();
            }
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NO_DISK;
            cancel(true);
            return;
        }
    }

    public void stopDownload() {
        mCancelCode = ErrorConst.ERROR_CANCEL;
        cancel(true);
    }
}

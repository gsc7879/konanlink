package com.konantech.konanlink.device.connector;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.dignara.lib.fragment.PartFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.konantech.konanlink.constant.ErrorConst;

import java.util.Collection;

public abstract class GoogleConnector extends Connector {
    private static final int REQUEST_ACCOUNT_PICKER       = 1000;
    private static final int REQUEST_AUTHORIZATION        = 1001;
    private static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;

    private GoogleAccountCredential mCredential;

    public GoogleConnector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener, Intent intent) {
        super(deviceKind, deviceKey, account, partFragment, isCert, oauthListener);
        mCredential = GoogleAccountCredential.usingOAuth2(partFragment.getActivity(), getScopes(deviceKind));
        mCredential.setSelectedAccountName(account);
        if (mCredential.getSelectedAccount() == null) {
            if (isCert) {
                partFragment.startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
            } else {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
            }
        } else {
            if (intent != null) {
                if (isCert) {
                    partFragment.startActivityForResult(intent, REQUEST_AUTHORIZATION);
                } else {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
                }
            } else {
                mOAuthListener.onConnected(mDeviceKind, mDeviceKey, getService(mCredential));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != Activity.RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == Activity.RESULT_OK && data != null && data.getExtras() != null) {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    mCredential.setSelectedAccountName(accountName);
                    addDevice(accountName, getDeviceName(), "", getService(mCredential));
                } else {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CANCEL);
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == Activity.RESULT_OK) {
                    mOAuthListener.onConnected(mDeviceKind, mDeviceKey, getService(mCredential));
                    return;
                }
                mPartFragment.startActivityForResult(mCredential.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER);
                break;
            default:
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CANCEL);
                break;
        }
    }


    private boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) mPartFragment.getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mPartFragment.getActivity());
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS ) {
            return false;
        }
        return true;
    }

    private void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, mPartFragment.getActivity(), REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    protected abstract Collection<String> getScopes(int deviceKind);

    protected abstract Object getService(GoogleAccountCredential credential);

    protected abstract String getDeviceName();

    @Override
    public void onResume() {}

    @Override
    public void onCancel() {}
}
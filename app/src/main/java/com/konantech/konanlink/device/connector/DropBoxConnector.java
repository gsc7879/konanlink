package com.konantech.konanlink.device.connector;

import android.content.Intent;
import android.os.AsyncTask;
import com.dignara.lib.fragment.PartFragment;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.users.FullAccount;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.data.DataBank;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class DropBoxConnector extends Connector {
    public final static String APP_KEY = "ecl2r4vk30xp74j";

    private static DbxRequestConfig config;

    private DbxClientV2 dbxClientV2;

    public DropBoxConnector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, account, partFragment, isCert, oauthListener);
        config = new DbxRequestConfig(partFragment.getString(R.string.app_name), Locale.getDefault().toString());
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);

        String accessToken;
        if (deviceInfo != null && StringUtils.isNotBlank(deviceInfo.getAccessKey())) {
            accessToken = deviceInfo.getAccessKey();
            dbxClientV2 = new DbxClientV2(config, accessToken);
            mOAuthListener.onConnected(mDeviceKind, mDeviceKey, dbxClientV2);
        } else {
            if (isCert) {
                Auth.startOAuth2Authentication(partFragment.getActivity(), APP_KEY);
            } else {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
            }
        }
    }

    @Override
    protected void onNotMatchedAccount(String account) {
        Auth.startOAuth2Authentication(mPartFragment.getActivity(), APP_KEY);
    }

    @Override
    public void onResume() {
        String accessToken = Auth.getOAuth2Token();
        if (StringUtils.isNotBlank(accessToken)) {
            dbxClientV2 = new DbxClientV2(config, accessToken);
            checkAccount(accessToken);
        } else {
            mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
        }
    }

    @Override
    public void onCancel() {}

    private void checkAccount(final String accessToken) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    FullAccount fullAccount = dbxClientV2.users().getCurrentAccount();
                    return fullAccount.getEmail();
                } catch (DbxException e) {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_ACCOUNT);
                }
                return null;
            }

            @Override
            protected void onPostExecute(String loginAccount) {
                addDevice(loginAccount, DeviceConst.DEVICE_DROPBOX, accessToken, dbxClientV2);
            }

            @Override
            protected void onCancelled(String o) {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_ACCOUNT);
            }
        }.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}
}

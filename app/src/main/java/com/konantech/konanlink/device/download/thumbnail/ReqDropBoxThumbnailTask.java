package com.konantech.konanlink.device.download.thumbnail;

import com.dropbox.core.v2.DbxClientV2;

public class ReqDropBoxThumbnailTask extends ReqThumbnailTask {
//    private DropboxAPI<AndroidAuthSession> mDropBoxApi;
    private String                         mFilePath;

    public ReqDropBoxThumbnailTask(DbxClientV2 dropBoxApi, String filePath) {
        super(0);
//        mDropBoxApi = dropBoxApi;
        mFilePath   = filePath;
    }

    @Override
    protected Object doInBackground(Object... params) {
//        if (super.doInBackground(params) != null) {
//            return mCacheFile.getPath();
//        }
//        FileOutputStream outputStream             = null;
//        DropboxAPI.DropboxInputStream inputStream = null;
//        try {
//            outputStream = new FileOutputStream(mCacheFile);
//            inputStream  = mDropBoxApi.getThumbnailStream(mFilePath, DropboxAPI.ThumbSize.ICON_256x256, DropboxAPI.ThumbFormat.PNG);
//            inputStream.copyStreamToOutput(outputStream, null);
//            return mCacheFile.getPath();
//        } catch (FileNotFoundException | DropboxException e) {
//            cancel(true);
//        } finally {
//            IOUtils.closeQuietly(outputStream);
//            IOUtils.closeQuietly(inputStream);
//        }
        return null;
    }
}

package com.konantech.konanlink.device.download.origin;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.konantech.konanlink.constant.ErrorConst;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.OutputStream;

public class ReqGMailDownloadTask extends ReqDownloadTask {
    public ReqGMailDownloadTask(int callBackId, java.io.File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Gmail gmail   = (Gmail)  params[0];
        String fileId = (String) params[1];

        OutputStream outputStream = null;
        try {
            outputStream = FileUtils.openOutputStream(mDownloadFile);
            Message message = gmail.users().messages().get("me", fileId).setFormat("raw").execute();
            byte[] emailBytes = Base64.decodeBase64(message.getRaw());
            IOUtils.write(emailBytes, outputStream);
            return mDownloadFile;
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        } finally {
            closeStream(outputStream);
        }
        return null;
    }

    private void closeStream(OutputStream outputStream) {
        IOUtils.closeQuietly(outputStream);
    }
}

package com.konantech.konanlink.device.directory;

import android.content.Context;
import com.dropbox.core.BadRequestException;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.*;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.model.directory.DirectoryItem;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class ReqDropBoxDirectoryInfoTask extends ReqDirectoryInfoTask {
    public ReqDropBoxDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        DbxClientV2 dbxClient = (DbxClientV2)                      params[0];
        String path           = StringUtils.defaultString((String) params[1]);
        try {
            path = StringUtils.removeEnd(path, File.separator);
            ListFolderResult listFolderResult = dbxClient.files().listFolder(path);
            DirectoryInfo directoryInfo = new DirectoryInfo();
            for (Metadata metadata : listFolderResult.getEntries()) {
                if (metadata instanceof FolderMetadata) {
                    directoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DIRECTORY, addFolder((FolderMetadata) metadata));
                } else if (metadata instanceof FileMetadata) {
                    directoryInfo.addDirectoryItem(DirectoryInfo.TYPE_FILE, addFile((FileMetadata) metadata));
                }
            }
            directoryInfo.setParentPath(StringUtils.isBlank(path) ? null : FilenameUtils.getFullPath(path));
            directoryInfo.setCurrentPath(path);
            directoryInfo.setFolder(path);
            return directoryInfo;
        } catch (GetMetadataErrorException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        } catch (BadRequestException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        } catch (DbxException e) {
            mCancelCode = ErrorConst.ERROR_UNLINKED;
            cancel(true);
        } catch (Exception e) {
            mCancelCode = ErrorConst.ERROR_UNKNOWN;
            cancel(true);
        }
        return null;
    }

    private DirectoryItem addFolder(FolderMetadata folderMetadata) {
        DirectoryItem directoryItem = new DirectoryItem();
        directoryItem.setName(folderMetadata.getName());
        directoryItem.setPath(folderMetadata.getPathLower());
        directoryItem.setType(DirectoryInfo.TYPE_DIRECTORY);
        return directoryItem;
    }

    private DirectoryItem addFile(FileMetadata fileMetadata) {
        DirectoryItem directoryItem = new DirectoryItem();
        directoryItem.setName(fileMetadata.getName());
        directoryItem.setPath(fileMetadata.getPathLower());
        directoryItem.setType(DirectoryInfo.TYPE_FILE);
        directoryItem.setExtension(FilenameUtils.getExtension(fileMetadata.getName()));
        directoryItem.setSize(fileMetadata.getSize());
        directoryItem.setModifiedTime(fileMetadata.getClientModified());
        return directoryItem;

    }
}

package com.konantech.konanlink.device.directory;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.model.directory.DirectoryItem;
import com.microsoft.live.LiveConnectClient;
import com.microsoft.live.LiveOperation;
import com.microsoft.live.LiveOperationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class ReqOneDriveDirectoryInfoTask extends ReqDirectoryInfoTask {
    private static final String PATH_ROOT = "me/skydrive";

    public ReqOneDriveDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        LiveConnectClient connectClient = (LiveConnectClient) params[0];
        String fileId                   = (String)            params[1];
        String parentFolderId = StringUtils.isBlank(fileId) ? null : getParentId(connectClient, fileId);
        String path = StringUtils.isBlank(fileId) ? PATH_ROOT : fileId;
        String requestPath = String.format("%s/files?offset=0&limit=800", path);

        try {

            DirectoryInfo directoryInfo = new DirectoryInfo();
            boolean isNext = false;
            do {
                if (isCancelled()) {
                    break;
                }

                LiveOperation liveOperation = connectClient.get(requestPath);
                JSONObject result = liveOperation.getResult();
                if (!result.has("error")) {
                    OneDriveFile oneDriveFile = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create().fromJson(result.toString(), OneDriveFile.class);
                    for (OneDriveFile.DriveFile driveFile : oneDriveFile.getData()) {
                        boolean isDirectory = StringUtils.equals(driveFile.getType(), "folder") || StringUtils.equals(driveFile.getType(), "album");
                        String type = isDirectory ? DirectoryInfo.TYPE_DIRECTORY : DirectoryInfo.TYPE_FILE;
                        DirectoryItem directoryItem = new DirectoryItem();
                        directoryItem.setName(driveFile.getName());
                        directoryItem.setPath(driveFile.getId());
                        directoryItem.setLink(driveFile.getLink());
                        directoryItem.setType(type);
                        directoryItem.setExtension(FilenameUtils.getExtension(driveFile.getName()));
                        directoryItem.setSize(driveFile.getSize());
                        directoryItem.setModifiedTime(driveFile.getUpdateDate());
                        directoryInfo.addDirectoryItem(type, directoryItem);
                    }

                    if (((JSONObject) result.get("paging")).isNull("next")) {
                        isNext = false;
                    } else {
                        isNext = true;
                        requestPath = ((JSONObject) result.get("paging")).getString("next");
                    }
                }
            } while (isNext);

            directoryInfo.setParentPath(parentFolderId);
            directoryInfo.setCurrentPath(fileId);
            directoryInfo.setFolder(StringUtils.isBlank(fileId) ? null : getCurrentPath(connectClient, fileId));
            return directoryInfo;
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getParentId(LiveConnectClient connectClient, String folderId) {
        try {
            LiveOperation liveOperation = connectClient.get(folderId);
            JSONObject result = liveOperation.getResult();
            String parentId = result.getString("parent_id");
            String name = result.getString("name");
            return StringUtils.equalsIgnoreCase(name, "skydrive") ? null : parentId;
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getCurrentPath(LiveConnectClient connectClient, String fileId) {
        StringBuilder sb = new StringBuilder();

        try {
            LiveOperation liveOperation;
            JSONObject result;
            String name;
            do {
                liveOperation = connectClient.get(fileId);
                result = liveOperation.getResult();
                fileId = result.getString("parent_id");
                name = result.getString("name");
                if (StringUtils.equalsIgnoreCase(name, "skydrive")) {
                    return sb.toString();
                }
                sb.insert(0, String.format("/%s", name));
            } while (StringUtils.isNotBlank(fileId));
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package com.konantech.konanlink.device.download.origin;

import android.content.Context;
import android.net.Uri;

import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.constant.NetConst;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

public class ReqAgentDownloadTask extends ReqDownloadTask implements AgentApiManager.OnDownload {

    public ReqAgentDownloadTask(int callBackId, File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        AgentApiManager manager             = (AgentApiManager) params[0];
        Context context                     = (Context) params[1];
        CompositeSubscription subscription  = (CompositeSubscription)params[2];
        final String downloadUrl            =  StringUtils.defaultString((String) params[3]);

        try {
            manager.reqDownload(context, subscription, AndroidSchedulers.mainThread(), getDownloadUri(downloadUrl).toString(), mDownloadFile, this);
            return mDownloadFile;
        } catch (Exception e) {
            cancel(true);
        }
        return null;
    }

    @Override
    public void onSuccess(File downloadFile) {

    }

    @Override
    public void onFailed(int code, String msg) {
        mCancelCode = code;
        cancel(true);
    }

    @Override
    public void onProgress(long transSize, long totalSize) {
        publishProgress(transSize, totalSize);

    }

    private Uri getDownloadUri(String downloadUrl) {
        return new Uri.Builder().encodedPath(NetConst.getInstance().getLinkUrl())
                .appendEncodedPath(downloadUrl)
                .build();
    }

}

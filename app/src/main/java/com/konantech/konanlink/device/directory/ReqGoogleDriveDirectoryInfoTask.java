package com.konantech.konanlink.device.directory;

import android.content.Context;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.model.directory.DirectoryItem;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ReqGoogleDriveDirectoryInfoTask extends ReqDirectoryInfoTask {
    public ReqGoogleDriveDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Drive drive      = (Drive)   params[0];
        String path      = (String)  params[1];

        try {
            File currentFolder = getCurrentFolder(drive, path);
            String query = String.format("'%s' in parents and trashed=false", currentFolder.getId());
            FileList files = drive.files().list().setQ(query).execute();
            List<File> items = files.getItems();
            DirectoryInfo directoryInfo = new DirectoryInfo();
            for (File file : items) {
                boolean isDirectory = StringUtils.contains(file.getMimeType(), "folder");
                String type = isDirectory ? DirectoryInfo.TYPE_DIRECTORY : DirectoryInfo.TYPE_FILE;
                DirectoryItem directoryItem = new DirectoryItem();
                directoryItem.setName(file.getTitle());
                directoryItem.setPath(file.getId());
                directoryItem.setLink(file.getAlternateLink());
                directoryItem.setType(type);
                directoryItem.setExtension(FilenameUtils.getExtension(file.getTitle()));
                directoryItem.setModifiedTime(new Date(file.getModifiedDate().getValue()));
                directoryInfo.addDirectoryItem(type, directoryItem);

                try {
                    directoryItem.setSize(file.getFileSize());
                } catch (Exception e) {
                    directoryItem.setSize(0);
                }

                try {
                    directoryItem.setExtension(file.getFileExtension());
                } catch (Exception e) {
                    directoryItem.setExtension("ETC");
                }
            }
            directoryInfo.setParentPath(currentFolder.getParents().size() > 0 ? currentFolder.getParents().get(0).getId() : null);
            directoryInfo.setCurrentPath(path);
            directoryInfo.setFolder(getCurrentPath(drive, currentFolder));
            return directoryInfo;
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
        } catch (VerifyError e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }

    private File getCurrentFolder(Drive drive, String path) throws IOException, VerifyError{
        path = StringUtils.isBlank(path) ? "root" : path;
        return drive.files().get(path).execute();
    }

    private String getCurrentPath(Drive drive, File currentFolder) {
        StringBuilder sb = new StringBuilder(String.format("/%s", currentFolder.getTitle()));
        try {
            List<ParentReference> parents = currentFolder.getParents();
            while (parents.size() > 0) {
                currentFolder = drive.files().get(parents.get(0).getId()).execute();
                sb.insert(0, String.format("/%s", currentFolder.getTitle()));
                parents = currentFolder.getParents();
            }
        } catch (IOException ignored) {
        }
        return sb.toString();
    }
}

package com.konantech.konanlink.device.download.origin;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.GenericUrl;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.konantech.konanlink.constant.ErrorConst;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ReqGoogleDriveDownloadTask extends ReqDownloadTask {
    public ReqGoogleDriveDownloadTask(int callBackId, java.io.File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Drive drive   = (Drive)  params[0];
        String fileId = (String) params[1];

        File file;
        try {
            file = drive.files().get(fileId).execute();
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
            return null;
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
            return null;
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
            return null;
        }

        try {
            com.google.api.client.http.HttpResponse resp = drive.getRequestFactory().buildGetRequest(new GenericUrl(file.getDownloadUrl())).execute();
            InputStream inputStream = resp.getContent();
            OutputStream outputStream = FileUtils.openOutputStream(mDownloadFile);
            byte data[]    = new byte[1024];
            long totalSize = file.getFileSize();
            long transSize = 0;
            int count;

            try {
                while ((count = inputStream.read(data)) != -1) {
                    if (isCancelled()) {
                        closeStream(inputStream, outputStream);
                        break;
                    } else {
                        transSize += count;
                        publishProgress(transSize, totalSize);
                        outputStream.write(data, 0, count);
                    }
                }
                outputStream.flush();
                return mDownloadFile;
            } catch (IOException e) {
                mCancelCode = ErrorConst.ERROR_NO_DISK;
                cancel(true);
            } finally {
                closeStream(inputStream, outputStream);
            }
        } catch (Exception e) {
            cancel(true);
        }
        return null;
    }

    private void closeStream(InputStream inputStream, OutputStream outputStream) {
        IOUtils.closeQuietly(outputStream);
        IOUtils.closeQuietly(inputStream);
    }
}

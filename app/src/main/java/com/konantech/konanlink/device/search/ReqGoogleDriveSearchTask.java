package com.konantech.konanlink.device.search;

import android.content.Context;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.list.model.GoogleDriveSearchListSetData;
import com.konantech.konanlink.model.search.GoogleDriveSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReqGoogleDriveSearchTask extends ReqSearchTask {
    public ReqGoogleDriveSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Drive drive  = (Drive)  params[0];
        GoogleDriveSearchListSetData searchInfo = (GoogleDriveSearchListSetData) params[1];
        int offset   = (int)    params[2];
        int limit    = (int)    params[3];

        if (drive == null) {
            return null;
        }

        try {
            Drive.Files.List request = drive.files().list().setMaxResults(limit).setQ(createQuery(searchInfo.getKeyword()));
            List<GoogleDriveSearchInfo> dataList = new ArrayList<>();
            int resultCount = 0;
            int pageIndex   = 0;
            int page = offset / limit;
            boolean isNext  = false;
            do {
                FileList files = request.execute();
                if (pageIndex < page) {
                    pageIndex ++;
                    request.setPageToken(files.getNextPageToken());
                    continue;
                }

                List<File> items = files.getItems();
                resultCount += items.size();
                for (File file : items) {
                    addSearchInfo(file, dataList);
                }
                isNext = StringUtils.isNotBlank(files.getNextPageToken());
                request.setPageToken(null);
            } while (request.getPageToken() != null && !request.getPageToken().isEmpty());
            return new SearchResult<>(dataList, resultCount, isNext);
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
        } catch (VerifyError e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }

    private void addSearchInfo(File file, List<GoogleDriveSearchInfo> dataList) {
        GoogleDriveSearchInfo searchInfo = new GoogleDriveSearchInfo();
        List<ParentReference> parents = file.getParents();
        searchInfo.setName(file.getTitle());
        searchInfo.setPath(file.getId());
        searchInfo.setFolderName("");
        searchInfo.setFolderPath(parents.size() > 0 ? parents.get(0).getId() : "");
        searchInfo.setSize(file.getFileSize() == null ? file.size() : file.getFileSize());
        searchInfo.setModifiedTime(new Date(file.getModifiedDate().getValue()));
        searchInfo.setDescription(file.getDescription());
        searchInfo.setAlternateLink(file.getAlternateLink());
        searchInfo.setFileExtension(file.getFileExtension());
        searchInfo.setThumbNail(file.getThumbnailLink());
        searchInfo.setDownloadUrl(file.getDownloadUrl());
        addImageInfo(searchInfo, file);
        dataList.add(searchInfo);
    }

    private String createQuery(String keyword) {
        StringBuilder query = new StringBuilder();
        query.append(createKeyword(keyword));
        query.append(appendMimeTypes());
        query.append(" and trashed = false");
        return query.toString();
    }

    private String createKeyword(String keyword) {
        String[] split = StringUtils.split(keyword, " ");
        StringBuilder searchKeyword = new StringBuilder();
        StringBuilder exceptKeyword = new StringBuilder();
        for (String word : split) {
            if (StringUtils.startsWith(word, "-")) {
                if (exceptKeyword.length() > 0) {
                    exceptKeyword.append(" ");
                }
                exceptKeyword.append(StringUtils.removeStart(word, "-"));
            } else {
                if (searchKeyword.length() > 0) {
                    searchKeyword.append(" ");
                }
                searchKeyword.append(word);
            }
        }

        String keywordQuery = String.format("fullText contains '%s'", searchKeyword.toString());
        if (exceptKeyword.length() > 0) {
            keywordQuery += String.format(" and not fullText contains '%s'", exceptKeyword.toString());
        }
        return keywordQuery;
    }

    private String appendMimeTypes() {
        StringBuilder mimeTypeSb = new StringBuilder();
        exceptMimeType(mimeTypeSb, "folder");
        return mimeTypeSb.toString();
    }

    private void exceptMimeType(StringBuilder mimeTypeSb, String mimeType) {
        mimeTypeSb.append(" and ");
        mimeTypeSb.append(String.format("mimeType != 'application/vnd.google-apps.%s'", mimeType));
    }

    private File getParentFolder(Drive drive, File file) {
        if (file.getParents().size() == 0) {
            return null;
        }
        ParentReference parent = file.getParents().get(0);
        try {
            return drive.files().get(parent.getId()).execute();
        } catch (IOException e) {
            return null;
        }
    }

    private void addImageInfo(GoogleDriveSearchInfo searchInfo, File file) {
        try {
            File.ImageMediaMetadata imageMediaMetadata = file.getImageMediaMetadata();
            searchInfo.setResolution(String.format("%dx%d", imageMediaMetadata.getWidth(), imageMediaMetadata.getHeight()));
        } catch (Exception e) {
        }

        try {
            File.ImageMediaMetadata.Location location = file.getImageMediaMetadata().getLocation();
            String loc = String.format("%s,%s", location.getLatitude(), location.getLongitude());
            searchInfo.setCoordinates(loc);
        } catch (Exception e) {
        }
    }
}

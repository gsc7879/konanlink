package com.konantech.konanlink.device.download.thumbnail;

import org.apache.commons.io.FileUtils;

import java.io.File;

public class ReqLocalThumbnailTask extends ReqThumbnailTask {
    private String    mFilePath;

    public ReqLocalThumbnailTask(String filePath) {
        super(0);
        mFilePath = filePath;
    }

    @Override
    protected Object doInBackground(Object... params) {
        if (super.doInBackground(params) != null) {
            return mCacheFile.getPath();
        }

        File file = FileUtils.getFile(mFilePath);
        if (file.exists()) {
            mCacheFile = file;
            return mCacheFile.getPath();
        }
        return null;
    }
}

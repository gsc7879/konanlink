package com.konantech.konanlink.device.search;

import android.content.Context;

import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;
import com.konantech.konanlink.api.service.AgentApiService;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.constant.SearchConst;
import com.konantech.konanlink.list.model.AgentSearchListSetData;

import org.apache.commons.lang3.StringUtils;

import rx.subscriptions.CompositeSubscription;

public class ReqAgentSearchTask extends ReqSearchTask {
    public static final int ERROR_SEARCH_CONNECT = 1;
    public static final int ERROR_SEARCH_ERROR   = 2;
    public static final int ERROR_SEARCH_KEYWORD = 3;

    private SearchResponse mResponse;

    public ReqAgentSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        AgentApiManager agentApiManager     = (AgentApiManager) params[0];
        CompositeSubscription subscription  = (CompositeSubscription) params[1];
        AgentSearchListSetData searchInfo   = (AgentSearchListSetData) params[2];
        int offset                          = (int)       params[3];
        int limit                           = (int)       params[4];
        String url                          = NetConst.getInstance().getLinkUrl() + searchInfo.getSearchUrl();

        String keyword     = searchInfo.getKeyword();
        String range       = StringUtils.defaultString(searchInfo.getSearchType());
        String sortType    = StringUtils.defaultString(searchInfo.getSortValue(), SearchConst.OPT_LASTEST);
        String periodStart = StringUtils.defaultString(searchInfo.getPeriodStart());
        String periodEnd   = StringUtils.defaultString(searchInfo.getPeriodEnd());

        String deviceKey   = StringUtils.defaultString(searchInfo.getDeviceKey());

        if (StringUtils.isBlank(keyword)) {
            mCancelCode = ERROR_SEARCH_KEYWORD;
            cancel(true);
            return null;
        }

        try {
            agentApiManager.reqSearch(mContext, subscription, url, deviceKey, keyword, range, sortType, SearchConst.SIZE_SUMMARY, String.valueOf(offset / limit), String.valueOf(limit), periodStart, periodEnd, new Result.OnResultListener<SearchResponse>() {
                @Override
                public void onSuccess(SearchResponse data) {
                    mResponse = data;
                }

                @Override
                public void onFailed(int code, String msg) {
                    mCancelCode = code;
                    mData = msg;
                    cancel(true);
                }
            });
            return mResponse;
        } catch (Exception e) {
            cancel(true);
        }
        return null;
    }
}

package com.konantech.konanlink.device.download.origin;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DownloadErrorException;
import com.dropbox.core.v2.files.FileMetadata;
import com.konantech.konanlink.constant.ErrorConst;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReqDropBoxDownloadTask extends ReqDownloadTask {
    private FileOutputStream mOutputStream;
    private InputStream      mInputStream;

    public ReqDropBoxDownloadTask(int callBackId, File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        try {
            DbxClientV2 dbxClient = (DbxClientV2) params[0];
            String sourcePath     = (String)      params[1];
            try {
                mOutputStream = FileUtils.openOutputStream(mDownloadFile);
                DbxDownloader<FileMetadata> download = dbxClient.files().download(sourcePath);
                mInputStream   = download.getInputStream();
                byte data[]    = new byte[1024];
                long totalSize = download.getResult().getSize();
                long transSize = 0;
                int count;

                try {
                    while ((count = mInputStream.read(data)) != -1) {
                        if (isCancelled()) {
                            closeStream();
                            break;
                        } else {
                            transSize += count;
                            publishProgress(transSize, totalSize);
                            mOutputStream.write(data, 0, count);
                        }
                    }
                    mOutputStream.flush();
                    return mDownloadFile;
                } catch (IOException e) {
                    mCancelCode = ErrorConst.ERROR_NO_DISK;
                    cancel(true);
                } finally {
                    closeStream();
                }
                return mDownloadFile;
            } catch (IOException e) {
                mCancelCode = ErrorConst.ERROR_NO_DISK;
                cancel(true);
            } finally {
                closeStream();
            }
            return null;
        } catch (DownloadErrorException e) {
            e.printStackTrace();
        } catch (DbxException e) {
            mCancelCode = ErrorConst.ERROR_UNLINKED;
            cancel(true);
        }
        return null;
    }

    private void closeStream() {
        IOUtils.closeQuietly(mOutputStream);
        IOUtils.closeQuietly(mInputStream);
    }
}

package com.konantech.konanlink.device.download.thumbnail;

import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.constant.FileConst;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class ReqThumbnailTask extends WorkTask {
    protected int  mWidth;
    protected int  mHeight;
    protected File mCacheFile;
    protected String filepath;

    public ReqThumbnailTask(int callBackId) {
        super(callBackId);
    }

    @Override
    protected Object doInBackground(Object... params) {
        mWidth     = (int) params[0];
        mHeight    = (int) params[1];
        mCacheFile = FileUtils.getFile(FileConst.FOLDER_CACHE, (String) params[2]);

        if (mCacheFile.exists()) {
            return mCacheFile.getPath();
        }

        try {
            FileUtils.forceMkdir(mCacheFile.getParentFile());
            if (!FileConst.FILE_NOMEDIA.exists()) {
                FileConst.FILE_NOMEDIA.mkdirs();
                FileConst.FILE_NOMEDIA.createNewFile();
            }
        } catch (IOException e) {
            cancel(true);
        }
        return null;
    }
}

package com.konantech.konanlink.device.connector;

import android.content.Intent;
import android.os.AsyncTask;
import com.dignara.lib.fragment.PartFragment;
import com.evernote.client.android.EvernoteSession;
import com.evernote.edam.error.EDAMErrorCode;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.thrift.TException;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.data.DataBank;
import org.apache.commons.lang3.StringUtils;

public class EvernoteConnector extends Connector {
    private static final String CONSUMER_KEY    = "konanlink-1005";
    private static final String CONSUMER_SECRET = "9902c04275219892";

    private static final EvernoteSession.EvernoteService EVERNOTE_SERVICE = EvernoteSession.EvernoteService.PRODUCTION;

    private EvernoteSession mEvernoteSession;

    public EvernoteConnector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, account, partFragment, isCert, oauthListener);
        mEvernoteSession = new EvernoteSession.Builder(partFragment.getActivity())
                .setEvernoteService(EVERNOTE_SERVICE)
                .setSupportAppLinkedNotebooks(true)
                .build(CONSUMER_KEY, CONSUMER_SECRET)
                .asSingleton();
        check(isCert);
    }

    private void check(boolean isCert) {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
        if (deviceInfo == null || !StringUtils.equals(mEvernoteSession.getAuthToken(), deviceInfo.getAccessKey())) {
            mEvernoteSession.logOut();
        }

        if (mEvernoteSession.isLoggedIn()) {
            checkAccount();
        } else {
            if (isCert) {
                mEvernoteSession.authenticate(mPartFragment.getActivity());
            } else {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
            }
        }
    }

    @Override
    public void onResume() {
        if (mEvernoteSession.isLoggedIn()) {
            checkAccount();
        } else {
            mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CANCEL);
        }
    }

    @Override
    public void onCancel() {

    }

    private void checkAccount() {
        new AsyncTask<Void, Void, String>() {
            private int errorCode = 0;

            @Override
            protected String doInBackground(Void... params) {
                try {
                    return mEvernoteSession.getEvernoteClientFactory().getUserStoreClient().getUser().getUsername();
                } catch (TException e) {
                    errorCode = ErrorConst.ERROR_CONNECT_ACCOUNT;
                    cancel(true);
                } catch (EDAMUserException e) {
                    errorCode = (e.getErrorCode() == EDAMErrorCode.AUTH_EXPIRED) ? ErrorConst.ERROR_CONNECT_CERT : ErrorConst.ERROR_CONNECT_ACCOUNT;
                    cancel(true);
                } catch (EDAMSystemException e) {
                    errorCode = ErrorConst.ERROR_CONNECT_ACCOUNT;
                    cancel(true);
                }
                return null;
            }

            @Override
            protected void onPostExecute(String loginAccount) {
                addDevice(loginAccount, DeviceConst.DEVICE_EVERNOTE, mEvernoteSession.getAuthToken(), mEvernoteSession);
            }

            @Override
            protected void onCancelled(String o) {
                if (errorCode == ErrorConst.ERROR_CONNECT_CERT) {
                    DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
                    deviceInfo.setAccessKey(null);
                    deviceInfo.setConnector(null);
                    check(false);
                } else {
                    mOAuthListener.onConnectFail(mDeviceKind, errorCode);
                }
            }
        }.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}
}

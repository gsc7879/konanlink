package com.konantech.konanlink.device.search.model;

import com.google.gson.annotations.SerializedName;

import java.text.Normalizer;
import java.util.Date;
import java.util.List;

public class OneDriveFile {
    @SerializedName("data")
    private List<DriveFile> data;

    public List<DriveFile> getData() {
        return data;
    }

    public void setData(List<DriveFile> data) {
        this.data = data;
    }

    public class DriveFile {
        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("description")
        private String description;

        @SerializedName("updated_time")
        private Date updateDate;

        @SerializedName("size")
        private long size;

        @SerializedName("picture")
        private String picture;

        @SerializedName("height")
        private int height;

        @SerializedName("width")
        private int width;

        @SerializedName("location")
        private Location location;

        @SerializedName("parent_id")
        private String parentId;

        @SerializedName("type")
        private String type;

        @SerializedName("link")
        private String link;

        @SerializedName("images")
        private List<ImageInfo> imageInfoList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = Normalizer.normalize(name, Normalizer.Form.NFC);
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public Date getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(Date updateDate) {
            this.updateDate = updateDate;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public List<ImageInfo> getImageInfoList() {
            return imageInfoList;
        }

        public void setImageInfoList(List<ImageInfo> imageInfoList) {
            this.imageInfoList = imageInfoList;
        }

        public class ImageInfo {
            private int height;
            private int width;
            private String type;
            private String source;

            public int getHeight() {
                return height;
            }

            public void setHeight(int height) {
                this.height = height;
            }

            public int getWidth() {
                return width;
            }

            public void setWidth(int width) {
                this.width = width;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }
        }

        public class Location {
            private double latitude;
            private double longitude;
            private double altitude;

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getAltitude() {
                return altitude;
            }

            public void setAltitude(double altitude) {
                this.altitude = altitude;
            }
        }
    }
}

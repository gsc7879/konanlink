package com.konantech.konanlink.device.search;

import android.content.Context;

import com.konantech.konanlink.task.LoadingTask;

public abstract class ReqSearchTask extends LoadingTask {
    public ReqSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, 0, taskListener);
    }
}

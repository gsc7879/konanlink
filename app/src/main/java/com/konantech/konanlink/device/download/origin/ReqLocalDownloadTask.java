package com.konantech.konanlink.device.download.origin;

import com.konantech.konanlink.constant.ErrorConst;

import org.apache.commons.io.FileUtils;

import java.io.File;

public class ReqLocalDownloadTask extends ReqDownloadTask {
    public ReqLocalDownloadTask(int callBackId, File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        String localFilepath = (String) params[1];
        File localFile = FileUtils.getFile(localFilepath);
        if (localFile.exists()) {
            return localFile;
        } else {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }
}

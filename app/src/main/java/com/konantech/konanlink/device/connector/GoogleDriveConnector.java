package com.konantech.konanlink.device.connector;

import android.content.Intent;

import com.dignara.lib.fragment.PartFragment;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.konantech.konanlink.constant.DeviceConst;

import java.util.Arrays;
import java.util.Collection;

public class GoogleDriveConnector extends GoogleConnector {
    public GoogleDriveConnector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener, Intent intent) {
        super(deviceKind, deviceKey, account, partFragment, isCert, oauthListener, intent);
    }

    @Override
    protected Collection<String> getScopes(int deviceKind) {
        return Arrays.asList(DriveScopes.DRIVE);
    }

    @Override
    protected Drive getService(GoogleAccountCredential credential) {
        return new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential).build();
    }

    @Override
    protected String getDeviceName() {
        return DeviceConst.DEVICE_GOOGLEDRIVE;
    }
}
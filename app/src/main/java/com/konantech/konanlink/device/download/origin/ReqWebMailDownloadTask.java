package com.konantech.konanlink.device.download.origin;

import com.dignara.lib.io.ProgressCopy;
import com.konantech.konanlink.constant.ErrorConst;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Enumeration;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;

public class ReqWebMailDownloadTask extends ReqDownloadTask {
    private static final int IDX_MESSAGE = 0;
    private static final int IDX_ATTACH  = 1;

    private ProgressCopy mProgressCopy;

    public ReqWebMailDownloadTask(int callBackId, File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Store store        = (Store)  params[0];
        String messageInfo = (String) params[1];
        String folder      = (String) params[2];

        if (StringUtils.isBlank(messageInfo)) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
            return null;
        }

        String[] messageNumber = StringUtils.split(messageInfo, "#");
        IMAPFolder folderInbox = null;
        try {
            folderInbox = (IMAPFolder) store.getFolder(folder);
            folderInbox.open(Folder.READ_ONLY);
            Message message = folderInbox.getMessageByUID(NumberUtils.toLong(messageNumber[IDX_MESSAGE]));
            if (message == null) {
                mCancelCode = ErrorConst.ERROR_NOT_EXIST;
                cancel(true);
                return null;
            }
            File downloadFile;
            if (messageNumber.length == 1) {
                downloadFile = saveEml(message);
            } else {
                downloadFile = saveAttachedFile(message, NumberUtils.toInt(messageNumber[IDX_ATTACH]));
            }
            return downloadFile;
        } catch (IllegalStateException e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (MessagingException | IOException ex) {
            cancel(true);
        } finally {
            try {
                if (folderInbox != null) {
                    folderInbox.close(false);
                }
            } catch (MessagingException e) {
            }
            try {
                store.close();
            } catch (MessagingException e) {
            }
        }
        return null;
    }

    private File saveEml(Message message) throws IOException, MessagingException {
        Enumeration headers = ((IMAPMessage) message).getAllHeaderLines();
        if (headers != null) {
            FileUtils.writeLines(mDownloadFile, Collections.list(headers), "\n", true);
        }
        return saveStream(message.getInputStream(), message.getSize());
    }

    private File saveAttachedFile(Message message, int index) throws IOException, MessagingException {
        Multipart multiPart = (Multipart) message.getContent();
        MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(index + 1);
        return saveStream(bodyPart.getInputStream(), bodyPart.getSize());
    }

    private File saveStream(InputStream inputStream, long totalSize) {
        try {
            mProgressCopy = new ProgressCopy(inputStream, new FileOutputStream(mDownloadFile, true), totalSize, new ProgressCopy.OnProgress() {
                @Override
                public void onProgress(long transSize, long totalSize) {
                    publishProgress(transSize > totalSize ? totalSize : transSize, totalSize);
                }
            });
            mProgressCopy.startCopy();
            return mDownloadFile;
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void stopDownload() {
        super.stopDownload();
        if (mProgressCopy != null) {
            mProgressCopy.cancel();
        }
    }
}

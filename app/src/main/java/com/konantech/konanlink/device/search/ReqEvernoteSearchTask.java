package com.konantech.konanlink.device.search;

import android.content.Context;
import com.dignara.lib.utils.TextUtils;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.asyncclient.EvernoteClientFactory;
import com.evernote.client.android.asyncclient.EvernoteNoteStoreClient;
import com.evernote.edam.error.EDAMErrorCode;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.notestore.NoteMetadata;
import com.evernote.edam.notestore.NotesMetadataList;
import com.evernote.edam.notestore.NotesMetadataResultSpec;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.NoteSortOrder;
import com.evernote.edam.type.Notebook;
import com.evernote.edam.type.User;
import com.evernote.thrift.TException;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.list.model.EvernoteSearchListSetData;
import com.konantech.konanlink.model.search.EvernoteSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReqEvernoteSearchTask extends ReqSearchTask {
    public ReqEvernoteSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        EvernoteSession evernoteSession      = (EvernoteSession) params[0];
        EvernoteSearchListSetData searchInfo = (EvernoteSearchListSetData) params[1];
        int offset                           = (int)             params[2];
        int limit                            = (int)             params[3];

        if (evernoteSession == null) {
            return null;
        }

        String query = searchInfo.getKeyword();
        EvernoteClientFactory clientFactory = evernoteSession.getEvernoteClientFactory();
        NoteFilter noteFilter = new NoteFilter();
        noteFilter.setWords(query);
        try {
            EvernoteNoteStoreClient noteStoreClient = clientFactory.getNoteStoreClient();
            User user                               = clientFactory.getUserStoreClient().getUser();
            NotesMetadataList notesMetaList         = noteStoreClient.findNotesMetadata(createNoteFilter(query), offset, limit, createNotesMetadataResultSpec());
            List<EvernoteSearchInfo> dataList       = new ArrayList<>();
            List<NoteMetadata> notes = notesMetaList.getNotes();
            for (NoteMetadata noteMeta : notes) {
                addSearchInfo(noteStoreClient, user, noteMeta, dataList);
            }
            int resultCount = notes.size();
            return new SearchResult<>(dataList, resultCount, resultCount == limit);
        } catch (EDAMUserException e) {
            mCancelCode = e.getErrorCode() == EDAMErrorCode.AUTH_EXPIRED ? ErrorConst.ERROR_AUTH : ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (EDAMSystemException e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (EDAMNotFoundException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        } catch (TException e) {
            mCancelCode = ErrorConst.ERROR_UNKNOWN;
            cancel(true);
        }
        return null;
    }

    private void addSearchInfo(EvernoteNoteStoreClient noteStoreClient, User user, NoteMetadata noteMeta, List<EvernoteSearchInfo> dataList) throws EDAMUserException, EDAMSystemException, TException, EDAMNotFoundException {
        Note note = noteStoreClient.getNote(noteMeta.getGuid(), true, false, false, false);
        Notebook notebook = noteStoreClient.getNotebook(noteMeta.getNotebookGuid());

        EvernoteSearchInfo searchInfo = new EvernoteSearchInfo();
        searchInfo.setName(note.getTitle());
        searchInfo.setPath(getNoteUrl(user, note.getGuid()));
        searchInfo.setFolderName(notebook.getName());
        searchInfo.setFolderPath(getNoteUrl(user, note.getNotebookGuid()));
        searchInfo.setSize(note.getContentLength());
        searchInfo.setContent(TextUtils.removeTag(note.getContent()));
        searchInfo.setModifiedTime(new Date(note.getUpdated()));
        dataList.add(searchInfo);
    }

    private NoteFilter createNoteFilter(String keyword) {
        NoteFilter filter = new NoteFilter();
        filter.setOrder(NoteSortOrder.RELEVANCE.getValue());
        filter.setWords(keyword);
        return filter;
    }

    private NotesMetadataResultSpec createNotesMetadataResultSpec() {
        NotesMetadataResultSpec spec = new NotesMetadataResultSpec();
        spec.setIncludeTitle(true);
        spec.setIncludeContentLength(true);
        spec.setIncludeUpdated(true);
        spec.setIncludeNotebookGuid(true);
        return spec;
    }

    private String getNoteUrl(User user, String guId) {
        return String.format("https://www.evernote.com/shard/%s/nl/%s/%s", user.getShardId(), user.getId(), guId);
    }
}

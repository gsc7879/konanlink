package com.konantech.konanlink.device.search;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.list.model.OneDriveSearchListSetData;
import com.konantech.konanlink.model.search.SearchResult;
import com.microsoft.live.LiveConnectClient;
import com.microsoft.live.LiveOperation;
import com.microsoft.live.LiveOperationException;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class ReqOneDriveSearchTask extends ReqSearchTask {
    private static final String PATH_ROOT = "me/skydrive";

    public ReqOneDriveSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        LiveConnectClient connectClient      = (LiveConnectClient)         params[0];
        OneDriveSearchListSetData searchInfo = (OneDriveSearchListSetData) params[1];
        int offset                           = (int)                       params[2];
        int limit                            = (int)                       params[3];
        if (connectClient == null) {
            return null;
        }

        String keyword;
        try {
            keyword = URLEncoder.encode(searchInfo.getKeyword(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }

        String searchLink = String.format("%s/search?q=%s&offset=%d&limit=%d", PATH_ROOT, keyword, offset, limit);
        try {

            LiveOperation liveOperation = connectClient.get(searchLink);
            JSONObject result = liveOperation.getResult();
            if (!result.has("error")) {
                OneDriveFile oneDriveFile = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create().fromJson(result.toString(), OneDriveFile.class);
                List<OneDriveFile.DriveFile> driveFileList = oneDriveFile.getData();
                int resultSize = driveFileList.size();
                return new SearchResult<>(driveFileList, resultSize, resultSize == limit);
            }
        } catch (LiveOperationException e) {
            e.printStackTrace();
        }
        return null;
    }
}

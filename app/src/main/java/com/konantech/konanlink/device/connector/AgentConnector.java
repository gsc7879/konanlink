package com.konantech.konanlink.device.connector;

import android.content.Intent;

import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.constant.NetConst;

public class AgentConnector extends Connector {

    public AgentConnector(int deviceKind, String deviceKey, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, "", partFragment, isCert, oauthListener);
        mOAuthListener.onConnected(mDeviceKind, mDeviceKey, new AgentApiManager(NetConst.getInstance().getLinkUrl()));
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}

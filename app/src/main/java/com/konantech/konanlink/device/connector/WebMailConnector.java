package com.konantech.konanlink.device.connector;

import android.content.Intent;
import android.os.AsyncTask;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.constant.ErrorConst;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import java.util.Properties;

public class WebMailConnector extends Connector {
    public static final int INFO_HOST = 0;
    public static final int INFO_PORT = 1;
    public static final int INFO_USER = 2;
    public static final int INFO_PASS = 3;
    public static final int INFO_SSL  = 4;

    public static final String INFO_PROV = "imap";

    private OnConnectListener mOAuthListener;

    public WebMailConnector(int deviceKind, String deviceKey, String oauthKey, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, "", partFragment, isCert, oauthListener);
        mOAuthListener = oauthListener;
        String[] accountInfo  = StringUtils.splitByWholeSeparator(oauthKey, "[[0308]]");
        Properties properties = new Properties();
        properties.put(String.format("mail.%s.host", INFO_PROV), accountInfo[INFO_HOST]);
        properties.put(String.format("mail.%s.port", INFO_PROV), accountInfo[INFO_PORT]);
        properties.setProperty(String.format("mail.%s.connectiontimeout"     , INFO_PROV), "10000");
        properties.setProperty(String.format("mail.%s.timeout"               , INFO_PROV), "10000");
        properties.setProperty(String.format("mail.%s.socketFactory.fallback", INFO_PROV), "false");
        properties.setProperty(String.format("mail.%s.socketFactory.port"    , INFO_PROV), String.valueOf(accountInfo[INFO_PORT]));
        if (BooleanUtils.toBoolean(accountInfo[INFO_SSL])) {
            properties.setProperty(String.format("mail.%s.socketFactory.class", INFO_PROV), "javax.net.ssl.SSLSocketFactory");
        }
        new ConnectTask().execute(properties, accountInfo);
    }

    class ConnectTask extends AsyncTask<Object, Void, Store> {
        @Override
        protected Store doInBackground(Object... params) {
            Properties properties = (Properties) params[0];
            String[] accountInfo  = (String[])   params[1];
            try {
                Session session = Session.getDefaultInstance(properties, null);
                Store store = session.getStore(WebMailConnector.INFO_PROV);
                store.connect(accountInfo[WebMailConnector.INFO_USER], accountInfo[WebMailConnector.INFO_PASS]);
                if (store.isConnected()) {
                    return store;
                }
            } catch (NoSuchProviderException e) {
                cancel(true);
            } catch (MessagingException e) {
                cancel(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Store store) {
            if (store != null) {
                mOAuthListener.onConnected(mDeviceKind, mDeviceKey, store);
            }
        }

        @Override
        protected void onCancelled() {
            mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_NORMAL);
        }
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}

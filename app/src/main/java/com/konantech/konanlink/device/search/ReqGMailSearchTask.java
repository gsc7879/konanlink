package com.konantech.konanlink.device.search;

import android.content.Context;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.list.model.GmailSearchListSetData;
import com.konantech.konanlink.model.search.GMailSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReqGMailSearchTask extends ReqSearchTask {
    private static final String USER_ID = "me";

    public ReqGMailSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Gmail gmail                                 = (Gmail) params[0];
        GmailSearchListSetData searchListSetData    = (GmailSearchListSetData) params[1];
        int offset                                  = (int) params[2];
        int limit                                   = (int) params[3];

        if (gmail == null) {
            return null;
        }

        try {
            List<GMailSearchInfo> dataList = new ArrayList<>();
            Gmail.Users.Messages.List request = gmail.users().messages().list(USER_ID).setMaxResults((long) limit).setQ(searchListSetData.getKeyword());
            int resultCount = 0;
            int pageIndex   = 0;
            int page = offset / limit;
            boolean isNext  = false;
            do {
                ListMessagesResponse response = request.execute();
                if (pageIndex < page) {
                    pageIndex ++;
                    request.setPageToken(response.getNextPageToken());
                    continue;
                }

                List<Message> messages = response.getMessages();
                if (messages != null) {
                    for (Message message : messages) {
                        GMailSearchInfo searchInfo = new GMailSearchInfo();
                        searchInfo.setPath(message.getId());
                        dataList.add(searchInfo);
                    }
                }
                isNext = StringUtils.isNotBlank(response.getNextPageToken());
                request.setPageToken(null);
            } while (request.getPageToken() != null && !request.getPageToken().isEmpty());
            return new SearchResult<>(dataList, resultCount, isNext);
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
        } catch (VerifyError e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        } catch (IllegalArgumentException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }
}

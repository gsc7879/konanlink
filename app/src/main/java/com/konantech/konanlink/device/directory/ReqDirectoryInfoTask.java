package com.konantech.konanlink.device.directory;

import android.content.Context;
import com.konantech.konanlink.task.LoadingTask;

public abstract class ReqDirectoryInfoTask extends LoadingTask {
    public ReqDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, 0, taskListener);
    }
}

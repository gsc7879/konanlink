package com.konantech.konanlink.device.directory;

import android.content.Context;

import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.agent.response.DirectoryResponse;
import com.konantech.konanlink.api.service.AgentApiService;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.model.directory.DirectoryItem;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

import rx.subscriptions.CompositeSubscription;

public class ReqAgentDirectoryInfoTask extends ReqDirectoryInfoTask {
    private DirectoryInfo mDirectoryInfo;


    public ReqAgentDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        AgentApiManager agentManager        = (AgentApiManager) params[0];
        CompositeSubscription subscription  = (CompositeSubscription)params[1];
        final String path                   = StringUtils.defaultString((String) params[2]);
        String agentDeviceKey               = (String) params[3];
        String url                          = NetConst.getInstance().getLinkUrl() + String.format(AgentApiService.URL_DIRECTORY_INFO, agentDeviceKey);

        try {
            agentManager.reqDirectory(mContext, subscription, url, path, new Result.OnResultListener<DirectoryResponse>() {
                @Override
                public void onSuccess(DirectoryResponse data) {
                    mDirectoryInfo = new DirectoryInfo();

                    mDirectoryInfo.setParentPath(data.getParent());
                    mDirectoryInfo.setCurrentPath(path);
                    mDirectoryInfo.setFolder(path);

                    for (DirectoryResponse.Item item : data.getVolumes()) {
                        mDirectoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DRIVE, new DirectoryItem(item.getName(), item.getPath(), DirectoryInfo.TYPE_DRIVE, "", item.getExtension(), item.getSize(), new Date(item.getModifiedDate()), item.getDownloadURL(), item.getThumbnailURL()));
                    }

                    for (DirectoryResponse.Item item : data.getFolders()) {
                        mDirectoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DIRECTORY, new DirectoryItem(item.getName(), item.getPath(), DirectoryInfo.TYPE_DIRECTORY, "", item.getExtension(), item.getSize(), new Date(item.getModifiedDate()), item.getDownloadURL(), item.getThumbnailURL()));
                    }

                    for (DirectoryResponse.Item item : data.getFiles()) {
                        mDirectoryInfo.addDirectoryItem(DirectoryInfo.TYPE_FILE, new DirectoryItem(item.getName(), item.getPath(), DirectoryInfo.TYPE_FILE, "", item.getExtension(), item.getSize(), new Date(item.getModifiedDate()), item.getDownloadURL(), item.getThumbnailURL()));
                    }
                }

                @Override
                public void onFailed(int code, String msg) {
                    mCancelCode = code;
                    mData = msg;
                    cancel(true);
                }
            });

            return mDirectoryInfo;
        } catch (Exception e) {
            cancel(true);
        }
        return null;
    }
}

package com.konantech.konanlink.device.download.origin;

import com.konantech.konanlink.constant.ErrorConst;
import com.microsoft.live.LiveConnectClient;
import com.microsoft.live.LiveDownloadOperation;
import com.microsoft.live.LiveOperationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ReqOneDriveDownloadTask extends ReqDownloadTask {
    public ReqOneDriveDownloadTask(int callBackId, File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        LiveConnectClient connectClient = (LiveConnectClient) params[0];
        String fileId                   = (String)            params[1];

        try {
            LiveDownloadOperation liveDownloadOperation = connectClient.download(String.format("%s/content?download=true", fileId));
            InputStream inputStream = liveDownloadOperation.getStream();
            OutputStream outputStream = FileUtils.openOutputStream(mDownloadFile);
            byte data[]    = new byte[1024];
            long totalSize = liveDownloadOperation.getContentLength();
            long transSize = 0;
            int count;

            try {
                while ((count = inputStream.read(data)) != -1) {
                    if (isCancelled()) {
                        closeStream(inputStream, outputStream);
                        break;
                    } else {
                        transSize += count;
                        publishProgress(transSize, totalSize);
                        outputStream.write(data, 0, count);
                    }
                }
                outputStream.flush();
                return mDownloadFile;
            } catch (IOException e) {
                mCancelCode = ErrorConst.ERROR_NO_DISK;
                cancel(true);
            } finally {
                closeStream(inputStream, outputStream);
            }
            return mDownloadFile;
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void closeStream(InputStream inputStream, OutputStream outputStream) {
        IOUtils.closeQuietly(outputStream);
        IOUtils.closeQuietly(inputStream);
    }
}

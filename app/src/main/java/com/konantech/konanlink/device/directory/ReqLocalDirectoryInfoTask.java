package com.konantech.konanlink.device.directory;

import android.content.Context;
import com.dignara.lib.utils.StorageUtils;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.model.directory.DirectoryItem;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.util.Date;
import java.util.List;

import static com.dignara.lib.utils.StorageUtils.isRootFolder;

public class ReqLocalDirectoryInfoTask extends ReqDirectoryInfoTask {
    public ReqLocalDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    private final FileFilter mDirectoryFilter = new FileFilter() {
        public boolean accept(File file) {
            if (!file.getName().startsWith(".")) {
                return true;
            }
            return false;
        }
    };

    @Override
    protected Object doInBackground(Object... params) {
        List<StorageUtils.StorageInfo> storageList = ((List<StorageUtils.StorageInfo>) params[0]);
        String path                                = StringUtils.defaultString((String) params[1]);

        File folder = FileUtils.getFile(path);

        if (isRootFolder(storageList, folder)) {
            return getStorageInfo(storageList, path);
        } else {
            return getDirectoryInfo(folder, path);
        }
    }

    private DirectoryInfo getStorageInfo(List<StorageUtils.StorageInfo> storageList, String folderPath) {
        DirectoryInfo directoryInfo = new DirectoryInfo();
        for (StorageUtils.StorageInfo storageInfo : storageList) {
            DirectoryItem directoryItem = new DirectoryItem();
            directoryItem.setName(storageInfo.getDisplayName());
            directoryItem.setPath(storageInfo.path);
            directoryItem.setType(DirectoryInfo.TYPE_DRIVE);
            directoryItem.setExtension("");
            directoryItem.setSize(0);
            directoryItem.setModifiedTime(null);
            directoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DRIVE, directoryItem);
        }
        directoryInfo.setParentPath(null);
        directoryInfo.setCurrentPath(folderPath);
        return directoryInfo;
    }

    private DirectoryInfo getDirectoryInfo(File folder, String folderPath) {
        DirectoryInfo directoryInfo = new DirectoryInfo();
        for (File file : folder.listFiles(mDirectoryFilter)) {
            String type = file.isDirectory() ? DirectoryInfo.TYPE_DIRECTORY : DirectoryInfo.TYPE_FILE;
            DirectoryItem directoryItem = new DirectoryItem();
            directoryItem.setName(file.getName());
            directoryItem.setPath(file.getAbsolutePath());
            directoryItem.setType(type);
            directoryItem.setExtension(FilenameUtils.getExtension(file.getName()));
            directoryItem.setSize(file.length());
            directoryItem.setModifiedTime(new Date(file.lastModified()));
            directoryInfo.addDirectoryItem(type, directoryItem);
        }
        directoryInfo.setParentPath(folder.getParent());
        directoryInfo.setCurrentPath(folderPath);
        return directoryInfo;
    }
}

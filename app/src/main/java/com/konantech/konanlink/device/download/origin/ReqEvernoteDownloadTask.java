package com.konantech.konanlink.device.download.origin;

import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.asyncclient.EvernoteCallback;
import com.evernote.edam.type.User;
import com.evernote.thrift.transport.TTransportException;
import com.konantech.konanlink.constant.ErrorConst;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.CountDownLatch;

public class ReqEvernoteDownloadTask extends ReqDownloadTask {
    private CountDownLatch mCountDownLatch;
    private String         mNoteUrl;

    public ReqEvernoteDownloadTask(int callBackId, java.io.File downloadFolder, OnTaskListener taskListener) {
        super(callBackId, downloadFolder, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        EvernoteSession evernoteSession = (EvernoteSession) params[0];
        String guId                     = (String)          params[1];
        String fileType                 = (String)          params[2];
        mCountDownLatch                 = new CountDownLatch(1);

        try {
            if (StringUtils.equals(fileType, "note")) {
                return getNoteUrl(evernoteSession, guId);
            } else {
                return downloadNoteFile(evernoteSession, guId);
            }
        } catch (TTransportException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }

    private File downloadNoteFile(EvernoteSession evernoteSession, String guId) throws TTransportException {
        evernoteSession.getEvernoteClientFactory().getNoteStoreClient().getResourceDataAsync(guId, new EvernoteCallback<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                try {
                    OutputStream outputStream = FileUtils.openOutputStream(mDownloadFile);
                    try {
                        outputStream.write(bytes, 0, bytes.length);
                        outputStream.flush();
                        mCountDownLatch.countDown();
                    } catch (IOException e) {
                        mCancelCode = ErrorConst.ERROR_NO_DISK;
                        cancel(true);
                    } finally {
                        IOUtils.closeQuietly(outputStream);
                    }
                } catch (Exception e) {
                    cancel(true);
                }
            }

            @Override
            public void onException(Exception e) {
                cancel(true);
            }
        });

        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            cancel(true);
        }
        return mDownloadFile;
    }

    private String getNoteUrl(EvernoteSession evernoteSession, final String guId) throws TTransportException {
        evernoteSession.getEvernoteClientFactory().getUserStoreClient().getUserAsync(new EvernoteCallback<User>() {
            @Override
            public void onSuccess(User user) {
                String sharedId = user.getShardId();
                int userId = user.getId();
                mNoteUrl = String.format("https://www.evernote.com/shard/%s/nl/%s/%s", sharedId, userId, guId);
                mCountDownLatch.countDown();
            }

            @Override
            public void onException(Exception e) {
                mCancelCode = ErrorConst.ERROR_NOT_EXIST;
                cancel(true);
            }
        });
        try {
            mCountDownLatch.await();
        } catch (InterruptedException e) {
            cancel(true);
        }
        return mNoteUrl;
    }
}

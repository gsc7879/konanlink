package com.konantech.konanlink.device.connector;

import android.content.Intent;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.device.AddDeviceResult;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.db.manager.AccessKeyDBManager;
import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

public abstract class Connector {
    protected PartFragment      mPartFragment;
    protected OnConnectListener mOAuthListener;
    protected String            mDeviceKey;
    protected int               mDeviceKind;
    protected boolean           mIsCert;

    private String              mAccount;
    private AccessKeyDBManager  mAccessKeyDBManager;

    public interface OnConnectListener {
        void onConnected(int kind, String deviceKey, Object connector);
        void onConnectFail(int kind, int failCode);
    }

    protected Connector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        mDeviceKind         = deviceKind;
        mDeviceKey          = deviceKey;
        mAccount            = account;
        mPartFragment       = partFragment;
        mOAuthListener      = oauthListener;
        mIsCert             = isCert;
        mAccessKeyDBManager = new AccessKeyDBManager(mPartFragment.getActivity());
    }

    protected void addDevice(String loginAccount, String deviceName, String accessKey, Object connector) {
        if (StringUtils.isNotBlank(mAccount) && !StringUtils.equals(mAccount, loginAccount)) {
            onNotMatchedAccount(mAccount);
        } else {
            if (StringUtils.isBlank(mDeviceKey)) {
                regAddDevice(accessKey, loginAccount, deviceName, connector);
            } else {
                if (StringUtils.isNotBlank(accessKey) && !StringUtils.equals(DataBank.getInstance().getDeviceInfo(mDeviceKey).getAccessKey(), accessKey)) {
                    updateAccessKey(accessKey, mAccount, connector);
                } else {
                    mOAuthListener.onConnected(mDeviceKind, mDeviceKey, connector);
                }
            }
        }
    }

    protected void onNotMatchedAccount(String account) {
        mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_ACCOUNT);
    }

    private void updateAccessKey(String accessKey, String account, final Object connector) {
        mAccessKeyDBManager.update(new DBWorkTask.OnDBExecutionListener() {
            @Override
            public void onDBReady(int i) {
            }

            @Override
            public void onDBResult(int i, Object o) {
                mOAuthListener.onConnected(mDeviceKind, mDeviceKey, connector);
            }

            @Override
            public void onDBFailed(int i, int i1) {
                mOAuthListener.onConnected(mDeviceKind, mDeviceKey, connector);
            }
        }, mDeviceKey, accessKey, account);
    }

    private void regAddDevice(final String accessKey, final String account, String deviceName, final Object connector) {
        mDeviceKey = UUID.randomUUID().toString();
        new DeviceApiManager().reqAddDevice(mPartFragment.getActivity(), mPartFragment.getSubscriptions(), mDeviceKey, deviceName, mDeviceKind, "", account, "", IndexingConst.STATUS_SELF, new Result.OnResultListener<AddDeviceResult>() {
            @Override
            public void onSuccess(AddDeviceResult data) {
                updateAccessKey(accessKey, account, connector);
            }

            @Override
            public void onFailed(int code, String msg) {
                if (code == ErrorConst.ERR_3005_LIMIT_REGISTERED_DEVICE) {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_LIMIT);
                } else {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_WAS);
                }
            }
        });
    }

    public void cancel() {
        onCancel();
    }

    public abstract void onResume();

    public abstract void onCancel();

    public abstract void onActivityResult(final int requestCode, final int resultCode, final Intent data);
}

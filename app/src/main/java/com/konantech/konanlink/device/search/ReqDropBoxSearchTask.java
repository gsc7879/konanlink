package com.konantech.konanlink.device.search;

import android.content.Context;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.SearchErrorException;
import com.dropbox.core.v2.files.SearchMatch;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.model.search.DropboxSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.list.model.DropboxSearchListSetData;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ReqDropBoxSearchTask extends ReqSearchTask {
    public ReqDropBoxSearchTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        DbxClientV2 dbxClient               = (DbxClientV2)              params[0];
        DropboxSearchListSetData searchInfo = (DropboxSearchListSetData) params[1];
        int offset                          = (int)                      params[2];
        int limit                           = (int)                      params[3];

        if (dbxClient == null && StringUtils.isBlank(searchInfo.getKeyword())) {
            return null;
        }

        try {
            com.dropbox.core.v2.files.SearchResult searchResult = dbxClient.files().searchBuilder("", searchInfo.getKeyword()).withStart((long) offset).withMaxResults((long) limit).start();
            List<DropboxSearchInfo> dataList = new ArrayList<>();

            Metadata metadata;
            for (SearchMatch searchMatch : searchResult.getMatches()) {
                metadata = searchMatch.getMetadata();
                if (metadata instanceof FileMetadata) {
                    addSearchInfo((FileMetadata) metadata, dataList);
                }
            }
            return new SearchResult<>(dataList, (int) searchResult.getStart(), searchResult.getMore());
        } catch (SearchErrorException e) {
            mCancelCode = ErrorConst.ERROR_UNKNOWN;
            cancel(true);
        } catch (DbxException e) {
            mCancelCode = ErrorConst.ERROR_UNLINKED;
            cancel(true);
        }
        return null;
    }

    private void addSearchInfo(FileMetadata metadata, List<DropboxSearchInfo> dataList) {
        DropboxSearchInfo searchInfo = new DropboxSearchInfo();
        searchInfo.setModifiedTime(metadata.getClientModified());
        searchInfo.setName(metadata.getName());
        searchInfo.setPath(metadata.getPathLower());
        searchInfo.setSize(metadata.getSize());
        searchInfo.setThumbNail("");
        searchInfo.setFolderName(FilenameUtils.getFullPath(metadata.getPathDisplay()));
        searchInfo.setFolderPath(FilenameUtils.getFullPath(metadata.getPathLower()));
        dataList.add(searchInfo);
    }
}

package com.konantech.konanlink.device.connector;

import android.content.Intent;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.StorageUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.ErrorConst;

import java.util.List;

public class LocalStorageConnector extends Connector {
    public LocalStorageConnector(int deviceKind, String deviceKey, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, "", partFragment, isCert, oauthListener);

        StorageUtils.setName(partFragment.getString(R.string.s34_text_builtin_memory), partFragment.getString(R.string.s34_text_external_memory_01));
        List<StorageUtils.StorageInfo> storageList = StorageUtils.getStorageList();
        if (storageList.size() > 0) {
            mOAuthListener.onConnected(mDeviceKind, mDeviceKey, storageList);
        } else {
            mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_NORMAL);
        }
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }
}

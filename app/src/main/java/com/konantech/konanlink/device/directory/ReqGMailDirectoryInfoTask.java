package com.konantech.konanlink.device.directory;

import android.content.Context;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAuthIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.*;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.model.directory.DirectoryItem;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

public class ReqGMailDirectoryInfoTask extends ReqDirectoryInfoTask {
    private static final String USER_ID = "me";

    public ReqGMailDirectoryInfoTask(int callBackId, Context context, OnTaskListener taskListener) {
        super(callBackId, context, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Gmail gmail    = (Gmail)  params[0];
        String labelId = (String) params[1];

        try {
            DirectoryInfo directoryInfo = new DirectoryInfo();
            if (StringUtils.isBlank(labelId)) {
                addLabelInfoList(gmail, directoryInfo);
                directoryInfo.setParentPath(null);
            } else {
                addMailInfoList(gmail, Arrays.asList(labelId), directoryInfo);
                directoryInfo.setParentPath("");
            }
            directoryInfo.setCurrentPath(labelId);
            directoryInfo.setFolder(folderInfo(gmail, labelId));
            return directoryInfo;
        } catch (UserRecoverableAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_RECOVERABLE;
            mData = e.getIntent();
            cancel(true);
        } catch (GoogleAuthIOException e) {
            mCancelCode = ErrorConst.ERROR_AUTH;
            cancel(true);
        } catch (VerifyError e) {
            mCancelCode = ErrorConst.ERROR_CONNECT;
            cancel(true);
        } catch (IOException e) {
            mCancelCode = ErrorConst.ERROR_NOT_EXIST;
            cancel(true);
        }
        return null;
    }

    private String folderInfo(Gmail gmail, String messageId) throws IOException {
        Label label = gmail.users().labels().get(USER_ID, messageId).execute();
        return label.getName();
    }

    private void addLabelInfoList(Gmail gmail, DirectoryInfo directoryInfo) throws IOException {
        ListLabelsResponse listResponse = gmail.users().labels().list(USER_ID).execute();
        for (Label label : listResponse.getLabels()) {
            DirectoryItem directoryItem = new DirectoryItem();
            directoryItem.setName(label.getName());
            directoryItem.setPath(label.getId());
            directoryItem.setType(DirectoryInfo.TYPE_DIRECTORY);
            directoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DIRECTORY, directoryItem);
        }
    }

    private void addMailInfoList(Gmail gmail, List<String> labelIds, DirectoryInfo directoryInfo) throws IOException {
        ListMessagesResponse response = gmail.users().messages().list(USER_ID).setLabelIds(labelIds).execute();
        while (response.getMessages() != null) {
            for (Message message : response.getMessages()) {
                DirectoryItem directoryItem = new DirectoryItem();
                directoryItem.setPath(message.getId());
                directoryItem.setType(DirectoryInfo.TYPE_FILE);
                directoryItem.setSize(message.size());
                directoryItem.setExtension("mail");
                directoryInfo.addDirectoryItem(DirectoryInfo.TYPE_DIRECTORY, directoryItem);
            }

            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = gmail.users().messages().list(USER_ID).setLabelIds(labelIds).setPageToken(pageToken).execute();
            } else {
                break;
            }
        }
    }
}

package com.konantech.konanlink.device.download.thumbnail;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;

public class ReqGoogleDriveThumbnailTask extends ReqThumbnailTask {
    private Drive          mDrive;
    private String         mFileId;

    public ReqGoogleDriveThumbnailTask(Drive drive, String fileId) {
        super(0);
        mDrive  = drive;
        mFileId = fileId;
    }

    @Override
    protected Object doInBackground(Object... params) {
        try {
            File file = mDrive.files().get(mFileId).execute();
            return file.getThumbnailLink();
        } catch (Exception e) {
            cancel(true);
        }
        return null;
    }
}

package com.konantech.konanlink.device.connector;

import android.content.Intent;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.data.DataBank;
import com.microsoft.live.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.Arrays;

public class OneDriveConnector extends Connector implements LiveAuthListener {
    private static final String CLIENT_ID = "000000004818894C";
    private static final String[] SCOPES = {"wl.signin", "wl.basic", "wl.skydrive", "wl.offline_access", "onedrive.readonly"};

    private LiveAuthClient mAuthClient;
    private DeviceInfo mDeviceInfo;

    public OneDriveConnector(int deviceKind, String deviceKey, String account, PartFragment partFragment, boolean isCert, OnConnectListener oauthListener) {
        super(deviceKind, deviceKey, account, partFragment, isCert, oauthListener);
        mAuthClient = new LiveAuthClient(mPartFragment.getActivity(), CLIENT_ID);

        mDeviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
        if (mDeviceInfo != null && StringUtils.isNotBlank(mDeviceInfo.getAccessKey())) {
            mAuthClient.initialize(Arrays.asList(SCOPES), this, new Object(), mDeviceInfo.getAccessKey());
        } else {
            if (mDeviceInfo == null) {
                mAuthClient.logout(this);
            } else {
                if (mIsCert) {
                    mAuthClient.login(mPartFragment.getActivity(), Arrays.asList(SCOPES), this);
                } else {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
                }
            }
        }
    }

    @Override
    public void onAuthComplete(LiveStatus status, LiveConnectSession session, Object userState) {
        if (status == LiveStatus.CONNECTED) {
            LiveConnectClient liveConnectClient = new LiveConnectClient(session);
            if (mDeviceInfo != null && StringUtils.equals(session.getRefreshToken(), mDeviceInfo.getAccessKey())) {
                mOAuthListener.onConnected(mDeviceKind, mDeviceKey, liveConnectClient);
            } else {
                getInfo(liveConnectClient);
            }
        } else {
            if (mIsCert) {
                mAuthClient.login(mPartFragment.getActivity(), Arrays.asList(SCOPES), this);
            } else {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
            }
        }
    }

    @Override
    public void onAuthError(LiveAuthException exception, Object userState) {
        if (StringUtils.equals(exception.getError(), "invalid_grant")) {
            mAuthClient.logout(this);
        } else {
            mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_CERT);
        }
    }

    private void getInfo(final LiveConnectClient connectClient) {
        connectClient.getAsync("me", new LiveOperationListener() {
            @Override
            public void onError(LiveOperationException exception, LiveOperation operation) {
                mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_ACCOUNT);
            }

            @Override
            public void onComplete(LiveOperation operation) {
                JSONObject result = operation.getResult();
                if (result.has("error")) {
                    mOAuthListener.onConnectFail(mDeviceKind, ErrorConst.ERROR_CONNECT_ACCOUNT);
                } else {
                    User user = new User(result);
                    addDevice(user.getId(), DeviceConst.DEVICE_ONEDRIVE, connectClient.getSession().getRefreshToken(), connectClient);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    @Override
    public void onResume() {}

    @Override
    public void onCancel() {}

    public class User {
        private final JSONObject mUserObj;

        public User(JSONObject userObj) {
            assert userObj != null;
            mUserObj = userObj;
        }
        public String getId() {
            return mUserObj.optString("id");
        }
        public String getName() {
            return mUserObj.optString("name");
        }
        public String getFirstName() {
            return mUserObj.optString("first_name");
        }
        public String getLastName() {
            return mUserObj.optString("last_name");
        }
        public String getLink() {
            return mUserObj.optString("link");
        }
        public int getBirthDay() {
            return mUserObj.optInt("birth_day");
        }
        public int getBirthMonth() {
            return mUserObj.optInt("birth_month");
        }
        public int getBirthYear() {
            return mUserObj.optInt("birth_year");
        }
        public String getGender() {
            return mUserObj.isNull("gender") ? null : mUserObj.optString("gender");
        }
        public String getLocale() {
            return mUserObj.optString("locale");
        }
        public String getUpdatedTime() {
            return mUserObj.optString("updated_time");
        }
        public JSONObject toJson() {
            return mUserObj;
        }
    }
}
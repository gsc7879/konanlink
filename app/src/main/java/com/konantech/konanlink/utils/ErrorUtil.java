package com.konantech.konanlink.utils;

import android.content.Context;

import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.page.fragment.IntroPageFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by kimmyeongseong on 2016. 9. 23..
 */

public class ErrorUtil {

    public static String getErrorMsg(Context context, int code) {
        switch (code) {
            case ErrorConst.ERR_1001_ALREADY_REGISTERED_EMAIL:
                return context.getResources().getText(R.string.was_error_1001).toString();
            case ErrorConst.ERR_1002_NOT_EXIST_EMAIL:
                return context.getResources().getText(R.string.was_error_1002).toString();
            case ErrorConst.ERR_1003_NOT_EQUALS_PASSWORD:
                return context.getResources().getText(R.string.was_error_1003).toString();
            case ErrorConst.ERR_1005_NOT_ADMIN:
                return context.getResources().getText(R.string.was_error_1005).toString();
            case ErrorConst.ERR_1201_EMPTY_API_KEY:
                return context.getResources().getText(R.string.was_error_1201).toString();
            case ErrorConst.ERR_1202_NOT_EXIST_API_KEY:
                return context.getResources().getText(R.string.was_error_1202).toString();
            case ErrorConst.ERR_2001_INPUT_PARAM_FIELD_IS_EMPTY:
                return context.getResources().getText(R.string.was_error_2001).toString();
            case ErrorConst.ERR_2002_INPUT_PARAM_LIMIT_MINIMUM:
                return context.getResources().getText(R.string.was_error_2002).toString();
            case ErrorConst.ERR_2003_INPUT_PARAM_LIMIT_MAXIMUM:
                return context.getResources().getText(R.string.was_error_2003).toString();
            case ErrorConst.ERR_2004_INPUT_PARAM_USE_FORBIDDEN_CHAR:
                return context.getResources().getText(R.string.was_error_2004).toString();
            case ErrorConst.ERR_2005_INVALID_EMAIL_FORM:
                return context.getResources().getText(R.string.was_error_2005).toString();
            case ErrorConst.ERR_3001_ALREADY_REGISTERED_DEVICE:
                return context.getResources().getText(R.string.was_error_3001).toString();
            case ErrorConst.ERR_3002_NOT_MY_DEVICE:
                return context.getResources().getText(R.string.was_error_3002).toString();
            case ErrorConst.ERR_3003_NOT_EXIST_DEVICE:
                return context.getResources().getText(R.string.was_error_3003).toString();
            case ErrorConst.ERR_3004_INVALID_PUSH_MESSAGE:
                return context.getResources().getText(R.string.was_error_3004).toString();
            case ErrorConst.ERR_3005_LIMIT_REGISTERED_DEVICE:
                return context.getResources().getText(R.string.was_error_3005).toString();
            case ErrorConst.ERR_4001_ALREADY_REGISTRATION_PAYMENT:
                return context.getResources().getText(R.string.was_error_4001).toString();
            case ErrorConst.ERR_4201_COUPON_EXPIRE_DATE:
                return context.getResources().getText(R.string.was_error_4201).toString();
            case ErrorConst.ERR_4202_WRONG_CODE:
                return context.getResources().getText(R.string.was_error_4202).toString();
        }
        return context.getResources().getText(R.string.was_error_9999).toString();
    }

    public static int getErrorCode(Throwable e) {
        try {
            if (e instanceof HttpException) {
                ResponseBody responseBody =((HttpException) e).response().errorBody();
                if (responseBody != null) {
                    String error = responseBody.string();
                    JSONObject json = new JSONObject(error);
                    return json.getInt("code");
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        return ErrorConst.ERROR_SYSTEM;
    }

    public static boolean isCheckLoadingPage(Context context, int errorCode) {
        switch (errorCode) {
            case ErrorConst.ERR_1201_EMPTY_API_KEY:
            case ErrorConst.ERR_1202_NOT_EXIST_API_KEY:
                IntentChangeUtils.goLoadingPage(context, IntroPageFragment.class);
                return true;
        }
        return false;
    }
}

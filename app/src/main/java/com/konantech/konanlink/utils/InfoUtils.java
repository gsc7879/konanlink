package com.konantech.konanlink.utils;

import android.content.Context;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DeviceConst;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class InfoUtils {
    public static String getSearchTarget(int kind) {
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                return DeviceConst.TARGET_SEARCH_PC;
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
                return DeviceConst.TARGET_SEARCH_MOBILE;
            case DeviceConst.KIND_DROPBOX:
                return DeviceConst.TARGET_SEARCH_DROPBOX;
            case DeviceConst.KIND_GOOGLEDRIVE:
                return DeviceConst.TARGET_SEARCH_GOOGLEDRIVE;
            case DeviceConst.KIND_EVERNOTE:
                return DeviceConst.TARGET_SEARCH_EVERNOTE;
            case DeviceConst.KIND_WEBMAIL:
                return DeviceConst.TARGET_SEARCH_WEBMAIL;
            case DeviceConst.KIND_ONEDRIVE:
                return DeviceConst.TARGET_SEARCH_ONEDRIVE;
            case DeviceConst.KIND_GMAIL:
                return DeviceConst.TARGET_SEARCH_GMAIL;
        }
        return null;
    }

    public static int getTypeIconResId(String type, boolean isAttached) {
        if (StringUtils.equals(type, "doc")) {
            if (isAttached) {
                return R.drawable.ic_file_doc_att;
            } else {
                return R.drawable.ic_file_doc;
            }
        } else if (StringUtils.equals(type, "code")) {
            if (isAttached) {
                return R.drawable.ic_file_code_att;
            } else {
                return R.drawable.ic_file_code;
            }
        } else if (StringUtils.equals(type, "hwp")) {
            if (isAttached) {
                return R.drawable.ic_file_hwp_att;
            } else {
                return R.drawable.ic_file_hwp;
            }
        } else if (StringUtils.equals(type, "music")) {
            if (isAttached) {
                return R.drawable.ic_file_music_att;
            } else {
                return R.drawable.ic_file_music;
            }
        } else if (StringUtils.equals(type, "pdf")) {
            if (isAttached) {
                return R.drawable.ic_file_pdf_att;
            } else {
                return R.drawable.ic_file_pdf;
            }
        } else if (StringUtils.equals(type, "txt")) {
            if (isAttached) {
                return R.drawable.ic_file_txt_att;
            } else {
                return R.drawable.ic_file_txt;
            }
        } else if (StringUtils.equals(type, "zip")) {
            if (isAttached) {
                return R.drawable.ic_file_zip_att;
            } else {
                return R.drawable.ic_file_zip;
            }
        } else if (StringUtils.equals(type, "image")) {
            if (isAttached) {
                return R.drawable.ic_file_image_att;
            } else {
                return R.drawable.ic_file_image;
            }
        } else if (StringUtils.equals(type, "ppt")) {
            if (isAttached) {
                return R.drawable.ic_file_ppt_att;
            } else {
                return R.drawable.ic_file_ppt;
            }
        } else if (StringUtils.equals(type, "video")) {
            if (isAttached) {
                return R.drawable.ic_file_video_att;
            } else {
                return R.drawable.ic_file_video;
            }
        } else if (StringUtils.equals(type, "xls")) {
            if (isAttached) {
                return R.drawable.ic_file_xls_att;
            } else {
                return R.drawable.ic_file_xls;
            }
        } else if (StringUtils.equals(type, "mail")) {
            if (isAttached) {
                return R.drawable.ic_file_mail_att;
            } else {
                return R.drawable.ic_file_mail;
            }
        } else if (StringUtils.equals(type, "note")) {
            if (isAttached) {
                return R.drawable.ic_file_evernote_att;
            } else {
                return R.drawable.ic_file_evernote;
            }
        } else {
            if (isAttached) {
                return R.drawable.ic_file_etc_att;
            } else {
                return R.drawable.ic_file_etc;
            }
        }
    }

    public static String getServerLocale(Context context) {
        Locale locale = context.getResources().getConfiguration().locale;
        if (StringUtils.equals(locale.getCountry(), Locale.KOREA.getCountry()))  {
            return locale.getLanguage();
        } else {
            return Locale.ENGLISH.getLanguage();
        }
    }
}

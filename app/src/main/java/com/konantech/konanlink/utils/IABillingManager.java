package com.konantech.konanlink.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.IInAppBillingService;
import com.konantech.konanlink.utils.billing.IabHelper;
import com.konantech.konanlink.utils.billing.IabResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.konantech.konanlink.utils.billing.IabHelper.BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE;

public class IABillingManager {
    private static final int API_VERSION = 3;

    private IInAppBillingService mInAppBillingService;
    private ServiceConnection    mServiceConnection;
    public static IabHelper      mIabHelper;

    public void setInit(final Context context, final String base64EncodedPublicKey, final IabHelper.OnIabSetupFinishedListener IabSetupFinishedListener) {
        if (mServiceConnection == null) {
            mServiceConnection = new ServiceConnection() {
                @Override
                public void onServiceDisconnected(ComponentName name) {
                    mInAppBillingService = null;
                }

                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    mInAppBillingService = IInAppBillingService.Stub.asInterface(service);

                    mIabHelper = new IabHelper(context, base64EncodedPublicKey);
                    mIabHelper.enableDebugLogging(true);
                    mIabHelper.startSetup(IabSetupFinishedListener);
                }
            };
        }

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        if (!context.getPackageManager().queryIntentServices(serviceIntent, 0).isEmpty()) {
            if (!context.bindService(serviceIntent, mServiceConnection, Context.BIND_AUTO_CREATE)) {
                if (IabSetupFinishedListener != null) {
                    IabSetupFinishedListener.onIabSetupFinished(
                            new IabResult(BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE,
                                    "Billing service unavailable on device."));
                }
            }
        }
        else {
            if (IabSetupFinishedListener != null) {
                IabSetupFinishedListener.onIabSetupFinished(
                        new IabResult(BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE,
                                "Billing service unavailable on device."));
            }
        }
    }

    public List<SkuItem> getSKUs(Context context, ArrayList<String> itemList, String skuType) {
        if (mInAppBillingService == null) {
            return null;
        }
        Bundle querySKUs = new Bundle();
        querySKUs.putStringArrayList(IabHelper.GET_SKU_DETAILS_ITEM_LIST, itemList);
        try {
            Bundle skuDetails = mInAppBillingService.getSkuDetails(API_VERSION, context.getPackageName(), skuType, querySKUs);
            if (skuDetails.getInt(IabHelper.RESPONSE_CODE) == IabHelper.BILLING_RESPONSE_RESULT_OK) {
                ArrayList<String> skuDetailList = skuDetails.getStringArrayList(IabHelper.RESPONSE_GET_SKU_DETAILS_LIST);
                List<SkuItem> skuItemList = new LinkedList<>();
                for (String skuInfo : skuDetailList) {
                    try {
                        JSONObject object        = new JSONObject(skuInfo);
                        String productId         = object.getString("productId");
                        String type              = object.getString("type");
                        String price             = object.getString("price");
                        String priceAmountMicros = object.getString("price_amount_micros");
                        String priceCurrencyCode = object.getString("price_currency_code");
                        String title             = object.getString("title");
                        String description       = object.getString("description");
                        skuItemList.add(new SkuItem(productId, type, price, priceAmountMicros, priceCurrencyCode, title, description));
                    } catch (JSONException e) {
                        continue;
                    }
                }
                return skuItemList;
            }
        } catch (RemoteException ignored) {
        }
        return null;
    }

    public boolean buyProduct(Activity activity, int requestCode, String productId, String type, String developerPayload, IabHelper.OnIabPurchaseFinishedListener IabPurchaseFinishedListener) {
        if (mInAppBillingService == null) {
            return false;
        }
        try {
            Bundle buyIntentBundle = mInAppBillingService.getBuyIntent(API_VERSION, activity.getPackageName(), productId, type, developerPayload);
            if (buyIntentBundle.getInt(IabHelper.RESPONSE_CODE) == IabHelper.BILLING_RESPONSE_RESULT_OK) {
                mIabHelper.launchPurchaseFlow(activity, activity.getPackageName(), requestCode, IabPurchaseFinishedListener, developerPayload);
                return true;
            }
        } catch (RemoteException e) {
            return false;
        }
        return false;
    }

    public List<String> getPurchases(Activity activity, String type) {
        try {
            Bundle ownedItems = mInAppBillingService.getPurchases(API_VERSION, activity.getPackageName(), type, null);
            int response = ownedItems.getInt(IabHelper.RESPONSE_CODE);
            if (response == 0) {
                return ownedItems.getStringArrayList(IabHelper.RESPONSE_INAPP_PURCHASE_DATA_LIST);
            }
        } catch (RemoteException e) {
            return null;
        }
        return null;
    }

    public void consumePurchase(Activity activity, String purchaseToken) {
        try {
            mInAppBillingService.consumePurchase(API_VERSION, activity.getPackageName(), purchaseToken);
        } catch (RemoteException ignored) {
        }
    }

    public void close(Context context) {
        if (mIabHelper != null) {
            mIabHelper.dispose();
            mIabHelper = null;
        }

        if (mInAppBillingService != null) {
            context.unbindService(mServiceConnection);
            mServiceConnection   = null;
            mInAppBillingService = null;
        }
    }

    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return mIabHelper.handleActivityResult(requestCode, resultCode, data);
    }
}



package com.konantech.konanlink.utils;

import android.util.Log;

public class Tick {
    private static long timeMillis;

    public Tick() {
        this.timeMillis = System.currentTimeMillis();
    }

    public static void timeLog(String tag) {
        long currentTimeMillis = System.currentTimeMillis();
        long gapTime = currentTimeMillis - timeMillis;
        timeMillis = currentTimeMillis;
        Log.e(tag, String.format("GapTime = %d", gapTime));
    }
}

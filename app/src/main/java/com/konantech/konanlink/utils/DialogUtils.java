package com.konantech.konanlink.utils;

import android.app.FragmentManager;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.dialog.KonanLinkDialogMaker;

public class DialogUtils {
    public static void showListDialog(String title, String[] titleArray, String[] valueArray, int[] icons, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.LIST, title, titleArray, valueArray, icons);
        dialogController.show(fragmentManager, "list");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showContentsListDialog(String title, String path, String info, String contents, String thumbnail, String[] titleArray, String[] valueArray, int[] icons, int deviceKind, String deviceKey, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.LIST_CONTENTS, title, path, info, contents, thumbnail, titleArray, valueArray, icons, deviceKind, deviceKey);
        dialogController.show(fragmentManager, "contentsList");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showSelectedDialog(String title, String[] titleArray, String[] valueArray, int selectedIndex, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.LIST_SELECTED, title, titleArray, valueArray, selectedIndex);
        dialogController.show(fragmentManager, "selectList");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showCheckListDialog(String title, String[] titleArray, String[] valueArray, String[] checked, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.LIST_CHECK, title, titleArray, valueArray, checked);
        dialogController.show(fragmentManager, "checklist");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showConverseDialog(String title, String content, int negativeStringId, int positiveStringId, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.CONVERSE, title, content, negativeStringId, positiveStringId);
        dialogController.show(fragmentManager, "converse");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showConverseCenterDialog(String title, String content, int negativeStringId, int positiveStringId, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.CONVERSE_CENTER, title, content, negativeStringId, positiveStringId);
        dialogController.show(fragmentManager, "converse");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showPasswordLockDialog(int type, boolean isCancelable, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.PASSWORD_LOCK, type, isCancelable);
        dialogController.show(fragmentManager, "password_lock");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showAlertDialog(String title,  String content, int btnStringId, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.ALERT, title, content, btnStringId);
        dialogController.show(fragmentManager, "alert");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static void showWebDialog(String content, FragmentManager fragmentManager, DialogController.OnDismissListener dismissListener) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.WEB, content);
        dialogController.show(fragmentManager, "web");
        dialogController.setOnDismissListener(dismissListener);
    }

    public static DialogController showLoadingDialog(String content, FragmentManager fragmentManager) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.LOADING, content);
        dialogController.show(fragmentManager, "loading");
        return dialogController;
    }

    public static DialogController showInfoDialog(FragmentManager fragmentManager, int type) {
        DialogController dialogController = KonanLinkDialogMaker.getInstance().getDialogFragment(KonanLinkDialogMaker.INFO, type);
        dialogController.show(fragmentManager, "info");
        return dialogController;
    }
}

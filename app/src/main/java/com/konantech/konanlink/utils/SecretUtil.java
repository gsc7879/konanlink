package com.konantech.konanlink.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by kimmyeongseong on 2016. 9. 23..
 */

public class SecretUtil {

    public static byte[] AES_KEY = new byte[] { 0x73, 0x78, 0x6f, 0x62, 0x64, 0x6e, 0x69, 0x66, 0, 0, 0, 0, 0, 0, 0, 0};
    public static byte[] AES_IV  = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public static String encryptAES128(String input) {
        try{
            SecretKeySpec secretKeySpec = new SecretKeySpec(AES_KEY, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(AES_IV));
            byte[] encrypted = cipher.doFinal(input.getBytes("UTF-8"));
            return Base64.encodeToString(encrypted, Base64.DEFAULT);

        }catch(Exception e){
            return "";
        }
    }

    public static String decryptAES128(String input) {
        try{
            SecretKeySpec secretKeySpec = new SecretKeySpec(AES_KEY, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(AES_IV));
            byte[] byteStr = Base64.decode(input.getBytes(), Base64.DEFAULT);
            String string = new String(cipher.doFinal(byteStr), "UTF-8");
            string = removeInvalidXMLCharacters(string);
//            string = removeXMLMarkups(string);
            return string;
        }catch(Exception e){
            return "";
        }
    }

    public static String removeInvalidXMLCharacters(String s) {
        StringBuilder out = new StringBuilder();

        int codePoint;
        int i = 0;

        while (i < s.length()) {
            // This is the unicode code of the character.
            codePoint = s.codePointAt(i);
            if ((codePoint == 0x9) ||
                    (codePoint == 0xA) ||
                    (codePoint == 0xD) ||
                    ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||
                    ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||
                    ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF))) {
                out.append(Character.toChars(codePoint));
            }
            i += Character.charCount(codePoint);
        }
        return out.toString();
    }


}

package com.konantech.konanlink.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.konantech.konanlink.KonanLinkLoading;
import com.konantech.konanlink.KonanLinkMain;
import com.konantech.konanlink.data.DataBank;

public class IntentChangeUtils {
    public static void goLoadingPage(Context context, Class pageClass) {
        DataBank.getInstance().clearPreference();
        Intent intent = new Intent(context, KonanLinkLoading.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("page", pageClass.getName());
        context.startActivity(intent);
    }

    public static void goMainPage(Activity activity) {
        if (activity == null) {
            return;
        }
        Intent intent = new Intent(activity, KonanLinkMain.class);
        activity.startActivity(intent);
        activity.finish();
    }
}

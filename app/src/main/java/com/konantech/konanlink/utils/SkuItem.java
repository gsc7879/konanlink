package com.konantech.konanlink.utils;

public class SkuItem {
    private String productId;
    private String type;
    private String price;
    private String priceAmountMicros;
    private String priceCurrencyCode;
    private String title;
    private String description;

    public SkuItem(String productId, String type, String price, String priceAmountMicros, String priceCurrencyCode, String title, String description) {
        this.productId = productId;
        this.type = type;
        this.price = price;
        this.priceAmountMicros = priceAmountMicros;
        this.priceCurrencyCode = priceCurrencyCode;
        this.title = title;
        this.description = description;
    }

    public String getProductId() {
        return productId;
    }

    public String getType() {
        return type;
    }

    public String getPrice() {
        return price;
    }

    public String getPriceAmountMicros() {
        return priceAmountMicros;
    }

    public String getPriceCurrencyCode() {
        return priceCurrencyCode;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}

package com.konantech.konanlink.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.konantech.konanlink.constant.UsersConst;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

public class GCMManager {
    private static final String PROPERTY_REG_ID      = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static final int ERROR_NOT_UNKNOWN = 0;
    public static final int ERROR_NOT_SUPPORT = 1;

    private OnGCMListener mGCMListener;

    public interface OnGCMListener {
        void onRegId(String regId);
        void onRegIdFailed(int errorCode);
    }

    public void requestRegId(Context context, OnGCMListener gcmListener) {
        mGCMListener = gcmListener;
        if (checkPlayServices(context)) {
            String regId = getRegistrationId(context);
            if (StringUtils.isBlank(regId)) {
                registerInBackground(context);
            } else {
                mGCMListener.onRegId(regId);
            }
        } else {
            mGCMListener.onRegIdFailed(ERROR_NOT_SUPPORT);
        }
    }

    private boolean checkPlayServices(Context context) {
        if (context == null) {
            return false;
        }

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return context.getSharedPreferences(GCMManager.class.getName(), Context.MODE_PRIVATE);
    }

    private void registerInBackground(final Context context) {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                try {
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
                    String regId = gcm.register(UsersConst.SENDER_ID);
                    storeRegistrationId(context, regId);
                    mGCMListener.onRegId(regId);
                } catch (IOException ex) {
                    cancel(true);
                }
                return null;
            }

            @Override
            protected void onCancelled(Object o) {
                mGCMListener.onRegIdFailed(ERROR_NOT_UNKNOWN);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
}

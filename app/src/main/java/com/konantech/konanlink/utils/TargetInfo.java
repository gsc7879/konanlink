package com.konantech.konanlink.utils;

public class TargetInfo {
    private String target;
    private String deviceKey;
    private String page;
    private String maxCount;

    public TargetInfo(String target, String deviceKey, String page, String maxCount) {
        this.target    = target;
        this.deviceKey = deviceKey;
        this.page      = page;
        this.maxCount  = maxCount;
    }

    public String getTarget() {
        return target;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public String getPage() {
        return page;
    }

    public String getMaxCount() {
        return maxCount;
    }
}

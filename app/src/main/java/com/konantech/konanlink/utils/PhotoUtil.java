package com.konantech.konanlink.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.GpsDirectory;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.translate.Translate;
import com.google.api.services.translate.TranslateRequestInitializer;
import com.google.api.services.translate.model.TranslationsListResponse;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by kimmyeongseong on 2016. 9. 23..
 */

public class PhotoUtil {

    private static final int MAX_RESULTS = 2;
    private static final String CLOUD_VISION_API_KEY = "AIzaSyCKEstDt7jETDvDzrlIgfBIxuTah8ZKQZA";
    public static final GoogleClientRequestInitializer KEY_INITIALIZER = new TranslateRequestInitializer(CLOUD_VISION_API_KEY);

    public static String getMimeType(String url) {
        String type = null;
        if (url.contains(".")) {
            String extension = MimeTypeMap.getFileExtensionFromUrl(url.substring(url.lastIndexOf(".")));
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        }
        return type;
    }

    public static String getMetaInfoFromCloudVision(File imgFile) {
        try {
            HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = GsonFactory.getDefaultInstance();

            Vision.Builder builder = new Vision.Builder(httpTransport, jsonFactory, null);
            builder.setApplicationName("konanlink");
            builder.setVisionRequestInitializer(new
                    VisionRequestInitializer(CLOUD_VISION_API_KEY));
            Vision vision = builder.build();

            List<Feature> featureList = new ArrayList<>();
            Feature labelDetection = new Feature();
            labelDetection.setType("LABEL_DETECTION");
            labelDetection.setMaxResults(MAX_RESULTS);
            featureList.add(labelDetection);

            Feature textDetection = new Feature();
            textDetection.setType("TEXT_DETECTION");
            textDetection.setMaxResults(MAX_RESULTS);
            featureList.add(textDetection);

            Feature landmarkDetection = new Feature();
            landmarkDetection.setType("LANDMARK_DETECTION");
            landmarkDetection.setMaxResults(MAX_RESULTS);
            featureList.add(landmarkDetection);

            List<AnnotateImageRequest> imageList = new ArrayList<>();
            AnnotateImageRequest annotateImageRequest = new AnnotateImageRequest();
            Image base64EncodedImage = getBase64EncodedJpeg(BitmapFactory.decodeFile(imgFile.getAbsolutePath()));
            annotateImageRequest.setImage(base64EncodedImage);
            annotateImageRequest.setFeatures(featureList);
            imageList.add(annotateImageRequest);

            BatchAnnotateImagesRequest batchAnnotateImagesRequest =
                    new BatchAnnotateImagesRequest();
            batchAnnotateImagesRequest.setRequests(imageList);

            Vision.Images.Annotate annotateRequest =
                    vision.images().annotate(batchAnnotateImagesRequest);
            // Due to a bug: requests to Vision API containing large images fail when GZipped.
            annotateRequest.setDisableGZipContent(true);

            BatchAnnotateImagesResponse response = annotateRequest.execute();
            return convertResponseToString(response);

        } catch (GoogleJsonResponseException e) {
        } catch (IOException e) {
        }

        return "";
    }

    public static Image getBase64EncodedJpeg(Bitmap bitmap) {
        Image image = new Image();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        image.encodeContent(imageBytes);
        return image;
    }

    public static String convertResponseToString(BatchAnnotateImagesResponse response) {
        StringBuilder message = new StringBuilder("");
        List<EntityAnnotation> labels = response.getResponses().get(0).getLabelAnnotations();
        if (labels != null) {
            for (EntityAnnotation label : labels) {
                message.append(String.format(Locale.getDefault(), "%s", label.getDescription()));
                message.append(" ");
            }
        }

        List<EntityAnnotation> texts = response.getResponses().get(0)
                .getTextAnnotations();
        if (texts != null) {
            for (EntityAnnotation text : texts) {
                message.append(String.format(Locale.getDefault(), "%s",
                        text.getDescription()));
                message.append(" ");
            }
        }

        List<EntityAnnotation> landmarks = response.getResponses().get(0)
                .getLandmarkAnnotations();
        if (landmarks != null) {
            for (EntityAnnotation landmark : landmarks) {
                message.append(String.format(Locale.getDefault(), "%s", landmark.getDescription()));
                message.append(" ");
            }
        }

        return trans(message.toString());
    }


    public static String trans(String result) {

        Translate.Builder builder;
        HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = AndroidJsonFactory.getDefaultInstance();
        builder = new Translate.Builder(httpTransport, jsonFactory, null);
        builder.setApplicationName("konanlink");
        builder.setGoogleClientRequestInitializer(KEY_INITIALIZER);

        List<String> qList = new ArrayList<String>();
        qList.add(result);

        Translate.Translations.List list = null;
        try {
            list = builder.build().translations().list(qList, Locale.getDefault().getLanguage()); //your language code here~
            TranslationsListResponse response = list.execute();
            return response.getTranslations().get(0).getTranslatedText();
        } catch (IOException e) {
            return "";
        }
    }

    public static String getAddress(Context context, File file, Locale locale) {
        Geocoder geocoder = new Geocoder(context, locale);

        try {
            GeoLocation location = getGeoLocation(getMetadata(file));
            List<Address> addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, get just a single address.
                    1);
            if (addresses == null || addresses.size() == 0) {
                return "";
            }

            List addressList = new ArrayList();
            for (Address addressResult : addresses) {
                for (int addressLineIndex = 0; addressLineIndex <= addressResult.getMaxAddressLineIndex(); addressLineIndex++) {
                    for (String address : StringUtils.split(addressResult.getAddressLine(addressLineIndex), " ")) {
                        if (!addressList.contains(address)) {
                            addressList.add(address);
                        }
                    }
                }
            }
            return StringUtils.trim(StringUtils.join(addressList, " "));

        } catch (Exception e) {
            return "";
        }

    }

    public static GeoLocation getGeoLocation(Metadata metadata) {
        if (metadata.containsDirectoryOfType(GpsDirectory.class)) {
            GeoLocation geoLocation = ((GpsDirectory) metadata.getFirstDirectoryOfType(GpsDirectory.class)).getGeoLocation();
            return geoLocation;
        } else {
            return null;
        }
    }

    public static Metadata getMetadata(File file) throws ImageProcessingException, IOException {
        return ImageMetadataReader.readMetadata(file);
    }


    public static String getDate(File file) {

        try {
            String mimeType = getMimeType(file.getAbsolutePath());
            if (mimeType.endsWith("jpg") || mimeType.endsWith("jpeg") || mimeType.endsWith("JPG") || mimeType.endsWith("JPEG")) {
                ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                if (exif != null) {
                    String result = exif.getAttribute(ExifInterface.TAG_DATETIME);
                    return (result != null) ? result : "";
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }

        return "";
    }

    public static boolean isImageFile(File file) {
        if(file.exists()) {
            String mimeType = getMimeType(file.getAbsolutePath());
            if (mimeType != null && mimeType.startsWith("image/")) {
                return true;
            }
            return false;
        }

        return false;
    }

}

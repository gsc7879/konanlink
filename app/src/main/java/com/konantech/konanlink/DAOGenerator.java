package com.konantech.konanlink;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class DAOGenerator {
    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.konantech.konanlink.dao");
        //  최근검색
        addRecentKeyword(schema);
        // 파일 정보 수집
        addFileMetaInfo(schema);

        try {
            new DaoGenerator().generateAll(schema, FileUtils.getFile(new File("."), "app/src/main/java").getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addRecentKeyword(Schema schema) {
        Entity keywordEntity = schema.addEntity("RecentKeyword");
        keywordEntity.addIdProperty().autoincrement();
        keywordEntity.addStringProperty("keyword").unique().notNull();
        keywordEntity.addIntProperty("count").notNull();
        keywordEntity.addDateProperty("searchDate").notNull();
    }

    private static void addFileMetaInfo(Schema schema) {
        Entity keywordEntity = schema.addEntity("PhotoMetaInfo");
        keywordEntity.addIdProperty().autoincrement();
        keywordEntity.addStringProperty("fileAbsPath").unique().notNull();   // 파일위치 (절대 경로 키로사용)
        keywordEntity.addStringProperty("fileName").notNull();              // 파일명
        keywordEntity.addStringProperty("keyWord").notNull();               // 주소 + 주소(영문) + 메타
        keywordEntity.addStringProperty("address").notNull();               // 촬영장소
        keywordEntity.addStringProperty("addressEN").notNull();             // 촬영장소(영문)
        keywordEntity.addStringProperty("metaInfo").notNull();              // 메타 인포
        keywordEntity.addStringProperty("recordDate").notNull();            // 촬영일
        keywordEntity.addLongProperty("modifiedDate").notNull();            // 수정일
    }

}

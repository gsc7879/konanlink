package com.konantech.konanlink;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.dignara.lib.annotation.InitActivity;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PageFragment;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.page.fragment.LoadingPageFragment;

@InitActivity(pageLayoutId = R.id.page_layout)
public class KonanLinkLoading extends AppCompatActivity {
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        DataBank.getInstance().initData(getApplicationContext());
        setContentView(R.layout.main_loading);
        deviceInfo();
        setAppManager(icicle);
    }

    private void deviceInfo() {
        DataBank.INFO.put(DataBank.DEVICE_NAME, Build.MODEL);
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            DataBank.INFO.put(DataBank.VERSION_NAME, packageInfo.versionName);
            DataBank.INFO.put(DataBank.VERSION_CODE, packageInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            finish();
        }
    }

    private void setAppManager(Bundle icicle) {
        String pageTag = getIntent().getStringExtra("page");

        try {
            AppManager.getInstance().initActivity(this, icicle, null, (Class<? extends PageFragment>) Class.forName(pageTag));
        } catch (Exception e) {
            AppManager.getInstance().initActivity(this, icicle, null, LoadingPageFragment.class);
        }
    }

    @Override
    public void onBackPressed() {
        if (!AppManager.getInstance().sendBackPressEvent()) {
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        AppManager.getInstance().saveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        AppManager.getInstance().requestPermissionsResult(requestCode, permissions, grantResults);
    }
}

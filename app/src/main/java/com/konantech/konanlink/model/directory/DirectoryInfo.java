package com.konantech.konanlink.model.directory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DirectoryInfo {
    public static final String TYPE_UP        = "U";
    public static final String TYPE_DRIVE     = "V";
    public static final String TYPE_DIRECTORY = "D";
    public static final String TYPE_FILE      = "F";

    private String                           mFolder;
    private String                           mCurrentPath;
    private String                           mParentPath;
    private Map<String, List<DirectoryItem>> mDirectoryDataMap;

    public DirectoryInfo() {
        mDirectoryDataMap = new HashMap<>();
    }

    public void setCurrentPath(String mCurrentPath) {
        this.mCurrentPath = mCurrentPath;
    }

    public void setParentPath(String mParentPath) {
        this.mParentPath = mParentPath;
    }

    public String getCurrentPath() {
        return mCurrentPath;
    }

    public String getParentPath() {
        return mParentPath;
    }

    public String getFolder() {
        return mFolder;
    }

    public void setFolder(String folder) {
        this.mFolder = folder;
    }

    public void addDirectoryItem(String type, DirectoryItem directoryItem) {
        if (!mDirectoryDataMap.containsKey(type)) {
            mDirectoryDataMap.put(type, new ArrayList<DirectoryItem>());
        }
        mDirectoryDataMap.get(type).add(directoryItem);
    }

    public List<DirectoryItem> getDirectoryItemMap(String type) {
        return mDirectoryDataMap.get(type);
    }
}

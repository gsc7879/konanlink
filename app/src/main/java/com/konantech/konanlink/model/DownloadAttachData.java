package com.konantech.konanlink.model;


import com.dignara.lib.fragment.FragmentData;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.data.DataBank;

import java.io.File;

public class DownloadAttachData extends FragmentData {
    private String fileTitle;
    private String fileType;
    private String filePath;
    private String fileExtension;
    private String folderPath;
    private String downloadURL;
    private long fileSize;
    private long fileModifyTime;
    private boolean isAttached;
    private boolean isShare;
    private DeviceInfo deviceInfo;
    private DeviceInfo hostDeviceInfo;
    private File downloadTarget;

    private DeviceInfo getHostDeviceInfo(DeviceInfo deviceInfo) {
        if (hostDeviceInfo == null) {
            hostDeviceInfo = deviceInfo.isSearchHost() ? deviceInfo : DataBank.getInstance().getDeviceInfo(deviceInfo.getKey()).getHostInfo();
        }
        return hostDeviceInfo;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFolderPath() {
        return folderPath;
    }

    public void setFolderPath(String folderPath) {
        this.folderPath = folderPath;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getFileModifyTime() {
        return fileModifyTime;
    }

    public void setFileModifyTime(long fileModifyTime) {
        this.fileModifyTime = fileModifyTime;
    }

    public boolean isAttached() {
        return isAttached;
    }

    public void setAttached(boolean attached) {
        isAttached = attached;
    }

    public boolean isShare() {
        return isShare;
    }

    public void setShare(boolean share) {
        isShare = share;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public File getDownloadTarget() {
        return downloadTarget;
    }

    public void setDownloadTarget(File downloadTarget) {
        this.downloadTarget = downloadTarget;
    }
}
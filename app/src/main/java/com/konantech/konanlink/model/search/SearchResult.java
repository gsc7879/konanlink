package com.konantech.konanlink.model.search;

import java.util.List;

public class SearchResult<T> {
    private List<T> resultList;
    private int     offset;
    private boolean isNext;

    public SearchResult(List<T> resultList, int offset, boolean isNext) {
        this.resultList = resultList;
        this.offset     = offset;
        this.isNext     = isNext;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public int getOffset() {
        return offset;
    }

    public boolean isNext() {
        return isNext;
    }
}

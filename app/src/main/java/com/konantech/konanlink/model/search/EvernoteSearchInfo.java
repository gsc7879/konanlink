package com.konantech.konanlink.model.search;

public class EvernoteSearchInfo extends SearchInfo {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

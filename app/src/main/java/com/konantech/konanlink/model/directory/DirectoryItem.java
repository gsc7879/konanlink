package com.konantech.konanlink.model.directory;

import java.util.Date;

public class DirectoryItem  {
    private String name;
    private String path;
    private String type;
    private String link;
    private String extension;
    private long   size;
    private Date   modifiedTime;
    private String downloadURL;
    private String thumNailURL;

    public DirectoryItem() {}

    public DirectoryItem(String name, String path, String type, String link, String extension, long size, Date modifiedTime, String downloadURL, String thumNailURL) {
        this.name = name;
        this.path = path;
        this.type = type;
        this.extension = extension;
        this.size = size;
        this.link = link;
        this.modifiedTime = modifiedTime;
        this.downloadURL = downloadURL;
        this.thumNailURL = thumNailURL;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String ext) {
        this.extension = ext;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    public String getThumNailURL() {
        return thumNailURL;
    }

    public void setThumNailURL(String thumNailURL) {
        this.thumNailURL = thumNailURL;
    }
}

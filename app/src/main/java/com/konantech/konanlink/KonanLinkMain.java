package com.konantech.konanlink;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dignara.lib.annotation.InitActivity;
import com.dignara.lib.fragment.AppManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.db.DBManager;
import com.konantech.konanlink.menu.DrawerMenuFragment;
import com.konantech.konanlink.page.fragment.MainPageFragment;
import com.konantech.konanlink.service.CollectPhotoService;
import com.konantech.konanlink.service.FileWatcherService;
import com.konantech.konanlink.service.IndexingService;
import com.konantech.konanlink.service.PassCodeBroadcastReceiver;

@InitActivity(pageLayoutId = R.id.page_layout, menuLayoutId = R.id.menu_layout)
public class KonanLinkMain extends AppCompatActivity {
    private BroadcastReceiver mPassCodeBroadcastReceiver;

    @Override
    public void onCreate(Bundle icicle) {
//        SpringApplication.run(Agent.class, new String[] { "service" });

        super.onCreate(icicle);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.main);

        DataBank.getInstance().setContext(getApplicationContext());
        DBManager.getInstance().setKeywordDao(getApplicationContext());
        setAppManager(icicle);
        startService();
        setPassCode();

    }

    private void setAppManager(Bundle icicle) {
        AppManager.getInstance().initActivity(this, icicle, DrawerMenuFragment.class, MainPageFragment.class);
    }

    private void setPassCode() {
        IntentFilter intentFilter  = new IntentFilter(Intent.ACTION_SCREEN_ON);
        mPassCodeBroadcastReceiver = new PassCodeBroadcastReceiver();
        registerReceiver(mPassCodeBroadcastReceiver, intentFilter);
    }

    @Override
    public void onBackPressed() {
        if (!AppManager.getInstance().sendBackPressEvent()) {
           finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        AppManager.getInstance().saveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPassCodeBroadcastReceiver != null) {
            unregisterReceiver(mPassCodeBroadcastReceiver);
            mPassCodeBroadcastReceiver = null;
        }
    }

    private void startService() {
        startService(new Intent(this, IndexingService.class));
        startService(new Intent(this, FileWatcherService.class));
        startService(new Intent(this, CollectPhotoService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(getApplicationContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        AppManager.getInstance().sendActivityResult(requestCode, resultCode, data);
    }
}

package com.konantech.konanlink.page.sub;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dignara.lib.annotation.PageFragmentInfo;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.page.fragment.IntroPageFragment;
import com.konantech.konanlink.page.fragment.JoinPageFragment;
import com.konantech.konanlink.page.fragment.LoginPageFragment;

import java.util.ArrayList;
import java.util.List;

@PageFragmentInfo(layout = R.layout.page_join)
public class IntroPagerAdapter extends PagerAdapter implements View.OnClickListener {
    private IntroPageFragment mIntroPageFragment;
    private List<View>        mIntroViewList;

    public IntroPagerAdapter(IntroPageFragment introPageFragment, boolean isGift) {
        mIntroPageFragment = introPageFragment;
        LayoutInflater layoutInflater = mIntroPageFragment.getActivity().getLayoutInflater();

        mIntroViewList = new ArrayList<>();
//        mIntroViewList.add(createView(layoutInflater, R.layout.layout_intro_main, GAConst.SCREEN_TUTO_1));
        mIntroViewList.add(createView(layoutInflater, R.layout.layout_intro_device, GAConst.SCREEN_TUTO_2));
        mIntroViewList.add(createView(layoutInflater, R.layout.layout_intro_indexing, GAConst.SCREEN_TUTO_3));
        mIntroViewList.add(createView(layoutInflater, R.layout.layout_intro_privacy , GAConst.SCREEN_TUTO_4));

        if (isGift) {
            mIntroViewList.add(createView(layoutInflater, R.layout.layout_intro_gift, null));
        }
        View setupLayout = createView(layoutInflater, R.layout.layout_intro_setup, GAConst.SCREEN_TUTO_END);
        setupLayout.findViewById(R.id.btn_intro_login).setOnClickListener(this);
        setupLayout.findViewById(R.id.btn_intro_join).setOnClickListener(this);
        mIntroViewList.add(setupLayout);
    }

    private View createView(LayoutInflater inflater, int layoutResId, String screenName) {
        View view = inflater.inflate(layoutResId, null);
        view.setTag(screenName);
        return view;
    }

    public String getScreenName(int position) {
        Object tag = mIntroViewList.get(position).getTag();
        if (tag != null) {
            return (String) tag;
        }
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup pager, int position) {
        View view = mIntroViewList.get(position);
        pager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public int getCount() {
        return mIntroViewList.size();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_intro_login:
                mIntroPageFragment.switchPage(LoginPageFragment.class);
                break;
            case R.id.btn_intro_join:
                mIntroPageFragment.switchPage(JoinPageFragment.class);
                break;
        }
    }
}

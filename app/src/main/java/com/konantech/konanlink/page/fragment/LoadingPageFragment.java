package com.konantech.konanlink.page.fragment;

import android.Manifest;
import android.os.AsyncTask;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PageFragment;
import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.IntentUtils;
import com.dignara.lib.utils.NetworkUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.system.SystemInfo;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.LoadingPartFragment;
import com.konantech.konanlink.task.management.CleanSystemTask;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.IntentChangeUtils;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

@PageFragmentInfo(
        screenName     = GAConst.SCREEN_LOADING,
        layout         = R.layout.page_loading,
        isAddBackStack = false,
        permissions    = Manifest.permission.READ_PHONE_STATE)
public class LoadingPageFragment extends PageFragment {
    private Animation mAnimation;

    @InjectView(id = R.id.text_loading_status)
    private TextView mStatusText;

    @InjectView(id = R.id.img_loading_search)
    private ImageView mSearchImg;

    @InjectView(id = R.id.img_loading_test_mode)
    private ImageView mTestModeImg;

    @Override
    protected void onSwitchedPage(Class prevPageClass, Object... etcData) {
        mStatusText.setText(R.string.s01_text_initiate_konanlink_00);
        mTestModeImg.setVisibility(NetConst.getInstance().isTestMode() ? View.VISIBLE : View.GONE);

        if (NetworkUtils.isNetworkConnected(getActivity())) {
            startLoading();
            requestCheckSystem();
        } else {
            showErrorDialog(getString(R.string.s01_dialog_error_cannot_connect_network));
        }
    }

    @Override
    public void onInitView(View view) {}

    private void startLoading() {
        if (mAnimation == null) {
            mAnimation = new CircleAnimation(mSearchImg, AnimationConst.BIG_RADIOUS);
            mAnimation.setDuration(AnimationConst.BIG_DURATION);
            mAnimation.setRepeatCount(Animation.INFINITE);
        }
        mSearchImg.startAnimation(mAnimation);
    }

    private void requestCheckSystem() {
        new SystemApiManager().reqSystemInfo(getActivity(), mSubscriptions, new Result.OnResultListener<SystemInfo>() {
            @Override
            public void onSuccess(SystemInfo systemInfo) {
                DataBank.getInstance().setLastAnnounceDate(systemInfo.getLastAnnounceTime());
                if (systemInfo.isMaintenance()) {
                    showErrorDialog(getString(R.string.s01_dialog_error_under_maintenance));
                    return;
                }

                final int recentVersion = systemInfo.getRecentVersion();
                if (StringUtils.isNotBlank(systemInfo.getMessage())) {
                    DialogUtils.showWebDialog(systemInfo.getMessage(), getFragmentManager(), new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            checkUpdate(recentVersion);
                        }
                    });
                } else {
                    checkUpdate(recentVersion);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
                showErrorDialog(getString(R.string.s01_dialog_error_cannot_access_server));
            }
        });
    }

    private void checkUpdate(int recentVersion) {
        int appVersion = (int) DataBank.INFO.get(DataBank.VERSION_CODE);
        if (recentVersion > appVersion) {
            DialogUtils.showConverseDialog("", getString(R.string.s92_dialog_text_major_update), R.string.s92_dialog_btn_update_later, R.string.s92_dialog_btn_update, getFragmentManager(), new DialogController.OnDismissListener() {
                @Override
                public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                    if (isPositive) {
                        update();
                    }
                    System.exit(0);
                }
            });
        } else {
            requestInitSystem();
        }
    }

    private void update() {
        IntentUtils.openingMarket(getActivity(), getActivity().getPackageName());
    }

    private void requestInitSystem() {
        CleanSystemTask cleanSystemTask = new CleanSystemTask(0, new WorkTask.OnTaskListener() {
            @Override
            public void onStartTask(int callBackId) {}

            @Override
            public void onEndTask(int callBackId, Object object) {
                loadingStart();
            }

            @Override
            public void onCancelTask(int callBackId, int cancelCode, Object data) {
                loadingStart();
            }

            @Override
            public void onTaskProgress(int callBackId, Object... params) {
            }
        });
        cleanSystemTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void loadingStart() {
        attachFragment(R.id.layout_loading_main, LoadingPartFragment.class, mStatusText);
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(LoadingPartFragment.class)) {
            if (data.length > 0) {
                switch ((LoadingPartFragment.LoadingError) data[0]) {
                    case NOT_REGISTERED_DEVICE:
                        switchPage(RegisterPageFragment.class);
                        break;
                    case NOT_EXIST_KEY:
                        switchPage(LoginPageFragment.class);
                        break;
                    case INIT_DEVICE:
                        switchPage(IntroPageFragment.class);
                        break;
                }
            } else {
                mSearchImg.clearAnimation();
                IntentChangeUtils.goMainPage(getActivity());
            }
        }
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                System.exit(0);
            }
        });
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            showErrorDialog(getString(R.string.s98_dialog_text_for_marshmallow));
            return false;
        }
        return true;
    }
}
package com.konantech.konanlink.page.fragment;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.fragment.PageFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AccountApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.SendEmailResult;
import com.konantech.konanlink.constant.GAConst;

import org.apache.commons.lang3.StringUtils;

@PageFragmentInfo(screenName = GAConst.SCREEN_RESET, layout = R.layout.page_login_find, isAddBackStack = false)
public class LoginFindPageFragment extends PageFragment implements Result.OnResultListener<SendEmailResult> {
    @InjectView(id = R.id.edit_login_find_email)
    private EditText mEmailText;

    @InjectView(id = R.id.text_login_find_email_error)
    private TextView mErrorText;

    @InjectView(id = R.id.toolbar_login_find)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
    }

    @Override
    protected void onLeavePage(Class<? extends PageFragment> nextPageClass) {
        mEmailText.setText("");
        mEmailText.requestFocus();

        mErrorText.setText("");
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s08_title_password_reset);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchPrevPage();
            }
        });
    }

    @Click(R.id.btn_login_find_send)
    public void sendEmail() {
        if (StringUtils.isBlank(mEmailText.getText())) {
            mErrorText.setText(R.string.s99_error_typein_email);
            return;
        }
        new AccountApiManager().reqSendEmail(getActivity(), mSubscriptions, mEmailText.getText().toString(), this);
    }

    @Override
    public boolean onBackPressed() {
        switchPrevPage();
        return true;
    }

    @Override
    public void onSuccess(SendEmailResult result) {
        Snackbar.make(getView(), String.format(getString(R.string.s08_toast_send_temp_password)), Snackbar.LENGTH_SHORT).show();
        switchPrevPage();
    }

    @Override
    public void onFailed(int code, String msg) {
        if (StringUtils.isNotBlank(msg)) {
            mErrorText.setText(msg);
        } else {
            mErrorText.setText(R.string.s08_error_send_password_failed);
        }
    }

}
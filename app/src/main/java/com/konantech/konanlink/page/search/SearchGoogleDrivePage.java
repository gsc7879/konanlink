package com.konantech.konanlink.page.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.adapter.GoogleDriveSearchListAdapter;
import com.konantech.konanlink.list.model.GoogleDriveSearchListSetData;
import com.konantech.konanlink.model.search.GoogleDriveSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.GoogleDriveSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;

import org.apache.commons.lang3.StringUtils;

public class SearchGoogleDrivePage extends SearchPage<GoogleDriveSearchListAdapter> {
    private GoogleDriveSearchListSetData mGoogleDriveSearchListSetData;

    public SearchGoogleDrivePage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
    }

    @Override
    protected GoogleDriveSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mGoogleDriveSearchListSetData = new GoogleDriveSearchListSetData();
        GoogleDriveSearchListAdapter searchListAdapter = new GoogleDriveSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_googledrive, R.id.list_search_googledrive, R.layout.adapter_search_googledrive, R.layout.layout_list_no, mGoogleDriveSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_googledrive_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(GoogleDriveSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResult<GoogleDriveSearchInfo>) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    @Override
    protected void onSearch(String keyword) {
        if (StringUtils.equals(keyword, mGoogleDriveSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0) {
            return;
        }

        mGoogleDriveSearchListSetData.setKeyword(keyword);
        mSearchListAdapter.loadList();
    }
}

package com.konantech.konanlink.page.fragment;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.fragment.PageFragment;
import com.dignara.lib.utils.KeyboardUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.part.LoginPartFragment;

import org.apache.commons.lang3.StringUtils;

@PageFragmentInfo(screenName = GAConst.SCREEN_LOGIN, layout = R.layout.page_login)
public class LoginPageFragment extends PageFragment {
    @InjectView(id = R.id.edit_login_email)
    private EditText mEmailEdit;

    @InjectView(id = R.id.edit_login_password)
    private EditText mPassEdit;

    @InjectView(id = R.id.text_login_email_error)
    private TextView mEmailErrorText;

    @InjectView(id = R.id.text_login_password_error)
    private TextView mPassErrorText;

    @InjectView(id = R.id.toolbar_login)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
    }

    @Override
    protected void onLeavePage(Class<? extends PageFragment> nextPageClass) {
        mEmailEdit.setText("");
        mPassEdit.setText("");

        mEmailEdit.requestFocus();

        mEmailErrorText.setText("");
        mPassErrorText.setText("");
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s06_title_signin);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchPrevPage();
            }
        });
    }

    @Override
    protected void onSwitchedPage(Class prevPageClass, Object... etcData) {
        if (etcData.length > 1) {
            mEmailEdit.setText((String) etcData[0]);
            mPassEdit.setText((String) etcData[1]);
            login();
        }
    }

    private boolean loginCheck() {
        if (StringUtils.isBlank(mEmailEdit.getText())) {
            setErrorMsg(R.string.s99_error_typein_email, 0);
            return false;
        }

        if (StringUtils.isBlank(mPassEdit.getText())) {
            setErrorMsg(0, R.string.s99_error_typein_password);
            return false;
        }
        return true;
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(LoginPartFragment.class)) {
            if (data.length > 1) {
                switch ((int) data[0]) {
                    case ErrorConst.ERR_1002_NOT_EXIST_EMAIL:
                        setErrorMsg(R.string.was_error_1002, 0);
                        break;
                    case ErrorConst.ERR_1003_NOT_EQUALS_PASSWORD:
                        setErrorMsg(0, R.string.was_error_1003);
                        break;
                    default:
                        Snackbar.make(getView(), (String) data[1], Snackbar.LENGTH_LONG).show();
                        break;
                }
            } else {
                switchPage(RegisterPageFragment.class);
            }
        }
    }

    private void setErrorMsg(int emailErrorResId, int passErrorResId) {
        if (emailErrorResId > 0) {
            mEmailErrorText.setText(emailErrorResId);
        } else {
            mEmailErrorText.setText("");
        }

        if (passErrorResId > 0) {
            mPassErrorText.setText(passErrorResId);
        } else {
            mPassErrorText.setText("");
        }

    }

    @Click(R.id.btn_login_find)
    public void loginFind() {
        switchPage(LoginFindPageFragment.class);
    }

    @Click(R.id.btn_login_confirm)
    public void login() {
        if (loginCheck()) {
            KeyboardUtils.hideSoftInput(getActivity());
            attachFragment(R.id.layout_login_main, LoginPartFragment.class, mEmailEdit.getText().toString(), mPassEdit.getText().toString());
        }
    }
}
package com.konantech.konanlink.page.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.adapter.DropboxSearchListAdapter;
import com.konantech.konanlink.list.model.DropboxSearchListSetData;
import com.konantech.konanlink.model.search.DropboxSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.DropboxSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;

import org.apache.commons.lang3.StringUtils;

public class SearchDropBoxPage extends SearchPage<DropboxSearchListAdapter> {
    private DropboxSearchListSetData mDropboxSearchListSetData;

    public SearchDropBoxPage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
    }

    @Override
    protected DropboxSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mDropboxSearchListSetData = new DropboxSearchListSetData();
        DropboxSearchListAdapter searchListAdapter = new DropboxSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_dropbox, R.id.list_search_dropbox, R.layout.adapter_search_dropbox, R.layout.layout_list_no, mDropboxSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_dropbox_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(DropboxSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResult<DropboxSearchInfo>) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    @Override
    protected void onSearch(String keyword) {
        if (StringUtils.equals(keyword, mDropboxSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0) {
            return;
        }

        mDropboxSearchListSetData.setKeyword(keyword);
        mSearchListAdapter.loadList();
    }
}

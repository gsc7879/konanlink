package com.konantech.konanlink.page.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.adapter.GMailSearchListAdapter;
import com.konantech.konanlink.list.model.GmailSearchListSetData;
import com.konantech.konanlink.model.search.GMailSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.GmailSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;

import org.apache.commons.lang3.StringUtils;

public class SearchGmailPage extends SearchPage<GMailSearchListAdapter> {
    private GmailSearchListSetData mGmailSearchListSetData;

    public SearchGmailPage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
    }

    @Override
    protected GMailSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mGmailSearchListSetData = new GmailSearchListSetData();
        GMailSearchListAdapter searchListAdapter = new GMailSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_gmail, R.id.list_search_gmail, R.layout.adapter_search_gmail, R.layout.layout_list_no, mGmailSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_gmail_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(GmailSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResult<GMailSearchInfo>) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    @Override
    protected void onSearch(String keyword) {
        if (StringUtils.equals(keyword, mGmailSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0) {
            return;
        }

        mGmailSearchListSetData.setKeyword(keyword);
        mSearchListAdapter.loadList();
    }
}

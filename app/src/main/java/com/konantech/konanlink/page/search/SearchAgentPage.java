package com.konantech.konanlink.page.search;

import android.content.res.Resources;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.list.popup.ListQuickPopupMenu;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.dialog.ListSelectedDialogFragment;
import com.konantech.konanlink.list.adapter.AgentSearchListAdapter;
import com.konantech.konanlink.list.adapter.PopupMenuListAdapter;
import com.konantech.konanlink.list.data.PopupMenuListViewData;
import com.konantech.konanlink.list.model.AgentSearchListSetData;
import com.konantech.konanlink.list.model.PopupMenuListData;
import com.konantech.konanlink.list.model.PopupMenuListSetData;
import com.konantech.konanlink.part.AgentSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class SearchAgentPage extends SearchPage<AgentSearchListAdapter> {
    protected static final int TYPE   = 0;
    protected static final int SORT   = 1;
    protected static final int PERIOD = 2;

    private ListQuickPopupMenu   mQuickSearchOptionMenu;
    private PopupMenuListAdapter mPopupMenuListAdapter;
    private PopupMenuListSetData mPopupMenuListSetData;
    private AgentSearchListSetData mAgentSearchListSetData;

    private String[] mKindTitles;
    private String[] mKindValues;
    private String[] mSortTitles;
    private String[] mSortValues;
    private String[] mPeriodTitles;
    private String[] mPeriodValues;

    private Object[] mSearchOptIndex;

    public SearchAgentPage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
        initSearchOptions();
        createPopupMenu();
    }

    @Override
    protected void onSearch(String keyword) {
        if (!StringUtils.equals(keyword, null) && (StringUtils.equals(keyword, mAgentSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0)) {
            return;
        }

        if (keyword != null) {
            mAgentSearchListSetData.setKeyword(keyword);
        }
        mAgentSearchListSetData.setSearchType(getSearchType());
        mAgentSearchListSetData.setSortValue(getSortValue());
        mAgentSearchListSetData.setPeriodStart(getPeriodStart());
        mAgentSearchListSetData.setPeriodEnd(getPeriodEnd());
        mAgentSearchListSetData.setSearchTarget(InfoUtils.getSearchTarget(mDeviceInfo.getKind()));
        mAgentSearchListSetData.setDeviceKey(mDeviceInfo.getKey());
        mAgentSearchListSetData.setAgentDeviceKey(mDeviceInfo.getHostInfo().getKey());
        mSearchListAdapter.loadList();
    }

    protected void initSearchOptions() {
        Resources resources = mContext.getResources();
        mKindTitles     = getTypeTitle(mDeviceInfo.getKind());
        mKindValues     = getTypeValue(mDeviceInfo.getKind());
        mSortTitles     = resources.getStringArray(R.array.sort_file_title);
        mSortValues     = resources.getStringArray(R.array.sort_file_value);
        mPeriodTitles   = resources.getStringArray(R.array.period_file_title);
        mPeriodValues   = resources.getStringArray(R.array.period_file_value);
        mSearchOptIndex = new Object[] {new LinkedList(Arrays.asList(mKindValues)), 0, 0};
    }

    @Override
    protected AgentSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mAgentSearchListSetData = new AgentSearchListSetData();
        AgentSearchListAdapter searchListAdapter = new AgentSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_kms, R.id.list_search_kms, R.layout.adapter_search_kms, R.layout.layout_list_no, mAgentSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_kms_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    @Click(R.id.btn_list_search_kms_option)
    public void showPopupMenu(View view) {
        if (mQuickSearchOptionMenu == null) {
            createPopupMenu();
        } else {
            mPopupMenuListSetData.setPopupMenuListDataList(getPopupMenuListDataList());
        }
        mPopupMenuListAdapter.loadList();
        mQuickSearchOptionMenu.show(view);
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(AgentSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResponse) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    private List<PopupMenuListData> getPopupMenuListDataList() {
        List<PopupMenuListData> popupMenuListDataList = new ArrayList<>();
        String[] menuArray = new String[] {getFileTypeMenuString(), getSortTitle(), getPeriodTitle()};
        int selectedIndex = 0;
        for (int i = 0; i < menuArray.length; i++) {
            popupMenuListDataList.add(new PopupMenuListData(menuArray[i], selectedIndex == i));
        }
        return popupMenuListDataList;
    }

    private void createPopupMenu() {
        mPopupMenuListSetData  = new PopupMenuListSetData();
        mPopupMenuListSetData.setPopupMenuListDataList(getPopupMenuListDataList());
        mQuickSearchOptionMenu = new ListQuickPopupMenu(mPartFragment.getActivity(), R.layout.layout_popup_search_option);
        mPopupMenuListAdapter  = new PopupMenuListAdapter(mQuickSearchOptionMenu.getRootView(), new PopupMenuListAdapter.OnSelectedItemListener() {
            @Override
            public void onSelectedItem(int callbackId, PopupMenuListViewData listViewData) {
                switch (listViewData.getIdx()) {
                    case TYPE:
                        showFileTypeDialog();
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_VIEW, GAConst.EVENT_FILTER, GAConst.LABEL_TYPE);
                        break;
                    case SORT:
                        showSortOptDialog();
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_VIEW, GAConst.EVENT_FILTER, GAConst.LABEL_ORDER);
                        break;
                    case PERIOD:
                        showPeriodOptDialog();
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_VIEW, GAConst.EVENT_FILTER, GAConst.LABEL_DATE);
                        break;
                }
                mQuickSearchOptionMenu.dismiss();
            }
        }, R.id.layout_popup_search_option, R.id.list_popup_search_option, R.layout.adapter_popup_search_option, R.layout.layout_list_no, mPopupMenuListSetData);
        mQuickSearchOptionMenu.setListAdapter(mPopupMenuListAdapter);
    }

    protected String getFileTypeMenuString() {
        List<String> checkedList = (List<String>) mSearchOptIndex[TYPE];
        if (checkedList.size() == mKindValues.length) {
            return mContext.getString(R.string.s20_link_by_all_results);
        }
        return mContext.getString(R.string.s20_link_by_some_files);
    }

    protected String getPeriodTitle() {
        return mPeriodTitles[(int) mSearchOptIndex[PERIOD]];
    }

    protected String getSortTitle() {
        return mSortTitles[(int) mSearchOptIndex[SORT]];
    }

    protected String getSortValue() {
        return mSortValues[(int) mSearchOptIndex[SORT]];
    }

    protected String getPeriodStart() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        switch ((int) mSearchOptIndex[PERIOD]) {
            case 1:
                calendar.add(Calendar.DATE, -7);
                break;
            case 2:
                calendar.add(Calendar.MONTH, -1);
                break;
            case 3:
                calendar.add(Calendar.YEAR, -1);
                break;
            default:
                return "";
        }
        return getStringTime(calendar.getTime(), "yyyyMMdd");
    }

    protected String getPeriodEnd() {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        switch ((int) mSearchOptIndex[PERIOD]) {
            case 1:
            case 2:
            case 3:
                return getStringTime(calendar.getTime(), "yyyyMMdd");
            default:
                return "";
        }
    }

    private String getStringTime(Date attendDate, String dateFormat) {
        if (attendDate == null) {
            return "-";
        }
        FastDateFormat format = FastDateFormat.getInstance(dateFormat, TimeZone.getDefault(), Locale.getDefault());
        return format.format(attendDate);
    }

    protected void showPeriodOptDialog() {
        DialogUtils.showSelectedDialog(mContext.getString(R.string.s23_text_time_period), mPeriodTitles, mPeriodValues, (int) mSearchOptIndex[PERIOD], mPartFragment.getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    mSearchOptIndex[PERIOD] = dataMap.get(ListSelectedDialogFragment.DATA_INDEX);
                    onSearch(null);
                }
            }
        });
    }

    protected void showSortOptDialog() {
        DialogUtils.showSelectedDialog(mContext.getString(R.string.s22_text_sort_policy), mSortTitles, mSortValues, (int) mSearchOptIndex[SORT], mPartFragment.getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    mSearchOptIndex[SORT] = dataMap.get(ListSelectedDialogFragment.DATA_INDEX);
                    onSearch(null);
                }
            }
        });
    }

    protected void showFileTypeDialog() {
        List<String> selectFileType = (List<String>) mSearchOptIndex[TYPE];
        DialogUtils.showCheckListDialog(mContext.getString(R.string.s21_text_file_type), mKindTitles, mKindValues, selectFileType.toArray(new String[selectFileType.size()]), mPartFragment.getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    List<String> selectFileType = (List<String>) mSearchOptIndex[TYPE];
                    selectFileType.clear();
                    for (int key : dataMap.keySet()) {
                        selectFileType.add((String) dataMap.get(key));
                    }
                    onSearch(null);
                }
            }
        });
    }

    protected String getSearchType() {
        StringBuilder sb = new StringBuilder();
        List<String> selectFileType = (List<String>) mSearchOptIndex[TYPE];
        if (selectFileType.size() == mKindValues.length) {
            return "all";
        }
        for (String value : selectFileType) {
            sb.append(value);
            sb.append(" ");
        }
        return StringUtils.trim(StringUtils.replace(sb.toString().trim(), " ", "|"));
    }

    public String[] getTypeTitle(int kind) {
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                return mContext.getResources().getStringArray(R.array.kind_file_title_pc);
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
                return mContext.getResources().getStringArray(R.array.kind_file_title_mobile);
            case DeviceConst.KIND_WEBMAIL:
                return mContext.getResources().getStringArray(R.array.kind_file_title_webmail);
        }
        return null;
    }

    public String[] getTypeValue(int kind) {
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                return mContext.getResources().getStringArray(R.array.kind_file_value_pc);
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
                return mContext.getResources().getStringArray(R.array.kind_file_value_mobile);
            case DeviceConst.KIND_WEBMAIL:
                return mContext.getResources().getStringArray(R.array.kind_file_value_webmail);
        }
        return null;
    }
}

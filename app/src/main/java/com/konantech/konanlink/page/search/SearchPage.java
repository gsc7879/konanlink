package com.konantech.konanlink.page.search;

import android.content.Context;
import android.view.View;

import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.AnnotationUtils;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.adapter.SearchListAdapter;
import com.konantech.konanlink.part.SearchListPartFragment;

public abstract class SearchPage<T extends SearchListAdapter> {
    protected PartFragment mPartFragment;
    protected Context      mContext;
    protected DeviceInfo   mDeviceInfo;
    protected T            mSearchListAdapter;

    private View           mListLayout;

    public SearchPage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        mPartFragment = searchListPartFragment;
        mContext      = mainLayout.getContext();
        mDeviceInfo   = deviceInfo;
        mListLayout   = mainLayout;

        setAdapter(mainLayout, searchListPartFragment);
        AnnotationUtils.setClickEvent(mainLayout, this);
    }

    protected abstract void onSearch(String keyword);

    private void setAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mSearchListAdapter = createAdapter(mainLayout, searchListPartFragment);
        mSearchListAdapter.setDeviceInfo(mDeviceInfo);
    }

    public abstract void onDetachedFragment(Class detachedClass, Object... data);

    protected abstract T createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment);

    public View getPageLayout() {
        return mListLayout;
    }

    public DeviceInfo getDeviceInfo() {
        return mDeviceInfo;
    }
}

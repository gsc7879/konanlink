package com.konantech.konanlink.page.fragment;

import android.Manifest;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PageFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.PasswordDialogragment;
import com.konantech.konanlink.list.adapter.RecentKeywordListAdapter;
import com.konantech.konanlink.list.model.RecentKeywordListSetData;
import com.konantech.konanlink.part.DeviceAddPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;
import com.konantech.konanlink.utils.BadgeUtil;
import com.konantech.konanlink.utils.DialogUtils;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

@PageFragmentInfo(
        screenName  = GAConst.SCREEN_RECENT,
        layout      = R.layout.page_main,
        permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE} )
public class MainPageFragment extends PageFragment {
    private RecentKeywordListAdapter mRecentKeywordListAdapter;
    private String                   mKeyword;
    private RecentKeywordListSetData mRecentKeywordListSetData;

    @InjectView(id = R.id.layout_main_recent)
    private View mRecentListLayout;

    @InjectView(id = R.id.layout_main_info_add_device)
    private View mAddDeviceLayout;

    @InjectView(id = R.id.img_main_test_mode)
    private View mTestModeImg;

    @InjectView(id = R.id.text_popup_hint)
    private TextView mHintText;


    @InjectView(id = R.id.searchbox_main)
    private SearchBox mSearchBox;

    @Override
    public void onInitView(View view) {
        setToolbar();
        showHintPopup();
        showPasscode();
        setListLayout();
        setLayout();
        showNewNotiIcon();
        showNewNotiBadge();
    }

    private void setListLayout() {
        mRecentKeywordListSetData = new RecentKeywordListSetData();
        mRecentKeywordListAdapter = new RecentKeywordListAdapter(this, R.id.layout_main_recent, R.id.list_keyword, R.layout.adapter_keyword, R.layout.layout_list_no, mRecentKeywordListSetData);
        searchKeyword("");
    }

    private void setLayout() {
        boolean isAddedSearchAbleDevice = DataBank.getInstance().isAddedSearchAbleDevice();
        mRecentListLayout.setVisibility(isAddedSearchAbleDevice ? View.VISIBLE : View.GONE);
        mAddDeviceLayout.setVisibility(isAddedSearchAbleDevice ? View.GONE : View.VISIBLE);
        mTestModeImg.setVisibility(NetConst.getInstance().isTestMode() ? View.VISIBLE : View.GONE);
    }

    private void showHintPopup() {
        mHintText.setVisibility(DataBank.getInstance().isAddedSearchAbleDevice() && !DataBank.getInstance().isHideSearchHint() ? View.VISIBLE : View.GONE);
    }

    private  void showNewNotiIcon() {
        mSearchBox.setVisibleNewNotIcon(DataBank.getInstance().isHideNewNoti() ? View.GONE : View.VISIBLE);
    }

    private void showNewNotiBadge() {
        BadgeUtil.updateBadgeCount(getActivity(), DataBank.getInstance().isHideNewNoti() ? 0 : 1);
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(SearchListPartFragment.class)) {
            showHintPopup();
            initSearch();
        }
        setLayout();
    }

    @Override
    protected void onDataReceived(Class sendFragmentClass, int dataType, Object... data) {
        switch (dataType) {
            case DataIdConst.CHANGED_DEVICE_INFO:
                setLayout();
                showHintPopup();
                searchKeyword(mKeyword);
                break;
            case DataIdConst.CHANGED_BADGE_STATUS:
                showNewNotiIcon();
                showNewNotiBadge();
                break;
        }
    }

    private void showPasscode() {
        if (DataBank.getInstance().isUsePassword()) {
            DialogUtils.showPasswordLockDialog(
                    PasswordDialogragment.TYPE_CHECK,
                    false,
                    getActivity().getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        }
                    });

        }
    }

    private void initSearch() {
        mKeyword = "";
        mSearchBox.closeSearch();
        mSearchBox.setSearchString(mKeyword);
        mSearchBox.setResourceImageLogo(R.drawable.ic_konanlink_title);
        loadRecentKeyword(mKeyword);
    }


    private void setKeyword(String keyword) {
        if (DataBank.getInstance().getDeviceIndexingCategoryMap().size() == 0) {
            return;
        }

        mKeyword = keyword;

        if (StringUtils.isNotBlank(mKeyword)) {
            hintCheck();
            GAManager.getInstance().sendEvent(GAConst.CATEGORY_SEARCH, GAConst.EVENT_KEYWORD);
            if (getActiveFragmentClass() == null) {
                if (!DataBank.getInstance().isAddedSearchAbleDevice()) {
                    Snackbar.make(getView(), R.string.s13_text_no_device, Snackbar.LENGTH_SHORT).show();
                    setLayout();
                } else {
                    attachFragment(R.id.layout_main_recent, SearchListPartFragment.class, mKeyword);
                }
            } else {
                sendData(getActiveFragmentClass(), DataIdConst.REQ_KEYWORD, mKeyword);
            }
        }
    }

    private void hintCheck() {
        if (!DataBank.getInstance().isHideSearchHint()) {
            DataBank.getInstance().setHideSearchHint(true);
            showHintPopup();
        }
    }

    @Override
    protected void onSwitchedPage(Class prevPageClass, Object... etcData) {
        searchKeyword("");
    }

    private void setToolbar() {
        mSearchBox.setResourceImageLogo(R.drawable.ic_konanlink_title);

        mSearchBox.setMenuListener(new SearchBox.MenuListener(){

            @Override
            public void onMenuClick() {
            if (getActiveFragmentClass() == null) {
                    AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
                } else {
                    detachChildFragment();
                }
            }

        });

        mSearchBox.setSearchListener(new SearchBox.SearchListener(){

            @Override
            public void onSearchOpened() {
                //Use this to tint the screen
            }

            @Override
            public void onSearchClosed() {
                //Use this to un-tint the screen
                setKeyword(mKeyword);
            }

            @Override
            public void onSearchTermChanged(String keyword) {
                loadRecentKeyword(keyword);
            }

            @Override
            public void onSearch(String keyword) {
                searchKeyword(keyword);
                mKeyword = keyword;
                mRecentKeywordListAdapter.updateKeyword(keyword);
            }

            @Override
            public void onResultClick(SearchResult result){
            }


            @Override
            public void onSearchCleared() {
            }

        });
    }

    private void searchKeyword(String keyword) {
        if (DataBank.getInstance().isAddedSearchAbleDevice()) {
            loadRecentKeyword(keyword);
        }
    }

    private void loadRecentKeyword(String keyword) {
        mRecentKeywordListSetData.setRecentKeyword(keyword);
        mRecentKeywordListAdapter.loadList();
    }

    public void closeSearchView(String keyword) {
        mKeyword = keyword;
        mSearchBox.setKeepArrowBtn();
        mSearchBox.search(keyword);
    }

    @Override
    protected boolean onBackPressed() {
        if (mSearchBox.isFocused()) {
            initSearch();
            return true;
        }
        return false;
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (isAdded() && requestCode == SearchBox.VOICE_RECOGNITION_CODE && resultCode == getActivity().RESULT_OK) {
//            ArrayList<String> matches = data
//                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//            mSearchBox.populateEditText(String.valueOf(matches));
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Click(R.id.btn_add_device)
    public void addDevice() {
        attachFragment(R.id.layout_main_main, DeviceAddPartFragment.class);
    }

    @Click(R.id.btn_list_keyword_delete)
    public void deleteAllKeyword() {
        mRecentKeywordListAdapter.deleteAll();
    }
}

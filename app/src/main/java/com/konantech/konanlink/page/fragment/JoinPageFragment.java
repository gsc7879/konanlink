package com.konantech.konanlink.page.fragment;

import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.fragment.PageFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AccountApiManager;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.JoinResult;
import com.konantech.konanlink.api.model.system.Rules;
import com.konantech.konanlink.api.service.SystemApiService;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.LoginPartFragment;
import com.konantech.konanlink.part.WebFragment;
import com.konantech.konanlink.utils.Validator;

import org.apache.commons.lang3.StringUtils;

@PageFragmentInfo(screenName = GAConst.SCREEN_JOIN, layout = R.layout.page_join)
public class JoinPageFragment extends PageFragment {
    @InjectView(id = R.id.edit_join_email)
    private EditText mEmailText;

    @InjectView(id = R.id.edit_join_password)
    private EditText mPassText;

    @InjectView(id = R.id.edit_join_password_re)
    private EditText mRePassText;

    @InjectView(id = R.id.check_join_allow_use)
    private CheckBox mAllowUse;

    @InjectView(id = R.id.check_join_allow_info)
    private CheckBox mAllowInfo;

    @InjectView(id = R.id.text_join_email_error)
    private TextView mEmailErrorText;

    @InjectView(id = R.id.text_join_password_error)
    private TextView mPassErrorText;

    @InjectView(id = R.id.text_join_password_re_error)
    private TextView mRePassErrorText;

    @InjectView(id = R.id.img_join_email_check)
    private View     mEmailCheck;

    @InjectView(id = R.id.img_join_password_check)
    private View     mPasswordCheck;

    @InjectView(id = R.id.img_join_password_re_check)
    private View     mRePasswordCheck;

    @InjectView(id = R.id.toolbar_join)
    private Toolbar  mToolbar;

    @InjectView(id = R.id.btn_join_confirm)
    private View     mJoinBtn;

    @Override
    public void onInitView(View view) {
        setToolbar();
        setTextFilter();
        setTermCheck();
        setJoinBtn();
    }

    private void setTermCheck() {
        boolean isKorea = DataBank.getInstance().isKorea();
        mAllowInfo.setChecked(!isKorea);
        mAllowUse.setChecked(!isKorea);
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s09_title_signup);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchPrevPage();
            }
        });
    }

    @Override
    protected void onLeavePage(Class<? extends PageFragment> nextPageClass) {
        mEmailText.setText("");
        mPassText.setText("");
        mRePassText.setText("");

        mEmailText.requestFocus();

        mEmailCheck.setSelected(false);
        mPasswordCheck.setSelected(false);
        mRePasswordCheck.setSelected(false);

        mEmailErrorText.setText("");
        mPassErrorText.setText("");
        mRePassErrorText.setText("");

        setTermCheck();
        mJoinBtn.setEnabled(false);
    }

    private void setTextFilter() {
        mEmailText.addTextChangedListener(emailWatcher);
        mPassText.addTextChangedListener(passwordWatcher);
        mRePassText.addTextChangedListener(rePasswordWatcher);

        mEmailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && !mEmailCheck.isSelected()) {
                    if (StringUtils.isBlank(mEmailText.getText().toString())) {
                        setError(R.string.s99_error_typein_email, 0, 0);
                    } else {
                        setError(R.string.was_error_2005, 0, 0);
                    }
                }
            }
        });

        mPassText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && !mPasswordCheck.isSelected()) {
                    if (StringUtils.isBlank(mPassText.getText().toString())) {
                        setError(0, R.string.s99_error_typein_password, 0);
                    } else {
                        setError(0, R.string.was_error_2003, 0);
                    }
                }
            }
        });

        mRePassText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!mPasswordCheck.isSelected()) {
                        if (StringUtils.isBlank(mPassText.getText().toString())) {
                            setError(0, R.string.s99_error_typein_password, 0);
                        } else {
                            setError(0, R.string.was_error_2003, 0);
                        }
                    } else {
                        if (!mRePasswordCheck.isSelected()) {
                            if (StringUtils.isBlank(mPassText.getText().toString())) {
                                setError(0, 0,R.string.s99_error_typein_password);
                            } else {
                                setError(0, 0, R.string.s10_error_no_match_password);
                            }
                        }
                    }
                }
            }
        });
    }

    private boolean checkPasswordChar(char c) {
        if ((0x61 <= c && c <= 0x7A) || (0x41 <= c && c <= 0x5A) || (0x30 <= c && c <= 0x39) || (0x2D == c || 0x5F == c)) {
            return true;
        }
        return false;
    }

    private TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mEmailCheck.setSelected(Validator.email(s.toString()));
            setJoinBtn();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher passwordWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            for (int i = 0; i < s.length(); i++) {
                if (s.length() > 0 && !checkPasswordChar(s.charAt(i))) {
                    setError(0, R.string.was_error_2004, 0);
                    mPasswordCheck.setSelected(false);
                    setJoinBtn();
                    return;
                } else {
                    setError(0, 0, 0);
                }
            }
            mPasswordCheck.setSelected(s.length() >= 8 && s.length() <= 30);
            rePasswordCheck();
            setJoinBtn();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private TextWatcher rePasswordWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            rePasswordCheck();
            setJoinBtn();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };


    private void rePasswordCheck() {
        mRePasswordCheck.setSelected(mPasswordCheck.isSelected() && StringUtils.equals(mPassText.getText().toString(), mRePassText.getText().toString()));
    }

    private void setJoinBtn() {
        boolean isFillEditText = (mEmailCheck.isSelected() && mPasswordCheck.isSelected() && mRePasswordCheck.isSelected());
        if (isFillEditText) {
            setError(0,0,0);
        }
        mJoinBtn.setEnabled(isFillEditText && termsCheck());
    }

    @Click(R.id.btn_join_confirm)
    public void requestJoin() {
        if (inputFieldCheck() && termsCheck()) {
            final String password = mPassText.getText().toString();
            String email   = StringUtils.trim(mEmailText.getText().toString());
            String country = getResources().getConfiguration().locale.getCountry();
            String lang    = getResources().getConfiguration().locale.getLanguage();

            new AccountApiManager().reqJoin(getActivity(), mSubscriptions, email, password, country, lang, new Result.OnResultListener<JoinResult>() {

                @Override
                public void onSuccess(JoinResult result) {
                    attachFragment(R.id.layout_join_main, LoginPartFragment.class, result.getEmail(), password);
                }

                @Override
                public void onFailed(int code, String msg) {
                    switch (code) {
                        case ErrorConst.ERR_1001_ALREADY_REGISTERED_EMAIL:
                            setError(R.string.was_error_1001, 0, 0);
                            break;
                        default:
                            Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
                            break;
                    }
                }
            });
        }
    }

    @Click(R.id.btn_join_allow_use)
    public void allowUse() {
        showRules(SystemApiService.RULES_TYPE_SERVICE, R.string.s39_title_terms, R.string.s10_error_no_terms);
    }

    @Click(R.id.btn_join_allow_info)
    public void allowInfo() {
        showRules(SystemApiService.RULES_TYPE_PRIVACY, R.string.s39_title_privacy, R.string.s10_error_no_privacy);
    }

    @Click(R.id.check_join_allow_use)
    public void checkJoinAllowUseBtn() {
        setJoinBtn();
    }

    @Click(R.id.check_join_allow_info)
    public void checkJoinAllowInfoBtn() {
        setJoinBtn();
    }

    private void showRules(String type, final int titleResId, final int errorResId) {
        new SystemApiManager().reqRules(getActivity(), mSubscriptions, type, new Result.OnResultListener<Rules>() {
            @Override
            public void onSuccess(Rules data) {
                attachFragment(R.id.layout_join_main, WebFragment.class, data.getUrl(), getString(titleResId));
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), errorResId, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private boolean termsCheck() {
        if (!mAllowUse.isChecked()){
            showSnackbar(R.string.s10_toast_agree_terms);
            return false;
        }
        if (!mAllowInfo.isChecked()) {
            showSnackbar(R.string.s10_toast_agree_privacy);
            return false;
        }
        return true;
    }

    private boolean inputFieldCheck() {
        if (!mEmailCheck.isSelected()) {
            setError(R.string.s99_error_typein_email, 0, 0);
            return false;
        }

        if (!mPasswordCheck.isSelected()) {
            setError(0, R.string.s99_error_typein_password, 0);
            return false;
        }

        if (!mRePasswordCheck.isSelected()) {
            setError(0, 0, R.string.s10_error_confirm_password);
            return false;
        }

        setError(0, 0, 0);
        return true;
    }

    private void setError(int emailError, int passError, int rePassError) {
        if (emailError == 0) {
            mEmailErrorText.setText("");
        } else {
            mEmailErrorText.setText(emailError);
        }

        if (passError == 0) {
            mPassErrorText.setText("");
        } else {
            mPassErrorText.setText(passError);
        }

        if (rePassError == 0) {
            mRePassErrorText.setText("");
        } else {
            mRePassErrorText.setText(rePassError);
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(LoginPartFragment.class)) {
            if (data.length > 1) {
                switch ((int) data[0]) {
                    case ErrorConst.ERR_1002_NOT_EXIST_EMAIL:
                    case ErrorConst.ERR_1003_NOT_EQUALS_PASSWORD:
                        switchPage(LoginPageFragment.class);
                        break;
                    default:
                        Snackbar.make(getView(), (String) data[1], Snackbar.LENGTH_LONG).show();
                        switchPage(LoginPageFragment.class);
                        break;
                }
            } else {
                switchPage(RegisterPageFragment.class);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        switchPrevPage();
        return true;
    }
    
    private void showSnackbar(@StringRes int textResId) {
        Snackbar.make(getView(), textResId, Snackbar.LENGTH_SHORT).show();
    }
}

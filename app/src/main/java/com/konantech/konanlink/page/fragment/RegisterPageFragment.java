package com.konantech.konanlink.page.fragment;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.fragment.PageFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.device.AddDeviceResult;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.LoadingPartFragment;
import com.konantech.konanlink.part.UpdateInfoPartFragment;
import com.konantech.konanlink.service.IndexingManager;
import com.konantech.konanlink.utils.IntentChangeUtils;

@PageFragmentInfo(layout = R.layout.page_register, isAddBackStack = false)
public class RegisterPageFragment extends PageFragment {
    private static final int PAGE_ADD    = 0;
    private static final int PAGE_DELETE = 1;

    @InjectView(ids = {R.id.layout_register_add, R.id.layout_register_delete})
    private View[] mPageViews;

    @InjectView(id = R.id.text_register_status)
    private TextView mStatusText;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onSwitchedPage(Class prevPageClass, Object... etcData) {
    }

    private void checkAddedDevice() {
        if (DataBank.getInstance().isAddedDevice()) {
            IntentChangeUtils.goMainPage(getActivity());
        } else {
            registerDevice();
        }
    }

    private void setPage(int page) {
        for (int i = 0; i < mPageViews.length; i++) {
            mPageViews[i].setVisibility(page == i ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(UpdateInfoPartFragment.class)) {
            registerDeviceToHost();
        } else if (detachedClass.equals(LoadingPartFragment.class)) {
            if (data.length > 0) {
                switch ((LoadingPartFragment.LoadingError) data[0]) {
                    case NOT_REGISTERED_DEVICE:
                        break;
                    case NOT_EXIST_KEY:
                        switchPage(LoginPageFragment.class);
                        break;
                    case INIT_DEVICE:
                        switchPage(IntroPageFragment.class);
                        break;
                }
            } else {
                IndexingManager.getInstance().updateIndex(getActivity(), mSubscriptions, getView(), true, DataBank.getInstance().getDeviceInfo().getHostInfo().getKey());
                IntentChangeUtils.goMainPage(getActivity());
            }
        }
    }

    @Click(R.id.btn_register_deleted)
    public void registerDevice() {
        attachFragment(R.id.layout_register_main, UpdateInfoPartFragment.class, UpdateInfoPartFragment.TYPE_DEVICE_INFO, mStatusText);
    }

    private void registerDeviceToHost() {
        setPage(PAGE_ADD);
        regAddDevice();
    }

    private void regAddDevice() {
        String deviceKey  = DataBank.getInstance().getDeviceKey();
        String deviceName = (String) DataBank.INFO.get(DataBank.DEVICE_NAME);
        int deviceKind    = DeviceConst.KIND_ANDROID;

        new DeviceApiManager().reqAddDevice(getActivity(), mSubscriptions, deviceKey, deviceName, deviceKind, "", "", "", IndexingConst.STATUS_NOT, new Result.OnResultListener<AddDeviceResult>() {
            @Override
            public void onSuccess(AddDeviceResult data) {
                regSuccess();
            }

            @Override
            public void onFailed(int code, String msg) {
                if (code == ErrorConst.ERR_3001_ALREADY_REGISTERED_DEVICE) {
                    regSuccess();
                } else if (code == ErrorConst.ERR_3005_LIMIT_REGISTERED_DEVICE) {
                    if (mPageViews[PAGE_DELETE].getVisibility() == View.VISIBLE) {
                        Snackbar.make(getView(), getString(R.string.s12_error_already_2phones), Snackbar.LENGTH_LONG).show();
                    } else {
                        setPage(PAGE_DELETE);
                        GAManager.getInstance().sendScreen(GAConst.SCREEN_LIMIT);
                    }
                } else {
                    Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void regSuccess() {
        attachFragment(R.id.layout_register_main, LoadingPartFragment.class, mStatusText);
    }

    @Override
    public void onResume() {
        super.onResume();
        checkAddedDevice();
    }

    @Override
    protected boolean onBackPressed() {
        return true;
    }
}
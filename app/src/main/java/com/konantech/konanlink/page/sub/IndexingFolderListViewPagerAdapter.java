package com.konantech.konanlink.page.sub;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.dignara.lib.utils.StorageUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.IndexingFolderFragment;

import org.apache.commons.io.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class IndexingFolderListViewPagerAdapter extends PagerAdapter {
    private List<IndexingStoragePage> mIndexingStoragePageList;
    private ViewPager                 mViewPager;
    private Context                   mContext;

    public IndexingFolderListViewPagerAdapter(IndexingFolderFragment indexingFolderFragment, ViewPager viewPager) {
        mIndexingStoragePageList = new ArrayList<>();
        mViewPager               = viewPager;
        mContext                 = indexingFolderFragment.getActivity();
        addSearchDevicePage(indexingFolderFragment);
        StorageUtils.setName(mContext.getString(R.string.s34_text_builtin_memory), mContext.getString(R.string.s34_text_external_memory_01));
    }

    private void addSearchDevicePage(IndexingFolderFragment indexingFolderFragment) {
        List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();
        for (StorageUtils.StorageInfo storageInfo : storageInfoList) {
            mIndexingStoragePageList.add(new IndexingStoragePage(indexingFolderFragment.getActivity().getLayoutInflater().inflate(R.layout.layout_list_indexing_folder, null), FileUtils.getFile(storageInfo.path)));
        }
    }

    @Override
    public Object instantiateItem(ViewGroup pager, int position) {
        View view = mIndexingStoragePageList.get(position).getPageLayout();
        pager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (getCount() > 2) {
            return position > 0 ? String.format(mContext.getString(R.string.s34_text_external_memory_01), position) : mContext.getString(R.string.s34_text_builtin_memory);
        } else {
            return position > 0 ? mContext.getString(R.string.s34_text_external_memory_00) : mContext.getString(R.string.s34_text_builtin_memory);
        }
    }

    @Override
    public int getCount() {
        return mIndexingStoragePageList.size();
    }

    public void loadList(List<String> indexingFolderList) {
        mIndexingStoragePageList.get(mViewPager.getCurrentItem()).loadList(indexingFolderList);
    }

    public void saveCheckedFolderList() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < getCount(); i++) {
            for (String indexingFolderPath : mIndexingStoragePageList.get(i).getCheckedFolderList()) {
                sb.append(indexingFolderPath);
                sb.append(",");
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        DataBank.getInstance().saveIndexingFolders(sb.toString());
    }
}

package com.konantech.konanlink.page.sub;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.list.adapter.IndexingFolderListAdapter;
import com.konantech.konanlink.list.model.IndexingFolderListSetData;

import java.io.File;
import java.util.List;

public class IndexingStoragePage {
    private View                      mListLayout;
    private IndexingFolderListAdapter mIndexingFolderListAdapter;
    private IndexingFolderListSetData mIndexingFolderListSetData;

    public IndexingStoragePage(View mainLayout, File storage) {
        mListLayout = mainLayout;
        setAdapter(mainLayout, storage);
    }

    private void setAdapter(View mainLayout, File storage) {
        mIndexingFolderListSetData = new IndexingFolderListSetData();
        mIndexingFolderListAdapter = new IndexingFolderListAdapter(storage, mainLayout, R.id.layout_list_indexing_folder, R.id.list_indexing_folder, R.layout.adapter_indexing_folder, R.layout.layout_list_no, mIndexingFolderListSetData);
        mIndexingFolderListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_indexing_folder_reload), new int[]{R.color.konan});
    }

    public View getPageLayout() {
        return mListLayout;
    }

    public void loadList(List<String> indexingFolderList) {
        mIndexingFolderListSetData.setFolderList(indexingFolderList);
        mIndexingFolderListAdapter.loadList();
    }

    public List<String> getCheckedFolderList() {
        return mIndexingFolderListAdapter.getCheckedFolderList();
    }
}

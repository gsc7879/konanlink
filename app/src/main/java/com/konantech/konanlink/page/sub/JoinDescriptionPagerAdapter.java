package com.konantech.konanlink.page.sub;

import android.support.v4.view.PagerAdapter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.konantech.konanlink.R;

public class JoinDescriptionPagerAdapter extends PagerAdapter {
    private String[] mMsgArray;

    public JoinDescriptionPagerAdapter(String[] msgArray) {
        mMsgArray = msgArray;
    }

    @Override
    public Object instantiateItem(ViewGroup pager, int position) {
        TextView view = new TextView(pager.getContext());
        view.setGravity(Gravity.CENTER);
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        view.setTextColor(R.color.gray_3);
        view.setText(mMsgArray[position]);
        pager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public int getCount() {
        return mMsgArray.length;
    }
}

package com.konantech.konanlink.page.search;

import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.SearchListPartFragment;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class SearchListViewPagerAdapter extends PagerAdapter {
    private List<SearchPage> mSearchPageList;
    private Map<String, SearchPage> mDeviceSearchPage;
    private ViewPager        mViewPager;
    private TabLayout     mTabPagerLayout;

    public SearchListViewPagerAdapter(SearchListPartFragment searchListPartFragment, ViewPager viewPager, TabLayout tabPagerLayout) {
        mSearchPageList     = new ArrayList<>();
        mDeviceSearchPage   = new Hashtable<>();
        mViewPager          = viewPager;
        mTabPagerLayout     = tabPagerLayout;
        addSearchDevicePage(searchListPartFragment);
    }

    private void addSearchDevicePage(SearchListPartFragment searchListPartFragment) {
        Map<Integer, List<DeviceInfo>> categoryMap = DataBank.getInstance().getDeviceIndexingCategoryMap();
        LayoutInflater layoutInflater = searchListPartFragment.getActivity().getLayoutInflater();
        mTabPagerLayout.removeAllTabs();

        for (int categoryIdx : DeviceConst.CATEGORY_ARRAY) {
            if (categoryMap.containsKey(categoryIdx)) {
                for (DeviceInfo deviceInfo : categoryMap.get(categoryIdx)) {
                    if (deviceInfo.isSearchAble()) {
                        if ((deviceInfo.getCategory() == DeviceConst.CATEGORY_MOBILE) && (!deviceInfo.isMyDevice())) {
                            continue;
                        }
                        SearchPage searchPage = createSearchPage(layoutInflater, searchListPartFragment, deviceInfo);
                        mSearchPageList.add(searchPage);
                        mDeviceSearchPage.put(deviceInfo.getKey(), searchPage);
                        addPagerLayoutText(deviceInfo.getName());
                    }
                }
            }
        }

        mTabPagerLayout.post(new Runnable() {
            @Override
            public void run() {
                if (mTabPagerLayout.getWidth() < mTabPagerLayout.getResources().getDisplayMetrics().widthPixels) {
                    mTabPagerLayout.setTabMode(TabLayout.MODE_FIXED);
                    ViewGroup.LayoutParams mParams = mTabPagerLayout.getLayoutParams();
                    mParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    mTabPagerLayout.setLayoutParams(mParams);

                } else {
                    mTabPagerLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }
            }
        });
        mTabPagerLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void addPagerLayoutText(String deviceName) {
        mTabPagerLayout.addTab(mTabPagerLayout.newTab().setText(deviceName));
    }

    private SearchPage createSearchPage(LayoutInflater layoutInflater, SearchListPartFragment searchListPartFragment, DeviceInfo deviceInfo) {
        switch (deviceInfo.getKind()) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_WEBMAIL:
            case DeviceConst.KIND_FILESERVER:
                return new SearchAgentPage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_kms, null), searchListPartFragment);
            case DeviceConst.KIND_DROPBOX:
                return new SearchDropBoxPage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_dropbox, null), searchListPartFragment);
            case DeviceConst.KIND_GOOGLEDRIVE:
                return new SearchGoogleDrivePage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_googledrive, null), searchListPartFragment);
            case DeviceConst.KIND_EVERNOTE:
                return new SearchEvernotePage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_evernote, null), searchListPartFragment);
            case DeviceConst.KIND_ONEDRIVE:
                return new SearchOneDrivePage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_onedrive, null), searchListPartFragment);
            case DeviceConst.KIND_GMAIL:
                return new SearchGmailPage(deviceInfo, layoutInflater.inflate(R.layout.layout_list_search_gmail, null), searchListPartFragment);
        }
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup pager, final int position) {
        View view = mSearchPageList.get(position).getPageLayout();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(position, true);
            }
        });
        pager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mSearchPageList.get(position).getDeviceInfo().getName();
    }

    @Override
    public int getCount() {
        return mSearchPageList.size();
    }

    public void search(String keyword, int index) {
        if (mSearchPageList.size() > index) {
            mSearchPageList.get(index).onSearch(keyword);
        }
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (data.length > 0 && !mDeviceSearchPage.isEmpty()) {
            mDeviceSearchPage.get(data[0]).onDetachedFragment(detachedClass, data);
        }
    }
}

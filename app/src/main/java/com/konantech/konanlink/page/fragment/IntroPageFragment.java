package com.konantech.konanlink.page.fragment;

import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PageFragmentInfo;
import com.dignara.lib.fragment.PageFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.page.sub.IntroPagerAdapter;

@PageFragmentInfo(layout = R.layout.page_intro)
public class IntroPageFragment extends PageFragment {
    private IntroPagerAdapter mIntroPagerAdapter;

    @InjectView(id = R.id.pager_intro_description)
    private ViewPager mViewPager;

    @InjectView(id = R.id.layout_intro_indicator)
    private LinearLayout mIndicatorLayout;

    @InjectView(id = R.id.layout_intro_indicator_main)
    private LinearLayout mIndicatorMainLayout;

    @InjectView(id = R.id.btn_intro_prev)
    private View mPrev;

    @InjectView(id = R.id.btn_intro_next)
    private View mNext;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onSwitchedPage(Class prevPageClass, Object... etcData) {
        setPagerLayout(etcData.length > 0 && (boolean) etcData[0]);
    }

    private void setIndicatorLayout(int count) {
        if (mIndicatorLayout.getChildCount() != count) {
            mIndicatorLayout.removeAllViewsInLayout();
            if (count < 2) {
                mIndicatorMainLayout.setVisibility(View.GONE);
            } else {
                mIndicatorMainLayout.setVisibility(View.VISIBLE);
                for (int i = 0; i < count; i++) {
                    ImageView indicatorView = new ImageView(getActivity());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.gravity = Gravity.CENTER_VERTICAL;
                    mIndicatorLayout.addView(indicatorView, layoutParams);
                }
            }
        }
    }

    private void setPagerLayout(boolean isGift) {
        mIntroPagerAdapter = new IntroPagerAdapter(this, isGift);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int index, float v, int i2) {
            }

            @Override
            public void onPageSelected(int index) {
                setIndicator(index);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        mViewPager.setAdapter(mIntroPagerAdapter);
        setIndicatorLayout(mIntroPagerAdapter.getCount());
        setIndicator(0);
    }

    private void setIndicator(int index) {
        for (int i = 0; i < mIndicatorLayout.getChildCount(); i++) {
            ((ImageView) mIndicatorLayout.getChildAt(i)).setImageResource(index == i ? R.drawable.ic_pgindi_sel : R.drawable.ic_pgindi_nor);
        }
        int totalIndex = mIntroPagerAdapter.getCount();
        mPrev.setVisibility(index == 0 ? View.INVISIBLE : View.VISIBLE);
        mNext.setVisibility(index < totalIndex - 1 ? View.VISIBLE : View.INVISIBLE);
        GAManager.getInstance().sendScreen(mIntroPagerAdapter.getScreenName(index));
    }

    private void setPage(int gap) {
        int nextPage  = gap == 0 ? 0 : mViewPager.getCurrentItem() + gap;
        mViewPager.setCurrentItem(nextPage, true);
    }

    @Click(R.id.btn_intro_prev)
    public void introPrev() {
        setPage(-1);
    }

    @Click(R.id.btn_intro_next)
    public void introNext() {
        setPage(1);
    }
}
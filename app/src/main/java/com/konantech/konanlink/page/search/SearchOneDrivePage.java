package com.konantech.konanlink.page.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.list.adapter.OneDriveSearchListAdapter;
import com.konantech.konanlink.list.model.OneDriveSearchListSetData;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.OneDriveSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;

import org.apache.commons.lang3.StringUtils;

public class SearchOneDrivePage extends SearchPage<OneDriveSearchListAdapter> {
    private OneDriveSearchListSetData mOneDriveSearchListSetData;

    public SearchOneDrivePage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
    }

    @Override
    protected OneDriveSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mOneDriveSearchListSetData = new OneDriveSearchListSetData();
        OneDriveSearchListAdapter searchListAdapter = new OneDriveSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_onedrive, R.id.list_search_onedrive, R.layout.adapter_search_onedrive, R.layout.layout_list_no, mOneDriveSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_onedrive_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(OneDriveSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResult<OneDriveFile.DriveFile>) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    @Override
    protected void onSearch(String keyword) {
        if (StringUtils.equals(keyword, mOneDriveSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0) {
            return;
        }

        mOneDriveSearchListSetData.setKeyword(keyword);
        mSearchListAdapter.loadList();
    }
}

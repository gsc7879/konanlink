package com.konantech.konanlink.page.search;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.adapter.EvernoteSearchListAdapter;
import com.konantech.konanlink.list.model.EvernoteSearchListSetData;
import com.konantech.konanlink.model.search.EvernoteSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.EvernoteSearchPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;

import org.apache.commons.lang3.StringUtils;

public class SearchEvernotePage extends SearchPage<EvernoteSearchListAdapter> {
    private EvernoteSearchListSetData mEvernoteSearchListSetData;

    public SearchEvernotePage(DeviceInfo deviceInfo, View mainLayout, SearchListPartFragment searchListPartFragment) {
        super(deviceInfo, mainLayout, searchListPartFragment);
    }

    @Override
    protected EvernoteSearchListAdapter createAdapter(View mainLayout, SearchListPartFragment searchListPartFragment) {
        mEvernoteSearchListSetData = new EvernoteSearchListSetData();
        EvernoteSearchListAdapter searchListAdapter = new EvernoteSearchListAdapter(searchListPartFragment, mainLayout, R.id.layout_list_search_evernote, R.id.list_search_evernote, R.layout.adapter_search_evernote, R.layout.layout_list_no, mEvernoteSearchListSetData);
        searchListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) mainLayout.findViewById(R.id.layout_list_search_evernote_reload), new int[]{R.color.konan});
        return searchListAdapter;
    }

    public void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(EvernoteSearchPartFragment.class)) {
            mSearchListAdapter.endLoadList(data.length < 2 ? null : (SearchResult<EvernoteSearchInfo>) data[1], data.length < 3 ? -1 : (int) data[2]);
        }
    }

    @Override
    protected void onSearch(String keyword) {
        if (StringUtils.equals(keyword, mEvernoteSearchListSetData.getKeyword()) && mSearchListAdapter.getItemCount() != 0) {
            return;
        }

        mEvernoteSearchListSetData.setKeyword(keyword);
        mSearchListAdapter.loadList();
    }
}

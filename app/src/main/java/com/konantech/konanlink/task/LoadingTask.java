package com.konantech.konanlink.task;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.StringUtils;

public abstract class LoadingTask<T> extends WorkTask<T> {
    protected Context        mContext;

    private DialogController mDialogController;
    private String           mTitle;

    public LoadingTask(int callBackId, Context context, int titleResId, OnTaskListener taskListener) {
        super(callBackId, taskListener);
        mContext = context;
        mTitle   = titleResId > 0 ? mContext.getString(titleResId) : null;
    }

    @Override
    protected void onPreExecute() {
        if (StringUtils.isNotBlank(mTitle)) {
            mDialogController = DialogUtils.showLoadingDialog(mTitle, ((FragmentActivity) mContext).getFragmentManager());
        }
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(T object) {
        super.onPostExecute(object);
        closeDialog();
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
        closeDialog();
    }

    private void closeDialog() {
        if (mDialogController != null) {
            mDialogController.dismiss();
        }
    }
}

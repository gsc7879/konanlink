package com.konantech.konanlink.task.info;

import android.content.Context;

import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.ExifUtils;
import com.drew.imaging.ImageProcessingException;
import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.agent.request.MetaIndexRequest;
import com.konantech.konanlink.api.model.agent.response.MetaIndexResponse;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.api.service.AgentApiService;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.db.manager.FileDBManager;
import com.konantech.konanlink.db.manager.FileSelectDBInfo;
import com.konantech.konanlink.utils.PhotoUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import rx.subscriptions.CompositeSubscription;

public class ReqSendMetaTask extends WorkTask {
    public static final int MODIFIED_FILE_COUNT = 0;
    public static final int DELETED_FILE_COUNT  = 1;

    private static final int LIMIT = 500;

    private CompositeSubscription mSubscription;
    private FileDBManager         mFileDBManager;
    private int[]                 mFileCount;
    private DeviceInfo            mDeviceInfo;

    public ReqSendMetaTask(int callBackId, OnTaskListener taskListener) {
        super(callBackId, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Context context      = (Context)params[0];
        mSubscription        = (CompositeSubscription)params[1];
        mDeviceInfo          = DataBank.getInstance().getDeviceInfo((String) params[2]);
        boolean isRecreate   = (boolean)params[3];


        mFileCount     = new int[2];
        mFileDBManager = new FileDBManager(context);
        if (isRecreate) {
            mFileDBManager.initIndexingDB();
        }

        try {
            insertFileInfo(DataBank.getInstance().getIndexingFolders(false));
            sendModifiedMeta(context, isRecreate);
            if (mFileCount[MODIFIED_FILE_COUNT] > 0 || mFileCount[DELETED_FILE_COUNT] > 0) {
                mFileDBManager.initIndexingDB();
                mFileDBManager.copyCompareDB();
            }
            mFileDBManager = null;
        } catch (Exception ignored) {
            cancel(true);
        }
        return mFileCount;
    }

    private void sendModifiedMeta(Context context, boolean isRecreate) throws IOException {
        FileSelectDBInfo fileDBInfo;
        int offset = 0;
        do {
            fileDBInfo = mFileDBManager.selectModifiedFileList(offset, LIMIT);
            List<File> modifiedFileList = fileDBInfo.getFileList();
            sendChangedFileInfo(context, modifiedFileList, "add", isRecreate);
            mFileCount[MODIFIED_FILE_COUNT] += modifiedFileList.size();
            offset = fileDBInfo.getOffset();
            isRecreate = false;
        } while (fileDBInfo.isNext());

        offset = 0;
        do {
            fileDBInfo = mFileDBManager.selectDeletedFileList(offset, LIMIT);
            List<File> deletedFileList = fileDBInfo.getFileList();
            sendChangedFileInfo(context, deletedFileList, "delete", false);
            mFileCount[DELETED_FILE_COUNT] += deletedFileList.size();
            offset = fileDBInfo.getOffset();
        } while (fileDBInfo.isNext());
    }

    private void sendChangedFileInfo(Context context,  List<File> fileList, String type, boolean isRecreate) throws IOException {

        if (fileList.size() != 0) {
            MetaIndexRequest metaRequest = new MetaIndexRequest();
            metaRequest.setDeviceKey(DataBank.getInstance().getDeviceKey());
            metaRequest.setRecreate(isRecreate);
            for (File file : fileList) {
                MetaIndexRequest.Item item = new MetaIndexRequest.Item();
                item.setTitle(file.getName());
                item.setStatus(type);
                item.setPath(file.getAbsolutePath());
                item.setSize(file.length());
                item.setSearchKeyword(PhotoUtil.getAddress(context, file, Locale.getDefault())); //TODO: 이미지인경우 추후 메타 정보로..
                item.setCreatedTime(file.lastModified());
                item.setModifiedTime(file.lastModified());
                String[] array = getResolution(file).split("x");
                item.setWidth(array.length > 1 ? Integer.valueOf(array[0]) : 0);
                item.setHeight(array.length > 1 ? Integer.valueOf(array[1]) : 0);
                array = getCoordinates(file).split(",");
                item.setLatitude(array.length > 1 ? Double.valueOf(array[0]) : 0.0);
                item.setLongitude(array.length > 1 ? Double.valueOf(array[1]) : 0.0);
                metaRequest.getItems().add(item);
            }

            AgentApiManager agentManager = (AgentApiManager) mDeviceInfo.getConnector();
            if (agentManager != null) {
                String url = NetConst.getInstance().getLinkUrl() + String.format(AgentApiService.URL_MOBILE_META_INFO, mDeviceInfo.getHostInfo().getKey());
                agentManager.reqFileIndex(context, mSubscription, url, metaRequest, new Result.OnResultListener<MetaIndexResponse>() {
                    @Override
                    public void onSuccess(MetaIndexResponse data) {

                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        mCancelCode = code;
                        mData = msg;
                        cancel(true);
                    }
                });

            } else throw new IOException();
        }
    }

    private void insertFileInfo(List<String> searchFolderList) {
        mFileDBManager.initCompareDB();
        for (String folder : searchFolderList) {
            createFolderInfo(FileUtils.getFile(folder));
        }
    }

    private void createFolderInfo(File folder) {
        if (!folder.exists() || !folder.isDirectory() || folder.getName().startsWith(".")) {
            return;
        }

        for (File file : folder.listFiles()) {
            if (file.getName().startsWith(".")) {
                continue;
            }

            if (file.isDirectory()) {
                createFolderInfo(file);
                continue;
            }
            mFileDBManager.insertFileInfo(file.getAbsolutePath(), file.lastModified());
        }
    }

    private String getResolution(File file) {
        int[] imageSize = ExifUtils.getImageSize(file);
        if (imageSize == null) {
            return "";
        }
        return String.format("%dx%d", imageSize[0], imageSize[1]);
    }

    private String getCoordinates(File file) {
        if (FilenameUtils.isExtension(file.getName(), new String[]{"jpg", "jpeg", "JPG", "JPEG"})) {
            try {
                return ExifUtils.getGeoLocation(file);
            } catch (IOException | ImageProcessingException e) {
            }
        }
        return "";
    }
}

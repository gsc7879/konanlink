package com.konantech.konanlink.task.management;

import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.constant.FileConst;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class CleanSystemTask extends WorkTask {
    private static final File[] CLEAR_FOLDERS = {FileConst.FOLDER_CACHE, FileConst.FOLDER_TEMP, FileConst.FOLDER_INDEXING};

    public CleanSystemTask(int callBackId, OnTaskListener taskListener) {
        super(callBackId, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        cleanFolders();
        return null;
    }

    private void cleanFolders() {
        for (File clearFolder : CLEAR_FOLDERS) {
            try {
                FileUtils.deleteDirectory(clearFolder);
            } catch (IOException e) {
                continue;
            }
        }
    }
}

package com.konantech.konanlink.task.info;

import android.content.Context;
import android.os.Environment;

import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.dao.PhotoMetaInfo;
import com.konantech.konanlink.db.PhotoMetaDBManager;
import com.konantech.konanlink.utils.PhotoUtil;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class ReqSendPhotoMetaTask extends WorkTask {

    private PhotoMetaDBManager mPhotoMetaDBManager;

    public ReqSendPhotoMetaTask(Context context, int callBackId) {
        super(callBackId);
        mPhotoMetaDBManager = PhotoMetaDBManager.getInstance();
        mPhotoMetaDBManager.setFilesMetaDao(context);
    }

    public ReqSendPhotoMetaTask(int callBackId, OnTaskListener taskListener) {
        super(callBackId, taskListener);
    }

    @Override
    protected Object doInBackground(Object... params) {
        Context context     = (Context) params[0];
        boolean isRecreate  = (boolean) params[1];

        mPhotoMetaDBManager = PhotoMetaDBManager.getInstance();
        mPhotoMetaDBManager.setFilesMetaDao(context);

        if (isRecreate) {
            mPhotoMetaDBManager.initDataBase();
        }

        try {
            // DCIM
            insertFileInfo(context, Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath());
        } catch (Exception ignored) {
            cancel(true);
        }
        return 0;
    }

    private void insertFileInfo(Context context, String searchFolder) {
        createFolderInfo(context, FileUtils.getFile(searchFolder));
    }

    private void createFolderInfo(final Context context, File folder) {
        if (!folder.exists() || !folder.isDirectory() || folder.getName().startsWith(".")) {
            return;
        }


        for (final File file : folder.listFiles()) {
            try {
                if (!file.getName().startsWith(".")) {
                    if (file.isDirectory()) {
                        createFolderInfo(context, file);
                        continue;
                    }

                    if (PhotoUtil.isImageFile(file) && isInsertFileToDataBase(file)) {
                        insertOrReplacePhotoMetaInfo(context, file);
                    }
                 }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    public void insertOrReplacePhotoMetaInfo(Context context, File file) {
        String fileName = file.getName();
        String absPath = file.getAbsolutePath();
        String address = PhotoUtil.getAddress(context, file, Locale.getDefault());
        String addressEn = PhotoUtil.getAddress(context, file, Locale.ENGLISH);
        String recordDate = PhotoUtil.getDate(file);
        String jodaRecordDate = "";
        if (recordDate.length() != 0) {
            DateTime dateTime = DateTimeFormat.forPattern("yyyy:MM:dd HH:mm:ss").parseDateTime(recordDate);
            jodaRecordDate = DateTimeFormat.forPattern("yyyy MMMM dd").withLocale(Locale.getDefault()).print(dateTime);
        }

        String metaInfo = PhotoUtil.getMetaInfoFromCloudVision(file);
        String keyWord = new StringBuilder()
                .append(address)
                .append(" ")
                .append(addressEn)
                .append(" ")
                .append(jodaRecordDate)
                .append(" ")
                .append(metaInfo)
                .toString();

        mPhotoMetaDBManager.insertOrReplace(createPhotoMetaInfo(fileName, absPath, keyWord, address, addressEn, metaInfo, recordDate, file.lastModified()));
    }

    public void deletePhotoMetaInfo (File file) {
        mPhotoMetaDBManager.delete(file);
    }

    public PhotoMetaDBManager.SearchResult searchPhotoMetaInfoList(String keywords, int offset, int count) {
        return mPhotoMetaDBManager.searchList(keywords, offset, count);
    }

    public boolean isFileExist(File file) {
        return mPhotoMetaDBManager.existFile(file);
    }

    private boolean isInsertFileToDataBase (File file){
        return mPhotoMetaDBManager.isInsertDataBase(file);
    }

    private PhotoMetaInfo createPhotoMetaInfo(String fileName, String fileAbsPath, String keyword, String address, String addressEn, String metaInfo, String recordDate, long modifiedDate) {
        PhotoMetaInfo fileMetaInfo = new PhotoMetaInfo();
        fileMetaInfo.setFileAbsPath(fileAbsPath); // key
        fileMetaInfo.setFileName(fileName);
        fileMetaInfo.setKeyWord(keyword);
        fileMetaInfo.setAddress(address);
        fileMetaInfo.setAddressEN(addressEn);
        fileMetaInfo.setMetaInfo(metaInfo);
        fileMetaInfo.setRecordDate(recordDate);
        fileMetaInfo.setModifiedDate(modifiedDate);
        return fileMetaInfo;
    }
}

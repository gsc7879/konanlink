package com.konantech.konanlink.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.ImageUtils;

import org.apache.commons.io.FileUtils;

import java.io.File;

public class ResizeTask extends WorkTask {
    private File mTempFolder;
    private int  mWidth;
    private int  mHeight;

    public ResizeTask(int callBackId, File tempFolder, int width, int height, OnTaskListener taskListener) {
        super(callBackId, taskListener);
        mTempFolder = tempFolder;
        mWidth      = width;
        mHeight     = height;
    }

    @Override
    protected Object doInBackground(Object... params) {
        File resizeFile;
        Bitmap thumbnail = null;
        File[] resizeFiles = new File[params.length];
        for (int i = 0; i < params.length; i++) {
            try {
                File file  = (File) params[i];
                thumbnail  = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), mWidth, mHeight);
                resizeFile = FileUtils.getFile(mTempFolder, file.getName());
                ImageUtils.saveBitmapToFile(thumbnail, resizeFile);
                resizeFiles[i] = resizeFile;
            } catch (java.lang.OutOfMemoryError e) {
                return null;
            } finally {
                if (thumbnail != null) {
                    thumbnail.recycle();
                    thumbnail = null;
                }
            }
        }
        return resizeFiles;
    }
}

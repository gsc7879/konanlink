package com.konantech.konanlink;

import android.app.Activity;
import android.os.Bundle;
import com.dignara.lib.annotation.InitActivity;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.PasswordDialogragment;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.Map;

@InitActivity(pageLayoutId = R.id.page_layout)
public class PasswordDialog extends Activity {
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        showPassword();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void showPassword() {
        if (DataBank.getInstance().isUsePassword()) {
            DialogUtils.showPasswordLockDialog(
                    PasswordDialogragment.TYPE_CHECK,
                    false,
                    getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            finish();
                        }
                    });

        }
    }

}

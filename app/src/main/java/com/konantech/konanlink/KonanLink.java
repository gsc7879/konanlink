package com.konantech.konanlink;

import android.content.Context;
import android.support.multidex.MultiDex;
import com.dignara.lib.annotation.InitApplication;
import com.dignara.lib.fragment.DignaraApplication;
import com.konantech.konanlink.constant.FragmentConst;
import com.konantech.konanlink.constant.GAConst;

@InitApplication(
        pagePackage = FragmentConst.PACKAGE_PAGE,
        menuPackage = FragmentConst.PACKAGE_MENU,
        partPackage = FragmentConst.PACKAGE_PART,
        dataPackage = FragmentConst.PACKAGE_DATA,
        trackingId  = GAConst.TRACKING_ID)
public class KonanLink extends DignaraApplication {
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}

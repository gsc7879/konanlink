package com.konantech.konanlink.db.task.accesskey;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.dignara.lib.db.DBHelper;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;

import java.util.List;
import java.util.Map;

public class AccessKeyInjectDBTask extends DBWorkTask<Void> {
    public AccessKeyInjectDBTask(int callBackId, Context context, DBHelper dbHelper, OnDBExecutionListener dbExecutionListener) {
        super(callBackId, context, dbHelper, dbExecutionListener);
    }

    @Override
    protected Void doInBackground(Object... params) {
        Map<Integer, List<DeviceInfo>> deviceIndexingCategoryMap = DataBank.getInstance().getDeviceIndexingCategoryMap();
        if (deviceIndexingCategoryMap != null) {
            for (int category : DeviceConst.CATEGORY_ARRAY) {
                if (deviceIndexingCategoryMap.containsKey(category)) {
                    for (DeviceInfo deviceInfo : deviceIndexingCategoryMap.get(category)) {
                        setAccessKey(DatabaseUtils.sqlEscapeString(deviceInfo.getKey()), DatabaseUtils.sqlEscapeString(deviceInfo.getAccount()));
                    }
                }
            }
        }
        return null;
    }

    private void setAccessKey(String deviceKey, String account) {
        Cursor cursor = mDBHelper.selectDb(R.string.db_accesskey_select, deviceKey, account);

        DeviceInfo deviceInfo;
        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            deviceInfo = DataBank.getInstance().getDeviceInfo(deviceKey);
            if (deviceInfo != null) {
                deviceInfo.setAccessKey(cursor.getString(cursor.getColumnIndexOrThrow("key")));
            }
        }
        cursor.close();
    }
}

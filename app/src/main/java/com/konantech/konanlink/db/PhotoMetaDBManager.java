package com.konantech.konanlink.db;

import android.content.Context;
import android.util.Log;

import com.konantech.konanlink.dao.DaoMaster;
import com.konantech.konanlink.dao.PhotoMetaInfo;
import com.konantech.konanlink.dao.PhotoMetaInfoDao;

import java.io.File;
import java.util.List;
import de.greenrobot.dao.query.QueryBuilder;

public class PhotoMetaDBManager {
    private static PhotoMetaDBManager uniqueDBManager;

    private static final String DB_KEYWORD = "filemetainfo.db";

    private PhotoMetaInfoDao mPhotoMetaInfoDao;

    public interface OnDBResult<T> {
        void onDBSuccess(T result);

        void onDBFailed(int failCode);
    }

    public synchronized static PhotoMetaDBManager getInstance() {
        if (uniqueDBManager == null) {
            uniqueDBManager = new PhotoMetaDBManager();
        }
        return uniqueDBManager;
    }

    public void setFilesMetaDao(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, DB_KEYWORD, null);
        DaoMaster daoMaster = new DaoMaster(helper.getWritableDatabase());
        mPhotoMetaInfoDao = daoMaster.newSession().getPhotoMetaInfoDao();
    }

    public void initDataBase() {
        mPhotoMetaInfoDao.deleteAll();
    }

    public static class SearchResult {
        private int count;
        private int currPage;
        private int totalPage;

        private List<PhotoMetaInfo> list;
        private List<PhotoMetaInfo> viewList;

        public int getCount() {
            return count;
        }

        public SearchResult(int count) {
            this.count = count;
        }

        public int getCurrPage() {
            return currPage;
        }

        public void setCurrPage(int currPage) {
            this.currPage = currPage;
        }

        public int getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public List<PhotoMetaInfo> getViewList() {
            return this.viewList;
        }

        public void setList(List<PhotoMetaInfo> list) {
            this.list = list;
            this.viewList = this.list.subList(0, this.count < this.list.size() ? this.count : this.list.size());
        }

        public boolean isMore() {
            if (this.list == null) {
                return false;
            } else {
                return this.list.size() > count;
            }
        }

    }

    public SearchResult searchList(String keywords, int offset, int count) {
        QueryBuilder<PhotoMetaInfo> queryBuilder = mPhotoMetaInfoDao.queryBuilder();

        for (String keyword : keywords.split(" ")) {
            queryBuilder.where(PhotoMetaInfoDao.Properties.KeyWord.like(String.format("%%%s%%", keyword)));
        }

        SearchResult result = new SearchResult(count);
        result.setList(queryBuilder.offset(offset).limit(count + 1).list());
        result.setCurrPage(offset/count);

        int totalPage = 1 + ((int)queryBuilder.count() / count);
        if (((int)queryBuilder.count() % count) == 0) {
            totalPage = totalPage - 1;
        }
        result.setTotalPage(totalPage);
        return result;
    }

    public void insertOrReplace(PhotoMetaInfo filesMetaInfo) {
        try {
            mPhotoMetaInfoDao.insertOrReplace(filesMetaInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(File file) {
        try {
            QueryBuilder<PhotoMetaInfo> queryBuilder = mPhotoMetaInfoDao.queryBuilder();
            queryBuilder.where(PhotoMetaInfoDao.Properties.FileAbsPath.eq(file.getAbsolutePath()));
            queryBuilder.buildDelete().executeDeleteWithoutDetachingEntities();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isInsertDataBase(File file) {
        try {
            QueryBuilder<PhotoMetaInfo> queryBuilder = mPhotoMetaInfoDao.queryBuilder();
            queryBuilder.where(PhotoMetaInfoDao.Properties.FileAbsPath.eq(file.getAbsoluteFile()));
            List<PhotoMetaInfo> list = queryBuilder.list();
            if (list.size() == 0) {
                return true;
            }
            PhotoMetaInfo metaInfo = list.get(0);
            if (metaInfo != null && (metaInfo.getModifiedDate() == file.lastModified())) {
                return false;
            }
        } catch (Exception e) {

        }
        return true;
    }

    public boolean existFile(File file) {
        try {
            QueryBuilder<PhotoMetaInfo> queryBuilder = mPhotoMetaInfoDao.queryBuilder();
            queryBuilder.where(PhotoMetaInfoDao.Properties.FileAbsPath.eq(file.getAbsoluteFile()));
            List<PhotoMetaInfo> list = queryBuilder.list();
            return list.size() != 0 ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


}

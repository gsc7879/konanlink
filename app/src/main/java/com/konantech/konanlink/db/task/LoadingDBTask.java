package com.konantech.konanlink.db.task;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import com.dignara.lib.db.DBHelper;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.utils.DialogUtils;
import org.apache.commons.lang3.StringUtils;

public abstract class LoadingDBTask extends DBWorkTask {
    private DialogController mDialogController;
    private String           mTitle;

    public LoadingDBTask(int callBackId, Context context, int titleResId, DBHelper dbHelper, OnDBExecutionListener dbExecutionListener) {
        super(callBackId, context, dbHelper, dbExecutionListener);
        mTitle = mContext.getString(titleResId);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (StringUtils.isNotBlank(mTitle)) {
            mDialogController = DialogUtils.showLoadingDialog(mTitle, ((FragmentActivity) mContext).getFragmentManager());
        }
    }

    @Override
    protected void onPostExecute(Object object) {
        if (mDialogController != null) {
            mDialogController.dismiss();
        }
        super.onPostExecute(object);
    }
}
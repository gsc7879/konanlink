package com.konantech.konanlink.db.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileSelectDBInfo {
    private List<File> mFileList;
    private int        mOffset;
    private boolean    mIsNext;

    public FileSelectDBInfo(int offset, boolean isNext) {
        mFileList = new ArrayList<>();
        mIsNext   = isNext;
        mOffset   = offset;
    }

    public void addFile(File file) {
        mFileList.add(file);
    }

    public List<File> getFileList() {
        return mFileList;
    }

    public boolean isNext() {
        return mIsNext;
    }

    public int getOffset() {
        return mOffset;
    }
}

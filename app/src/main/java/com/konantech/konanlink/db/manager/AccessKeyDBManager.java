package com.konantech.konanlink.db.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.dignara.lib.db.DBManager;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.R;
import com.konantech.konanlink.db.task.accesskey.AccessKeyDeleteDBTask;
import com.konantech.konanlink.db.task.accesskey.AccessKeyUpdateDBTask;

public class AccessKeyDBManager extends DBManager {
    public static final int DB_SELECT     = 0;
    public static final int DB_DELETE     = 1;
    public static final int DB_DELETE_ALL = 2;
    public static final int DB_UPDATE     = 3;

    private static final String NAME_DB = "access_key.db";
    private static final int VERSION_DB = 1;

    public AccessKeyDBManager(Context context) {
        super(context, R.string.db_accesskey_create, R.string.db_accesskey_drop, NAME_DB, VERSION_DB);
    }

    public String getAccessKey(String deviceKey, String account) {
        String accessKey = "";
        Cursor cursor = null;
        try {
            cursor = getDBHelper().selectDb(R.string.db_accesskey_select, DatabaseUtils.sqlEscapeString(deviceKey), DatabaseUtils.sqlEscapeString(account));
            if (cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                accessKey = cursor.getString(cursor.getColumnIndexOrThrow("key"));
            }
        } finally {
            cursor.close();
        }
        
        return accessKey;
    }

    public void deleteAll(DBWorkTask.OnDBExecutionListener dbExecutionListener) {
        execute(new AccessKeyDeleteDBTask(DB_DELETE_ALL, mContext, getDBHelper(), dbExecutionListener));
    }

    public void delete(DBWorkTask.OnDBExecutionListener dbExecutionListener, String deviceKey, String account) {
        execute(new AccessKeyDeleteDBTask(DB_DELETE, mContext, getDBHelper(), dbExecutionListener), deviceKey, account);
    }

    public void update(DBWorkTask.OnDBExecutionListener dbExecutionListener, String deviceKey, String key, String account) {
        execute(new AccessKeyUpdateDBTask(DB_UPDATE, mContext, getDBHelper(), dbExecutionListener), deviceKey, key, account);
    }
}

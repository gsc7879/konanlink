package com.konantech.konanlink.db;

import android.content.Context;
import com.konantech.konanlink.dao.DaoMaster;
import com.konantech.konanlink.dao.RecentKeyword;
import com.konantech.konanlink.dao.RecentKeywordDao;
import de.greenrobot.dao.query.QueryBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.Calendar;
import java.util.List;

public class DBManager {
    private static DBManager uniqueDBManager;

    private static final String DB_KEYWORD = "recentKeyword.db";
    private static final int    COUNT_SHOW = 7;

    private RecentKeywordDao mRecentKeywordDao;

    public interface OnDBResult<T> {
        void onDBSuccess(T result);

        void onDBFailed(int failCode);
    }

    public synchronized static DBManager getInstance() {
        if (uniqueDBManager == null) {
            uniqueDBManager = new DBManager();
        }
        return uniqueDBManager;
    }

    public void setKeywordDao(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, DB_KEYWORD, null);
        DaoMaster daoMaster = new DaoMaster(helper.getWritableDatabase());
        mRecentKeywordDao = daoMaster.newSession().getRecentKeywordDao();
    }

    private RecentKeyword getRecentKeyword(String keyword) {
        QueryBuilder<RecentKeyword> queryBuilder = mRecentKeywordDao.queryBuilder();
        queryBuilder.where(RecentKeywordDao.Properties.Keyword.eq(keyword));
        List<RecentKeyword> list = queryBuilder.list();
        return list != null && list.size() > 0 ? list.get(0) : null;
    }

    public void saveKeyword(String keyword) {
        RecentKeyword recentKeyword = getRecentKeyword(keyword);
        if (recentKeyword == null) {
            recentKeyword = new RecentKeyword(null, keyword, 0, Calendar.getInstance().getTime());
            mRecentKeywordDao.insert(recentKeyword);
        } else {
            recentKeyword.setCount(recentKeyword.getCount() + 1);
            recentKeyword.setSearchDate(Calendar.getInstance().getTime());
            mRecentKeywordDao.update(recentKeyword);
        }
    }

    public List<RecentKeyword> getRecentKeywords(String keyword) {
        QueryBuilder<RecentKeyword> queryBuilder = mRecentKeywordDao.queryBuilder();
        if (StringUtils.isBlank(keyword)) {
            return queryBuilder.orderDesc(RecentKeywordDao.Properties.SearchDate).limit(COUNT_SHOW).list();
        } else {
            return queryBuilder.orderDesc(RecentKeywordDao.Properties.SearchDate).where(RecentKeywordDao.Properties.Keyword.like(String.format("%%%s%%", keyword))).limit(COUNT_SHOW).list();
        }
    }

    public void deleteById(long id) {
        mRecentKeywordDao.deleteByKey(id);
    }

    public void deleteAll() {
        mRecentKeywordDao.deleteAll();
    }
}

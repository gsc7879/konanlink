package com.konantech.konanlink.db.task.accesskey;

import android.content.Context;
import android.database.DatabaseUtils;
import com.dignara.lib.db.DBHelper;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.R;

public class AccessKeyDeleteDBTask extends DBWorkTask<Void> {
    public AccessKeyDeleteDBTask(int callBackId, Context context, DBHelper dbHelper, OnDBExecutionListener dbExecutionListener) {
        super(callBackId, context, dbHelper, dbExecutionListener);
    }

    @Override
    protected Void doInBackground(Object... params) {
        if (params.length > 1) {
            String deviceKey = DatabaseUtils.sqlEscapeString((String) params[0]);
            String account   = DatabaseUtils.sqlEscapeString((String) params[1]);

            mDBHelper.execSQL(R.string.db_accesskey_delete, deviceKey, account);
        } else {
            mDBHelper.execSQL(R.string.db_accesskey_delete_all);
        }
        return null;
    }
}

package com.konantech.konanlink.db.task.accesskey;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.dignara.lib.db.DBHelper;
import com.dignara.lib.task.DBWorkTask;
import com.konantech.konanlink.R;

public class AccessKeyUpdateDBTask extends DBWorkTask<Void> {
    public AccessKeyUpdateDBTask(int callBackId, Context context, DBHelper dbHelper, OnDBExecutionListener dbExecutionListener) {
        super(callBackId, context, dbHelper, dbExecutionListener);
    }

    @Override
    protected Void doInBackground(Object... params) {
        String deviceKey = DatabaseUtils.sqlEscapeString((String) params[0]);
        String key       = DatabaseUtils.sqlEscapeString((String) params[1]);
        String account   = DatabaseUtils.sqlEscapeString((String) params[2]);

        Cursor cursor = null;
        try {
            cursor = mDBHelper.selectDb(R.string.db_accesskey_exist, deviceKey, account);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToPosition(0);
                mDBHelper.execSQL(R.string.db_accesskey_update, key, cursor.getInt(cursor.getColumnIndexOrThrow("idx")));
            } else {
                mDBHelper.execSQL(R.string.db_accesskey_insert, deviceKey, key, account);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }
}

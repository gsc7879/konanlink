package com.konantech.konanlink.db.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import com.dignara.lib.db.DBManager;
import com.konantech.konanlink.R;
import org.apache.commons.io.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class FileDBManager extends DBManager {
    private static final String NAME_DB = "indexing.db";
    private static final int VERSION_DB = 1;

    public FileDBManager(Context context) {
        super(context, R.string.db_indexing_create, R.string.db_indexing_drop, NAME_DB, VERSION_DB);
    }

    public void initCompareDB() {
        getDBHelper().execSQL(R.string.db_indexing_init_compare);
    }
    public void initIndexingDB() {
        getDBHelper().execSQL(R.string.db_indexing_init_indexing);
    }

    public void copyCompareDB() {
        getDBHelper().execSQL(R.string.db_indexing_init_indexing);
        getDBHelper().execSQL(R.string.db_indexing_copy);
    }

    public void insertFileInfo(String filePath, long modifyTime) {
        getDBHelper().execSQL(R.string.db_indexing_insert, DatabaseUtils.sqlEscapeString(filePath), String.valueOf(modifyTime));
    }

    public FileSelectDBInfo selectDeletedFileList(int offset, int count) {
       return selectFileList(offset, count, R.string.db_indexing_select_deleted);
    }

    public FileSelectDBInfo selectModifiedFileList(int offset, int count) {
        return selectFileList(offset, count, R.string.db_indexing_select_modified);
    }

    public FileSelectDBInfo selectFileList(int offset, int count, int queryResId) {
        Cursor cursor = getDBHelper().selectDb(queryResId, count + 1, offset);
        int listCount = cursor.getCount();
        FileSelectDBInfo fileSelectDBInfo = new FileSelectDBInfo(offset + listCount - 1, listCount > count);
        listCount = listCount < count ? listCount : count;
        for (int i = 0; i < listCount; i++) {
            cursor.moveToPosition(i);
            fileSelectDBInfo.addFile(FileUtils.getFile(cursor.getString(cursor.getColumnIndexOrThrow("filePath"))));
        }
        cursor.close();
        return fileSelectDBInfo;
    }

    public List<String> selectIndexingList() {
        return selectList(R.string.db_indexing_select_indexing);
    }

    public List<String> selectCompareList() {
        return selectList(R.string.db_indexing_select_compare);
    }

    public List<String> selectList(int queryResId) {
        List<String> filePathList = new ArrayList<>();
        Cursor cursor = getDBHelper().selectDb(queryResId);
        int listCount = cursor.getCount();
        for (int i = 0; i < listCount; i++) {
            cursor.moveToPosition(i);
            filePathList.add(cursor.getString(cursor.getColumnIndexOrThrow("filePath")));
        }
        cursor.close();
        return filePathList;
    }
}

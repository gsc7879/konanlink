package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;

import org.apache.commons.lang3.StringUtils;

public class ConverseDialogFragment extends DialogController {
    @InjectView(id = R.id.text_converse_title)
    private TextView mTitleText;

    @InjectView(id = R.id.text_converse_content)
    private TextView mContentsText;

    @InjectView(id = R.id.btn_converse_negative)
    private TextView mNegativeBtnText;

    @InjectView(id = R.id.btn_converse_positive)
    private TextView mPositiveBtnText;

    public static ConverseDialogFragment getInstance(Bundle args) {
        ConverseDialogFragment converseDialogFragment = new ConverseDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        setContent();
    }

    private void setContent() {
        String title      = getArguments().getString("title");
        String content    = getArguments().getString("content");
        int negativeResId = getArguments().getInt("negativeResId");
        int positiveResId = getArguments().getInt("positiveResId");

        if (StringUtils.isBlank(title)) {
            mTitleText.setVisibility(View.GONE);
        } else {
            mTitleText.setVisibility(View.VISIBLE);
            mTitleText.setText(title);
        }
        mContentsText.setText(Html.fromHtml(content));
        mNegativeBtnText.setText(negativeResId);
        mPositiveBtnText.setText(positiveResId);
    }

    @Click(R.id.btn_converse_negative)
    public void closeNegative() {
        negativeDismiss();
    }

    @Click(R.id.btn_converse_positive)
    public void closePositive() {
        positiveDismiss();
    }
}

package com.konantech.konanlink.dialog;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;

import org.apache.commons.lang3.StringUtils;

public class WebDialogFragment extends DialogController {
    @InjectView(id = R.id.web_main)
    private WebView mWebView;

    public static WebDialogFragment getInstance(Bundle args) {
        WebDialogFragment converseDialogFragment = new WebDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        setWebView();
    }

    private void setWebView() {
        String content = getArguments().getString("content");
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        if (StringUtils.startsWith(content, "http")) {
            mWebView.loadUrl(content);
        } else {
            mWebView.loadData(content, "text/html", "UTF-8");
        }
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            @Override
            public void onPageFinished(WebView view, String url) {
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
                WebView newWebView = new WebView(getActivity());
                WebView.WebViewTransport transport = (WebView.WebViewTransport)resultMsg.obj;
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }
        });
    }

    @Click(R.id.btn_web_confirm)
    public void closeDialog() {
        positiveDismiss();
    }
}

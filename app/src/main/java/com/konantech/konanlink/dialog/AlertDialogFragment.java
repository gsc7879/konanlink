package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;

import org.apache.commons.lang3.StringUtils;

public class AlertDialogFragment extends DialogController {
    @InjectView(id = R.id.text_alert_title)
    private TextView mTitleText;

    @InjectView(id = R.id.text_alert_content)
    private TextView mContentsText;

    @InjectView(id = R.id.btn_alert_confirm)
    private TextView mBtnText;

    public static AlertDialogFragment getInstance(Bundle args) {
        AlertDialogFragment converseDialogFragment = new AlertDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        setContent();
    }

    private void setContent() {
        String title      = getArguments().getString("title");
        String content    = getArguments().getString("content");
        int positiveResId = getArguments().getInt("btnResId");

        if (StringUtils.isBlank(title)) {
            mTitleText.setVisibility(View.GONE);
        } else {
            mTitleText.setVisibility(View.VISIBLE);
            mTitleText.setText(title);
        }

        mContentsText.setText(Html.fromHtml(content));
        mBtnText.setText(positiveResId);
    }

    @Click(R.id.btn_alert_confirm)
    public void close() {
        positiveDismiss();
    }
}

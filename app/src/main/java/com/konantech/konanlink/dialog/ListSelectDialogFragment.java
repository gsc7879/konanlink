package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.list.adapter.DialogSelectListAdapter;
import com.konantech.konanlink.list.data.DialogSelectListViewData;
import com.konantech.konanlink.list.model.DialogSelectListSetData;

import org.apache.commons.lang3.StringUtils;

public class ListSelectDialogFragment extends DialogController implements DialogSelectListAdapter.OnSelectedItemListener {
    public static final int DATA_INDEX = 0;
    public static final int DATA_TITLE = 1;
    public static final int DATA_VALUE = 2;

    private DialogSelectListAdapter mDialogListAdapter;

    public static ListSelectDialogFragment getInstance(Bundle args) {
        ListSelectDialogFragment converseDialogFragment = new ListSelectDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @InjectView(id = R.id.listTitleTextView)
    private TextView mTitleText;

    @Override
    protected void onInitDialog(View view) {
        setContent(view);
    }

    private void setContent(View dialogView) {
        setTitle();
        setList(dialogView);
    }

    private void setList(View dialogView) {
        DialogSelectListSetData data = new DialogSelectListSetData();
        data.setTitles(getArguments().getStringArray("titles"));
        data.setValues(getArguments().getStringArray("values"));
        data.setIcons(getArguments().getIntArray("icons"));

        mDialogListAdapter = new DialogSelectListAdapter(dialogView, this, R.id.layout_list_dialog, R.id.list_dialog, R.layout.adapter_dialog_select, R.layout.layout_list_no, data);
        mDialogListAdapter.loadList();
    }

    private void setTitle() {
        String title = getArguments().getString("title");
        if (StringUtils.isNotBlank(title)) {
            mTitleText.setVisibility(View.VISIBLE);
            mTitleText.setText(title);
        } else {
            mTitleText.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSelectedItem(DialogSelectListViewData listViewData) {
        mDataMap.put(DATA_INDEX, listViewData.getIdx());
        mDataMap.put(DATA_TITLE, listViewData.getTitle());
        mDataMap.put(DATA_VALUE, listViewData.getValue());
        positiveDismiss();
    }
}

package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.list.adapter.DialogCheckListAdapter;
import com.konantech.konanlink.list.data.DialogCheckListViewData;
import com.konantech.konanlink.list.model.DialogListSetData;

import org.apache.commons.lang3.StringUtils;

public class ListCheckDialogFragment extends DialogController {
    private DialogCheckListAdapter mDialogListAdapter;

    @InjectView(id = R.id.text_list_check_title)
    private TextView mTitleText;

    public static ListCheckDialogFragment getInstance(Bundle args) {
        ListCheckDialogFragment converseDialogFragment = new ListCheckDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        setContent(view);
    }

    private void setContent(View dialogView) {
        setTitle();
        setList(dialogView);
    }

    private void setList(View dialogView) {
        DialogListSetData data = new DialogListSetData();
        data.setTitles(getArguments().getStringArray("titles"));
        data.setValues(getArguments().getStringArray("values"));
        data.setChecked(getArguments().getStringArray("checked"));

        mDialogListAdapter = new DialogCheckListAdapter(dialogView, R.id.layout_list_check_dialog, R.id.list_check_dialog, R.layout.adapter_dialog_check, R.layout.layout_list_no, data);
        mDialogListAdapter.loadList();
    }

    private void setTitle() {
        String title = getArguments().getString("title");
        if (StringUtils.isNotBlank(title)) {
            mTitleText.setVisibility(View.VISIBLE);
            mTitleText.setText(title);
        } else {
            mTitleText.setVisibility(View.GONE);
        }
    }

    @Click(R.id.btn_converse_negative)
    public void cancelDialog() {
        negativeDismiss();
    }

    @Click(R.id.btn_converse_positive)
    public void confirmDialog() {
        DialogCheckListViewData data;
        for (int i = 0; i < mDialogListAdapter.getItemCount(); i++) {
            data = (DialogCheckListViewData) mDialogListAdapter.getItem(i);
            if (data.isChecked()) {
                mDataMap.put(data.getIdx(), data.getValue());
            }
        }
        if (mDataMap.size() == 0) {
            Snackbar.make(getView(), R.string.s21_error_more_than_one, Snackbar.LENGTH_SHORT).show();
            return;
        }
        positiveDismiss();
    }
}

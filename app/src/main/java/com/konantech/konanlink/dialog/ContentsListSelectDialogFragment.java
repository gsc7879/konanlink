package com.konantech.konanlink.dialog;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.bank.ImageBank;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.UnitUtils;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.adapter.DialogSelectListAdapter;
import com.konantech.konanlink.list.data.DialogSelectListViewData;
import com.konantech.konanlink.list.model.DialogSelectListSetData;
import com.microsoft.live.LiveConnectClient;
import com.microsoft.live.LiveOperation;
import com.microsoft.live.LiveOperationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class ContentsListSelectDialogFragment extends DialogController implements DialogSelectListAdapter.OnSelectedItemListener {
    public static final int DATA_INDEX = 0;
    public static final int DATA_TITLE = 1;
    public static final int DATA_VALUE = 2;

    private DialogSelectListAdapter mDialogListAdapter;

    @InjectView(id = R.id.text_list_folder)
    private TextView pathText;

    @InjectView(id = R.id.text_list_title)
    private TextView titleText;

    @InjectView(id = R.id.text_list_info)
    private TextView infoText;

    @InjectView(id = R.id.text_list_contents)
    private TextView contentsText;

    @InjectView(id = R.id.img_list_contents)
    private ImageView thumbnailImg;

    public static ContentsListSelectDialogFragment getInstance(Bundle args) {
        ContentsListSelectDialogFragment converseDialogFragment = new ContentsListSelectDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        GAManager.getInstance().sendScreen(GAConst.SCREEN_PREVIEW);
        setContent(view);
    }

    private void setContent(View dialogView) {
        setFolder();
        setTitle();
        setList(dialogView);
    }

    private void setFolder() {
        int kind      = getArguments().getInt("kind");
        String folder = getArguments().getString("folder");
        String key    = getArguments().getString("key");
        if (DataBank.getInstance().getAccountInfo().isExpired() || StringUtils.isBlank(folder)) {
            pathText.setVisibility(View.GONE);
        } else {
            pathText.setVisibility(View.VISIBLE);
        }

        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_GMAIL:
                pathText.setText(folder);
                break;
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_ONEDRIVE:
                getFolderPath(kind, key, folder);
                pathText.setText(R.string.s99_text_loading);
                break;
        }
    }

    private void setList(View dialogView) {
        DialogSelectListSetData data = new DialogSelectListSetData();
        data.setTitles(getArguments().getStringArray("titles"));
        data.setValues(getArguments().getStringArray("values"));
        data.setIcons(getArguments().getIntArray("icons"));
        mDialogListAdapter = new DialogSelectListAdapter(dialogView, this, R.id.layout_list_dialog, R.id.list_dialog, R.layout.adapter_dialog_contents, R.layout.layout_list_no, data);
        mDialogListAdapter.loadList();
    }

    private void setTitle() {
        String title     = getArguments().getString("title");
        String info      = getArguments().getString("info");
        String contents  = getArguments().getString("contents");
        String thumbnail = getArguments().getString("thumbnail");

        titleText.setText(title);
        infoText.setText(info);

        if (StringUtils.isNotBlank(contents)) {
            contentsText.setVisibility(View.VISIBLE);
            contentsText.setVerticalScrollBarEnabled(true);
            contentsText.setMovementMethod(new ScrollingMovementMethod());
            contentsText.setText(Html.fromHtml(contents));
        }

        if (StringUtils.isNotBlank(thumbnail)) {
            thumbnailImg.setVisibility(View.VISIBLE);
            int thumbnailPixel = UnitUtils.convertDpToPixel(getActivity(), 240);
            if (StringUtils.startsWith(thumbnail, "http")) {
                ImageBank.getInstance().getPicasso()
                        .with(getActivity())
                        .load(thumbnail)
                        .error(R.drawable.failed).resize(thumbnailPixel, thumbnailPixel)
                        .centerCrop()
                        .into(thumbnailImg);
            } else {
                ImageBank.getInstance().getPicasso()
                        .with(getActivity())
                        .load(FileUtils.getFile(thumbnail))
                        .error(R.drawable.failed).resize(thumbnailPixel, thumbnailPixel)
                        .centerCrop()
                        .into(thumbnailImg);
            }
        }
    }

    @Override
    public void onSelectedItem(DialogSelectListViewData listViewData) {
        mDataMap.put(DATA_INDEX, listViewData.getIdx());
        mDataMap.put(DATA_TITLE, listViewData.getTitle());
        mDataMap.put(DATA_VALUE, listViewData.getValue());
        positiveDismiss();
    }

    private void getFolderPath(final int kind, final String key, final String folder) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(key);
                if (deviceInfo != null) {
                    switch (kind) {
                        case DeviceConst.KIND_GOOGLEDRIVE:
                            return getCurrentPath((Drive) deviceInfo.getConnector(), folder);
                        case DeviceConst.KIND_ONEDRIVE:
                            return getCurrentPath((LiveConnectClient) deviceInfo.getConnector(), folder);
                    }
                }
                return "";
            }

            @Override
            protected void onPostExecute(String folderPath) {
                if (StringUtils.isBlank(folderPath)) {
                    pathText.setVisibility(View.GONE);
                } else {
                    pathText.setVisibility(View.VISIBLE);
                    pathText.setText(folderPath);
                }
            }

            @Override
            protected void onCancelled(String o) {
                pathText.setVisibility(View.GONE);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private String getCurrentPath(LiveConnectClient connectClient, String fileId) {
        StringBuilder sb = new StringBuilder();

        try {
            LiveOperation liveOperation;
            JSONObject result;
            String name;
            do {
                liveOperation = connectClient.get(fileId);
                result = liveOperation.getResult();
                fileId = result.getString("parent_id");
                name = result.getString("name");
                if (StringUtils.equalsIgnoreCase(name, "skydrive")) {
                    return sb.toString();
                }
                sb.insert(0, String.format("/%s", name));
            } while (StringUtils.isNotBlank(fileId));
        } catch (LiveOperationException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getCurrentPath(Drive drive, String path) {
        path = StringUtils.isBlank(path) ? "root" : path;
        File currentFolder;
        try {
            currentFolder = drive.files().get(path).execute();
        } catch (IOException e) {
            return "";
        }
        StringBuilder sb = new StringBuilder(String.format("/%s", currentFolder.getTitle()));
        try {
            List<ParentReference> parents = currentFolder.getParents();
            while (parents.size() > 0) {
                currentFolder = drive.files().get(parents.get(0).getId()).execute();
                sb.insert(0, String.format("/%s", currentFolder.getTitle()));
                parents = currentFolder.getParents();
            }
        } catch (IOException ignored) {
        }
        return sb.toString();
    }
}

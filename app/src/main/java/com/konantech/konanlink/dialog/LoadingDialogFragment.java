package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import org.apache.commons.lang3.StringUtils;

public class LoadingDialogFragment extends DialogController {
    @InjectView(id = R.id.img_loading_search)
    private ImageView loadingImg;

    @InjectView(id = R.id.text_loading_content)
    private TextView loadingText;

    private Animation anim;

    public static LoadingDialogFragment getInstance(Bundle args) {
        LoadingDialogFragment converseDialogFragment = new LoadingDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        setContent();
        startLoading();
    }

    private void setContent() {
        String content = getArguments().getString("content");
        if (StringUtils.isNotBlank(content)) {
            loadingText.setText(content);
        }
    }

    private void startLoading() {
        if (anim == null) {
            anim = new CircleAnimation(loadingImg, AnimationConst.SMALL_RADIOUS);
            anim.setDuration(AnimationConst.SMALL_DURATION);
            anim.setRepeatCount(Animation.INFINITE);
        }
        loadingImg.startAnimation(anim);
    }
}

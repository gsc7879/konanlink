package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;

public class InfoDialogFragment extends DialogController {
    public static final int TYPE_AGENT      = 0;
    public static final int TYPE_PC         = 1;
    public static final int TYPE_MOBILE     = 2;
    public static final int TYPE_OUTLOOK    = 3;
    public static final int TYPE_IMAP       = 4;
    public static final int TYPE_FILESERVER = 5;

    @InjectView(id = R.id.img_info)
    private ImageView imageView;

    @InjectView(id = R.id.text_info_content)
    private TextView textView;


    public static InfoDialogFragment getInstance(Bundle args) {
        InfoDialogFragment converseDialogFragment = new InfoDialogFragment();
        converseDialogFragment.setArguments(args);
        return converseDialogFragment;
    }

    @Override
    protected void onInitDialog(View view) {
        int type = getArguments().getInt("type");
        int imgResId  = 0;
        int textResId = 0;
        switch (type) {
            case TYPE_AGENT:
                imgResId = R.drawable.ic_illust_comsearch;
                textResId = R.string.s13_text_install_pc;
                break;
            case TYPE_PC:
                imgResId = R.drawable.ic_illust_comsearch;
                textResId = R.string.s45_dialog_text_2computers;
                break;
            case TYPE_MOBILE:
                imgResId = R.drawable.ic_illust_pairphone_gray;
                textResId = R.string.s45_dialog_text_2smartphones;
                break;
            case TYPE_OUTLOOK:
                imgResId = R.drawable.ic_illust_comsearch;
                textResId = R.string.s45_dialog_text_outlook;
                break;
            case TYPE_IMAP:
                imgResId = R.drawable.ic_illust_comsearch;
                textResId = R.string.s45_dialog_text_imap;
                break;
            case TYPE_FILESERVER:
                imgResId = R.drawable.ic_illust_comsearch;
                textResId = R.string.s45_dialog_text_fileserver;
                break;
        }
        imageView.setImageResource(imgResId);
        textView.setText(textResId);
    }

    @Click(R.id.btn_intro_pc_ok)
    public void introHide() {
        positiveDismiss();
    }
}

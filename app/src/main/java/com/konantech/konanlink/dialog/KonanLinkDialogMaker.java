package com.konantech.konanlink.dialog;

import android.os.Bundle;

import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.dialog.DialogMaker;
import com.konantech.konanlink.R;

public class KonanLinkDialogMaker extends DialogMaker {
    private static KonanLinkDialogMaker uniqueDialogManager;

    public static final int CONVERSE        = 0;
    public static final int CONVERSE_CENTER = 1;
    public static final int ALERT           = 2;
    public static final int LOADING         = 3;
    public static final int LIST            = 4;
    public static final int LIST_CHECK      = 5;
    public static final int LIST_SELECTED   = 6;
    public static final int LIST_CONTENTS   = 7;
    public static final int WEB             = 8;
    public static final int INFO            = 9;
    public static final int PASSWORD_LOCK   = 10;

    public synchronized static KonanLinkDialogMaker getInstance() {
        if (uniqueDialogManager == null) {
            uniqueDialogManager = new KonanLinkDialogMaker();
        }
        return uniqueDialogManager;
    }

    private KonanLinkDialogMaker() {
    }

    @Override
    public DialogController getDialogFragment(int dialogId, Object... etcData) {
        Bundle bundle;
        DialogController dialogController = null;
        switch (dialogId) {
            case CONVERSE:
                bundle = getBundle(R.layout.dialog_converse, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"     , (String) etcData[0]);
                bundle.putString("content"   , (String) etcData[1]);
                bundle.putInt("negativeResId", (int)    etcData[2]);
                bundle.putInt("positiveResId", (int)    etcData[3]);
                bundle.putInt("styleId"      , R.style.AppTheme_dialog);
                dialogController = ConverseDialogFragment.getInstance(bundle);
                break;
            case CONVERSE_CENTER:
                bundle = getBundle(R.layout.dialog_converse_center, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"     , (String) etcData[0]);
                bundle.putString("content"   , (String) etcData[1]);
                bundle.putInt("negativeResId", (int)    etcData[2]);
                bundle.putInt("positiveResId", (int)    etcData[3]);
                bundle.putInt("styleId"      , R.style.AppTheme_dialog);
                dialogController = ConverseDialogFragment.getInstance(bundle);
                break;
            case ALERT:
                bundle = getBundle(R.layout.dialog_alert, android.R.anim.fade_in, android.R.anim.fade_out, false, dialogId);
                bundle.putString("title"  , (String) etcData[0]);
                bundle.putString("content", (String) etcData[1]);
                bundle.putInt("btnResId"  , (int)    etcData[2]);
                bundle.putInt("styleId"   , R.style.AppTheme_dialog);
                dialogController = AlertDialogFragment.getInstance(bundle);
                break;
            case WEB:
                bundle = getBundle(R.layout.dialog_web, android.R.anim.fade_in, android.R.anim.fade_out, false, dialogId);
                bundle.putString("content", (String) etcData[0]);
                bundle.putInt("styleId"   , R.style.AppTheme_dialog);
                dialogController = WebDialogFragment.getInstance(bundle);
                break;
            case LOADING:
                bundle = getBundle(R.layout.dialog_loading, android.R.anim.fade_in, android.R.anim.fade_out, false, dialogId);
                bundle.putString("content", (String) etcData[0]);
                bundle.putInt("styleId"   , R.style.AppTheme_dialog);
                dialogController = LoadingDialogFragment.getInstance(bundle);
                break;
            case LIST:
                bundle = getBundle(R.layout.dialog_list_select, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"      , (String)   etcData[0]);
                bundle.putStringArray("titles", (String[]) etcData[1]);
                bundle.putStringArray("values", (String[]) etcData[2]);
                bundle.putIntArray("icons"    , (int[])    etcData[3]);
                bundle.putInt("styleId"       , R.style.AppTheme_dialog);
                dialogController = ListSelectDialogFragment.getInstance(bundle);
                break;
            case LIST_CHECK:
                bundle = getBundle(R.layout.dialog_list_check, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"        , (String)   etcData[0]);
                bundle.putStringArray("titles"  , (String[]) etcData[1]);
                bundle.putStringArray("values"  , (String[]) etcData[2]);
                bundle.putStringArray("checked" , (String[]) etcData[3]);
                bundle.putInt("styleId"         , R.style.AppTheme_dialog);
                dialogController = ListCheckDialogFragment.getInstance(bundle);
                break;
            case LIST_SELECTED:
                bundle = getBundle(R.layout.dialog_list_selected, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"      , (String)   etcData[0]);
                bundle.putStringArray("titles", (String[]) etcData[1]);
                bundle.putStringArray("values", (String[]) etcData[2]);
                bundle.putInt("selectedIndex" , (int)      etcData[3]);
                bundle.putInt("styleId"       , R.style.AppTheme_dialog);
                dialogController = ListSelectedDialogFragment.getInstance(bundle);
                break;
            case LIST_CONTENTS:
                bundle = getBundle(R.layout.dialog_list_contents, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putString("title"      , (String)   etcData[0]);
                bundle.putString("folder"     , (String)   etcData[1]);
                bundle.putString("info"       , (String)   etcData[2]);
                bundle.putString("contents"   , (String)   etcData[3]);
                bundle.putString("thumbnail"  , (String)   etcData[4]);
                bundle.putStringArray("titles", (String[]) etcData[5]);
                bundle.putStringArray("values", (String[]) etcData[6]);
                bundle.putIntArray("icons"    , (int[])    etcData[7]);
                bundle.putInt("kind"          , (int)      etcData[8]);
                bundle.putString("key"        , (String)   etcData[9]);
                bundle.putInt("styleId", R.style.AppTheme_dialog);
                dialogController = ContentsListSelectDialogFragment.getInstance(bundle);
                break;
            case INFO:
                bundle = getBundle(R.layout.dialog_info, android.R.anim.fade_in, android.R.anim.fade_out, true, dialogId);
                bundle.putInt("type"   , (int) etcData[0]);
                bundle.putInt("styleId", R.style.AppTheme_dialog);
                dialogController = InfoDialogFragment.getInstance(bundle);
                break;
            case PASSWORD_LOCK:
                bundle = getBundle(R.layout.layout_password, android.R.anim.fade_in, android.R.anim.fade_out, (boolean)etcData[1], dialogId);
                bundle.putInt("type"   , (int) etcData[0]);
                dialogController = PasswordDialogragment.getInstance(bundle);
                break;
        }
        return dialogController;
    }
}

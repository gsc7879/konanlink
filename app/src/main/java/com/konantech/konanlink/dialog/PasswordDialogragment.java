package com.konantech.konanlink.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class PasswordDialogragment extends DialogController {
    private static final String STATE_TYPE           = "type";
    private static final String STATE_FIRST_PASSWORD = "firstPassword";

    public static final int TYPE_CHECK     = 0;
    public static final int TYPE_SETTING   = 1;
    public static final int TYPE_REMOVE    = 2;
    public static final int TYPE_CHANGE    = 3;
    public static final int TYPE_CANCEL    = 4;
    public static final int TYPE_RESETTING = 5;

    private int         mType;
    private String      mFirstPassword;
    
    @InjectView(id = R.id.text_password_info)
    private TextView    mInfoText;

    @InjectView(ids = {R.id.text_password_1, R.id.text_password_2, R.id.text_password_3, R.id.text_password_4})
    private ImageView[] mPasswordImages;

    @InjectView(id = R.id.layout_password_num)
    private View        mKeypadLayout;



    public static PasswordDialogragment getInstance(Bundle args) {
        PasswordDialogragment passwordDialogragment = new PasswordDialogragment();
        passwordDialogragment.setArguments(args);
        return passwordDialogragment;
    }

    private void initInfoText() {
        switch (mType) {
            case TYPE_CHANGE:
                mInfoText.setText(R.string.s30_error_typein_current_passcode);
                break;
            case TYPE_REMOVE:
                mInfoText.setText(R.string.s30_error_typein_current_passcode);
                break;
            case TYPE_RESETTING:
                mInfoText.setText(R.string.s30_error_typein_new_passcode);
                break;
            case TYPE_CHECK:
            case TYPE_SETTING:
                mInfoText.setText(R.string.s30_error_typein_passcode);
                break;
        }
    }

    @Click(R.id.btn_password_num_back)
    public void removePassword() {
        ImageView mPasswordImage;
        for (int i = mPasswordImages.length - 1; i >= 0; i--) {
            mPasswordImage = mPasswordImages[i];
            if (mPasswordImage.getTag() != null) {
                mPasswordImage.setTag(null);
                mPasswordImage.setVisibility(View.INVISIBLE);
                return;
            }
        }
    }

    @Click({R.id.btn_password_num_0,
            R.id.btn_password_num_1,
            R.id.btn_password_num_2,
            R.id.btn_password_num_3,
            R.id.btn_password_num_4,
            R.id.btn_password_num_5,
            R.id.btn_password_num_6,
            R.id.btn_password_num_7,
            R.id.btn_password_num_8,
            R.id.btn_password_num_9})
    public void addPassword(View view) {
        int inputNumber = NumberUtils.toInt((String) view.getTag());
        StringBuilder passwordSb = new StringBuilder();
        for (ImageView passwordImage : mPasswordImages) {
            if (passwordImage.getTag() == null) {
                passwordImage.setTag(inputNumber);
                passwordImage.setVisibility(View.VISIBLE);
                passwordSb.append(inputNumber);
                break;
            } else {
                passwordSb.append(passwordImage.getTag());
            }
        }

        if (passwordSb.length() == mPasswordImages.length) {
            mKeypadLayout.setEnabled(false);
            final String password = passwordSb.toString();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkPassword(password);
                }
            }, 100);
        }
    }

    private void resetPasswordImg() {
        for (ImageView passwordImage : mPasswordImages) {
            passwordImage.setTag(null);
            passwordImage.setVisibility(View.INVISIBLE);
        }
    }

    private void checkPassword(String password) {
        switch (mType) {
            case TYPE_SETTING:
            case TYPE_RESETTING:
                if (mFirstPassword == null) {
                    mFirstPassword = password;
                    mInfoText.setText(mType == TYPE_RESETTING ? R.string.s30_error_retypein_new_passcode : R.string.s30_error_retypein_passcode);
                    resetPasswordImg();
                } else {
                    if (isMatchPassword(mFirstPassword, password)) {
                        DataBank.getInstance().setPassword(password);
                        dismiss();
                    }
                }
                break;
            case TYPE_REMOVE:
                if (isMatchPassword(password)) {
                    DataBank.getInstance().setPassword(null);
                    dismiss();
                }
                break;
            case TYPE_CHECK:
                if (isMatchPassword(password)) {
                    dismiss();
                }
                break;
            case TYPE_CHANGE:
                if (isMatchPassword(password)) {
                    resetPasswordImg();
                    mType = TYPE_RESETTING;
                    initInfoText();
                }
        }
        mKeypadLayout.setEnabled(true);
    }

    private boolean isMatchPassword(String password) {
        if (DataBank.getInstance().isMatchPassword(password)) {
            return true;
        } else {
            Snackbar.make(getView(), R.string.s30_error_incorrect_current_passcode, Snackbar.LENGTH_SHORT).show();
            resetPasswordImg();
        }
        return false;
    }

    private boolean isMatchPassword(String firstPassword, String secondPassword) {
        if (StringUtils.equals(firstPassword, secondPassword)) {
            return true;
        } else {
            Snackbar.make(getView(), R.string.s99_error_no_match_passcode, Snackbar.LENGTH_SHORT).show();
            resetPasswordImg();
        }
        return false;
    }

    @Override
    protected void onInitDialog(View view) {
        setContent();
    }

    private void setContent() {
        mType = getArguments().getInt("type");
        initInfoText();
    }
}
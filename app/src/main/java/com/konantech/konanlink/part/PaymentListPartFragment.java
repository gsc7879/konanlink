package com.konantech.konanlink.part;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.list.adapter.PaymentListAdapter;

@PartFragmentInfo(screenName = GAConst.SCREEN_PAYMENT_LIST, layout = R.layout.part_list_payment)
public class PaymentListPartFragment extends PartFragment {
    private PaymentListAdapter mPaymentListAdapter;

    @InjectView(id = R.id.toolbar_payment_list)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        mPaymentListAdapter = new PaymentListAdapter(this, R.id.layout_list_payment, R.id.list_payment, R.layout.adapter_payment, R.layout.layout_list_no);
        mPaymentListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) view.findViewById(R.id.layout_payment_refresh), new int[]{R.color.konan});
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mPaymentListAdapter.loadList();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s26_link_payment_history);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }
}
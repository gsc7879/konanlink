package com.konantech.konanlink.part;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.konantech.konanlink.R;

import java.util.Date;

@PartFragmentInfo(layout = R.layout.part_payment_check)
public class PaymentCheckFragment extends PaymentFragment {
    private TextView mStatusText;

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        super.onAttached(attachFragmentClass, data);
        mStatusText = (TextView) data[0];
        if (mStatusText != null) {
            mStatusText.setText(R.string.s99_text_loading);
        }
        initBilling();
    }

    @Override
    public void onInitView(View view) {}

    @Override
    protected void initBillingFailed() {
        detachFragment();
    }

    @Override
    protected void initBillingFinished() {
        detachFragment();
    }

    @Override
    protected void onRegisterFailed(int type, int code, String msg) {
        detachFragment(mIsPurchase);
    }

    @Override
    protected void onRegisterSuccess(int term, String description, int paymentType, Date expireDate) {
        detachFragment(mIsPurchase);
    }
}
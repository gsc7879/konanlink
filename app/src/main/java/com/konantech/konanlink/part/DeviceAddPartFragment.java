package com.konantech.konanlink.part;

import android.Manifest;
import android.content.res.TypedArray;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.device.AddDeviceResult;
import com.konantech.konanlink.api.model.device.FileServerInfo;
import com.konantech.konanlink.api.model.device.LimitInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.InfoDialogFragment;
import com.konantech.konanlink.dialog.ListSelectDialogFragment;
import com.konantech.konanlink.list.adapter.MenuListAdapter;
import com.konantech.konanlink.list.data.MenuListViewData;
import com.konantech.konanlink.list.model.MenuListData;
import com.konantech.konanlink.list.model.MenuListSetData;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@PartFragmentInfo(
        screenName = GAConst.SCREEN_DEVICE_ADD,
        layout = R.layout.part_add_device,
        permissions = Manifest.permission.GET_ACCOUNTS)
public class DeviceAddPartFragment extends PartFragment implements MenuListAdapter.OnSelectMenuListener {
    private MenuListAdapter mMenuListAdapter;
    private MenuListSetData mMenuListSetData;

    @InjectView(id = R.id.toolbar_add_device)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        mMenuListSetData = new MenuListSetData();
        mMenuListAdapter = new MenuListAdapter(getView(), this, R.id.layout_add_device, R.id.list_add_device, R.layout.adapter_menu, R.layout.layout_list_no, mMenuListSetData);
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mMenuListSetData.setMenuListDataList(getMenuListData(-1));
        mMenuListAdapter.loadList();
    }

    private List<MenuListData> getMenuListData(int selectedIndex) {
        List<MenuListData> menuListDataList = new ArrayList<>();
        TypedArray iconArray = getResources().obtainTypedArray(R.array.device_add_icons);
        String[] titleArray  = getResources().getStringArray(R.array.device_add_titles);
        String[] valueArray  = getResources().getStringArray(R.array.device_add_value);
        for (int i = 0; i < titleArray.length; i++) {
            menuListDataList.add(new MenuListData(iconArray.getDrawable(i), titleArray[i], valueArray[i], selectedIndex == i));
        }
        return menuListDataList;
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s25_link_add_device);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }

    @Override
    public void onSelectedItem(MenuListViewData listViewData) {
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_SERVICE, GAConst.EVENT_ADD);
        LimitInfo deviceLimitInfo = DataBank.getInstance().getDeviceLimitInfo();
        int deviceKind = NumberUtils.toInt(listViewData.getValue());
        switch (deviceKind) {
            case DeviceConst.CATEGORY_PC:
                showInfoPopup(InfoDialogFragment.TYPE_PC, true);
                break;
            case DeviceConst.CATEGORY_MOBILE:
                showInfoPopup(InfoDialogFragment.TYPE_MOBILE, true);
                break;
            case DeviceConst.KIND_OUTLOOK:
                showInfoPopup(InfoDialogFragment.TYPE_OUTLOOK, true);
                break;
            case DeviceConst.KIND_WEBMAIL:
                showInfoPopup(InfoDialogFragment.TYPE_IMAP, true);
                break;
            case DeviceConst.KIND_FILESERVER:
                if (deviceLimitInfo.getRemains() > 0) {
                    reqAddFileServer();
                } else {
                    Snackbar.make(getView(), String.format(getString(R.string.s45_error_too_many_accounts), deviceLimitInfo.getLimit()), Snackbar.LENGTH_SHORT).show();
                }
                break;
            default:
                if (deviceLimitInfo.getRemains() > 0) {
                    attachFragment(R.id.layout_add_device_main, ConnectorPartFragment.class, deviceKind, "");
                } else {
                    Snackbar.make(getView(), String.format(getString(R.string.s45_error_too_many_accounts), deviceLimitInfo.getLimit()), Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void reqAddFileServer() {
        new DeviceApiManager().reqFileServerList(getActivity(), mSubscriptions, DataBank.getInstance().getUsersEmail(), 0, 100, new Result.OnResultListener<ResultList<FileServerInfo>>() {
            @Override
            public void onSuccess(ResultList<FileServerInfo> data) {
                List<FileServerInfo> list = data.getList();
                if (list.isEmpty()) {
                    Snackbar.make(getView(), R.string.s45_error_no_server, Snackbar.LENGTH_LONG).show();
                    return;
                }

                int fileServerCount = list.size();
                String[] titles = new String[fileServerCount];
                String[] values = new String[fileServerCount];
                int[] icons     = new int[fileServerCount];
                FileServerInfo fileServerInfo;
                for (int i = 0; i < fileServerCount; i++) {
                    fileServerInfo = list.get(i);
                    titles[i] = fileServerInfo.getDeviceName();
                    values[i] = fileServerInfo.getDeviceKey();
                    icons[i]  = R.drawable.ic_fileserver;
                }
                showFileServerListDialog(titles, values, icons);
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), R.string.s45_error_no_read_list, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void showFileServerListDialog(String[] titles, String[] keys, int[] icons) {
        DialogUtils.showListDialog(
                getString(R.string.s45_dialog_title_server_list),
                titles,
                keys,
                icons,
                getFragmentManager(), new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        if (isPositive) {
                            String deviceName = (String) dataMap.get(ListSelectDialogFragment.DATA_TITLE);
                            String deviceKey  = (String) dataMap.get(ListSelectDialogFragment.DATA_VALUE);
                            regAddDevice(deviceKey, deviceName);
                        }
                    }
                });
    }

    private void regAddDevice(final String deviceKey, final String deviceName) {
        new DeviceApiManager().reqAddDevice(getActivity(), mSubscriptions, deviceKey, deviceName, DeviceConst.KIND_FILESERVER, "", "", "", IndexingConst.STATUS_REGULAR, new Result.OnResultListener<AddDeviceResult>() {
            @Override
            public void onSuccess(AddDeviceResult data) {
                Snackbar.make(getView(), String.format(getString(R.string.s45_toast_added_sucessfully), deviceName), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onFailed(int code, String msg) {
                if (code == ErrorConst.ERR_3005_LIMIT_REGISTERED_DEVICE) {
                    Snackbar.make(getView(), String.format(getString(R.string.s19_error_accounts), DataBank.getInstance().getDeviceLimitInfo().getLimit()), Snackbar.LENGTH_LONG).show();
                } else if (code == ErrorConst.ERR_3001_ALREADY_REGISTERED_DEVICE) {
                    Snackbar.make(getView(), String.format(getString(R.string.s45_error_server_already_added), deviceName), Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(getView(), R.string.s98_error_unknown, Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showInfoPopup(int type, boolean agentCheck) {
        DialogUtils.showInfoDialog(getFragmentManager(), agentCheck && !DataBank.getInstance().isHostSetup() ? InfoDialogFragment.TYPE_AGENT : type);
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(ConnectorPartFragment.class)) {
            if ((boolean) data[1]) {
                Snackbar.make(getView(), R.string.s44_toast_device_added, Snackbar.LENGTH_SHORT).show();
                detachFragment();
            } else {
                String error = getString(R.string.s44_error_add_failed);
                switch ((int) data[2]) {
                    case ErrorConst.ERROR_CONNECT_CANCEL:
                        break;
                    case ErrorConst.ERROR_CONNECT_NORMAL:
                        error = getString(R.string.s99_error_device_issue);
                        break;
                    case ErrorConst.ERROR_CONNECT_CERT:
                        error = getString(R.string.s99_error_no_3rd_certification);
                        break;
                    case ErrorConst.ERROR_CONNECT_LIMIT:
                        error = String.format(getString(R.string.s19_error_accounts), DataBank.getInstance().getDeviceLimitInfo().getLimit());
                        break;
                    case ErrorConst.ERROR_CONNECT_ACCOUNT:
                        error = getString(R.string.s98_error_wrong_account);
                        break;
                    case ErrorConst.ERROR_CONNECT_WAS:
                        error = getString(R.string.was_error_9999);
                        break;
                }
                Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            showErrorDialog(getString(R.string.s98_dialog_text_for_accounts));
            return false;
        }
        return true;
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                detachFragment();
            }
        });
    }
}
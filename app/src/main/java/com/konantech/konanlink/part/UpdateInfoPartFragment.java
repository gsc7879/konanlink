package com.konantech.konanlink.part;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AccountApiManager;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.api.model.device.DeviceResultList;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.service.GCMRegistrationService;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Map;

@PartFragmentInfo(layout = R.layout.layout_update_info, isUseBackPressEvent = false)
public class UpdateInfoPartFragment extends PartFragment {
    public static final int TYPE_USER_INFO = 0;
    public static final int TYPE_DEVICE_INFO = 1;
    public static final int TYPE_ALL = 2;

    private int      mLoadingType;
    private TextView mStatusText;
    private Object   mByPassData;
    private Dialog   mGoogleAPIErrorDialog;

    @Override
    public void onInitView(View view) {
    }

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mLoadingType = (int) data[0];
        mStatusText = data.length > 1 ? (TextView) data[1] : null;
        mByPassData = data.length > 2 ? data[2] : null;
    }

    private void startUpdateInfo() {
        if (mLoadingType == TYPE_DEVICE_INFO) {
            requestDeviceInfo();
        } else {
            requestUserInfo();
        }
    }

    private void setText(int textResId) {
        if (mStatusText != null) {
            mStatusText.setText(textResId);
        }
    }

    private void requestUserInfo() {
        new AccountApiManager().reqUsersInfo(getActivity(), mSubscriptions, new Result.OnResultListener<AccountInfo>() {
            @Override
            public void onSuccess(AccountInfo accountInfo) {
                DataBank.getInstance().setAccountInfo(accountInfo);

                if (mLoadingType != TYPE_USER_INFO) {
                    requestDeviceInfo();
                } else {
                    detachFragment(true, mByPassData);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
                detachFragment(false, mByPassData);
            }
        });
    }

    private void requestDeviceInfo() {
        new DeviceApiManager().reqDeviceList(getActivity(), mSubscriptions, new Result.OnResultListener<DeviceResultList<DeviceInfo>>() {
            @Override
            public void onSuccess(DeviceResultList<DeviceInfo> data) {
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        return getRegId(getActivity());
                    }

                    @Override
                    protected void onPostExecute(String regId) {
                        if (updateNotificationId(regId)) {
                            detachFragment(true, mByPassData);
                        }
                    }
                }.execute();
            }

            @Override
            public void onFailed(int code, String msg) {
                detachFragment(false, mByPassData);
            }
        });
    }

    public boolean updateNotificationId(String regId) {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo();
        String notificationId = deviceInfo == null ? null : deviceInfo.getNotificationId();
        if (StringUtils.isBlank(regId) || !StringUtils.equals(notificationId, regId)) {
            return reqToken(getActivity());
        }
        return true;
    }

    private String getRegId(Context context) {
        try {
            String senderId = context.getString(R.string.gcm_defaultSenderId);
            return InstanceID.getInstance(context).getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
        } catch (IOException e) {
            return null;
        }
    }

    private boolean reqToken(Context context) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result == ConnectionResult.SUCCESS) {
            context.startService(new Intent(context, GCMRegistrationService.class));
        } else {
            if (googleAPI.isUserResolvableError(result)) {
                mGoogleAPIErrorDialog = googleAPI.getErrorDialog(getActivity(), result, 9000, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        showErrorDialog(getString(R.string.s95_dialog_text_latest_playservice));
                    }
                });
                mGoogleAPIErrorDialog.show();
                return false;
            }
        }
        return true;
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                System.exit(0);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleAPIErrorDialog != null) {
            mGoogleAPIErrorDialog.dismiss();
        }
        startUpdateInfo();
    }
}
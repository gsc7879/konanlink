package com.konantech.konanlink.part;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.list.adapter.DirectoryListAdapter;
import com.konantech.konanlink.list.model.DirectorySetData;
import com.konantech.konanlink.model.directory.DirectoryInfo;

import org.apache.commons.lang3.StringUtils;

@PartFragmentInfo(screenName = GAConst.SCREEN_FOLDER, layout = R.layout.part_directory)
public class DirectoryPartFragment extends PartFragment {
    private DirectoryListAdapter mDirectoryListAdapter;
    private DeviceInfo mDeviceInfo;
    private DirectorySetData     mDirectorySetData;

    @InjectView(id = R.id.text_list_directory_path)
    private TextView mPathText;

    @InjectView(id = R.id.toolbar_explorer)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        mDirectorySetData     = new DirectorySetData();
        mDirectoryListAdapter = new DirectoryListAdapter(this, R.id.layout_list_directory, R.id.list_directory, R.layout.adapter_directory, R.layout.layout_list_no, mDirectorySetData);
        mDirectoryListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) view.findViewById(R.id.layout_explorer_refresh), new int[]{R.color.konan});
        setToolbar();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s40_title_file_explorer);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mDeviceInfo = (DeviceInfo) data[0];
        mDirectorySetData.setDeviceInfo(mDeviceInfo);
        if (data.length > 0) {
            mDirectorySetData.setCurrentPath((String) data[1]);
        }
        mDirectoryListAdapter.loadList();
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(DirectoryInfoPartFragment.class)) {
            if (data.length > 0 && data[0] instanceof DirectoryInfo) {
                mDirectoryListAdapter.endLoadList(data.length > 0 ? (DirectoryInfo) data[0] : null);
            } else {
                detachFragment();
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (!mDirectoryListAdapter.isLoading()) {
            detachFragment();
        }
        return true;
    }

    public void setPathText() {
        String folder  = mDirectorySetData.getFolder();
        String account = mDeviceInfo.getAccount();
        mPathText.setText(StringUtils.isNotBlank(folder) ? folder : mDeviceInfo.getName() + (StringUtils.isBlank(account) ? "" : String.format("(%s)", mDeviceInfo.getAccount())));
    }
}
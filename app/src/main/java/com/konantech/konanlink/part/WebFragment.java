package com.konantech.konanlink.part;

import android.graphics.Bitmap;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;

import org.apache.commons.lang3.StringUtils;

@PartFragmentInfo(layout = R.layout.layout_web)
public class WebFragment extends PartFragment implements SwipeRefreshLayout.OnRefreshListener {
    @InjectView(id = R.id.web_main)
    private WebView mWebView;

    @InjectView(id = R.id.refresh_web)
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @InjectView(id = R.id.toolbar_web)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.konan);
    }

    private void setToolbar(String title) {
        mToolbar.setTitle(title);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detachFragment();
            }
        });
    }

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        String content = (String) data[0];
        setWebView(content);
        if (data.length > 1) {
            String title = (String) data[1];
            setToolbar(title);
        } else {
            mToolbar.setVisibility(View.GONE);
        }
    }

    private void setWebView(String content) {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        if (StringUtils.startsWith(content, "http")) {
            mWebView.loadUrl(content);
        } else {
            mWebView.loadData(content, "text/html", "UTF-8");
        }
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mSwipeRefreshLayout.setRefreshing(true);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
                WebView newWebView = new WebView(getActivity());
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();
                return true;
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            detachFragment();
        }
        return true;
    }

    @Override
    public void onRefresh() {
        mWebView.reload();
    }
}
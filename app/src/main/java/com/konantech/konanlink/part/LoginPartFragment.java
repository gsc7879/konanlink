package com.konantech.konanlink.part;

import android.os.Build;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AccountApiManager;
import com.konantech.konanlink.api.manager.PaymentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.account.LoginResult;
import com.konantech.konanlink.api.model.payment.PaymentResult;
import com.konantech.konanlink.api.service.PaymentApiService;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

@PartFragmentInfo(layout = R.layout.part_login, isUseBackPressEvent = false)
public class LoginPartFragment extends PartFragment {
    @InjectView(id = R.id.text_loading_content)
    private TextView mStatusText;

    @InjectView(id = R.id.layout_login_loading)
    private View mLoginLayout;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        String email    = (String) data[0];
        String password = (String) data[1];

        if (data.length > 2) {
            mStatusText = (TextView) data[2];
            mLoginLayout.setVisibility(View.GONE);
        }
        mStatusText.setText(R.string.s99_text_loading);
        login(email, password);
    }

    private void login(String email, String password) {
        new AccountApiManager().reqLogin(getActivity(), mSubscriptions, email, password, new Result.OnResultListener<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                DataBank.getInstance().setUserData(loginResult.getApiKey(), loginResult.getEmail());
                defaultIndexingFolder();
                if (!loginResult.isGiftRegistered() && StringUtils.equalsIgnoreCase(Build.BRAND, "SAMSUNG")) {
                    registerGift();
                } else {
                    detachFragment();
                }
            }

            @Override
            public void onFailed(int code, String msg) {
                detachFragment(code, msg);
            }
        });
    }

    private void registerGift() {
        new PaymentApiManager().reqRegister(getActivity(), mSubscriptions, PaymentApiService.TYPE_GIFT_SAMSUNG, DataBank.getInstance().getDeviceKey(), 0, 0, Build.BRAND, new Result.OnResultListener<PaymentResult>() {
            @Override
            public void onSuccess(PaymentResult result) {
                AccountInfo accountInfo = DataBank.getInstance().getAccountInfo();
                accountInfo.setExpireRemainTime(result.getExpireRemainTime());
                accountInfo.setExpireDate(result.getExpireDate());

                Snackbar.make(getView(), R.string.s91_toast_samsung_benefit, Snackbar.LENGTH_LONG).show();
                detachFragment();
            }

            @Override
            public void onFailed(int code, String msg) {
                detachFragment();
            }
        });
    }

    private void defaultIndexingFolder() {
        StringBuilder sb = new StringBuilder();
        for (String folder : IndexingConst.DEFAULT_INDEXING_FOLDER) {
            File indexingFolder = FileUtils.getFile(Environment.getExternalStorageDirectory(), folder);
            if (indexingFolder.exists()) {
                sb.append(indexingFolder.getAbsolutePath());
                sb.append(",");
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        DataBank.getInstance().saveIndexingFolders(sb.toString());
    }
}
package com.konantech.konanlink.part;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.directory.ReqAgentDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqDropBoxDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqGMailDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqGoogleDriveDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqLocalDirectoryInfoTask;
import com.konantech.konanlink.device.directory.ReqOneDriveDirectoryInfoTask;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.Map;

@PartFragmentInfo(layout = R.layout.part_directory_loading)
public class DirectoryInfoPartFragment extends PartFragment implements WorkTask.OnTaskListener {
    private DeviceInfo           mDeviceInfo;
    private ReqDirectoryInfoTask mReqDirectoryInfoTask;
    private String               mDirectoryPath;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mDeviceInfo    = (DeviceInfo) data[0];
        mDirectoryPath = (String)     data[1];
        expireCheck();
    }

    public void reqDirectoryInfo(int kind, Object... data) {
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                mReqDirectoryInfoTask = new ReqAgentDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mSubscriptions, mDirectoryPath, mDeviceInfo.getHostInfo().getKey());
                break;
            case DeviceConst.KIND_ANDROID:
                mReqDirectoryInfoTask = new ReqLocalDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mDirectoryPath);
                break;
            case DeviceConst.KIND_DROPBOX:
                mReqDirectoryInfoTask = new ReqDropBoxDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mDirectoryPath);
                break;
            case DeviceConst.KIND_GOOGLEDRIVE:
                mReqDirectoryInfoTask = new ReqGoogleDriveDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mDirectoryPath);
                break;
            case DeviceConst.KIND_ONEDRIVE:
                mReqDirectoryInfoTask = new ReqOneDriveDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mDirectoryPath);
                break;
            case DeviceConst.KIND_GMAIL:
                mReqDirectoryInfoTask = new ReqGMailDirectoryInfoTask(0, getActivity(), this);
                mReqDirectoryInfoTask.execute(data[0], mDirectoryPath);
                break;
        }
    }

    private void checkConnector(int errorCode, Intent intent) {
        attachFragment(R.id.layout_directory_loading, ConnectorPartFragment.class, mDeviceInfo.getKind(), mDeviceInfo.getKey(), null, true, errorCode, intent);
    }

    private void expireCheck() {
        AccountInfo accountInfo = DataBank.getInstance().getAccountInfo();
        if (accountInfo.isExpired()) {
            DialogUtils.showConverseCenterDialog(
                    String.format(getString(R.string.s24_dialog_title_premium_upgrade)),
                    String.format(getString(R.string.s19_dialog_text_premium_upgrade), 3),
                    R.string.s99_dialog_link_upgrade_later,
                    R.string.s99_dialog_btn_premium_upgrade,
                    getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            if (isPositive) {
                                GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_POPUP);
                                attachFragment(R.id.layout_directory_loading, PaymentReqPartFragment.class);
                            } else {
                                detachFragment();
                            }
                        }
                    });
        } else {
            checkConnector(ErrorConst.ERROR_NONE, null);
        }
    }

    @Override
    public void onStartTask(int callBackId) {
    }

    @Override
    public void onEndTask(int callBackId, Object object) {
        if (object != null) {
            mReqDirectoryInfoTask = null;
            detachFragment((DirectoryInfo) object);
        } else {
            onCancelTask(callBackId, ErrorConst.ERROR_UNKNOWN, null);
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(PaymentReqPartFragment.class)) {
            if (data.length > 0 && (boolean) data[0]) {
                expireCheck();
            } else {
                detachFragment();
            }
        } else if (detachedClass.equals(ConnectorPartFragment.class)) {
            if (data.length > 1 && (boolean) data[1]) {
                reqDirectoryInfo(mDeviceInfo.getKind(), mDeviceInfo.getConnector());
            } else {
                detachFragment(data[1]);
            }
        }
    }

    @Override
    public void onCancelTask(int callBackId, int cancelCode, Object data) {
        if (!isAdded()) {
            return;
        }
        String error = getString(R.string.s99_error_cannot_read);
        if (data == null) {
            switch (cancelCode) {
                case ErrorConst.ERROR_SYSTEM:
                    error = getString(R.string.s99_error_device_issue);
                    break;
                case ErrorConst.ERROR_AUTH:
                    error = getString(R.string.s99_error_no_3rd_certification);
                    break;
                case ErrorConst.ERROR_RECOVERABLE:
                    mDeviceInfo.setConnector(null);
                    checkConnector(cancelCode, (Intent) data);
                    return;
                case ErrorConst.ERROR_CONNECT:
                    error = getString(R.string.s18_text_disconnected);
                    break;
                case ErrorConst.ERROR_UNLINKED:
                    mDeviceInfo.setConnector(null);
                    checkConnector(cancelCode, null);
                    return;
            }
        } else {
            error = data.toString();
        }

        Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
        mReqDirectoryInfoTask = null;
        onBackPressed();
    }

    @Override
    public void onTaskProgress(int callBackId, Object... params) {
    }

    @Override
    public boolean onBackPressed() {
        if (mReqDirectoryInfoTask != null) {
            mReqDirectoryInfoTask.cancel(true);
        } else {
            detachFragment();
        }
        return true;
    }
}
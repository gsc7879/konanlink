package com.konantech.konanlink.part;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.UnitUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.system.Rules;
import com.konantech.konanlink.api.service.PaymentApiService;
import com.konantech.konanlink.api.service.SystemApiService;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.utils.SkuItem;
import com.konantech.konanlink.utils.billing.IabHelper;
import com.konantech.konanlink.utils.billing.IabResult;
import com.konantech.konanlink.utils.billing.Purchase;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

@PartFragmentInfo(screenName = GAConst.SCREEN_PAYMENT, layout = R.layout.part_payment_request)
public class PaymentReqPartFragment extends PaymentFragment {
    public static final int REQUEST_PURCHASE = 1001;
    public static final int COUPON_DIGIT     = 12;

    @InjectView(id = R.id.edit_payment_coupon)
    private EditText mCouponEdit;

    @InjectView(id = R.id.text_payment_title)
    private TextView mTitleText;

    @InjectView(id = R.id.text_payment_description)
    private TextView mDescriptionText;

    @InjectView(id = R.id.text_payment_coupon_error)
    private TextView mCouponErrorText;

    @InjectView(id = R.id.layout_payment_items)
    private ViewGroup mSkuItemLayout;

    @InjectView(id = R.id.img_payment_coupon_check)
    private View mCouponCheck;

    @InjectView(id = R.id.toolbar_payment)
    private Toolbar mToolbar;

    @InjectView(id = R.id.check_payment_terms)
    private CheckBox mTermsCheck;

    @Override
    public void onInitView(View view) {
        mCouponEdit.addTextChangedListener(passwordWatcher);
        setTermCheck();
    }

    private TextWatcher passwordWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            for (int i = 0; i < s.length(); i++) {
                if (s.length() > 0 && !checkCouponCode(s.charAt(i))) {
                    mCouponErrorText.setText(R.string.was_error_4202);
                    return;
                } else {
                    mCouponErrorText.setText("");
                }
            }
            mCouponCheck.setSelected(s.length() == COUPON_DIGIT);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    private boolean checkCouponCode(char c) {
        return isAlphabetLower(c) || isAlphabetUpper(c) || isNumber(c);
    }

    private boolean isAlphabetLower(char c) {
        return (0x61 <= c && c <= 0x7A);
    }

    private boolean isAlphabetUpper(char c) {
        return (0x41 <= c && c <= 0x5A);
    }

    private boolean isNumber(char c) {
        return (0x30 <= c && c <= 0x39);
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        super.onAttached(attachFragmentClass, data);
        setToolbar();
        setAccountInfo();
    }

    private void setToolbar() {
        mToolbar.setTitle(mIsUpgrade ? getString(R.string.s27_title_upgrade_premium) : getString(R.string.s27_title_extend_premium));
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onDataReceived(Class sendFragmentClass, int dataType, Object... data) {
        switch (dataType) {
            case DataIdConst.CHANGED_USERS_INFO:
                break;
        }
    }

    @Override
    protected void initBillingFailed() {
        Snackbar.make(getView(), R.string.s26_error_initialize_payment, Snackbar.LENGTH_SHORT).show();
        detachFragment(mIsPurchase);
    }

    @Override
    protected void initBillingFinished() {
        setSkuLayout();
    }

    @Override
    protected void onRegisterFailed(int type, int code, String msg) {
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_BUY, GAConst.LABEL_FAIL);
        if (type == PaymentApiService.TYPE_COUPON) {
            initCouponEdit(msg);
        } else {
            Snackbar.make(getView(), R.string.s27_dialog_error_cannot_apply_server, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onRegisterSuccess(int term, String description, int paymentType, Date expireDate) {
        String message = "";
        switch (paymentType) {
            case PaymentApiService.TYPE_COUPON:
                message = getString(R.string.s27_dialog_text_extend_coupon);
                break;
            case PaymentApiService.TYPE_GOOGLE:
                String period = getPeriodString(term);
                message = MessageFormat.format(getString(mIsUpgrade ? R.string.s27_dialog_text_upgrade : R.string.s27_dialog_text_extend), period, description, period);
                break;
        }
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_BUY, GAConst.LABEL_SUCCEED);
        detachFragment(mIsPurchase);
    }

    private void setAccountInfo() {
        setLayout();
        initBilling();
    }

    private void setSkuLayout() {
        List<SkuItem> responseList = mIABillingManager.getSKUs(getActivity(), getSkuList(), IabHelper.ITEM_TYPE_INAPP);
        if (responseList == null || responseList.size() == 0) {
            Snackbar.make(getView(), R.string.s26_error_initialize_payment, Snackbar.LENGTH_SHORT).show();
            return;
        }
        mSkuItemLayout.removeAllViews();
        for (SkuItem skuItem : responseList) {
            TextView skuItemBtn = new TextView(getActivity());
            skuItemBtn.setText(String.format(getString(R.string.s27_btn_pay), skuItem.getDescription(), skuItem.getPrice()));
            skuItemBtn.setTag(skuItem);
            skuItemBtn.setGravity(Gravity.CENTER);
            skuItemBtn.setTextColor(Color.WHITE);
            skuItemBtn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            skuItemBtn.setBackgroundResource(R.color.konan);
            skuItemBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SkuItem selectedSkuItem = (SkuItem) view.getTag();
                    if (selectedSkuItem == null) {
                        Snackbar.make(getView(), R.string.s26_error_initialize_payment, Snackbar.LENGTH_SHORT).show();
                    } else {
                        requestPayment(PaymentApiService.TYPE_GOOGLE, selectedSkuItem);
                    }
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, UnitUtils.convertDpToPixel(getActivity(), 50));
            params.setMargins(0, 10, 0, 0);
            mSkuItemLayout.addView(skuItemBtn, params);
        }
    }

    private void setLayout() {
        int titleTextResId;
        if (mIsUpgrade) {
            titleTextResId = R.string.s27_title_upgrade_premium;
        } else {
            titleTextResId = R.string.s27_title_extend_premium;
        }
        mTitleText.setText(titleTextResId);
        mDescriptionText.setText(String.format(getString(R.string.s27_text_premium_benefit), 3));
    }

    @Click(R.id.btn_payment_terms_detail)
    public void showTerms() {
        new SystemApiManager().reqRules(getActivity(), mSubscriptions, SystemApiService.RULES_TYPE_PAYMENT, new Result.OnResultListener<Rules>() {
            @Override
            public void onSuccess(Rules data) {
                attachFragment(R.id.layout_payment_main, WebFragment.class, data.getUrl(), getString(R.string.s38_link_pricing_terms));
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), R.string.s27_error_no_pricing_terms, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Click(R.id.btn_payment_coupon)
    public void paymentCoupon() {
        if (!mCouponCheck.isSelected()) {
            initCouponEdit(getString(R.string.was_error_4202));
            return;
        }
        String couponCode = mCouponEdit.getText().toString();
        registerPayment(PaymentApiService.TYPE_COUPON, couponCode, 0, couponCode);
    }

    private void initCouponEdit(String msg) {
        mCouponEdit.setText("");
        mCouponEdit.requestFocus();
        mCouponErrorText.setText(msg);
    }

    private void setTermCheck() {
        mTermsCheck.setChecked(!DataBank.getInstance().isKorea());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PURCHASE) {
            if (!mIABillingManager.handleActivityResult(requestCode, resultCode, data)) {
                Snackbar.make(getView(), mIsUpgrade ? R.string.s27_error_fail_upgrade : R.string.s27_error_fail_extend, Snackbar.LENGTH_SHORT).show();
                detachFragment(mIsPurchase);
            }
        } else {
            detachFragment(mIsPurchase);
        }
    }

    private void requestPayment(final int paymentType, final SkuItem skuItem) {
        if (!mTermsCheck.isChecked()) {
            Snackbar.make(getView(), R.string.s27_toast_agree_pricing_terms, Snackbar.LENGTH_SHORT).show();
            return;
        }
        String developerPayload = DataBank.getInstance().getUsersEmail();
        if (!mIABillingManager.buyProduct(getActivity(), REQUEST_PURCHASE, skuItem.getProductId(), IabHelper.ITEM_TYPE_INAPP, developerPayload, new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                registerPayment(paymentType, info.getToken(), skuItem);
            }
        })) {
            Snackbar.make(getView(), mIsUpgrade ? R.string.s27_error_fail_upgrade : R.string.s27_error_fail_extend, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    protected boolean onBackPressed() {
        detachFragment(mIsPurchase);
        return true;
    }
}
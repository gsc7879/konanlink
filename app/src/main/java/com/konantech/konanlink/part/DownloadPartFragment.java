package com.konantech.konanlink.part;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.IntentUtils;
import com.dignara.lib.utils.NetworkUtils;
import com.dignara.lib.utils.TextUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.DownloadConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.FileConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.constant.NotificationConst;
import com.konantech.konanlink.constant.TypeConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.download.origin.ReqAgentDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqDropBoxDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqEvernoteDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqGMailDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqGoogleDriveDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqLocalDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqOneDriveDownloadTask;
import com.konantech.konanlink.device.download.origin.ReqWebMailDownloadTask;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.service.NotificationBroadcastReceiver;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Map;

@PartFragmentInfo(
        layout      = R.layout.part_downloader,
        permissions = Manifest.permission.WRITE_EXTERNAL_STORAGE)
public class DownloadPartFragment extends PartFragment implements WorkTask.OnTaskListener {
    private DownloadAttachData mDownloadData;
    private File               mDownloadFile;
    private ReqDownloadTask    mReqDownloadTask;
    private Animation          mAnimation;


    @InjectView(id = R.id.img_loading_search)
    private ImageView mSearchImg;

    @InjectView(id = R.id.text_loading_content)
    private TextView mDownloadStatusText;

    @InjectView(id = R.id.btn_download_cancel)
    private TextView mDownloadCancelBtn;

    @Override
    public void onInitView(View view) {
        setContent();
        startLoading();
    }

    private void startLoading() {
        if (mAnimation == null) {
            mAnimation = new CircleAnimation(mSearchImg, AnimationConst.SMALL_RADIOUS);
            mAnimation.setDuration(AnimationConst.SMALL_DURATION);
            mAnimation.setRepeatCount(Animation.INFINITE);
        }
        mSearchImg.startAnimation(mAnimation);
    }

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mDownloadData = (DownloadAttachData) data[0];
        startDownload();
    }

    private void setContent() {
        mDownloadStatusText.setText(R.string.s99_text_loading);
        mDownloadCancelBtn.setVisibility(View.INVISIBLE);
    }

    private void expireCheck() {
        AccountInfo accountInfo = DataBank.getInstance().getAccountInfo();
        if (accountInfo.isExpired() && isAbleDownload(mDownloadData.getFileModifyTime())) {
            DialogUtils.showConverseCenterDialog(
                    String.format(getString(R.string.s24_dialog_title_premium_upgrade)),
                    String.format(getString(R.string.s24_dialog_text_premium_upgrade), 3, 3),
                    R.string.s99_dialog_link_upgrade_later,
                    R.string.s99_dialog_btn_premium_upgrade,
                    getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            if (isPositive) {
                                GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_POPUP);
                                attachFragment(R.id.layout_downloader, PaymentReqPartFragment.class);
                            } else {
                                detachFragment();
                            }
                        }
                    });
        } else {
            checkDownload();
        }
    }

    private boolean isAbleDownload(long modifyTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -3);
        return modifyTime < calendar.getTimeInMillis();
    }

    private void checkDownload() {
        if (!mDownloadData.getDeviceInfo().isMyDevice() && NetworkUtils.isMobileConnected(getActivity())) {
            DialogUtils.showConverseDialog(
                    "",
                    getString(R.string.s41_dialog_text_incur_charges),
                    R.string.s41_dialog_btn_block,
                    R.string.s41_dialog_btn_allow,
                    getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            if (isPositive) {
                                checkConnector(ErrorConst.ERROR_NONE, null);
                            } else {
                                detachFragment();
                            }
                        }
                    });
        } else {
            checkConnector(ErrorConst.ERROR_NONE, null);
        }
    }

    private void checkConnector(int errorCode, Intent intent) {
        attachFragment(R.id.layout_downloader, ConnectorPartFragment.class, mDownloadData.getDeviceInfo().getKind(), mDownloadData.getDeviceInfo().getKey(), null, true, errorCode, intent);
    }

    public void reqDownload(int kind, Object... data) {
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                checkSize((AgentApiManager) data[0]);
                break;
            case DeviceConst.KIND_ANDROID:
                if (mDownloadData.getDeviceInfo().isMyDevice()) {
                    mReqDownloadTask = new ReqLocalDownloadTask(0, mDownloadFile, this);
                    mReqDownloadTask.execute(data[0], mDownloadData.getFilePath());
                } else {
                    checkSize((AgentApiManager) data[0]);
                }
                break;
            case DeviceConst.KIND_DROPBOX:
                mReqDownloadTask = new ReqDropBoxDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath());
                break;
            case DeviceConst.KIND_GOOGLEDRIVE:
                mReqDownloadTask = new ReqGoogleDriveDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath());
                break;
            case DeviceConst.KIND_EVERNOTE:
                mReqDownloadTask = new ReqEvernoteDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath(), mDownloadData.getFileType());
                break;
            case DeviceConst.KIND_WEBMAIL:
                mReqDownloadTask = new ReqWebMailDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath(), mDownloadData.getFolderPath());
                break;
            case DeviceConst.KIND_ONEDRIVE:
                mReqDownloadTask = new ReqOneDriveDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath());
                break;
            case DeviceConst.KIND_GMAIL:
                mReqDownloadTask = new ReqGMailDownloadTask(0, mDownloadFile, this);
                mReqDownloadTask.execute(data[0], mDownloadData.getFilePath());
                break;
        }
    }

    private void checkSize(final AgentApiManager agentApiManager) {
        if (mDownloadData.getFileSize() > DownloadConst.LIMIT_WORN && mDownloadData.getDeviceInfo().getConnectType() == NetConst.TYPE_CONNECT_RELAY) {
            DialogUtils.showConverseDialog(
                    "",
                    getResources().getString(R.string.s96_dialog_text_over_size),
                    R.string.s99_dialog_btn_cancel,
                    R.string.s96_dialog_btn_download,
                    getFragmentManager(),
                    new DialogController.OnDismissListener() {
                        @Override
                        public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                            if (isPositive) {
                                startAgentDownload(agentApiManager);
                            }
                            detachFragment();
                        }
                    });
        } else {
            startAgentDownload(agentApiManager);
        }
    }

    private void startAgentDownload(AgentApiManager agentApiManager) {
        mReqDownloadTask = new ReqAgentDownloadTask(0, mDownloadFile, this);
        mReqDownloadTask.execute(agentApiManager, getView().getContext(), mSubscriptions, mDownloadData.getDownloadURL());
    }

    public void connectFail(int failCode) {
        int errorMsgResId = 0;
        switch (failCode) {
            case ErrorConst.ERROR_CONNECT_CANCEL:
                break;
            case ErrorConst.ERROR_CONNECT_NORMAL:
                errorMsgResId = R.string.s99_error_device_issue;
                break;
            case ErrorConst.ERROR_CONNECT_CERT:
                errorMsgResId = R.string.s99_error_no_3rd_certification;
                break;
        }

        if (errorMsgResId != 0) {
            Snackbar.make(getView(), errorMsgResId, Snackbar.LENGTH_SHORT).show();
        }
        detachFragment();
    }

    private void startDownload() {
        String downloadFilePath = mDownloadData.getFileTitle();
        int kind = mDownloadData.getDeviceInfo().getKind();
        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                if (StringUtils.equals(mDownloadData.getFileType(), "mail")) {
                    downloadFilePath = String.format("%s.%s", mDownloadData.getFileTitle(), "html");
                }
                break;
            case DeviceConst.KIND_WEBMAIL:
                if (StringUtils.equals(mDownloadData.getFileType(), "mail")) {
                    downloadFilePath = String.format("%s.%s", mDownloadData.getFilePath(), "eml");
                }
                break;
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_EVERNOTE:
                break;
            case DeviceConst.KIND_GMAIL:
                downloadFilePath = String.format("%s.%s", mDownloadData.getFilePath(), "eml");
                break;
        }
        downloadFilePath = StringUtils.substring(downloadFilePath, -100);
        mDownloadFile = TextUtils.createUniqueFile(FileUtils.getFile(mDownloadData.getDownloadTarget() == null ? FileConst.FOLDER_DOWNLOAD : mDownloadData.getDownloadTarget(), FilenameUtils.getName(downloadFilePath)).getAbsolutePath(), "(%d)");
        expireCheck();
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(PaymentReqPartFragment.class)) {
            if (data.length > 0 && (boolean) data[0]) {
                startDownload();
            } else {
                detachFragment();
            }
        } else if (detachedClass.equals(ConnectorPartFragment.class)) {
            if (mDownloadData.getDeviceInfo().getConnector() != null) {
                reqDownload(mDownloadData.getDeviceInfo().getKind(), mDownloadData.getDeviceInfo().getConnector());
            } else {
                connectFail((int) data[2]);
            }
        }
    }

    @Click(R.id.btn_download_cancel)
    public void cancelDownload() {
        mDownloadStatusText.setText(R.string.s42_dialog_text_canceling);
        onBackPressed();
    }

    @Override
    public void onStartTask(int callBackId) {
        mDownloadStatusText.setText(R.string.s99_text_loading);
        mDownloadCancelBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEndTask(int callBackId, Object downloadFile) {
        if (downloadFile != null) {
            if (downloadFile instanceof String) {
                IntentUtils.openingUrl(getActivity(), (String) downloadFile);
                mReqDownloadTask = null;
                onBackPressed();
            } else if (downloadFile instanceof File) {
                openFile((File) downloadFile);
            }
        } else {
            onCancelTask(callBackId, ErrorConst.ERROR_UNKNOWN, null);
        }

        if (mDownloadData.getDeviceInfo().getKind() == DeviceConst.KIND_WEBMAIL) {
            mDownloadData.getDeviceInfo().setConnector(null);
        }
    }

    private void openFile(File downloadFile) {
        if (downloadFile != null && downloadFile.exists()) {
            if (mDownloadData.getDownloadTarget() != null) {
                showNotification(downloadFile);
            } else {
                if (mDownloadData.isShare()) {
                    try {
                        IntentUtils.sharingFile(getActivity(), downloadFile);
                    } catch (FileNotFoundException e) {
                        Snackbar.make(getView(), getString(R.string.s99_error_cannot_save), Snackbar.LENGTH_LONG).show();
                    } catch (ActivityNotFoundException e) {
                        if (FilenameUtils.isExtension(downloadFile.getName(), "")) {
                            Snackbar.make(getView(), getString(R.string.s99_error_shareapp), Snackbar.LENGTH_LONG).show();
                        } else {
                            showSearchMarketDialog(getActivity(), "", getString(R.string.s99_dialog_text_shareapp));
                        }
                    }
                } else {
                    try {
                        IntentUtils.openingFile(getActivity(), downloadFile);
                    } catch (FileNotFoundException e) {
                        Snackbar.make(getView(), getString(R.string.s99_error_cannot_save), Snackbar.LENGTH_LONG).show();
                    } catch (ActivityNotFoundException e) {
                        if (FilenameUtils.isExtension(downloadFile.getName(), "")) {
                            Snackbar.make(getView(), getString(R.string.s99_error_openapp), Snackbar.LENGTH_LONG).show();
                        } else {
                            showSearchMarketDialog(getActivity(), "", getString(R.string.s19_dialog_text_openapp));
                        }
                    }
                }
            }
        }
        mReqDownloadTask = null;
        onBackPressed();
    }

    public void showSearchMarketDialog(final Context context, String title, String content) {
        DialogUtils.showConverseDialog(title, content, R.string.s99_dialog_btn_no, R.string.s99_dialog_btn_search, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    IntentUtils.searchingMarket(context, mDownloadData.getFileExtension());
                }
            }
        });
    }

    @Override
    public void onCancelTask(int callBackId, int cancelCode, Object data) {
        if (!isAdded()) {
            return;
        }
        String errorMsg;
        switch (cancelCode) {
            case ErrorConst.ERROR_CANCEL:
                errorMsg = "";
                break;
            case ErrorConst.ERROR_SYSTEM:
                errorMsg = getString(R.string.s99_error_device_issue);
                break;
            case ErrorConst.ERROR_AUTH:
                errorMsg = getString(R.string.s99_error_no_3rd_certification);
                break;
            case ErrorConst.ERROR_NO_DISK:
                errorMsg = getString(R.string.s99_error_cannot_save);
                break;
            case ErrorConst.ERROR_CONNECT:
                errorMsg = getString(R.string.s18_text_disconnected);
                break;
            case ErrorConst.ERROR_RECOVERABLE:
                mDownloadData.getDeviceInfo().setConnector(null);
                checkConnector(cancelCode, (Intent) data);
                return;
            case ErrorConst.ERROR_UNLINKED:
                mDownloadData.getDeviceInfo().setConnector(null);
                checkConnector(cancelCode, null);
                return;
            default:
                errorMsg = getString(R.string.s99_error_cannot_read);
                break;
        }
        if (StringUtils.isNotBlank(errorMsg)) {
            Snackbar.make(getActivity().getWindow().getDecorView(), errorMsg, Snackbar.LENGTH_LONG).show();
        }
        mReqDownloadTask = null;
        if (mDownloadFile.exists()) {
            FileUtils.deleteQuietly(mDownloadFile);
        }
        detachFragment();
    }

    @Override
    public void onTaskProgress(int callBackId, Object... params) {
        setProgress((Long) params[0], (Long) params[1]);
    }

    private void setProgress(long transSize, long totalSize) {
        mDownloadStatusText.setText(String.format("%d%%", (int) ((transSize * 100) / totalSize)));
    }

    @Override
    public boolean onBackPressed() {
        if (mReqDownloadTask != null) {
            mDownloadCancelBtn.setVisibility(View.INVISIBLE);
            mReqDownloadTask.stopDownload();
            if (mReqDownloadTask.isCancelled()) {
                detachFragment();
            }
        } else {
            detachFragment();
        }
        return true;
    }

    private void showNotification(File downloadFile) {
        try {
            NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(NotificationConst.ID_DOWNLOAD_FILE, getNotification(getActivity(), NotificationConst.ID_DOWNLOAD_FILE, downloadFile));
        } catch (FileNotFoundException e) {
        }
    }

    private Notification getNotification(Context context, int notificationId, File downloadFile) throws FileNotFoundException {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setTicker(String.format(getString(R.string.s99_toast_save_file), downloadFile.getName()))
                .setSmallIcon(R.drawable.notification_status_2)
                .setVibrate(TypeConst.PATTERN_VIBRATION)
                .setContentTitle(context.getString(R.string.s97_text_konanlink))
                .setContentText(String.format(getString(R.string.s99_toast_save_file), downloadFile.getName()))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .addAction(0, getString(R.string.s99_link_share_file), getShareFileIntent(context, downloadFile, notificationId))
                .addAction(0, getString(R.string.s14_dialog_btn_delete), getDeleteIntent(notificationId, downloadFile.getPath()))
                .setContentIntent(getOpenFileIntent(context, downloadFile, notificationId));
        return builder.build();
    }

    private PendingIntent getShareFileIntent(Context context, File shareFile, int notificationId) throws FileNotFoundException {
        return PendingIntent.getActivity(context, notificationId, IntentUtils.getSharingFileIntent(shareFile), PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent getOpenFileIntent(Context context, File openFile, int notificationId) throws FileNotFoundException {
        return PendingIntent.getActivity(context, notificationId, IntentUtils.getOpenFileIntent(openFile), PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent getCancelIntent(int notificationId) {
        Intent intent = new Intent();
        intent.putExtra(NotificationBroadcastReceiver.NOTIFICATION_ID, notificationId);
        intent.setAction(NotificationBroadcastReceiver.CANCEL);
        return PendingIntent.getBroadcast(getActivity(), notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent getDeleteIntent(int notificationId, String filepath) {
        Intent intent = new Intent();
        intent.putExtra(NotificationBroadcastReceiver.FILEPATH, filepath);
        intent.putExtra(NotificationBroadcastReceiver.NOTIFICATION_ID, notificationId);
        intent.setAction(NotificationBroadcastReceiver.DELETE);
        return PendingIntent.getBroadcast(getActivity(), notificationId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                detachFragment();
            }
        });
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            showErrorDialog(getString(R.string.s98_dialog_text_for_write));
            return false;
        }
        return true;
    }
}
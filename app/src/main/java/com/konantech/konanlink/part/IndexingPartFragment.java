package com.konantech.konanlink.part;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.ListSelectDialogFragment;
import com.konantech.konanlink.service.IndexingManager;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

@PartFragmentInfo(layout = R.layout.part_indexing)
public class IndexingPartFragment extends PartFragment {

    @InjectView(id = R.id.btn_indexing_folder)
    private View mFolderBtn;

    @InjectView(id = R.id.btn_indexing_update)
    private View mUpdateBtn;

    @InjectView(id = R.id.text_indexing_agent)
    private TextView mAgentNameText;

    @InjectView(id = R.id.switch_indexing_include)
    private SwitchCompat mIncludeIndexing;

    @InjectView(id = R.id.toolbar_indexing)
    private Toolbar mToolbar;

    private void setToolbar() {
        mToolbar.setTitle(R.string.s32_title_set_phone_settings);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    public void onInitView(View view) {
        setToolbar();
        setIndexingStatus();
    }

    private void setIndexingStatus() {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo();
        boolean isIncludeIndexing = deviceInfo.isIndexing();
        mIncludeIndexing.setChecked(isIncludeIndexing);
        mFolderBtn.setEnabled(isIncludeIndexing);
        mUpdateBtn.setEnabled(isIncludeIndexing);

        DeviceInfo hostDeviceInfo = deviceInfo.getHostInfo();
        mAgentNameText.setText(hostDeviceInfo == null ? "" : deviceInfo.getIndexingStatus() == IndexingConst.STATUS_REGULAR ? hostDeviceInfo.getName() : "");
    }

    @Override
    protected void onDataReceived(Class sendFragmentClass, int dataType, Object... data) {
        switch (dataType) {
            case DataIdConst.CHANGED_DEVICE_INFO:
                setIndexingStatus();
                break;
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(ConnectorPartFragment.class)) {
            IndexingManager.getInstance().updateIndex(getActivity(), mSubscriptions, getView(), true, getHostKey());
        } else {
            setIndexingStatus();
            GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_INDEXING, DataBank.getInstance().getDeviceInfo().isIndexing() ? GAConst.LABEL_ON : GAConst.LABEL_OFF);
        }
    }

    @Click(R.id.btn_indexing_include)
    public void indexingInclude() {
        if (!DataBank.getInstance().getDeviceInfo().isIndexing()) {
            showOpenMenu();
        } else {
            regDisableIndexing();
        }
    }

    @Click(R.id.btn_indexing_folder)
    public void indexingFolder() {
        attachFragment(R.id.layout_indexing_main, IndexingFolderFragment.class);
    }

    @Click(R.id.btn_indexing_update)
    public void indexingUpdate() {
        String hostKey = getHostKey();
        if (DataBank.getInstance().getDeviceInfo(hostKey).getConnector() == null) {
            checkConnector(ErrorConst.ERROR_NONE, null, hostKey);
        } else {
            IndexingManager.getInstance().updateIndex(getActivity(), mSubscriptions, getView(), true, hostKey);
        }
    }

    private void checkConnector(int errorCode, Intent intent, String hostKey) {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(hostKey);
        attachFragment(R.id.layout_indexing_main, ConnectorPartFragment.class, deviceInfo.getKind(), deviceInfo.getKey(), null, true, errorCode, intent);
    }

    private String getHostKey() {
        List<DeviceInfo> hostList = DataBank.getInstance().getHostList();
        int hostCount = hostList.size();
        DeviceInfo deviceInfo;
        String hostKey = "";
        for (int i = 0; i < hostCount; i++) {
            deviceInfo = hostList.get(i);
            if(StringUtils.equals(deviceInfo.getName(), mAgentNameText.getText())) {
                hostKey = deviceInfo.getKey();
            }
        }

        return hostKey;
    }

    private void regAddDevice(final String hostKey) {
        new DeviceApiManager().reqUpdateHost(getActivity(), mSubscriptions, DataBank.getInstance().getDeviceKey(), hostKey, IndexingConst.STATUS_REGULAR, new Result.OnResultListener<ResultList<DeviceInfo>>() {
            @Override
            public void onSuccess(ResultList<DeviceInfo> result) {
                if (DataBank.getInstance().getDeviceInfo(hostKey).getConnector() == null) {
                    checkConnector(ErrorConst.ERROR_NONE, null, hostKey);
                } else {
                    IndexingManager.getInstance().updateIndex(getActivity(), mSubscriptions, getView(), true, hostKey);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), msg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void regDisableIndexing() {
        new DeviceApiManager().reqUpdateIndexingStatus(getActivity(), mSubscriptions, DataBank.getInstance().getDeviceKey(), IndexingConst.STATUS_NOT, new Result.OnResultListener<ResultList<DeviceInfo>>() {
            @Override
            public void onSuccess(ResultList<DeviceInfo> data) {
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), msg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void showOpenMenu() {
        List<DeviceInfo> hostList = DataBank.getInstance().getHostList();
        int hostCount = hostList.size();
        switch (hostCount) {
            case 0:
                Snackbar.make(getView(), R.string.s33_dialog_error_no_computer, Snackbar.LENGTH_SHORT).show();
                break;
            case 1:
                regAddDevice(hostList.get(0).getHostInfo().getKey());
                break;
            default:
                showHostList(hostList);
                break;
        }
    }

    private void showHostList(List<DeviceInfo> hostList) {
        int hostCount   = hostList.size();
        String[] titles = new String[hostCount];
        String[] keys   = new String[hostCount];
        DeviceInfo deviceInfo;
        DeviceInfo hostDeviceInfo = DataBank.getInstance().getDeviceInfo().getHostInfo();
        int selectIndex = 0;
        for (int i = 0; i < hostCount; i++) {
            deviceInfo = hostList.get(i);
            titles[i]  = deviceInfo.getName();
            keys[i]    = deviceInfo.getKey();
            if (StringUtils.equals(titles[i], hostDeviceInfo.getName())) {
                selectIndex = i;
            }
        }

        DialogUtils.showSelectedDialog(getString(R.string.s32_text_which_computer), titles, keys, selectIndex, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    String hostKey = (String) dataMap.get(ListSelectDialogFragment.DATA_VALUE);
                    regAddDevice(hostKey);
                }
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }
}
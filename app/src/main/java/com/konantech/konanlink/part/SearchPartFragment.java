package com.konantech.konanlink.part;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.ListSetData;
import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.device.search.ReqAgentSearchTask;
import com.konantech.konanlink.device.search.ReqDropBoxSearchTask;
import com.konantech.konanlink.device.search.ReqEvernoteSearchTask;
import com.konantech.konanlink.device.search.ReqGMailSearchTask;
import com.konantech.konanlink.device.search.ReqGoogleDriveSearchTask;
import com.konantech.konanlink.device.search.ReqOneDriveSearchTask;
import com.konantech.konanlink.device.search.ReqSearchTask;

@PartFragmentInfo(layout = R.layout.part_search_loading)
public class SearchPartFragment extends PartFragment implements WorkTask.OnTaskListener {
    private DeviceInfo    mDeviceInfo;
    private ReqSearchTask mReqSearchTask;
    private ListSetData   mSearchInfo;
    private int           mOffset;
    private int           mLimit;
    private TextView      mLoadingText;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mDeviceInfo           = (DeviceInfo)  data[0];
        mOffset               = (int)         data[1];
        mLimit                = (int)         data[2];
        mSearchInfo           = (ListSetData) data[3];
        mLoadingText          = (TextView)    data[4];
        checkConnector(ErrorConst.ERROR_NONE, null);
    }

    private void reqSearchInfo(int kind, Object... data) {
        if (cancelSearchTask()) {
            return;
        }

        switch (kind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_WEBMAIL:
            case DeviceConst.KIND_FILESERVER:
                mReqSearchTask = new ReqAgentSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSubscriptions, mSearchInfo, mOffset, mLimit);
                break;
            case DeviceConst.KIND_DROPBOX:
                mReqSearchTask = new ReqDropBoxSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSearchInfo, mOffset, mLimit);
                break;
            case DeviceConst.KIND_GOOGLEDRIVE:
                mReqSearchTask = new ReqGoogleDriveSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSearchInfo, mOffset, mLimit);
                break;
            case DeviceConst.KIND_EVERNOTE:
                mReqSearchTask = new ReqEvernoteSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSearchInfo, mOffset, mLimit);
                break;
            case DeviceConst.KIND_ONEDRIVE:
                mReqSearchTask = new ReqOneDriveSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSearchInfo, mOffset, mLimit);
                break;
            case DeviceConst.KIND_GMAIL:
                mReqSearchTask = new ReqGMailSearchTask(0, getActivity(), this);
                mReqSearchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data[0], mSearchInfo, mOffset, mLimit);
                break;
        }
    }

    private boolean cancelSearchTask() {
        if (mReqSearchTask != null) {
            mReqSearchTask.cancel(true);
            mReqSearchTask = null;
            return true;
        }
        return false;
    }

    private void checkConnector(int errorCode, Intent intent) {
        attachFragment(R.id.layout_search_loading_main, ConnectorPartFragment.class, mDeviceInfo.getHostInfo().getKind(), mDeviceInfo.getHostInfo().getKey(), mLoadingText, true, errorCode, intent);
    }

    @Override
    public void onStartTask(int callBackId) {
    }

    @Override
    public void onEndTask(int callBackId, Object object) {
        if (object != null) {
            detachFragment(mDeviceInfo.getKey(), object);
        } else {
            onCancelTask(callBackId, ErrorConst.ERROR_UNKNOWN, null);
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(PaymentReqPartFragment.class)) {
            if (data.length > 0 && (boolean) data[0]) {
                checkConnector(-1, null);
            } else {
                detachFragment(mDeviceInfo.getKey());
            }
        } else if (detachedClass.equals(ConnectorPartFragment.class)) {
//            mDeviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceInfo.getKey());
            if (data.length > 1 && (boolean) data[1]) {
                reqSearchInfo(mDeviceInfo.getHostInfo().getKind(), mDeviceInfo.getHostInfo().getConnector());
            } else {
                detachFragment(mDeviceInfo.getKey(), null, data[2]);
            }
        }
    }

    @Override
    public void onCancelTask(int callBackId, int cancelCode, Object data) {
        if (!isAdded()) {
            return;
        }

        switch (cancelCode) {
            case ErrorConst.ERROR_RECOVERABLE:
                mDeviceInfo.getHostInfo().setConnector(null);
                checkConnector(cancelCode, (Intent) data);
                return;
            case ErrorConst.ERROR_UNLINKED:
                mDeviceInfo.getHostInfo().setConnector(null);
                checkConnector(cancelCode, null);
                return;
            case ErrorConst.ERROR_AUTH:
                mDeviceInfo.getHostInfo().setConnector(null);
                checkConnector(cancelCode, null);
                return;

        }
        detachFragment(mDeviceInfo.getKey(), null, cancelCode);
    }

    @Override
    public void onTaskProgress(int callBackId, Object... params) {
    }

    @Override
    public boolean onBackPressed() {
        if (mReqSearchTask != null) {
            mReqSearchTask.cancel(true);
        }
        return true;
    }
}
package com.konantech.konanlink.part;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

@PartFragmentInfo(layout = R.layout.part_account_info, attachAnimation = R.animator.slide_in_up, detachAnimation = R.animator.slide_out_down)
public class AccountInfoPartFragment extends PartFragment {
    @InjectView(id = R.id.text_account_info_email)
    private TextView mEmailText;

    @InjectView(id = R.id.text_account_info_type)
    private TextView mTypeText;

    @InjectView(id = R.id.btn_account_info_payment)
    private TextView mPaymentText;

    @InjectView(id = R.id.toolbar_account_info)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s26_title_account_info);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        setAccountInfo();
    }

    private void setAccountInfo() {
        String accountType;
        String paymentTitle;
        if (DataBank.getInstance().getAccountInfo().isExpired()) {
            accountType  = getString(R.string.s26_text_account_free);
            paymentTitle = getString(R.string.s26_link_upgrade_premium);
        } else {
            accountType  = String.format(getString(R.string.s26_text_account_premium), FastDateFormat.getInstance("MM/dd/yyyy", Locale.getDefault()).format(DataBank.getInstance().getAccountInfo().getExpireDate()));
            paymentTitle = getString(R.string.s26_link_extend_premium);
        }
        mEmailText.setText(DataBank.getInstance().getUsersEmail());
        mTypeText.setText(accountType);
        mPaymentText.setText(paymentTitle);
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(PaymentReqPartFragment.class)) {
            setAccountInfo();
        }
    }

    @Click(R.id.btn_account_info_payment)
    public void paymentRequest() {
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_ACCOUNT);
        attachFragment(R.id.layout_account_info_main, PaymentReqPartFragment.class);
    }

    @Click(R.id.btn_account_info_payment_list)
    public void paymentList() {
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_ACCOUNT, GAConst.LABEL_HISTORY);
        attachFragment(R.id.layout_account_info_main, PaymentListPartFragment.class);
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }
}
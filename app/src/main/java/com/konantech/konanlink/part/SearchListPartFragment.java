package com.konantech.konanlink.part;

import android.Manifest;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.page.search.SearchListViewPagerAdapter;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

@PartFragmentInfo(screenName = GAConst.SCREEN_SEARCH, layout = R.layout.part_list_search,
        permissions = Manifest.permission.GET_ACCOUNTS)
public class SearchListPartFragment extends PartFragment {
    private static final String KEY_KEYWORD = "keyword";
    private static final String KEY_INDEX   = "index";

    private String                     mKeyword;
    private int                        mPagerIndex;
    private SearchListViewPagerAdapter mSearchListViewPagerAdapter;
    private PublishSubject<String>     mPublishSubject;

    @InjectView(id = R.id.pager_list_search)
    private ViewPager mViewPager;

    @InjectView(id = R.id.sliding_tab_strip)
    private TabLayout mTabLayout;

    @Override
    protected void onLoadInstanceState(Bundle savedInstanceState) {
        super.onLoadInstanceState(savedInstanceState);
        mKeyword    = savedInstanceState.getString(KEY_KEYWORD);
        mPagerIndex = savedInstanceState.getInt(KEY_INDEX);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                loadList();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_KEYWORD, mKeyword);
        outState.putInt(KEY_INDEX     , mPagerIndex);
    }

    @Override
    public void onInitView(View view) {
        makeItem();
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mKeyword = (String) data[0];
        mViewPager.setCurrentItem(mPagerIndex);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                loadList();
            }
        });
    }

    private void makeItem() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                mPagerIndex = i;
                mPublishSubject.onNext("" + i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mSearchListViewPagerAdapter = new SearchListViewPagerAdapter(this, mViewPager, mTabLayout);
        mViewPager.setAdapter(mSearchListViewPagerAdapter);
        mViewPager.setOffscreenPageLimit(mSearchListViewPagerAdapter.getCount());
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        mPublishSubject = PublishSubject.create();
        mPublishSubject.throttleWithTimeout(500, TimeUnit.MILLISECONDS)
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new Action1<String>() {
                            @Override
                            public void call(String text) {
                                loadList();
                            }
                        }
                );
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }

    @Override
    protected void onDataReceived(Class sendClass, int dataType, Object... data) {
        switch (dataType) {
            case DataIdConst.REQ_KEYWORD:
                mKeyword = (String) data[0];
                loadList();
                break;
            case DataIdConst.CHANGED_DEVICE_INFO:
                mPagerIndex = 0;
                makeItem();
                loadList();
                break;
        }
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (DataBank.getInstance().isAddedSearchAbleDevice()) {
            if (data.length > 0) {
                mSearchListViewPagerAdapter.onDetachedFragment(detachedClass, data);
            }
        } else {
            detachFragment();
        }
    }

    private void loadList() {
        mSearchListViewPagerAdapter.search(mKeyword, mPagerIndex);
    }


    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            showErrorDialog(getString(R.string.s98_dialog_text_for_accounts));
            return false;
        }
        return true;
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                detachFragment();
            }
        });
    }
}
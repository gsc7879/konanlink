package com.konantech.konanlink.part;

import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.PasswordDialogragment;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.Map;

@PartFragmentInfo(layout = R.layout.part_settings_password)
public class PasswordSettingsFragment extends PartFragment {
    @InjectView(id = R.id.btn_settings_password_change)
    private View mModifyPasswordBtn;

    @InjectView(id = R.id.switch_settings_password_use)
    private SwitchCompat mPassUseSwitch;

    @InjectView(id = R.id.toolbar_settings_password)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        setUsePassword();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s30_title_set_passcode);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    private void setUsePassword() {
        boolean isUsePassword = DataBank.getInstance().isUsePassword();
        mPassUseSwitch.setChecked(isUsePassword);
        mModifyPasswordBtn.setEnabled(isUsePassword);
    }

    @Click(R.id.btn_settings_password_change)
    public void passwordChange() {
        DialogUtils.showPasswordLockDialog(
                PasswordDialogragment.TYPE_CHANGE,
                true,
                getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        setUsePassword();
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_PASSCODE, DataBank.getInstance().isUsePassword() ? GAConst.LABEL_ON : GAConst.LABEL_OFF);
                    }
                });
    }

    @Click(R.id.btn_settings_password_use)
    public void passwordUse() {
        DialogUtils.showPasswordLockDialog(
                DataBank.getInstance().isUsePassword() ? PasswordDialogragment.TYPE_REMOVE : PasswordDialogragment.TYPE_SETTING,
                true,
                getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        setUsePassword();
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_PASSCODE, DataBank.getInstance().isUsePassword() ? GAConst.LABEL_ON : GAConst.LABEL_OFF);
                    }
                });
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        setUsePassword();
        GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_PASSCODE, DataBank.getInstance().isUsePassword() ? GAConst.LABEL_ON : GAConst.LABEL_OFF);
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }
}
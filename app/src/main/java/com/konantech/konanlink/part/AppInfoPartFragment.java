package com.konantech.konanlink.part;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.system.Rules;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.utils.InfoUtils;

@PartFragmentInfo(screenName = GAConst.SCREEN_APP_INFO, layout = R.layout.part_app_info, attachAnimation = R.animator.slide_in_up, detachAnimation = R.animator.slide_out_down)
public class AppInfoPartFragment extends PartFragment {
    @InjectView(id = R.id.text_app_info_version)
    private TextView mVersionText;

    @InjectView(id = R.id.toolbar_app_info)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s38_title_app_info);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mVersionText.setText((String) DataBank.getInstance().INFO.get(DataBank.VERSION_NAME));
    }

    @Click(R.id.btn_app_info_help)
    public void showHelp() {
        String helpUrl = String.format("help?locale=%s", InfoUtils.getServerLocale(getActivity()));
        attachFragment(R.id.layout_app_info_main, WebFragment.class, String.format("%s/%s", NetConst.getInstance().getBaseUrl(), helpUrl), getString(R.string.s38_link_help));
    }

    @Click(R.id.btn_app_info_service)
    public void showServiceTerms() {
        showTerms("service", R.string.s38_link_terms, R.string.s38_error_no_terms);
    }

    @Click(R.id.btn_app_info_privacy)
    public void showPrivacyTerms() {
        showTerms("privacy", R.string.s38_link_privacy, R.string.s38_error_no_privacy);
    }

    @Click(R.id.btn_app_info_payment)
    public void showPaymentTerms() {
        showTerms("pricing", R.string.s38_link_pricing_terms, R.string.s38_error_no_pricing_terms);
    }

    private void showTerms(String type, final int titleResId, final int errorResId) {
        new SystemApiManager().reqRules(getActivity(), mSubscriptions, type, new Result.OnResultListener<Rules>() {
            @Override
            public void onSuccess(Rules data) {
                attachFragment(R.id.layout_app_info_main, WebFragment.class, data.getUrl(), getString(titleResId));
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(getView(), errorResId, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }
}
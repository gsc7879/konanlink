package com.konantech.konanlink.part;

import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.PaymentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.payment.PaymentResult;
import com.konantech.konanlink.api.service.PaymentApiService;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.utils.IABillingManager;
import com.konantech.konanlink.utils.SkuItem;
import com.konantech.konanlink.utils.billing.IabHelper;
import com.konantech.konanlink.utils.billing.IabResult;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class PaymentFragment extends PartFragment {
    protected static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArX6bRtbTo2enCzTB8WkdRuwywXXvndU0EY4I2A/FanAUjCNkoyaLW1c8SWchj9wGbqJDSR7drEag1xzWzQeilUK+fzlNuJ/yb8kQzIHjCng2R7AxCCxc/sJ58qtNbcmMJo4dnNvD5NJFs13VfErNmTxzO+wlvwk+rF0Lf3Ve7vTsiphF5doAC2DuqpPFC6H1xCFQPtqd8JcK8Iv+Se7Th20lUumuLp4hKUFp33uioLy4go092dUWjkLA0Hze+iOntPOP1lMYztdcwZwyF0jBu8n0+aU55GaX6a3+syBVvLSpNR4UnFUAxL5YULftZ930vvwyFyXu7B6p2Otx0vPBLQIDAQAB";

    protected static final String SKU_MONTH_1 = "1month";
    protected static final String SKU_YEAR_1  = "1year";

    protected AccountInfo      mAccountInfo;
    protected IABillingManager mIABillingManager;
    protected boolean          mIsPurchase;
    protected boolean          mIsUpgrade;

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mAccountInfo = DataBank.getInstance().getAccountInfo();
        mIsUpgrade   = mAccountInfo.isExpired();
    }

    protected void initBilling() {
        mIABillingManager = new IABillingManager();
        mIABillingManager.setInit(getActivity(), base64EncodedPublicKey, new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (result.isFailure()) {
                    PaymentFragment.this.initBillingFailed();
                    return;
                }
                PaymentFragment.this.paymentCheck();
            }
        });
    }

    private void paymentCheck() {
        List<String> purchaseDataList = mIABillingManager.getPurchases(getActivity(), IabHelper.ITEM_TYPE_INAPP);
        if (purchaseDataList != null) {
            for (int i = 0; i < purchaseDataList.size(); ++i) {
                String purchaseData = purchaseDataList.get(i);
                if (StringUtils.isNotBlank(purchaseData)) {
                    try {
                        JSONObject purchaseItemInfo = new JSONObject(purchaseData);
                        String productId            = purchaseItemInfo.getString("productId");
                        String purchaseToken        = purchaseItemInfo.getString("purchaseToken");

                        ArrayList<String> skuList = new ArrayList<>();
                        skuList.add(productId);
                        List<SkuItem> responseList = mIABillingManager.getSKUs(getActivity(), skuList, IabHelper.ITEM_TYPE_INAPP);
                        if (responseList != null) {
                            if (responseList.size() > 0) {
                                registerPayment(PaymentApiService.TYPE_GOOGLE, purchaseToken, responseList.get(0));
                                return;
                            }
                        }
                    } catch (JSONException e) {
                        continue;
                    }
                }
            }
        }
        initBillingFinished();
    }

    protected ArrayList<String> getSkuList() {
        ArrayList<String> skuList = new ArrayList<>();
        skuList.add(SKU_YEAR_1);
        skuList.add(SKU_MONTH_1);
        return skuList;
    }

    protected void registerPayment(int paymentType, String purchaseToken, SkuItem skuItem) {
        int term = 0;
        String description = String.format("%s(%s)", skuItem.getPrice(), skuItem.getPriceCurrencyCode());
        switch (skuItem.getProductId()) {
            case SKU_MONTH_1:
                term = 1;
                break;
            case SKU_YEAR_1:
                term = 12;
                break;
        }
        registerPayment(paymentType, purchaseToken, term, description);
    }

    protected void registerPayment(final int paymentType, final String paymentKey, int term, String description) {
        new PaymentApiManager().reqRegister(getActivity(), mSubscriptions, paymentType, paymentKey, term, 0, description, new Result.OnResultListener<PaymentResult>() {
            @Override
            public void onSuccess(PaymentResult result) {
                AccountInfo accountInfo = DataBank.getInstance().getAccountInfo();
                accountInfo.setExpireRemainTime(result.getExpireRemainTime());
                accountInfo.setExpireDate(result.getExpireDate());

                mIsPurchase = true;
                mIABillingManager.consumePurchase(getActivity(), paymentKey);
                onRegisterSuccess(result.getTerm(), result.getDescription(), result.getPaymentType(), result.getExpireDate());
            }

            @Override
            public void onFailed(int code, String msg) {
                if (paymentType == PaymentApiService.TYPE_GOOGLE && code == ErrorConst.ERR_4001_ALREADY_REGISTRATION_PAYMENT) {
                    mIABillingManager.consumePurchase(getActivity(), paymentKey);
                }
                onRegisterFailed(paymentType, code, msg);
            }
        });
    }

    @Override
    public void onDestroy() {
        if (mIABillingManager != null) {
            mIABillingManager.close(getActivity());
        }
        super.onDestroy();
    }

    protected String getPeriodString(int tMonth) {
        int year  = tMonth % 12;
        int month = tMonth / 12;

        StringBuilder sb = new StringBuilder();
        if (year > 0) {
            sb.append(String.format(getString(R.string.s28_text_payment_years), year));
        }

        if (month > 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(String.format(getString(R.string.s28_text_payment_months), month));
        }
        return sb.toString();
    }

    protected abstract void initBillingFailed();

    protected abstract void initBillingFinished();

    protected abstract void onRegisterFailed(int type, int code, String msg);

    protected abstract void onRegisterSuccess(int term, String description, int paymentType, Date expireDate);

}
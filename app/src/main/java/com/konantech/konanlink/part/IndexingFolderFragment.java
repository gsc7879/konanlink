package com.konantech.konanlink.part;

import android.Manifest;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.page.sub.IndexingFolderListViewPagerAdapter;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.List;
import java.util.Map;

@PartFragmentInfo(
        layout      = R.layout.part_indexing_folder,
        permissions = Manifest.permission.WRITE_EXTERNAL_STORAGE)
public class IndexingFolderFragment extends PartFragment {
    private IndexingFolderListViewPagerAdapter mIndexingFolderListViewPagerAdapter;
    private List<String>                       mIndexingFolderList;

    @InjectView(id = R.id.pager_indexing_folder)
    private ViewPager mViewPager;

    @InjectView(id = R.id.toolbar_indexing_folder)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        setFolderList();
        makeItem();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s34_title_set_folder);
        mToolbar.setNavigationIcon(R.drawable.bg_btn_page_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        loadList();
    }

    private void makeItem() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                loadList();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        mIndexingFolderListViewPagerAdapter = new IndexingFolderListViewPagerAdapter(this, mViewPager);
        mViewPager.setAdapter(mIndexingFolderListViewPagerAdapter);
    }

    private void loadList() {
        mIndexingFolderListViewPagerAdapter.loadList(mIndexingFolderList);
    }

    private void setFolderList() {
        mIndexingFolderList = DataBank.getInstance().getIndexingFolders(false);
    }

    @Override
    public boolean onBackPressed() {
        mIndexingFolderListViewPagerAdapter.saveCheckedFolderList();
        detachFragment();
        return true;
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                detachFragment();
            }
        });
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0) {
            showErrorDialog(getString(R.string.s98_dialog_text_for_write));
            return false;
        }
        return true;
    }
}
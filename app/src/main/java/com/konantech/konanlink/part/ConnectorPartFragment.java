package com.konantech.konanlink.part;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.connector.AgentConnector;
import com.konantech.konanlink.device.connector.Connector;
import com.konantech.konanlink.device.connector.DropBoxConnector;
import com.konantech.konanlink.device.connector.EvernoteConnector;
import com.konantech.konanlink.device.connector.GMailConnector;
import com.konantech.konanlink.device.connector.GoogleDriveConnector;
import com.konantech.konanlink.device.connector.LocalStorageConnector;
import com.konantech.konanlink.device.connector.OneDriveConnector;
import com.konantech.konanlink.device.connector.WebMailConnector;
import com.konantech.konanlink.utils.SecretUtil;

@PartFragmentInfo(
        layout      = R.layout.part_connector,
        permissions = {Manifest.permission.GET_ACCOUNTS})
public class ConnectorPartFragment extends PartFragment implements Connector.OnConnectListener {
    private Connector mConnector;
    private Intent    mIntent;
    private String    mDeviceKey;
    private int       mDeviceKind;
    private int       mErrorCode;
    private boolean   mIsCert;
    private TextView  mLoadingText;

    @Override
    public void onInitView(View view) {
    }

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mDeviceKind  = (int)    data[0];
        mDeviceKey   = (String) data[1];
        mLoadingText = data.length > 2 ? (TextView) data[2] : null;
        mIsCert      = data.length > 3 ? (boolean)  data[3] : true;
        mErrorCode   = data.length > 4 ? (int)      data[4] : ErrorConst.ERROR_NONE;
        mIntent      = data.length > 5 ? (Intent)   data[5] : null;
        setContent();
    }

    private void setContent() {
        if (mLoadingText != null) {
            mLoadingText.setText(R.string.s99_text_connecting);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("deviceKey", mDeviceKey);
        outState.putInt("deviceKind", mDeviceKind);
        outState.putInt("errorCode", mErrorCode);
        outState.putBoolean("isCert", mIsCert);
    }

    @Override
    protected void onLoadInstanceState(Bundle savedInstanceState) {
        mDeviceKey = savedInstanceState.getString("deviceKey");
        mDeviceKind = savedInstanceState.getInt("deviceKind");
        mErrorCode = savedInstanceState.getInt("errorCode");
        mIsCert = savedInstanceState.getBoolean("isCert");
    }

    private void connectDevice() {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
        String account        = deviceInfo == null ? null : deviceInfo.getAccount();

        if (mErrorCode == ErrorConst.ERROR_NONE && deviceInfo != null && deviceInfo.getConnector() != null) {
            detachFragment(mDeviceKind, true, mDeviceKind, deviceInfo.getConnector());
            return;
        }
        switch (mDeviceKind) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_FILESERVER:
                mConnector = new AgentConnector(mDeviceKind, mDeviceKey, this, mIsCert, this);
                break;
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
                if (deviceInfo != null && deviceInfo.isMyDevice()) {
                    mConnector = new LocalStorageConnector(mDeviceKind, mDeviceKey, this, mIsCert, this);
                } else {
                    mConnector = new AgentConnector(mDeviceKind, mDeviceKey, this, mIsCert, this);
                }
                break;
            case DeviceConst.KIND_DROPBOX:
                if (mErrorCode == ErrorConst.ERROR_UNLINKED) {
                    if (deviceInfo != null) {
                        deviceInfo.setAccessKey(null);
                    }
                }
                mConnector = new DropBoxConnector(mDeviceKind, mDeviceKey, account, this, mIsCert, this);
                break;
            case DeviceConst.KIND_GOOGLEDRIVE:
                mConnector = new GoogleDriveConnector(mDeviceKind, mDeviceKey, account, this, mIsCert, this, mErrorCode == ErrorConst.ERROR_RECOVERABLE ? mIntent : null);
                break;
            case DeviceConst.KIND_EVERNOTE:
                mConnector = new EvernoteConnector(mDeviceKind, mDeviceKey, account, this, mIsCert, this);
                break;
            case DeviceConst.KIND_WEBMAIL:
                if (deviceInfo != null) {
                    mConnector = new WebMailConnector(mDeviceKind, mDeviceKey, SecretUtil.decryptAES128(deviceInfo.getAccessKey()), this, mIsCert, this);
                }
                break;
            case DeviceConst.KIND_ONEDRIVE:
                mConnector = new OneDriveConnector(mDeviceKind, mDeviceKey, account, this, mIsCert, this);
                break;
            case DeviceConst.KIND_GMAIL:
                mConnector = new GMailConnector(mDeviceKind, mDeviceKey, account, this, mIsCert, this, mErrorCode == ErrorConst.ERROR_RECOVERABLE ? mIntent : null);
                break;
            default:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onConnected(int kind, String deviceKey, Object data) {
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
        if (deviceInfo != null) {
            deviceInfo.setConnector(data);
        }
        detachFragment(mDeviceKind, true);
    }

    @Override
    public void onConnectFail(int kind, int failCode) {
        if (!isAdded()) {
            return;
        }
        DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceKey);
        if (deviceInfo != null) {
            deviceInfo.setConnector(null);
        }
        detachFragment(mDeviceKind, false, failCode, "");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mConnector != null) {
            mConnector.onResume();
        } else {
            connectDevice();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mConnector.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onBackPressed() {
        if (mConnector != null) {
            mConnector.cancel();
            return true;
        }
        return false;
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        if (permissions.length > 0 && (mDeviceKind == DeviceConst.KIND_GOOGLEDRIVE || mDeviceKind == DeviceConst.KIND_GMAIL)) {
            detachFragment(mDeviceKind, false, ErrorConst.ERROR_PERMISSION, "");
            return false;
        }
        return true;
    }
}
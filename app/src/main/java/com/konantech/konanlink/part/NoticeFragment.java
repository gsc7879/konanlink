package com.konantech.konanlink.part;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.adapter.NoticeListAdapter;
import com.konantech.konanlink.page.fragment.MainPageFragment;

@PartFragmentInfo(screenName = GAConst.SCREEN_NOTICE, layout = R.layout.layout_notice)
public class NoticeFragment extends PartFragment {
    private NoticeListAdapter mNoticeListAdapter;

    @InjectView(id = R.id.toolbar_notice)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        mNoticeListAdapter = new NoticeListAdapter(this, R.id.layout_list_notice, R.id.list_notice, R.layout.adapter_notice, R.layout.layout_list_no);
        mNoticeListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) view.findViewById(R.id.layout_notice_refresh), new int[]{R.color.konan});
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        DataBank.getInstance().setHideNewNoti(true);
        AppManager.getInstance().sendData(MainPageFragment.class, DataIdConst.CHANGED_BADGE_STATUS);
        mNoticeListAdapter.loadList();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s37_title_notice);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        if (mChildFragmentClass != null) {
            detachChildFragment();
        } else {
            detachFragment();
        }
        return true;
    }
}
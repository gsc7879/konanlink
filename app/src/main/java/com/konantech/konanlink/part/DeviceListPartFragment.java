package com.konantech.konanlink.part;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.adapter.DeviceListAdapter;
import com.konantech.konanlink.list.model.DeviceListSetData;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@PartFragmentInfo(
        screenName  = GAConst.SCREEN_DEVICE_LIST,
        layout      = R.layout.part_list_device,
        permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.GET_ACCOUNTS})
public class DeviceListPartFragment extends PartFragment {
    private DeviceListAdapter mDeviceListAdapter;
    private DeviceListSetData mDeviceListSetData;

    @InjectView(id = R.id.toolbar_device_list)
    private Toolbar mToolbar;

    @Override
    public void onInitView(View view) {
        setToolbar();
        mDeviceListSetData = new DeviceListSetData();
        mDeviceListAdapter = new DeviceListAdapter(this, R.id.layout_list_device, R.id.list_device, R.layout.adapter_device, R.layout.layout_list_no, mDeviceListSetData);
        mDeviceListAdapter.setSwipeRefreshLayout((SwipeRefreshLayout) view.findViewById(R.id.layout_device_refresh), new int[]{R.color.konan});
    }

    @Override
    protected void onLoadInstanceState(Bundle savedInstanceState) {
        mDeviceListAdapter.loadList();
    }

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        mDeviceListSetData.setRefresh(false);
        mDeviceListAdapter.loadList();
    }

    private void setToolbar() {
        mToolbar.setTitle(R.string.s25_link_device);
        mToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppManager.getInstance().getMenuManager().showMenu(getFragmentManager());
            }
        });
    }

    @Override
    protected void onDataReceived(Class sendFragmentClass, int dataType, Object... data) {
        switch (dataType) {
            case DataIdConst.CHANGED_DEVICE_INFO:
                mDeviceListSetData.setRefresh(false);
                mDeviceListAdapter.loadList();
                break;
        }
    }

    @Override
    public boolean onBackPressed() {
        detachFragment();
        return true;
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(UpdateInfoPartFragment.class)) {
            attachFragment(R.id.layout_list_device_list_main, ConnectAllPartFragment.class, getDeviceList(), mDeviceListAdapter.getLoadingMsgTextView());
        } else if (detachedClass.equals(ConnectAllPartFragment.class)) {
            mDeviceListAdapter.endLoadList((List) data[0]);
        } else if (detachedClass.equals(DirectoryPartFragment.class)) {
            mDeviceListAdapter.loadList();
        }
    }

    private List<DeviceInfo> getDeviceList() {
        List<DeviceInfo> deviceInfoList = new ArrayList<>();
        Map<Integer, List<DeviceInfo>> deviceIndexingCategoryMap = DataBank.getInstance().getDeviceIndexingCategoryMap();
        if (deviceIndexingCategoryMap != null) {
            for (int category : DeviceConst.CATEGORY_ARRAY) {
                if (deviceIndexingCategoryMap.containsKey(category)) {
                    for (DeviceInfo deviceInfo : deviceIndexingCategoryMap.get(category)) {
                        if ((deviceInfo.getCategory() == DeviceConst.CATEGORY_MOBILE) && (!deviceInfo.isMyDevice())) {
                            continue;
                        }
                        deviceInfoList.add(deviceInfo);
                    }
                }
            }
        }
        return deviceInfoList;
    }

    @Override
    protected boolean onDeniedPermissions(String[] permissions) {
        int index = 1;
        if (permissions.length > 0) {
            StringBuffer sb = new StringBuffer();
            if (ArrayUtils.contains(permissions, Manifest.permission.READ_PHONE_STATE)) {
                if (permissions.length > 1) {
                    sb.append(String.format("%d. ", index++));
                }
                sb.append(getString(R.string.s98_dialog_text_for_marshmallow));
            }

            if (ArrayUtils.contains(permissions, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (sb.length() > 0) {
                    sb.append("<br><br>");
                }
                if (permissions.length > 1) {
                    sb.append(String.format("%d. ", index++));
                }
                sb.append(getString(R.string.s98_dialog_text_for_write));
            }

            if (ArrayUtils.contains(permissions, Manifest.permission.GET_ACCOUNTS)) {
                if (sb.length() > 0) {
                    sb.append("<br><br>");
                }
                if (permissions.length > 1) {
                    sb.append(String.format("%d. ", index++));
                }
                sb.append(getString(R.string.s98_dialog_text_for_accounts));
            }
            showErrorDialog(sb.toString());
            return false;
        }
        return true;
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                detachFragment();
            }
        });
    }
}
package com.konantech.konanlink.part;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.data.DeviceListViewData;

import java.util.ArrayList;
import java.util.List;

@PartFragmentInfo(layout = R.layout.part_connector_all)
public class ConnectAllPartFragment extends PartFragment {
    private List<DeviceInfo>         mDeviceList;
    private List<DeviceListViewData> mDeviceViewList;
    private TextView                 mLoadingText;

    @Override
    public void onInitView(View view) {
        setContent();
    }

    @Override
    protected void onAttached(Class fromFragmentClass, Object... data) {
        mDeviceList  = (List)     data[0];
        mLoadingText = (TextView) data[1];

        if (mDeviceList.isEmpty()) {
            detachFragment(mDeviceViewList);
        }
        callConnector(0);
    }

    private void setContent() {
        mDeviceViewList = new ArrayList<>();
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        int addedListSize = mDeviceViewList.size();
        mDeviceViewList.add(new DeviceListViewData(mDeviceList.get(addedListSize), data.length > 2 ? (int) data[2] : 0));
        addedListSize ++;
        if (addedListSize < mDeviceList.size()) {
            final int nextAddedListSize = addedListSize;
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    callConnector(nextAddedListSize);
                }
            });
        } else {
            detachFragment(mDeviceViewList);
        }
    }

    private void callConnector(int index) {
        DeviceInfo deviceInfo = mDeviceList.get(index);
        attachFragment(R.id.layout_connector_all_main, ConnectorPartFragment.class, deviceInfo.getKind(), deviceInfo.getKey(), mLoadingText, false);
    }
}
package com.konantech.konanlink.part;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.PartFragmentInfo;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultMap;
import com.konantech.konanlink.api.model.device.RegisterStatusResult;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

@PartFragmentInfo(layout = R.layout.part_loading, isUseBackPressEvent = false)
public class LoadingPartFragment extends PartFragment {
    public enum LoadingError {
        NOT_REGISTERED_DEVICE,
        NOT_EXIST_KEY,
        INIT_DEVICE
    }

    @InjectView(id = R.id.layout_loading)
    private View mLoadingLayout;

    private TextView mStatusText;

    @Override
    public void onInitView(View view) {}

    @Override
    protected void onAttached(Class attachFragmentClass, Object... data) {
        if (data.length > 0) {
            mStatusText = (TextView) data[0];
            mStatusText.setText(R.string.s01_text_initiate_konanlink_00);
        }
        mLoadingLayout.setVisibility(mStatusText == null ? View.VISIBLE : View.GONE);
        requestExtensionList();
    }

    private void updateInfo(int updateType) {
        attachFragment(R.id.layout_loading, UpdateInfoPartFragment.class, updateType, mStatusText);
    }

    private void paymentCheck() {
        attachFragment(R.id.layout_loading, PaymentCheckFragment.class, mStatusText);
    }

    private void requestExtensionList() {
        new SystemApiManager().reqExtensionList(getActivity(), mSubscriptions, new Result.OnResultListener<ResultMap<String, List<String>>>() {
            @Override
            public void onSuccess(ResultMap<String, List<String>> data) {
                DataBank.getInstance().setExtensionTypeMap(data.getMap());
                checkRegisterDevice();
            }

            @Override
            public void onFailed(int code, String msg) {
                showErrorDialog(getString(R.string.s01_dialog_error_cannot_access_server));
            }
        });
    }

    private void checkRegisterDevice() {
        new DeviceApiManager().reqRegisterStatus(getActivity(), mSubscriptions, new Result.OnResultListener<RegisterStatusResult>() {
            @Override
            public void onSuccess(RegisterStatusResult data) {
                if (StringUtils.isBlank(DataBank.getInstance().getApiKey())) {
                    detachFragment(data.isRegistered() ? LoadingError.NOT_EXIST_KEY : LoadingError.INIT_DEVICE);
                } else {
                    if (data.isRegistered()) {
                        updateInfo(UpdateInfoPartFragment.TYPE_ALL);
                    } else {
                        DataBank.getInstance().initPreferences(getActivity());
                        detachFragment(LoadingError.NOT_REGISTERED_DEVICE, data.isGiftAble());
                    }
                }
            }

            @Override
            public void onFailed(int code, String msg) {
                showErrorDialog(getString(R.string.s01_dialog_error_cannot_access_server));
            }
        });
    }

    @Override
    protected void onDetachedFragment(Class detachedClass, Object... data) {
        if (detachedClass.equals(UpdateInfoPartFragment.class)) {
            boolean success = (boolean) data[0];
            if (!success) {
                showErrorDialog(getString(R.string.s01_dialog_error_cannot_access_server));
                return;
            }
            paymentCheck();
        } else  if (detachedClass.equals(PaymentCheckFragment.class)) {
            if (data.length > 0) {
                updateInfo(UpdateInfoPartFragment.TYPE_USER_INFO);
            } else {
                checkDevice();
            }
        }
    }

    private void checkDevice() {
        if (DataBank.getInstance().isAddedDevice()) {
            detachFragment();
        } else {
            detachFragment(LoadingError.NOT_REGISTERED_DEVICE);
        }
    }

    private void showErrorDialog(String content) {
        DialogUtils.showAlertDialog("", content, R.string.s99_dialog_btn_okay, getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                System.exit(0);
            }
        });
    }
}
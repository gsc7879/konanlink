package com.konantech.konanlink;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;

import java.util.List;

public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        reqResetHostConnector(getConnectType(cm.getActiveNetworkInfo()));
    }

    private void reqResetHostConnector(int connectType) {
        List<DeviceInfo> hostList = DataBank.getInstance().getHostList();
        if (hostList == null) {
            return;
        }
        for (DeviceInfo deviceInfo : hostList) {
            deviceInfo.resetConnector(connectType);
        }
    }

    private int getConnectType(NetworkInfo activeNetwork) {
        if (activeNetwork == null) {
            return NetConst.TYPE_CONNECT_DISABLE;
        }

        switch (activeNetwork.getType()) {
            case ConnectivityManager.TYPE_MOBILE:
                return NetConst.TYPE_CONNECT_RELAY;
            case ConnectivityManager.TYPE_WIFI:
                return NetConst.TYPE_CONNECT_LOCAL;
        }
        return NetConst.TYPE_CONNECT_DISABLE;
    }
}
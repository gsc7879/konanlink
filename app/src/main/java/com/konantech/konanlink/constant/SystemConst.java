package com.konantech.konanlink.constant;

public interface SystemConst {



    String RESPONSE_LAST_ANNOUNCE_TIME = "last_announce_time";
    String RESPONSE_MAINTENANCE        = "maintenance";

    String RESPONSE_MESSAGE            = "message";
    String RESPONSE_VERSION            = "version";
    String RESPONSE_SHOW_VERSION       = "version_show";


}

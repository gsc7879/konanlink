package com.konantech.konanlink.constant;

public interface TypeConst {
    long[] PATTERN_VIBRATION = new long[]{500, 100, 500, 100};

    String NOTI_DELETED_DEVICE      = "deleted_device";
    String NOTI_CHANGE_DEVICE_INFO  = "changed_device_info";
    String NOTI_CHANGE_USER_INFO    = "changed_user_info";
    String NOTI_INFO_INDEXING       = "indexing_count";
    String NOTI_NEW_ANNOUNCE        = "new_announce";

    String NOTI_DOWNLOAD            = "download";
    String NOTI_THUMBNAIL           = "thumbnail";
    // facebook
    String NOTI_FACEBOOK_SEARCH     = "facebook_search";
    String NOTI_FACEBOOK_DOWNLOAD   = "facebook_download";


}

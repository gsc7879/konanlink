package com.konantech.konanlink.constant;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class NetConst {
    private static NetConst uniqueNetConst;

    public static final int TYPE_CONNECT_DISABLE = 0;
    public static final int TYPE_CONNECT_LOCAL   = 1;
    public static final int TYPE_CONNECT_NAT     = 2;
    public static final int TYPE_CONNECT_RELAY   = 3;

    public static final int PORT_ROUTER          = 50007;
    public static final int AGENT_ROUTER         = 40020;

    public static final int TIMEOUT_LOCAL        = 1000;
    public static final int TIMEOUT_RELAY        = 3000;

    public static final String STATUS_FAIL       = "fail";

    private String routerIp;
    private String baseUrl;
    private String linkUrl;

    private boolean isTestMode;

    public synchronized static NetConst getInstance() {
        if (uniqueNetConst == null) {
            uniqueNetConst = new NetConst();
        }
        return uniqueNetConst;
    }

    public NetConst() {
        loadProperties();
    }

    private void loadProperties() {
        Properties prop = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = FileUtils.openInputStream(FileUtils.getFile(FileConst.FOLDER_HOME, "config"));
            prop.load(inputStream);
            routerIp   = prop.getProperty("router.ip");
            baseUrl    = prop.getProperty("base.url");
            linkUrl    = prop.getProperty("link.url");
            isTestMode = true;
        } catch (IOException ignored) {
            routerIp   = "router.konanlink.com";
            baseUrl    = "https://api.konanlink.com";
            linkUrl    = "https://link.konanlink.com"; //TODO: 상용 수정 필요
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    public boolean isTestMode() {
        return isTestMode;
    }

    public String getRouterIp() {
        return routerIp;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getLinkUrl() {
        return linkUrl;
    }
}
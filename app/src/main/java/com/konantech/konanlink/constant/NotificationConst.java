package com.konantech.konanlink.constant;

public interface NotificationConst {
    int ID_DOWNLOAD_SERVER  = 101;
    int ID_INFO_INDEXING    = 102;
    int ID_INFO_REPORTING   = 103;
    int ID_DOWNLOAD_FILE    = 104;

    String PARAM_TYPE       = "type";
    String PARAM_MSG        = "msg";
    String PARAM_NOTI_TYPE  = "notitype";
    String PARAM_MESSAGE    = "message";

    String PARAM_REQUEST_ID         = "request_id";
    String PARAM_RESPONSE_URL       = "response_url";
    String PARAM_FILE_PATH          = "path";

    // faceBook search
    String PARAM_FACEBOOK_SEARCH_USER_ID        = "user_id";
    String PARAM_FACEBOOK_SEARCH_SENDER_ID      = "sender_id";
    String PARAM_FACEBOOK_SEARCH_ACCESS_TOKEN   = "access_token";
    String PARAM_FACEBOOK_SEARCH_KEYWORD        = "keyword";
    String PARAM_FACEBOOK_SEARCH_PAGE           = "page";
    String PARAM_FACEBOOK_SEARCH_SIZE           = "size";


    // thumNail
    String PARAM_THUMBNAIL_WIDTH            = "width";
    String PARAM_THUMBNAIL_HEIGHT           = "height";
}

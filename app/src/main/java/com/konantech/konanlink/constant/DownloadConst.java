package com.konantech.konanlink.constant;

public interface DownloadConst {
    long LIMIT_WORN              = 20971520;

    String PARAM_WIDTH           = "width";
    String PARAM_HEIGHT          = "height";
    String PARAM_TYPE            = "type";
    String PARAM_FILE_PATH       = "filepath";
    String PARAM_TARGET          = "target";
    String PARAM_DEVICE_KEY      = "devicekey";
    String PARAM_OFFSET          = "offset";
    String PARAM_TIME_CREATE     = "createtime";
    String PARAM_TIME_ACCESS     = "lastaccesstime";
    String PARAM_TIME_WRITE_LAST = "lastwritetime";
    String PARAM_LENGTH          = "length";
    String PARAM_EXT             = "ext";
}

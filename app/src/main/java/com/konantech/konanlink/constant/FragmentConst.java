package com.konantech.konanlink.constant;

public interface FragmentConst {
    /* Package */
    String PACKAGE_PAGE = "com.konantech.konanlink.page.fragment";
    String PACKAGE_PART = "com.konantech.konanlink.part";
    String PACKAGE_MENU = "com.konantech.konanlink.menu";
    String PACKAGE_DATA = "com.konantech.konanlink.data";
}
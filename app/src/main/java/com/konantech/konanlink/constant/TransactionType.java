package com.konantech.konanlink.constant;

public interface TransactionType {
    String CONNECTED            = "connected";
    String DIRECTORY            = "directory";
    String CLOSED               = "closed";
    String REGISTER             = "register";
    String REGISTERED           = "registered";
    String SEARCH               = "search";
    String INDEXING_INFO        = "indexing_info";
    String SLACK_SEARCH         = "slack_search";
    String DOWNLOAD             = "download";
    String SLACK_UPLOAD         = "slack_upload";
    String THUMBNAIL            = "thumbnail";
    String PONG                 = "pong";
    String FACEBOOK_DOWNLOAD    = "facebook_download";
    String MOBILE_META          = "mobile_meta";

}


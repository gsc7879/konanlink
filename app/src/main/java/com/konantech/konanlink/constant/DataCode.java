package com.konantech.konanlink.constant;

public interface DataCode {
    static final int CODE_SUCCESS       = 200;
    static final int CODE_ERROR_UNKNOWN = 999;

}

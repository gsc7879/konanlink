package com.konantech.konanlink.constant;

public interface IndexingConst {
    String[] DEFAULT_INDEXING_FOLDER = {"DCIM", "Download"};

    int STATUS_NOT           = 0;
    int STATUS_REGULAR       = 1;
    int STATUS_CERTIFICATION = 2;
    int STATUS_SELF          = 3;

    String PARAM_TARGET      = "target";
    String PARAM_DEVICE_KEY  = "devicekey";
    String PARAM_COMMAND     = "command";

    String COMMAND_REMOVE    = "remove";
}

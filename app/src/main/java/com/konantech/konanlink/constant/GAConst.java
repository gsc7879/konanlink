package com.konantech.konanlink.constant;

public interface GAConst {
    String TRACKING_ID         = "UA-68564483-1";

    /* screen name */
    String SCREEN_LOADING      = "kl_scr_initializing";
    String SCREEN_TUTO_1       = "kl_scr_tutorial1";
    String SCREEN_TUTO_2       = "kl_scr_tutorial2";
    String SCREEN_TUTO_3       = "kl_scr_tutorial3";
    String SCREEN_TUTO_4       = "kl_scr_tutorial4";
    String SCREEN_TUTO_END     = "kl_scr_signup_signin";
    String SCREEN_LOGIN        = "kl_scr_signin";
    String SCREEN_RESET        = "kl_scr_reset_passwword";
    String SCREEN_JOIN         = "kl_scr_signup";
    String SCREEN_LIMIT        = "kl_scr_upto_2phones";
    String SCREEN_RECENT       = "kl_scr_main";
    String SCREEN_SEARCH       = "kl_scr_search_result";
    String SCREEN_PREVIEW      = "kl_scr_preview";
    String SCREEN_DEVICE_LIST  = "kl_scr_device_list";
    String SCREEN_DEVICE_ADD   = "kl_scr_device_add";
    String SCREEN_PAYMENT      = "kl_scr_premium_upgade";
    String SCREEN_PAYMENT_LIST = "kl_scr_menu_history";
    String SCREEN_NOTICE       = "kl_scr_menu_notice";
    String SCREEN_APP_INFO     = "kl_scr_menu_appinfo";
    String SCREEN_FOLDER       = "kl_scr_file_explorer";


    /* category */
    String CATEGORY_SEARCH     = "search";
    String CATEGORY_VIEW       = "view";
    String CATEGORY_PREVIEW    = "preview";
    String CATEGORY_PAY        = "pay";
    String CATEGORY_MENU       = "menu";
    String CATEGORY_ADD        = "add";
    String CATEGORY_SERVICE    = "service";

    /* event */
    String EVENT_KEYWORD       = "keyword";
    String EVENT_FILTER        = "filter";
    String EVENT_OPEN          = "open";
    String EVENT_SHARE         = "share";
    String EVENT_EXPLORER      = "explorer";
    String EVENT_SAVE          = "savetophone";
    String EVENT_POPUP         = "popup";
    String EVENT_PASSCODE      = "passcode";
    String EVENT_INDEXING      = "indexing";
    String EVENT_ACCOUNT       = "account";
    String EVENT_BUY           = "buy";
    String EVENT_FEEDBACK      = "feedback";
    String EVENT_SERVICE       = "service";
    String EVENT_RECOMMEND     = "recommend";
    String EVENT_DELETE        = "delete";
    String EVENT_ADD           = "add";

    /* label */
    String LABEL_TYPE          = "type";
    String LABEL_ORDER         = "order";
    String LABEL_DATE          = "date";
    String LABEL_HISTORY       = "history";
    String LABEL_SUCCEED       = "succeed";
    String LABEL_FAIL          = "fail";
    String LABEL_ON            = "on";
    String LABEL_OFF           = "off";
}



























package com.konantech.konanlink.constant;

public interface AnimationConst {
    float BIG_RADIOUS   = 30;
    float SMALL_RADIOUS = 20;

    int BIG_DURATION    = 1000;
    int SMALL_DURATION  = 1000;
}

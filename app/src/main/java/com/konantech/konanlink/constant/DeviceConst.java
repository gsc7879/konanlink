package com.konantech.konanlink.constant;

public interface DeviceConst {
    int CATEGORY_ALL      = -1;

    int CATEGORY_PC       = 0;
    int KIND_WIN          = 1;
    int KIND_MAC          = 2;
    int KIND_LINUX        = 3;
    int KIND_OUTLOOK      = 4;

    int CATEGORY_MOBILE   = 1;
    int KIND_ANDROID      = 11;
    int KIND_IOS          = 12;

    int CATEGORY_CLOUD    = 2;
    int KIND_DROPBOX      = 21;
    int KIND_GOOGLEDRIVE  = 22;
    int KIND_ONEDRIVE     = 23;
    int KIND_GMAIL        = 24;

    int CATEGORY_INTERNET = 3;
    int KIND_WEBMAIL      = 31;
    int KIND_EVERNOTE     = 32;

    int CATEGORY_SERVER   = 4;
    int KIND_FILESERVER   = 41;

    String TARGET_SEARCH_PC           = "pc";
    String TARGET_SEARCH_MOBILE       = "mobile";
    String TARGET_SEARCH_DROPBOX      = "dropbox";
    String TARGET_SEARCH_GOOGLEDRIVE  = "googledrive";
    String TARGET_SEARCH_EVERNOTE     = "evernote";
    String TARGET_SEARCH_WEBMAIL      = "pop3";
    String TARGET_SEARCH_ONEDRIVE     = "onedrive";
    String TARGET_SEARCH_GMAIL        = "gmail";

    String DEVICE_DROPBOX             = "Dropbox";
    String DEVICE_GOOGLEDRIVE         = "GoogleDrive";
    String DEVICE_EVERNOTE            = "Evernote";
    String DEVICE_ONEDRIVE            = "OneDrive";
    String DEVICE_GMAIL               = "GMail";

    String TYPE_ANDROID               = "android";

    int[] CATEGORY_ARRAY = {DeviceConst.CATEGORY_ALL, DeviceConst.CATEGORY_PC, DeviceConst.CATEGORY_CLOUD, DeviceConst.CATEGORY_INTERNET, DeviceConst.CATEGORY_MOBILE, DeviceConst.CATEGORY_SERVER};
}
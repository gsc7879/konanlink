package com.konantech.konanlink.constant;

public interface ErrorConst {
    // Sucess


    // User
    int ERR_1001_ALREADY_REGISTERED_EMAIL       = 1001; // 이미 등록된 이메일
    int ERR_1002_NOT_EXIST_EMAIL                = 1002; // 존재하지 않는 이메일
    int ERR_1003_NOT_EQUALS_PASSWORD            = 1003; // 패스워드가 일치하지 않음
    int ERR_1004_WRONG_EMAIL_CONFIRM_KEY        = 1004; // 존재하지 않는 이메일 확인키
    int ERR_1005_NOT_ADMIN                      = 1005; // 관리자가 아님
    int ERR_1201_EMPTY_API_KEY                  = 1201; // API키가 빈값
    int ERR_1202_NOT_EXIST_API_KEY              = 1202; // 존재하지 않는 API키
    int ERR_1203_INVALID_USERS_TYPE             = 1203; // 잘못된 유저타입

    // Check
    int ERR_2001_INPUT_PARAM_FIELD_IS_EMPTY     = 2001; // 빈 입력 필드
    int ERR_2002_INPUT_PARAM_LIMIT_MINIMUM      = 2002; // 최소 입력문자 제한
    int ERR_2003_INPUT_PARAM_LIMIT_MAXIMUM      = 2003; // 최대 입력문자 제한
    int ERR_2004_INPUT_PARAM_USE_FORBIDDEN_CHAR = 2004; // 입력제한 문자
    int ERR_2005_INVALID_EMAIL_FORM             = 2005; // 올바르지 않은 이메일 형식

    // Device
    int ERR_3001_ALREADY_REGISTERED_DEVICE      = 3001; // 이미 등록된 기기
    int ERR_3002_NOT_MY_DEVICE                  = 3002; // 내 기기가 아닌경우
    int ERR_3003_NOT_EXIST_DEVICE               = 3003; // 존재하지 않는 기기
    int ERR_3004_INVALID_PUSH_MESSAGE           = 3004; // 잘못된 푸쉬 요청
    int ERR_3005_LIMIT_REGISTERED_DEVICE        = 3005; // 디바이스 등록대수 초과
    int ERR_3006_NOT_EXIST_AGENT                = 3006; // 에이전트가 존재하지 않는경우

    // Payment
    int ERR_4001_ALREADY_REGISTRATION_PAYMENT   = 4001; // 중복 결제
    int ERR_4101_PAYPAL_PAYMENT_FAIL            = 4101; // 페이팔 결제 실패
    int ERR_4201_COUPON_EXPIRE_DATE             = 4201; // 쿠폰 유효기간 경과
    int ERR_4202_WRONG_CODE                     = 4202; // 올바르지 않은 쿠폰번호
    int ERR_4301_USED_KEY                       = 4301; // 이미 사용된 결제키
    int ERR_4302_WRONG_VENDOR                   = 4302; // 벤더사 에러

    // Download
    int ERR_5001_NOT_MY_DEVICE                  = 5001; //내 디바이스가 아님
    int ERR_5002_EMPTY_FILEPATH                 = 5002; //파일경로 누락
    int ERR_5003_EMPTY_FILENAME                 = 5003; //파일명 누락
    int ERR_5004_REQUEST_TO_DEVICE_FAIL         = 5004; //디바이스에 다운로드 요청이 실패함 (연결 실패 등)
    int ERR_5005_EMPTY_S3KEY                    = 5005; //S3KEY 누락
    int ERR_5006_INVALID_S3KEY                  = 5006; //S3KEY가 유효하지 않음
    int ERR_5007_INVALID_USER                   = 5007; //다운로드 요청한 USER가 아님
    int ERR_5008_INVALID_STATUS                 = 5008; //잘못된 상태값

    // Commons
    int ERR_6001_INVALID_PARAMETERS             = 6001; //파라미터에러

    // Slack
    int ERR_7001_SLACK_NOT_REGISTERED           = 7001; // 등록하지 않은 슬랙유저
    int ERR_7002_SLACK_NOT_ADDED                = 7002; // 추가하지 않은 슬랙서비스
    int ERR_7003_SLACK_SEARCH_KEYWORD           = 7003; // 키워드가 입력되지 않음
    int ERR_7005_SLACK_NOT_SELECT_AGENT         = 7005; // 검색할 에이전트가 선택되지 않음
    int ERR_7006_SLACK_ALREADY_ADDED_TO_USER    = 7006; // 해당 유저에게 이미 추가되어 있는 슬랙 서비스
    int ERR_7007_SLACK_ACCESS_ERROR             = 7007; // 유효하지 않은 코드

    // Agent
    int ERR_8001_REDIS                          = 8001; // 레디스 서버 에러
    int ERR_8002_AGENT_NOT_CONNECTED            = 8002; // 접속되지 않은 에이전트
    int ERR_8003_WRONG_REQUEST_ID               = 8003; // 올바르지 않은 요청 아이디
    int ERR_8004_WRONG_TRANSACTION              = 8004; // 올바르지 않은 트랜젝션
    int ERR_8005_FILE_NOT_FOUND                 = 8005; // 존재하지 않는 파일
    int ERR_8006_ACCESS_DENIED_PATH             = 8006; // 폴더 접근 금지
    int ERR_8007_RESIZE                         = 8007; // 이미지 리사이징 에러
    int ERR_8008_KIS                            = 8008; // KIS 에러
    int ERR_8009_JSON_TO_XML                    = 8009; // Json 컨버팅 에러
    int ERR_8010_DOWNLOAD                       = 8010; // 다운로드 에러

    // Third Part
    int ERROR_NONE            = 0;
    int ERROR_UNKNOWN         = 1;
    int ERROR_NOT_EXIST       = 2;
    int ERROR_UNLINKED        = 3;
    int ERROR_CANCEL          = 4;
    int ERROR_NO_DISK         = 6;
    int ERROR_RECOVERABLE     = 8;
    int ERROR_AUTH            = 9;
    int ERROR_TIMEOUT         = 10;
    int ERROR_CONNECT         = 11;
    int ERROR_SYSTEM          = 12;
    int ERROR_PERMISSION      = 13;
    int ERROR_CONNECT_CANCEL  = 14;
    int ERROR_CONNECT_NORMAL  = 15;
    int ERROR_CONNECT_CERT    = 16;
    int ERROR_CONNECT_LIMIT   = 17;
    int ERROR_CONNECT_ACCOUNT = 18;
    int ERROR_CONNECT_WAS     = 19;
    int ERROR_REDIRECT_URL    = 20;
}

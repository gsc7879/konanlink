package com.konantech.konanlink.constant;

public interface SearchConst {
    String PARAM_KEYWORD               = "keyword";
    String PARAM_SORT                  = "sort";
    String PARAM_RANGE                 = "range";
    String PARAM_PERIOD_START          = "start";
    String PARAM_PERIOD_END            = "end";
    String PARAM_SUMMARY               = "summarysize";
    String PARAM_TAG_HIGHLIGHT_START   = "starttag";
    String PARAM_TAG_HIGHLIGHT_END     = "endtag";

    String SIZE_SUMMARY                = "2000";

    String TAG_HIGHLIGHT_START         = "<font color='#d18604'><b>";
    String TAG_HIGHLIGHT_END           = "</b></font>";

    String DATE_FORMAT_SEARCH          = "yyyyMMddHHmmss";
    String OPT_LASTEST                 = "lastest";
    String OPT_ACCURACY                = "accuracy";

    String RESPONSE_KEY_DEVICE         = "devicekey";
    String RESPONSE_KEYWORD_HIGHLIGHT  = "highlightkeyword";
    String RESPONSE_TARGET             = "target";
    String RESPONSE_PAGE               = "page";
    String RESPONSE_COUNT_MAX          = "maxcount";
    String RESPONSE_COUNT_TOTAL        = "totalcount";
    String RESPONSE_ITEM               = "item";
    String RESPONSE_FOLDER             = "folder";
    String RESPONSE_RESOLUTION         = "resolution";
    String RESPONSE_LOCATION           = "location";
    String RESPONSE_TYPE               = "type";
    String RESPONSE_TITLE              = "filename";
    String RESPONSE_HIGHLIGHT_TITLE    = "filenamehighlight";
    String RESPONSE_URL                = "url";
    String RESPONSE_SUMMARY            = "summary";
    String RESPONSE_TIME_MODIFY        = "modifytime";
    String RESPONSE_INDEX              = "index";
    String RESPONSE_SIZE               = "size";
    String RESPONSE_INFO_EMAIL         = "emailinfo";
    String RESPONSE_EMAIL_TITLE        = "title";
    String RESPONSE_EMAIL_FROM         = "from";
    String RESPONSE_EMAIL_TO           = "to";
    String RESPONSE_EXTENSION          = "extension";
    String RESPONSE_ATTACHED           = "attached";
}

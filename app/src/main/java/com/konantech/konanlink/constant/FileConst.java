package com.konantech.konanlink.constant;

import android.os.Environment;
import org.apache.commons.io.FileUtils;

import java.io.File;

public interface FileConst {
    File FOLDER_HOME     = FileUtils.getFile(Environment.getExternalStorageDirectory(), "KonanLink");
    File FILE_NOMEDIA    = FileUtils.getFile(FileConst.FOLDER_HOME, ".nomedia");
    File FOLDER_DOWNLOAD = FileUtils.getFile(FOLDER_HOME, "download");
    File FOLDER_INDEXING = FileUtils.getFile(FOLDER_HOME, "indexing");
    File FOLDER_CACHE    = FileUtils.getFile(FOLDER_HOME, "cache");
    File FOLDER_TEMP     = FileUtils.getFile(FOLDER_HOME, "temp");
}

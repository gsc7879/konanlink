package com.konantech.konanlink.constant;

public interface DirectoryConst {
    String RESPONSE_ITEM           = "item";
    String RESPONSE_TYPE           = "type";
    String RESPONSE_EXTENSION      = "ext";
    String RESPONSE_SIZE           = "length";
    String RESPONSE_NAME           = "name";
    String RESPONSE_PATH           = "path";
    String RESPONSE_MODIFIED_TIME  = "modifiedtime";
    String RESPONSE_PATH_CURRENT   = "currentpath";
    String RESPONSE_PATH_PARENT    = "parentpath";
}

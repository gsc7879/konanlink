package com.konantech.konanlink.constant;

public interface DataIdConst {
    int REQ_KEYWORD             = 0;
    int CHANGED_USERS_INFO      = 1;
    int CHANGED_DEVICE_INFO     = 2;
    int CHANGED_BADGE_STATUS    = 3;
}

package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class ExtensionMenuListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public TextView mTitle;

    public ExtensionMenuListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_combobox_main);
        mTitle = (TextView) convertView.findViewById(R.id.text_combobox_title);
    }
}

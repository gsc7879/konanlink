package com.konantech.konanlink.list.adapter;

import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.IntentUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.ListSelectDialogFragment;
import com.konantech.konanlink.list.data.DirectoryListViewData;
import com.konantech.konanlink.list.model.DirectorySetData;
import com.konantech.konanlink.list.viewholder.DirectoryListViewHolder;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.model.directory.DirectoryItem;
import com.konantech.konanlink.part.DirectoryInfoPartFragment;
import com.konantech.konanlink.part.DirectoryPartFragment;
import com.konantech.konanlink.part.DownloadPartFragment;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

public class DirectoryListAdapter extends ListViewAdapter<DirectoryListViewData, DirectoryListViewHolder, DirectorySetData, DirectoryInfo> {
    private DirectoryPartFragment mPageFragment;
    private Animation             mAnimation;

    @InjectView(id = R.id.text_listno)
    private TextView mNoListText;

    @InjectView(id = R.id.img_listno)
    private ImageView mNoListImage;

    public DirectoryListAdapter(DirectoryPartFragment pageFragment, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DirectorySetData data) {
        super(pageFragment.getView(), listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mPageFragment  = pageFragment;
    }

    private void startLoadingAnimation() {
        if (mAnimation == null) {
            mAnimation = new CircleAnimation(mNoListImage, AnimationConst.BIG_RADIOUS);
            mAnimation.setDuration(AnimationConst.BIG_DURATION);
            mAnimation.setRepeatCount(Animation.INFINITE);
        }
        mNoListImage.startAnimation(mAnimation);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        startLoadingAnimation();
        setMessage(R.drawable.img_search, mContext.getString(R.string.s99_text_loading));
        mPageFragment.attachFragment(R.id.layout_list_directory_main, DirectoryInfoPartFragment.class, mListSetData.getDeviceInfo(), mListSetData.getCurrentPath());
    }

    @Override
    protected void onEndLoad(DirectoryInfo directoryInfo) {
        mListSetData.setParentPath(directoryInfo.getParentPath());
        mListSetData.setCurrentPath(directoryInfo.getCurrentPath());
        mListSetData.setFolder(directoryInfo.getFolder());
        mPageFragment.setPathText();

        DeviceInfo deviceInfo = mListSetData.getDeviceInfo();

        if (directoryInfo.getParentPath() != null || directoryInfo.getCurrentPath().endsWith(":\\")) {
            addItem(new DirectoryListViewData(deviceInfo, new DirectoryItem(mContext.getString(R.string.s40_link_parent_folder), "", DirectoryInfo.TYPE_UP, "", "", 0, null, "", "")));
        }

        List<DirectoryItem> directoryItemMap = directoryInfo.getDirectoryItemMap(DirectoryInfo.TYPE_DRIVE);
        if (directoryItemMap != null) {
            for (DirectoryItem directoryItem : directoryItemMap) {
                addItem(new DirectoryListViewData(deviceInfo, directoryItem));
            }
        }

        directoryItemMap = directoryInfo.getDirectoryItemMap(DirectoryInfo.TYPE_DIRECTORY);
        if (directoryItemMap != null) {
            for (DirectoryItem directoryItem : directoryItemMap) {
                addItem(new DirectoryListViewData(deviceInfo, directoryItem));
            }
        }

        directoryItemMap = directoryInfo.getDirectoryItemMap(DirectoryInfo.TYPE_FILE);
        if (directoryItemMap != null) {
            for (DirectoryItem directoryItem : directoryItemMap) {
                addItem(new DirectoryListViewData(deviceInfo, directoryItem));
            }
        }
    }

    @Override
    protected void onSetData(DirectoryListViewHolder listViewHolder, final DirectoryListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listViewData.getType()) {
                    case DirectoryInfo.TYPE_DIRECTORY:
                    case DirectoryInfo.TYPE_DRIVE:
                        mListSetData.setCurrentPath(listViewData.getPath());
                        loadList();
                        break;
                    case DirectoryInfo.TYPE_UP:
                        mListSetData.setCurrentPath(mListSetData.getParentPath());
                        loadList();
                        break;
                    case DirectoryInfo.TYPE_FILE:
                        if (StringUtils.isBlank(listViewData.getExtension())) {
                            IntentUtils.openingUrl(getContext(), listViewData.getLink());
                        } else {
                            downloadFile(listViewData, null, false);
                        }
                        break;
                }
            }
        });

        listViewHolder.mEtcBtn.setImageResource(mListSetData.getDeviceInfo().isMyDevice() ? R.drawable.btn_share : R.drawable.btn_more);
        listViewHolder.mEtcBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListSetData.getDeviceInfo().isMyDevice()) {
                    downloadFile(listViewData, null, true);
                } else {
                    showOpenMenu(listViewData);
                }
            }
        });
    }

    private void showOpenMenu(final DirectoryListViewData listViewData) {
        DialogUtils.showListDialog(
                "",
                getMenuRes(),
                getMenuValue(),
                getIConsRes(),
                mPageFragment.getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    switch ((String) dataMap.get(ListSelectDialogFragment.DATA_VALUE)) {
                        case "OPEN":
                            downloadFile(listViewData, null, false);
                            break;
                        case "SHARE":
                            downloadFile(listViewData, null, true);
                            break;
                        case "SAVE":
                            downloadFile(listViewData, FileUtils.getFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)), false);
                            break;
                    }
                }
            }
        });
    }

    private int[] getIConsRes() {
        switch (mListSetData.getDeviceKind()) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_ONEDRIVE:
            case DeviceConst.KIND_GMAIL:
            case DeviceConst.KIND_FILESERVER:
                return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_downtophone};
            case DeviceConst.KIND_EVERNOTE:
            case DeviceConst.KIND_WEBMAIL:
                return new int[]{R.drawable.ic_down, R.drawable.ic_share_small};
        }
        return null;
    }

    private String[] getMenuRes() {
        switch (mListSetData.getDeviceKind()) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_ONEDRIVE:
            case DeviceConst.KIND_GMAIL:
            case DeviceConst.KIND_FILESERVER:
                return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s99_link_save_file)};
            case DeviceConst.KIND_EVERNOTE:
            case DeviceConst.KIND_WEBMAIL:
                return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file)};
        }
        return null;
    }

    private String[] getMenuValue() {
        switch (mListSetData.getDeviceKind()) {
            case DeviceConst.KIND_WIN:
            case DeviceConst.KIND_MAC:
            case DeviceConst.KIND_LINUX:
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_ONEDRIVE:
            case DeviceConst.KIND_GMAIL:
            case DeviceConst.KIND_FILESERVER:
                return new String[]{"OPEN", "SHARE", "SAVE"};
            case DeviceConst.KIND_WEBMAIL:
            case DeviceConst.KIND_EVERNOTE:
                return new String[]{"OPEN", "SHARE"};
        }
        return null;
    }

    private void downloadFile(DirectoryListViewData data, File downloadTarget, boolean isShare) {
        DownloadAttachData attachData = new DownloadAttachData();
        attachData.setFileTitle(data.getTitle());
        attachData.setFileExtension(data.getExtension());
        attachData.setFileType(DataBank.getInstance().getExtensionType(data.getExtension()));
        attachData.setFilePath(data.getPath());
        attachData.setFolderPath(data.getFolderPath());
        attachData.setDownloadURL(data.getDownloadURL());
        attachData.setFileSize(data.getSize());
        attachData.setFileModifyTime(data.getModifyTime().getTime());
        attachData.setAttached(false);
        attachData.setShare(isShare);
        attachData.setDeviceInfo(mListSetData.getDeviceInfo());
        attachData.setDownloadTarget(downloadTarget);
        mPageFragment.attachFragment(R.id.layout_list_directory_main, DownloadPartFragment.class, attachData);
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        mNoListImage.clearAnimation();
        if (itemCount == 0) {
            setMessage(R.drawable.img_search_noresult, mContext.getString(R.string.s40_error_nofiles));
        } else {
            setMessage(0, "");
        }
    }

    @Override
    protected DirectoryListViewHolder createListViewHolder(View parent, int viewType) {
        return new DirectoryListViewHolder(parent);
    }

    @Override
    protected void onCleanup() {
    }

    private void setMessage(int imgResId, String message) {
        mNoListImage.setVisibility(imgResId == 0 ? View.GONE : View.VISIBLE);
        mNoListImage.setImageResource(imgResId == 0 ? android.R.color.transparent : imgResId);
        mNoListText.setText(message);
    }
}
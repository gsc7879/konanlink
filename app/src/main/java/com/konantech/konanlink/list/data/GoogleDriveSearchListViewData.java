package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.GoogleDriveSearchFileListController;
import com.konantech.konanlink.model.search.GoogleDriveSearchInfo;

import java.util.Date;

public class GoogleDriveSearchListViewData extends SearchListViewData {
    private GoogleDriveSearchInfo searchInfo;

    public GoogleDriveSearchListViewData(GoogleDriveSearchInfo searchInfo, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.searchInfo = searchInfo;
    }

    public void setFolder(String folder) {
        searchInfo.setFolderName(folder);
    }

    public String getAlternateLink() {
        return searchInfo.getAlternateLink();
    }

    public String getFileExtension() {
        return searchInfo.getFileExtension();
    }

    @Override
    protected ListViewController createViewController() {
        return new GoogleDriveSearchFileListController(this);
    }

    @Override
    public String getTitle() {
        return searchInfo.getName();
    }

    @Override
    public String getFolder() {
        return searchInfo.getFolderName();
    }

    @Override
    public String getFolderPath() {
        return searchInfo.getFolderPath();
    }

    @Override
    public Date getModifyTime() {
        return searchInfo.getModifiedTime();
    }

    @Override
    public long getSize() {
        return searchInfo.getSize();
    }

    @Override
    public String getSummary() {
        return searchInfo.getDescription();
    }

    @Override
    public String getThumbnail() {
        return searchInfo.getThumbNail();
    }

    @Override
    public String getPath() {
        return searchInfo.getPath();
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
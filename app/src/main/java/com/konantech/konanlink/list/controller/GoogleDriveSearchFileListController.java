package com.konantech.konanlink.list.controller;

import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.TextUtils;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.GoogleDriveSearchListViewData;
import com.konantech.konanlink.list.viewholder.GoogleDriveSearchListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class GoogleDriveSearchFileListController extends SearchListController<GoogleDriveSearchListViewData, GoogleDriveSearchListViewHolder> {
    private String mTitle;
    private String mInfo;
    private String mThumbnail;
    private String mDescription;
    private String mExif;
    private int    mTypeResId;

    public GoogleDriveSearchFileListController(GoogleDriveSearchListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle       = mViewData.getTitle();
        mThumbnail   = mViewData.getThumbnail();
        mDescription = mViewData.getSummary();
        mInfo        = getInfo();
        mTypeResId   = InfoUtils.getTypeIconResId(DataBank.getInstance().getExtensionType(FilenameUtils.getExtension(mViewData.getTitle())), false);
    }

    private String getInfo() {
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(mViewData.getModifyTime());
        String size = TextUtils.toNumInUnits(mViewData.getSize());
        return String.format("%s | %s", date, size);
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, GoogleDriveSearchListViewHolder viewHolder) {
        mPicasso.load(mTypeResId).into(viewHolder.mIcon);
        viewHolder.mName.setText(mTitle);
        viewHolder.mInfo.setText(mInfo);

        if (StringUtils.isBlank(mThumbnail)) {
            viewHolder.mThumbnail.setVisibility(View.GONE);
        } else {
            viewHolder.mThumbnail.setVisibility(View.VISIBLE);
            mPicasso.load(mThumbnail).into(viewHolder.mThumbnail);
        }

        if (StringUtils.isBlank(mDescription)) {
            viewHolder.mDescription.setVisibility(View.GONE);
        } else {
            viewHolder.mDescription.setVisibility(View.VISIBLE);
            viewHolder.mDescription.setText(mDescription);
        }

        if (StringUtils.isBlank(mExif)) {
            viewHolder.mExif.setVisibility(View.GONE);
        } else {
            viewHolder.mExif.setVisibility(View.VISIBLE);
            viewHolder.mExif.setText(mExif);
        }
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.R;
import com.konantech.konanlink.dao.RecentKeyword;
import com.konantech.konanlink.db.DBManager;
import com.konantech.konanlink.list.data.RecentKeywordListViewData;
import com.konantech.konanlink.list.model.RecentKeywordListSetData;
import com.konantech.konanlink.list.viewholder.RecentKeywordListViewHolder;
import com.konantech.konanlink.page.fragment.MainPageFragment;
import com.konantech.konanlink.part.DeviceAddPartFragment;
import com.konantech.konanlink.utils.DialogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RecentKeywordListAdapter extends ListViewAdapter<RecentKeywordListViewData, RecentKeywordListViewHolder, RecentKeywordListSetData, List<RecentKeywordListViewData>> {
    private MainPageFragment mMainPageFragment;
    private DBManager mKeywordDBManager;

    @InjectView(id = R.id.text_listno)
    private TextView mNoListText;

    @InjectView(id = R.id.img_listno)
    private ImageView mNoListImg;

    @InjectView(id = R.id.btn_list_keyword_delete)
    private TextView mDeleteAllBtn;

    public RecentKeywordListAdapter(MainPageFragment mainPageFragment, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, RecentKeywordListSetData data) {
        super(mainPageFragment.getView(), listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mMainPageFragment = mainPageFragment;
        mKeywordDBManager = DBManager.getInstance();
    }

    public void updateKeyword(String keyword) {
        mKeywordDBManager.saveKeyword(keyword);
    }

    public void deleteAll() {
        showDeleteDialog();
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        setMessage(0, "");
        recentKeywordLoad();
    }

    private void recentKeywordLoad() {
        String inputKeyword = mListSetData.getRecentKeyword();
        List<RecentKeyword> recentKeywords = mKeywordDBManager.getRecentKeywords(inputKeyword);
        List<RecentKeywordListViewData> recentKeywordListViewDataList = new ArrayList<>();
        for (RecentKeyword recentKeyword : recentKeywords) {
            recentKeywordListViewDataList.add(new RecentKeywordListViewData(recentKeyword, inputKeyword));
        }
        endLoadList(recentKeywordListViewDataList);
    }

    @Override
    protected void onEndLoad(List<RecentKeywordListViewData> recentKeywordList) {
        for (RecentKeywordListViewData recentKeywordListViewData : recentKeywordList) {
            addItem(recentKeywordListViewData);
        }
    }

    @Override
    protected RecentKeywordListViewHolder createListViewHolder(View parent, int viewType) {
        return new RecentKeywordListViewHolder(parent);
    }

    @Override
    protected void onSetData(RecentKeywordListViewHolder listViewHolder, final RecentKeywordListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateKeyword(listViewData.getKeyword());
                mMainPageFragment.closeSearchView(listViewData.getKeyword());
            }
        });

        listViewHolder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mKeywordDBManager.deleteById(listViewData.getIdx());
                removeItem(listViewData);
                resultCheck(getItemCount());
            }
        });
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        resultCheck(itemCount);
    }

    private void resultCheck(int itemCount) {
        if (itemCount == 0) {
            setMessage(R.drawable.img_search_noresult, mContext.getString(R.string.s99_text_no_keyword_history));
            setDeleteAllBtnLayout(false);
        } else {
            setMessage(0, "");
            setDeleteAllBtnLayout(true);
        }
    }

    private void setDeleteAllBtnLayout(boolean isVisible) {
        mDeleteAllBtn.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onCleanup() {
    }

    private void showDeleteDialog() {
        DialogUtils.showConverseDialog("", mContext.getString(R.string.s14_dialog_text_delete_keywords), R.string.s99_dialog_btn_cancel, R.string.s14_dialog_btn_delete, mMainPageFragment.getFragmentManager(), new DialogController.OnDismissListener() {
            @Override
            public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                if (isPositive) {
                    mKeywordDBManager.deleteAll();
                    loadList();
                }
            }
        });
    }

    private void setMessage(int imgResId, String message) {
        mNoListText.setText(message);
        mNoListImg.setImageResource(imgResId == 0 ? android.R.color.transparent : imgResId);
        mNoListImg.setVisibility(imgResId == 0 ? View.GONE : View.VISIBLE);
    }

    @Click(R.id.btn_listno)
    public void addDevice() {
        mMainPageFragment.attachFragment(R.id.layout_main_main, DeviceAddPartFragment.class);
    }
}

package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.pager.IsNextListPager;
import com.dignara.lib.utils.IntentUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.list.data.OneDriveSearchListViewViewData;
import com.konantech.konanlink.list.model.OneDriveSearchListSetData;
import com.konantech.konanlink.list.viewholder.OneDriveSearchListViewHolder;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.DownloadPartFragment;
import com.konantech.konanlink.part.OneDriveSearchPartFragment;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class OneDriveSearchListAdapter extends SearchListAdapter<OneDriveSearchListViewViewData, OneDriveSearchListViewHolder, OneDriveSearchListSetData, SearchResult<OneDriveFile.DriveFile>> {
    private static final int COUNT_LIMIT = 50;

    private IsNextListPager mNextListPager;

    @InjectView(id = R.id.text_list_search_onedrive_device_name)
    private TextView mResultText;

    public OneDriveSearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, OneDriveSearchListSetData data) {
        super(partFragment, parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNextListPager = new IsNextListPager(COUNT_LIMIT);
        setNextListController(mNextListPager);
    }

    @Override
    protected void startDownload(OneDriveSearchListViewViewData searchFileListData, File downloadTarget, boolean isShare) {
        if (StringUtils.isBlank(searchFileListData.getFileExtension())) {
            IntentUtils.openingUrl(getContext(), searchFileListData.getLink());
        } else {
            DownloadAttachData attachData = new DownloadAttachData();
            String title = searchFileListData.getTitle();
            String extension = FilenameUtils.getExtension(title);
            attachData.setFileTitle(title);
            attachData.setFileExtension(extension);
            attachData.setFileType(DataBank.getInstance().getExtensionType(extension));
            attachData.setFilePath(searchFileListData.getPath());
            attachData.setFolderPath(searchFileListData.getFolderPath());
            attachData.setFileSize(searchFileListData.getSize());
            attachData.setFileModifyTime(searchFileListData.getModifyTime().getTime());
            attachData.setAttached(false);
            attachData.setShare(isShare);
            attachData.setDeviceInfo(mDeviceInfo);
            attachData.setDownloadTarget(downloadTarget);
            mPageFragment.attachFragment(R.id.layout_main_main, DownloadPartFragment.class, attachData);
        }
    }

    @Override
    protected OneDriveSearchListViewHolder createListViewHolder(View parent, int viewType) {
        return new OneDriveSearchListViewHolder(parent);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        super.onStartLoad(isFirst);
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        mPageFragment.attachFragment(R.id.layout_list_search_main, OneDriveSearchPartFragment.class, mDeviceInfo, mNextListPager.getOffset(), mNextListPager.getLimit(), mListSetData, mNoListText);
    }

    @Override
    protected void onEndLoad(SearchResult<OneDriveFile.DriveFile> searchResult) {
        for (OneDriveFile.DriveFile driveFile : searchResult.getResultList()) {
            addItem(new OneDriveSearchListViewViewData(driveFile, mDeviceInfo));
        }
        mNextListPager.setListCount(searchResult.isNext(), getItemCount());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        super.onAddedItems(itemCount, errorCode);
    }

    @Override
    protected String[] getMenuRes(OneDriveSearchListViewViewData searchListData) {
        if (StringUtils.isBlank(searchListData.getFileExtension())) {
            return new String[]{mContext.getString(R.string.s19_link_file_explorer)};
        } else {
            return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s19_link_file_explorer), mContext.getString(R.string.s99_link_save_file)};
        }
    }

    @Override
    protected String[] getMenuValue(OneDriveSearchListViewViewData searchListData) {
        if (StringUtils.isBlank(searchListData.getFileExtension())) {
            return new String[]{"FOLDER"};
        } else {
            return new String[]{"OPEN", "SHARE", "FOLDER", "SAVE"};
        }
    }

    @Override
    protected int[] getMenuIcons(OneDriveSearchListViewViewData searchListData) {
        if (StringUtils.isBlank(searchListData.getFileExtension())) {
            return new int[]{R.drawable.ic_filefolder};
        } else {
            return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_filefolder, R.drawable.ic_downtophone};
        }
    }
}

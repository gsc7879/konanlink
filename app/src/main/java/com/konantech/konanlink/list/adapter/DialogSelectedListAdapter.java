package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.view.View.OnClickListener;

import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.list.data.DialogSelectedListViewData;
import com.konantech.konanlink.list.model.DialogCheckListData;
import com.konantech.konanlink.list.model.DialogSelectedListSetData;
import com.konantech.konanlink.list.viewholder.DialogSelectedListViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DialogSelectedListAdapter extends ListViewAdapter<DialogSelectedListViewData, DialogSelectedListViewHolder, DialogSelectedListSetData, List<DialogCheckListData>> {
    protected OnSelectedItemListener mSelectedItemListener;

    public interface OnSelectedItemListener {
        void onSelectedItem(DialogSelectedListViewData listViewData);
    }

    public DialogSelectedListAdapter(View rootView, OnSelectedItemListener selectedItemListener, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DialogSelectedListSetData dialogSelectedListSetData) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, dialogSelectedListSetData);
        mSelectedItemListener = selectedItemListener;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        endLoadList(getCheckListData(mListSetData.getTitles(), mListSetData.getValues(), mListSetData.getSelectedIndex()));
    }

    private List<DialogCheckListData> getCheckListData(String[] titles, String[] values, int selectedIndex) {
        List<DialogCheckListData> checkListData = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            checkListData.add(new DialogCheckListData(titles[i], values[i], i == selectedIndex));
        }
        return checkListData;
    }

    @Override
    protected void onEndLoad(List<DialogCheckListData> list) {
        DialogCheckListData listData;
        for (int i = 0; i < list.size(); i++) {
            listData = list.get(i);
            addItem(new DialogSelectedListViewData(i, listData.getTitle(), listData.getValue(), listData.isChecked()));
        }
    }

    @Override
    protected DialogSelectedListViewHolder createListViewHolder(View parent, int viewType) {
        return new DialogSelectedListViewHolder(parent);
    }

    @Override
    protected void onSetData(DialogSelectedListViewHolder listViewHolder, final DialogSelectedListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < getItemCount(); i++) {
                    getItem(i).setSelected(false);
                }
                listViewData.setSelected(true);
                notifyDataSetChanged();
                mSelectedItemListener.onSelectedItem(listViewData);
            }
        });
    }

    @Override
    protected void onCleanup() {
    }
}

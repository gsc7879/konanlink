package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.model.device.DeviceInfo;

public class DialogListSetData extends ListSetData<DeviceInfo> {
    private String[] titles;
    private String[] values;
    private String[] checked;

    public String[] getTitles() {
        return titles;
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public String[] getChecked() {
        return checked;
    }

    public void setChecked(String[] checked) {
        this.checked = checked;
    }

    @Override
    protected void onInitData() {}
}

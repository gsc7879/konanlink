package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.DialogCheckListController;

public class DialogCheckListViewData extends ListViewData {
    private int      idx;
    private String   title;
    private String   value;
    private boolean  isChecked;

    public DialogCheckListViewData(int idx, String title, String value, boolean isChecked) {
        this.idx       = idx;
        this.title     = title;
        this.value     = value;
        this.isChecked = isChecked;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    protected ListViewController createViewController() {
        return new DialogCheckListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
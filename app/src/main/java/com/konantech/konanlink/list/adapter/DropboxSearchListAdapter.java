package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.pager.IsNextListPager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.DropboxSearchListViewViewData;
import com.konantech.konanlink.list.model.DropboxSearchListSetData;
import com.konantech.konanlink.list.viewholder.DropboxSearchListViewHolder;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.model.search.DropboxSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.DownloadPartFragment;
import com.konantech.konanlink.part.DropboxSearchPartFragment;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class DropboxSearchListAdapter extends SearchListAdapter<DropboxSearchListViewViewData, DropboxSearchListViewHolder, DropboxSearchListSetData, SearchResult<DropboxSearchInfo>> {
    private static final int COUNT_LIMIT = 50;

    private IsNextListPager mNextListPager;

    @InjectView(id = R.id.text_list_search_dropbox_device_name)
    private TextView mResultText;

    public DropboxSearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DropboxSearchListSetData data) {
        super(partFragment, parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNextListPager = new IsNextListPager(COUNT_LIMIT);
        setNextListController(mNextListPager);
    }

    @Override
    protected void startDownload(DropboxSearchListViewViewData searchFileListData, File downloadTarget, boolean isShare) {
        DownloadAttachData attachData = new DownloadAttachData();

        String title = searchFileListData.getTitle();
        String extension = FilenameUtils.getExtension(title);
        attachData.setFileTitle(title);
        attachData.setFileExtension(extension);
        attachData.setFileType(DataBank.getInstance().getExtensionType(extension));
        attachData.setFilePath(searchFileListData.getPath());
        attachData.setFolderPath(searchFileListData.getFolderPath());
        attachData.setFileSize(searchFileListData.getSize());
        attachData.setFileModifyTime(searchFileListData.getModifyTime().getTime());
        attachData.setAttached(false);
        attachData.setShare(isShare);
        attachData.setDeviceInfo(mDeviceInfo);
        attachData.setDownloadTarget(downloadTarget);
        mPageFragment.attachFragment(R.id.layout_main_main, DownloadPartFragment.class, attachData);
    }

    @Override
    protected DropboxSearchListViewHolder createListViewHolder(View parent, int viewType) {
        return new DropboxSearchListViewHolder(parent);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        super.onStartLoad(isFirst);
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        mPageFragment.attachFragment(R.id.layout_list_search_main, DropboxSearchPartFragment.class, mDeviceInfo, mNextListPager.getOffset(), mNextListPager.getLimit(), mListSetData, mNoListText);
    }

    @Override
    protected void onEndLoad(SearchResult<DropboxSearchInfo> searchResult) {
        for (DropboxSearchInfo searchInfo : searchResult.getResultList()) {
            addItem(new DropboxSearchListViewViewData(searchInfo, mDeviceInfo));
        }
        mNextListPager.setListCount(searchResult.isNext(), getItemCount());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        super.onAddedItems(itemCount, errorCode);
    }

    @Override
    protected String[] getMenuRes(DropboxSearchListViewViewData searchListData) {
        return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s19_link_file_explorer), mContext.getString(R.string.s99_link_save_file)};
    }

    @Override
    protected String[] getMenuValue(DropboxSearchListViewViewData searchListData) {
        return new String[]{"OPEN", "SHARE", "FOLDER", "SAVE"};
    }

    @Override
    protected int[] getMenuIcons(DropboxSearchListViewViewData data) {
        return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_filefolder, R.drawable.ic_downtophone};
    }
}

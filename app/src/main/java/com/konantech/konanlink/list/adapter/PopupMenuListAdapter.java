package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.view.View.OnClickListener;

import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.list.data.PopupMenuListViewData;
import com.konantech.konanlink.list.model.PopupMenuListData;
import com.konantech.konanlink.list.model.PopupMenuListSetData;
import com.konantech.konanlink.list.viewholder.PopupMenuListViewHolder;

import java.util.List;

public class PopupMenuListAdapter extends ListViewAdapter<PopupMenuListViewData, PopupMenuListViewHolder, PopupMenuListSetData, List<PopupMenuListData>> {
    private OnSelectedItemListener mSelectedItemListener;

    public interface OnSelectedItemListener {
        void onSelectedItem(int callbackId, PopupMenuListViewData listViewData);
    }

    public PopupMenuListAdapter(View rootView, OnSelectedItemListener selectedItemListener, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, PopupMenuListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mSelectedItemListener = selectedItemListener;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        endLoadList(mListSetData.getPopupMenuListDataList());
    }

    @Override
    protected void onEndLoad(List<PopupMenuListData> popupMenuListDataList) {
        PopupMenuListData popupMenuListData;
        for (int i = 0; i < popupMenuListDataList.size(); i++) {
            popupMenuListData = popupMenuListDataList.get(i);
            addItem(new PopupMenuListViewData(i, popupMenuListData.getTitle(), popupMenuListData.isSelected()));
        }
    }

    @Override
    protected PopupMenuListViewHolder createListViewHolder(View parent, int viewType) {
        return new PopupMenuListViewHolder(parent);
    }

    @Override
    protected void onSetData(PopupMenuListViewHolder listViewHolder, final PopupMenuListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItemListener.onSelectedItem(mListSetData.getCallbackId(), listViewData);
            }
        });
    }

    @Override
    protected void onCleanup() {
    }
}

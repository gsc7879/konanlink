package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class RecentKeywordListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public TextView mKeyword;
    public ImageButton mDeleteBtn;

    public RecentKeywordListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_combobox_keyword_main);
        mKeyword = (TextView) convertView.findViewById(R.id.text_combobox_keyword);
        mDeleteBtn = (ImageButton) convertView.findViewById(R.id.btn_combobox_delete);
    }
}

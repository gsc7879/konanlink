package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.DeviceListController;

public class DeviceListViewData extends ListViewData {
    private DeviceInfo deviceInfo;
    private int        errorCode;

    public DeviceListViewData(DeviceInfo deviceInfo, int errorCode) {
        this.deviceInfo = deviceInfo;
        this.errorCode  = errorCode;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public int getErrorCode() {
        return errorCode;
    }

    @Override
    protected ListViewController createViewController() {
        return new DeviceListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
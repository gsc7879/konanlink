package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class PaymentListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public TextView     mDate;
    public TextView     mTerm;
    public TextView     mDescription;

    public PaymentListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout      = (LinearLayout) convertView.findViewById(R.id.layout_payment);
        mDate        = (TextView) convertView.findViewById(R.id.text_payment_date);
        mTerm        = (TextView) convertView.findViewById(R.id.text_payment_term);
        mDescription = (TextView) convertView.findViewById(R.id.text_payment_description);
    }
}

package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.PopupMenuListController;

public class PopupMenuListViewData extends ListViewData {
    private int     idx;
    private String  title;
    private boolean isSelected;

    public PopupMenuListViewData(int idx, String title, boolean selected) {
        this.idx  = idx;
        this.title = title;
        isSelected = selected;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    @Override
    protected ListViewController createViewController() {
        return new PopupMenuListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
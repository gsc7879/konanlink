package com.konantech.konanlink.list.data;

import android.graphics.drawable.Drawable;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.MenuListController;

public class MenuListViewData extends ListViewData {
    private int      idx;
    private String   title;
    private String   value;
    private Drawable iconDrawable;
    private boolean  isSelected;

    public MenuListViewData(int idx, Drawable iconDrawable, String title, String value, boolean isSelected) {
        this.idx          = idx;
        this.title        = title;
        this.iconDrawable = iconDrawable;
        this.value        = value;
        this.isSelected   = isSelected;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public String getValue() {
        return value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    @Override
    protected ListViewController createViewController() {
        return new MenuListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
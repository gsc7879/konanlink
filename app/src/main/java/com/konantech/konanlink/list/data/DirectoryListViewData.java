package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.DirectoryListController;
import com.konantech.konanlink.model.directory.DirectoryItem;

import java.util.Date;

public class DirectoryListViewData extends SearchListViewData {
    private DirectoryItem mDirectoryItem;

    public DirectoryListViewData(DeviceInfo deviceInfo, DirectoryItem directoryItem) {
        super(deviceInfo);
        mDirectoryItem = directoryItem;
    }

    public String getLink() {
        return mDirectoryItem.getLink();
    }

    public String getType() {
        return mDirectoryItem.getType();
    }

    public String getExtension() {
        return mDirectoryItem.getExtension();
    }

    public void setModifiedTime(Date modifiedTime) {
        mDirectoryItem.setModifiedTime(modifiedTime);
    }

    public void setTitle(String title) {
        mDirectoryItem.setName(title);
    }

    public void setSize(long size) {
        mDirectoryItem.setSize(size);
    }

    public String getDownloadURL () {
        return mDirectoryItem.getDownloadURL();
    }

    @Override
    public String getTitle() {
        return mDirectoryItem.getName();
    }

    @Override
    public String getFolder() {
        return null;
    }

    @Override
    public String getFolderPath() {
        return null;
    }

    @Override
    public Date getModifyTime() {
        return mDirectoryItem.getModifiedTime();
    }

    public long getSize() {
        return mDirectoryItem.getSize();
    }

    @Override
    public String getSummary() {
        return null;
    }

    @Override
    public String getThumbnail() {
        return null;
    }

    @Override
    public String getPath() {
        return mDirectoryItem.getPath();
    }

    @Override
    protected ListViewController createViewController() {
        return new DirectoryListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
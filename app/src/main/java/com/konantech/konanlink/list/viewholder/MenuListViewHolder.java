package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class MenuListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public TextView     mTitle;

    public MenuListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_menu_main);
        mIcon   = (ImageView)    convertView.findViewById(R.id.img_menu_icon);
        mTitle  = (TextView)     convertView.findViewById(R.id.text_menu_title);
    }
}

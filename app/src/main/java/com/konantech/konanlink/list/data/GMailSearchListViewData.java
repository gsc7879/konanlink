package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.GMailSearchFileListController;
import com.konantech.konanlink.model.search.GMailSearchInfo;

import java.util.Date;

public class GMailSearchListViewData extends SearchListViewData {
    private GMailSearchInfo searchInfo;

    public GMailSearchListViewData(GMailSearchInfo searchInfo, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.searchInfo = searchInfo;
    }

    public String getSnippet() {
        return searchInfo.getSnippet();
    }

    public String getFrom() {
        return searchInfo.getFrom();
    }

    public String getTo() {
        return searchInfo.getTo();
    }

    public void setSnippet(String snippet) {
        searchInfo.setSnippet(snippet);
    }

    public void setFrom(String from) {
        searchInfo.setFrom(from);
    }

    public void setTo(String to) {
        searchInfo.setTo(to);
    }

    public void setSize(Integer size) {
        searchInfo.setSize(size);
    }

    public void setFolderPath(String folderPath) {
        searchInfo.setFolderPath(folderPath);
    }

    public void setTitle(String title) {
        searchInfo.setName(title);
    }

    public void setModifyTime(Date modifyTime) {
        searchInfo.setModifiedTime(modifyTime);
    }

    @Override
    protected ListViewController createViewController() {
        return new GMailSearchFileListController(this);
    }

    @Override
    public String getTitle() {
        return searchInfo.getName();
    }

    @Override
    public String getFolder() {
        return searchInfo.getFolderName();
    }

    @Override
    public String getFolderPath() {
        return searchInfo.getFolderPath();
    }

    @Override
    public Date getModifyTime() {
        return searchInfo.getModifiedTime() == null ? new Date(0) : searchInfo.getModifiedTime();
    }

    @Override
    public long getSize() {
        return searchInfo.getSize();
    }

    @Override
    public String getSummary() {
        return String.format("- From : %s<br>- To : %s<br><br>%s...", getFrom(), getTo(), getSnippet());
    }

    @Override
    public String getThumbnail() {
        return "";
    }

    @Override
    public String getPath() {
        return searchInfo.getPath();
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
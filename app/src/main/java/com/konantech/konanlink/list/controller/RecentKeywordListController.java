package com.konantech.konanlink.list.controller;

import android.text.Html;
import android.text.Spanned;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.RecentKeywordListViewData;
import com.konantech.konanlink.list.viewholder.RecentKeywordListViewHolder;

public class RecentKeywordListController extends ListViewController<RecentKeywordListViewData, RecentKeywordListViewHolder> {
    private Spanned keyword;

    public RecentKeywordListController(RecentKeywordListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        keyword = Html.fromHtml(mViewData.getTitle());
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, RecentKeywordListViewHolder listViewHolder) {
        listViewHolder.mKeyword.setText(keyword);
    }

    @Override
    protected void onCleanup() {
    }
}
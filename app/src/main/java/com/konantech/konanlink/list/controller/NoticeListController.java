package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.NoticeListViewData;
import com.konantech.konanlink.list.viewholder.NoticeListViewHolder;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class NoticeListController extends ListViewController<NoticeListViewData, NoticeListViewHolder> {
    private String mTitle;
    private String mDate;

    public NoticeListController(NoticeListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle      = mViewData.getTitle();
        mDate       = FastDateFormat.getInstance("yyyy-MM-dd", Locale.getDefault()).format(mViewData.getCreatedDate());
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, NoticeListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(mTitle);
        listViewHolder.mDate.setText(mDate);
        listViewHolder.mLayout.setSelected(mViewData.isRead());
    }

    @Override
    protected void onCleanup() {
    }
}

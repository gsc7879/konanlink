package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.model.device.DeviceInfo;

public class DeviceListSetData extends ListSetData<DeviceInfo> {
    private boolean isRefresh;

    public boolean isRefreshDeviceList() {
        return isRefresh;
    }

    public void setRefresh(boolean refresh) {
        isRefresh = refresh;
    }

    @Override
    protected void onInitData() {
    }
}

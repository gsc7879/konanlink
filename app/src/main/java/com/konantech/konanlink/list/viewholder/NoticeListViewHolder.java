package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class NoticeListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public TextView     mTitle;
    public TextView     mDate;

    public NoticeListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_notice);
        mTitle  = (TextView)     convertView.findViewById(R.id.text_notice_title);
        mDate  = (TextView)      convertView.findViewById(R.id.text_notice_date);
    }
}

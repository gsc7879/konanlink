package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.AgentSearchListController;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Date;

public class AgentSearchListViewViewData extends SearchListViewData {
    private SearchResponse.Item searchItem;
    private String      thumbnail;
    private String      thumbnailUrl;
    private String      downloadUrl;

    public AgentSearchListViewViewData(SearchResponse.Item agentSearchItem, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.searchItem = agentSearchItem;
    }

    public String getType() {
        return searchItem.getType();
    }

    public String getExtension() {
        return searchItem.getExtension();
    }

    public String getUrl() {
        return searchItem.getDownloadPath();
    }

    public boolean isEmail() {
        return StringUtils.equalsIgnoreCase(searchItem.getType(), "mail");
    }

    public boolean isAttached() {
        return searchItem.isAttached();
    }

    public String getHighlightTitle() {
        return StringUtils.defaultString(searchItem.getHighLightedTitle());
    }

    public String getEmailFrom() {
        return searchItem .getFrom();
    }

    public String getEmailTo() {
        return searchItem.getTo();
    }

    public String getLocation() {
        return searchItem.getLocation();
    }

    public String getResolution() {
        return searchItem.getResolution();
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getEmailTitle() {
        return searchItem.getTitle();
    }

    public String getThumbnailUrl() {
        return searchItem.getThumbNailUrl();
    }

    public String getDownloadUrl() {
        return searchItem.getDownloadURL();
    }

    @Override
    public long getSize() {
        return searchItem.getSize();
    }

    @Override
    public String getSummary() {
        return searchItem.getSummary();
    }

    @Override
    public Date getModifyTime() {
        return new Date( NumberUtils.toLong(searchItem.getModifyTime()));
    }

    @Override
    public String getTitle() {
        return searchItem.getTitle();
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public String getPath() {
        return searchItem.getDownloadPath();
    }

    @Override
    public String getFolder() {
        return searchItem.getFolder();
    }

    @Override
    public String getFolderPath() {
        return searchItem.getFolder();
    }

    @Override
    protected ListViewController createViewController() {
        return new AgentSearchListController(this);
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
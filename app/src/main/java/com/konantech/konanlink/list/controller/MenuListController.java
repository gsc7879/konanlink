package com.konantech.konanlink.list.controller;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.MenuListViewData;
import com.konantech.konanlink.list.viewholder.MenuListViewHolder;

import org.apache.commons.lang3.StringUtils;

public class MenuListController extends ListViewController<MenuListViewData, MenuListViewHolder> {
    private String   title;
    private Drawable iconDrawable;
    private boolean  isSelected;

    public MenuListController(MenuListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        title        = mViewData.getTitle();
        isSelected   = mViewData.isSelected();
        iconDrawable = mViewData.getIconDrawable();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, MenuListViewHolder listViewHolder) {
        if (StringUtils.isBlank(title)) {
            listViewHolder.mTitle.setVisibility(View.GONE);
        } else {
            listViewHolder.mTitle.setVisibility(View.VISIBLE);
            listViewHolder.mTitle.setText(title);
            listViewHolder.mTitle.setSelected(isSelected);
        }
        listViewHolder.mIcon.setImageDrawable(iconDrawable);
    }

    @Override
    protected void onCleanup() {
    }
}
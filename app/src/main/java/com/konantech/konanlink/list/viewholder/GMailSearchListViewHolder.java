package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.konantech.konanlink.R;

public class GMailSearchListViewHolder extends SearchListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public TextView     mName;
    public TextView     mDescription;
    public TextView     mInfo;
    public TextView     mFrom;
    public TextView     mTo;

    public GMailSearchListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public ViewGroup getLayout() {
        return mLayout;
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout      = (LinearLayout) convertView.findViewById(R.id.layout_file_search_gmail);
        mIcon        = (ImageView)    convertView.findViewById(R.id.img_file_search_gmail_icon);
        mName        = (TextView)     convertView.findViewById(R.id.text_file_search_gmail_name);
        mInfo        = (TextView)     convertView.findViewById(R.id.text_file_search_gmail_info);
        mDescription = (TextView)     convertView.findViewById(R.id.text_file_search_gmail_description);
        mFrom        = (TextView)     convertView.findViewById(R.id.text_file_search_gmail_from);
        mTo          = (TextView)     convertView.findViewById(R.id.text_file_search_gmail_to);
    }
}

package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

import java.util.List;

public class MenuListSetData extends ListSetData<List<MenuListData>> {
    private List<MenuListData> menuListDataList;

    public List<MenuListData> getMenuListDataList() {
        return menuListDataList;
    }

    public void setMenuListDataList(List<MenuListData> menuListDataList) {
        this.menuListDataList = menuListDataList;
    }

    @Override
    protected void onInitData() {}
}

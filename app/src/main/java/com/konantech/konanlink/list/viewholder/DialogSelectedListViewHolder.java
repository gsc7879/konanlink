package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class DialogSelectedListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public CheckBox     mTitle;

    public DialogSelectedListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_dialog_selected);
        mTitle  = (CheckBox) convertView.findViewById(R.id.select_dialog_selected_title);
    }
}

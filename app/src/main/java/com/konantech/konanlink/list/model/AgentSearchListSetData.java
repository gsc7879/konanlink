package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.service.AgentApiService;

public class AgentSearchListSetData extends ListSetData {
    private String keyword;
    private String searchType;
    private String sortValue;
    private String periodStart;
    private String periodEnd;
    private String searchTarget;
    private String deviceKey;
    private String agentDeviceKey;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSortValue() {
        return sortValue;
    }

    public void setSortValue(String sortValue) {
        this.sortValue = sortValue;
    }

    public String getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(String periodStart) {
        this.periodStart = periodStart;
    }

    public String getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(String periodEnd) {
        this.periodEnd = periodEnd;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getAgentDeviceKey() {
        return agentDeviceKey;
    }

    public void setAgentDeviceKey(String agentDeviceKey) {
        this.agentDeviceKey = agentDeviceKey;
    }

    public String getSearchUrl() {
        return String.format(AgentApiService.URL_SEARCH, agentDeviceKey);
    }

    @Override
    protected void onInitData() {}
}

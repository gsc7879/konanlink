package com.konantech.konanlink.list.controller;

import android.content.Context;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.view.View;
import android.webkit.MimeTypeMap;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.TextUtils;
import com.dignara.lib.utils.UnitUtils;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.Dimensions;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.GpsCoordinates;
import com.dropbox.core.v2.files.MediaInfo;
import com.dropbox.core.v2.files.Metadata;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.download.thumbnail.DropBoxThumbDownloader;
import com.konantech.konanlink.list.data.DropboxSearchListViewViewData;
import com.konantech.konanlink.list.viewholder.DropboxSearchListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class DropboxSearchListController extends SearchListController<DropboxSearchListViewViewData, DropboxSearchListViewHolder> {
    private String mTitle;
    private String mInfo;
    private String mThumbnail;
    private String mExif;
    private int    mTypeResId;
    private int    mThumbnailPixel;
    private AsyncTask<Void, Void, String> mExecute;
    private Picasso mDropBoxPicasso;

    public DropboxSearchListController(DropboxSearchListViewViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        setInfo();
        checkAccount(listViewAdapter);
        mTitle          = mViewData.getTitle();
        mThumbnail      = mViewData.getThumbnail();
        mTypeResId      = InfoUtils.getTypeIconResId(DataBank.getInstance().getExtensionType(FilenameUtils.getExtension(mViewData.getTitle())), false);
        mThumbnailPixel = UnitUtils.convertDpToPixel(listViewAdapter.getContext(), 62);
        if (mDropBoxPicasso == null) {
            mDropBoxPicasso = getPicasso(listViewAdapter.getContext(), (DbxClientV2) mViewData.getDeviceInfo().getConnector());
        }
    }

    private void setInfo() {
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(mViewData.getModifyTime());
        String size = TextUtils.toNumInUnits(mViewData.getSize());
        String resolution = mViewData.getResolution();
        mInfo = resolution == null ? String.format("%s | %s", date, size) : String.format("%s | %s | %s", date, resolution, size);
        mExif = mViewData.getAddress();
    }

    @Override
    public void setView(final ListViewAdapter listViewAdapter, final DropboxSearchListViewHolder viewHolder) {
        mPicasso.load(mTypeResId).into(viewHolder.mIcon);
        viewHolder.mName.setText(mTitle);
        viewHolder.mInfo.setText(mInfo);

        if (StringUtils.isBlank(mExif)) {
            viewHolder.mExif.setVisibility(View.GONE);
        } else {
            viewHolder.mExif.setVisibility(View.VISIBLE);
            viewHolder.mExif.setText(mExif);
        }

        String ext = mViewData.getTitle().substring(mViewData.getTitle().indexOf(".") + 1);
        String type =  MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
        if (type != null && type.startsWith("image/")) {
            viewHolder.mThumbnail.setVisibility(View.VISIBLE);
            mDropBoxPicasso.load(DropBoxThumbDownloader.buildPicassoUri(mViewData.getPath()))
                    .error(android.R.color.transparent)
                    .resize(mThumbnailPixel, mThumbnailPixel)
                    .centerCrop()
                    .into(viewHolder.mThumbnail);

        } else {
            viewHolder.mThumbnail.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
        if (mExecute != null) {
            mExecute.cancel(true);
            mExecute = null;
        }
    }


    private void checkAccount(final ListViewAdapter listViewAdapter) {
        if (mExecute != null) {
            return;
        }
        mExecute = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                setMediaInfo((DbxClientV2) mViewData.getDeviceInfo().getConnector(), listViewAdapter.getContext());
                return null;
            }

            @Override
            protected void onPostExecute(String loginAccount) {
                setInfo();
                listViewAdapter.notifyDataSetChanged();

            }

            @Override
            protected void onCancelled(String o) {
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setMediaInfo(final DbxClientV2 dbxClient, Context context) {
        try {
            Metadata metadata = dbxClient.files().getMetadata(mViewData.getPath(), true);
            MediaInfo mediaInfo = ((FileMetadata) metadata).getMediaInfo();
            if (mediaInfo != null) {
                Dimensions dimensions = mediaInfo.getMetadataValue().getDimensions();
                if (dimensions != null) {
                    mViewData.setResolution(String.format("%d x %d", dimensions.getWidth(), dimensions.getHeight()));
                }

                GpsCoordinates location = mediaInfo.getMetadataValue().getLocation();
                if (location != null) {
                    mViewData.setAddress(new Geocoder(context, Locale.getDefault()).getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0).getAddressLine(0));
                }
            }
        } catch (Exception e) {
        }
    }

    private Picasso getPicasso(Context context, DbxClientV2 clientV2) {
        return new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(context))
                .addRequestHandler(new DropBoxThumbDownloader(clientV2))
                .build();
    }
}
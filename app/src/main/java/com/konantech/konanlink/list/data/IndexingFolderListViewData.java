package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.IndexingFolderListController;

import java.io.File;

public class IndexingFolderListViewData extends ListViewData {
    private File    folder;
    private boolean isIndexing;

    public IndexingFolderListViewData(File folder, boolean isIndexing) {
        this.folder     = folder;
        this.isIndexing = isIndexing;
    }

    public File getFolder() {
        return folder;
    }

    public String getFolderName() {
        return folder.getName();
    }

    public boolean isIndexing() {
        return isIndexing;
    }

    public void setIndexing(boolean isIndexing) {
        this.isIndexing = isIndexing;
    }

    @Override
    protected ListViewController createViewController() {
        return new IndexingFolderListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
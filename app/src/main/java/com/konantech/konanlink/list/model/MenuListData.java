package com.konantech.konanlink.list.model;

import android.graphics.drawable.Drawable;

public class MenuListData {
    private String   title;
    private String   value;
    private Drawable iconDrawable;
    private boolean  isSelected;

    public MenuListData(Drawable iconDrawable, String title, String value, boolean isSelected) {
        this.title        = title;
        this.value        = value;
        this.iconDrawable = iconDrawable;
        this.isSelected   = isSelected;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public Drawable getIconDrawable() {
        return iconDrawable;
    }

    public boolean isSelected() {
        return isSelected;
    }
}

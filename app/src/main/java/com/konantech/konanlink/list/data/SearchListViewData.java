package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.api.model.device.DeviceInfo;

import java.util.Date;

public abstract class SearchListViewData extends ListViewData {
    private DeviceInfo deviceInfo;

    public SearchListViewData(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public abstract String getTitle();

    public abstract String getFolder();

    public abstract String getFolderPath();

    public abstract Date getModifyTime();

    public abstract long getSize();

    public abstract String getSummary();

    public abstract String getThumbnail();

    public abstract String getPath();

    @Override
    protected void onCleanup() {
    }
}
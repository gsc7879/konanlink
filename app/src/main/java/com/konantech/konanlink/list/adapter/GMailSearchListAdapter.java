package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.pager.IsNextListPager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.GMailSearchListViewData;
import com.konantech.konanlink.list.model.GmailSearchListSetData;
import com.konantech.konanlink.list.viewholder.GMailSearchListViewHolder;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.model.search.GMailSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.DownloadPartFragment;
import com.konantech.konanlink.part.GmailSearchPartFragment;

import java.io.File;

public class GMailSearchListAdapter extends SearchListAdapter<GMailSearchListViewData, GMailSearchListViewHolder, GmailSearchListSetData, SearchResult<GMailSearchInfo>> {
    private static final int COUNT_LIMIT = 50;

    private IsNextListPager mNextListPager;

    @InjectView(id = R.id.text_list_search_gmail_device_name)
    private TextView mResultText;

    public GMailSearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, GmailSearchListSetData data) {
        super(partFragment, parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNextListPager = new IsNextListPager(COUNT_LIMIT);
        setNextListController(mNextListPager);
    }

    @Override
    protected void startDownload(GMailSearchListViewData searchFileListData, File downloadTarget, boolean isShare) {
        DownloadAttachData attachData = new DownloadAttachData();
        attachData.setFileTitle(searchFileListData.getTitle());
        attachData.setFileExtension("eml");
        attachData.setFileType("mail");
        attachData.setFilePath(searchFileListData.getPath());
        attachData.setFolderPath(searchFileListData.getFolderPath());
        attachData.setFileSize(searchFileListData.getSize());
        attachData.setFileModifyTime(searchFileListData.getModifyTime().getTime());
        attachData.setAttached(false);
        attachData.setShare(isShare);
        attachData.setDeviceInfo(mDeviceInfo);
        attachData.setDownloadTarget(downloadTarget);
        mPageFragment.attachFragment(R.id.layout_main_main, DownloadPartFragment.class, attachData);
    }

    @Override
    protected GMailSearchListViewHolder createListViewHolder(View parent, int viewType) {
        return new GMailSearchListViewHolder(parent);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        super.onStartLoad(isFirst);
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        mPageFragment.attachFragment(R.id.layout_list_search_main, GmailSearchPartFragment.class, mDeviceInfo, mNextListPager.getOffset(), mNextListPager.getLimit(), mListSetData, mNoListText);
    }

    @Override
    protected void onEndLoad(SearchResult<GMailSearchInfo> searchResult) {
        mDeviceInfo = DataBank.getInstance().getDeviceInfo(mDeviceInfo.getKey());
        for (GMailSearchInfo searchInfo : searchResult.getResultList()) {
            addItem(new GMailSearchListViewData(searchInfo, mDeviceInfo));
        }
        mNextListPager.setListCount(searchResult.isNext(), getItemCount());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        super.onAddedItems(itemCount, errorCode);
    }

    @Override
    protected String[] getMenuRes(GMailSearchListViewData searchListData) {
        return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s99_link_save_file)};
//        return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s19_link_file_explorer), mContext.getString(R.string.s99_link_save_file)};
    }

    @Override
    protected String[] getMenuValue(GMailSearchListViewData searchListData) {
        return new String[]{"OPEN", "SHARE", "SAVE"};
//        return new String[]{"OPEN", "SHARE", "FOLDER", "SAVE"};
    }

    @Override
    protected int[] getMenuIcons(GMailSearchListViewData data) {
        return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_downtophone};
//        return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_filefolder, R.drawable.ic_downtophone};
    }
}

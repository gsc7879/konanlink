package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

public class ExtensionMenuListSetData extends ListSetData {
    private int callbackId;
    private int selectedIndex;
    private String[] menuArray;

    public int getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(int callbackId) {
        this.callbackId = callbackId;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getMenuArray() {
        return menuArray;
    }

    public void setMenuArray(String[] menuArray) {
        this.menuArray = menuArray;
    }

    @Override
    protected void onInitData() {}
}

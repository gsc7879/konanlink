package com.konantech.konanlink.list.controller;

import android.content.Context;
import android.location.Geocoder;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.TextUtils;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.list.data.OneDriveSearchListViewViewData;
import com.konantech.konanlink.list.viewholder.OneDriveSearchListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class OneDriveSearchFileListController extends SearchListController<OneDriveSearchListViewViewData, OneDriveSearchListViewHolder> {
    private String mTitle;
    private String mInfo;
    private String mThumbnail;
    private String mDescription;
    private String mExif;
    private int    mTypeResId;

    public OneDriveSearchFileListController(OneDriveSearchListViewViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle       = mViewData.getTitle();
        mThumbnail   = mViewData.getThumbnail();
        mDescription = mViewData.getSummary();
        mInfo        = getInfo();
        mExif        = getExif(listViewAdapter.getContext());
        mTypeResId   = InfoUtils.getTypeIconResId(DataBank.getInstance().getExtensionType(FilenameUtils.getExtension(mViewData.getTitle())), false);
    }

    private String getInfo() {
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(mViewData.getModifyTime());
        String size = TextUtils.toNumInUnits(mViewData.getSize());
        String resolution = mViewData.getResolution();
        return resolution == null ? String.format("%s | %s", date, size) : String.format("%s | %s | %s", date, resolution, size);
    }

    private String getExif(Context context) {
        OneDriveFile.DriveFile.Location location = mViewData.getLocation();
        try {
            return new Geocoder(context, Locale.getDefault()).getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0).getAddressLine(0);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, OneDriveSearchListViewHolder viewHolder) {
        mPicasso.load(mTypeResId).into(viewHolder.mIcon);
        viewHolder.mName.setText(mTitle);
        viewHolder.mInfo.setText(mInfo);

        if (StringUtils.isBlank(mThumbnail)) {
            viewHolder.mThumbnail.setVisibility(View.GONE);
        } else {
            viewHolder.mThumbnail.setVisibility(View.VISIBLE);
            mPicasso.load(mThumbnail).into(viewHolder.mThumbnail);
        }

        if (StringUtils.isBlank(mDescription)) {
            viewHolder.mDescription.setVisibility(View.GONE);
        } else {
            viewHolder.mDescription.setVisibility(View.VISIBLE);
            viewHolder.mDescription.setText(mDescription);
        }

        if (StringUtils.isBlank(mExif)) {
            viewHolder.mExif.setVisibility(View.GONE);
        } else {
            viewHolder.mExif.setVisibility(View.VISIBLE);
            viewHolder.mExif.setText(mExif);
        }
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
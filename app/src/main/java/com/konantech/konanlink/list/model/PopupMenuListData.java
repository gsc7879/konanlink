package com.konantech.konanlink.list.model;

public class PopupMenuListData {
    private String  title;
    private boolean isSelected;

    public PopupMenuListData(String title, boolean selected) {
        this.title = title;
        isSelected = selected;
    }
    public String getTitle() {
        return title;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
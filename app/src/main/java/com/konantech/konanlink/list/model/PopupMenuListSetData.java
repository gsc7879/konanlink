package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

import java.util.List;

public class PopupMenuListSetData extends ListSetData<List<PopupMenuListData>> {
    private int callbackId;
    private List<PopupMenuListData> popupMenuListDataList;

    public int getCallbackId() {
        return callbackId;
    }

    public void setCallbackId(int callbackId) {
        this.callbackId = callbackId;
    }

    public List<PopupMenuListData> getPopupMenuListDataList() {
        return popupMenuListDataList;
    }

    public void setPopupMenuListDataList(List<PopupMenuListData> popupMenuListDataList) {
        this.popupMenuListDataList = popupMenuListDataList;
    }

    @Override
    protected void onInitData() {}
}

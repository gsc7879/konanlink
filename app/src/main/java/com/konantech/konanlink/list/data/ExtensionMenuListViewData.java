package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.ExtensionMenuListController;

public class ExtensionMenuListViewData extends ListViewData {
    private String title;
    private boolean isSelected;

    public ExtensionMenuListViewData(String title, boolean selected) {
        this.title = title;
        isSelected = selected;
    }

    public String getTitle() {
        return title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    @Override
    protected ListViewController createViewController() {
        return new ExtensionMenuListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.PaymentListController;

import java.util.Date;

public class PaymentListViewData extends ListViewData {
    private int    term;
    private int    paymentType;

    private String description;

    private Date   createdDate;

    public PaymentListViewData(int term, int paymentType, String description, Date createdDate) {
        this.term = term;
        this.paymentType = paymentType;
        this.description = description;
        this.createdDate = createdDate;
    }

    public int getTerm() {
        return term;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    protected ListViewController createViewController() {
        return new PaymentListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
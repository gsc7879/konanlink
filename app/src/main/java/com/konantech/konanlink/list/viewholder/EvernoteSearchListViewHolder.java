package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.konantech.konanlink.R;

public class EvernoteSearchListViewHolder extends SearchListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public TextView     mName;
    public TextView     mContent;
    public TextView     mInfo;

    public EvernoteSearchListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public ViewGroup getLayout() {
        return mLayout;
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout    = (LinearLayout) convertView.findViewById(R.id.layout_file_search_evernote);
        mIcon      = (ImageView)    convertView.findViewById(R.id.img_file_search_evernote_icon);
        mName      = (TextView)     convertView.findViewById(R.id.text_file_search_evernote_name);
        mContent   = (TextView)     convertView.findViewById(R.id.text_file_search_evernote_content);
        mInfo      = (TextView)     convertView.findViewById(R.id.text_file_search_evernote_info);
    }
}

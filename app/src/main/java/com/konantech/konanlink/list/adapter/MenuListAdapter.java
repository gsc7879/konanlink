package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.view.View.OnClickListener;

import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.list.data.MenuListViewData;
import com.konantech.konanlink.list.model.MenuListData;
import com.konantech.konanlink.list.model.MenuListSetData;
import com.konantech.konanlink.list.viewholder.MenuListViewHolder;

import java.util.List;

public class MenuListAdapter extends ListViewAdapter<MenuListViewData, MenuListViewHolder, MenuListSetData, List<MenuListData>> {
    private OnSelectMenuListener mSelectedItemListener;

    public interface OnSelectMenuListener {
        void onSelectedItem(MenuListViewData listViewData);
    }

    public MenuListAdapter(View rootView, OnSelectMenuListener selectedItemListener, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, MenuListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mSelectedItemListener = selectedItemListener;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        endLoadList(mListSetData.getMenuListDataList());
    }

    @Override
    protected void onEndLoad(List<MenuListData> menuListDataList) {
        MenuListData menuListData;
        for (int i = 0; i < menuListDataList.size(); i++) {
            menuListData = menuListDataList.get(i);
            addItem(new MenuListViewData(i, menuListData.getIconDrawable(), menuListData.getTitle(), menuListData.getValue(), menuListData.isSelected()));
        }
    }

    @Override
    protected MenuListViewHolder createListViewHolder(View parent, int viewType) {
        return new MenuListViewHolder(parent);
    }

    @Override
    protected void onSetData(MenuListViewHolder listViewHolder, final MenuListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItemListener.onSelectedItem(listViewData);
            }
        });
    }

    @Override
    protected void onCleanup() {
    }
}

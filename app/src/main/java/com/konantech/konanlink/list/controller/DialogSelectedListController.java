package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.DialogSelectedListViewData;
import com.konantech.konanlink.list.viewholder.DialogSelectedListViewHolder;

public class DialogSelectedListController extends ListViewController<DialogSelectedListViewData, DialogSelectedListViewHolder> {
    private String mTitle;

    public DialogSelectedListController(DialogSelectedListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle = mViewData.getTitle();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, DialogSelectedListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(mTitle);
        listViewHolder.mTitle.setChecked(mViewData.isSelected());
    }

    @Override
    protected void onCleanup() {
    }
}
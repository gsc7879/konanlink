package com.konantech.konanlink.list.controller;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.list.data.DeviceListViewData;
import com.konantech.konanlink.list.viewholder.DeviceListViewHolder;

public class DeviceListController extends ListViewController<DeviceListViewData, DeviceListViewHolder> {
    private String      mName;
    private DeviceInfo  mDeviceInfo;
    private String      mType;
    private int         mStatusTextResId;
    private int         mConnectColor;
    private boolean     isConnected;
    private boolean     isDeleteAble;

    public DeviceListController(DeviceListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mDeviceInfo  = mViewData.getDeviceInfo();
        mName        = mDeviceInfo.getName();
        mType        = getType();
        mType        = mType == null ? mDeviceInfo.getAccount() : mType;
        isDeleteAble = isDeleteAble() || mDeviceInfo.isMyDevice();
        setConnectInfo(listViewAdapter.getContext());
    }

    private void setConnectInfo(Context context) {
        switch (mViewData.getErrorCode()) {
            case ErrorConst.ERROR_NONE:
                isConnected      = true;
                mStatusTextResId = R.string.s25_text_search_available;
                mConnectColor    = ContextCompat.getColor(context, R.color.green);
                break;
            case ErrorConst.ERROR_CONNECT_NORMAL:
                isConnected      = false;
                mStatusTextResId = R.string.s25_text_search_unavailable;
                mConnectColor    = ContextCompat.getColor(context, R.color.color_text_999999);
                break;
            case ErrorConst.ERROR_CONNECT_CERT:
                isConnected      = false;
                mStatusTextResId = R.string.s25_text_certification_failed;
                mConnectColor    = ContextCompat.getColor(context, R.color.color_text_999999);
                break;
            case ErrorConst.ERROR_CONNECT_ACCOUNT:
                isConnected      = false;
                mStatusTextResId = R.string.s25_text_certification_failed;
                mConnectColor    = ContextCompat.getColor(context, R.color.color_text_999999);
                break;
            default:
                isConnected      = false;
                mStatusTextResId = R.string.none;
                mConnectColor    = ContextCompat.getColor(context, R.color.color_text_999999);
                break;
        }
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, DeviceListViewHolder listViewHolder) {
        listViewHolder.mName.setText(mName);
        listViewHolder.mType.setText(mType);
        listViewHolder.mDeleteBtn.setVisibility(isDeleteAble ? View.VISIBLE : View.GONE);
        listViewHolder.mStatusIcon.setSelected(isConnected);
        listViewHolder.mStatus.setTextColor(mConnectColor);
        listViewHolder.mStatus.setText(mStatusTextResId);
    }

    @Override
    protected void onCleanup() {
    }

    private boolean isDeleteAble() {
        switch (mDeviceInfo.getKind()) {
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_EVERNOTE:
            case DeviceConst.KIND_ONEDRIVE:
            case DeviceConst.KIND_GMAIL:
            case DeviceConst.KIND_FILESERVER:
                return true;
        }
        return false;
    }

    private String getType() {
        switch (mDeviceInfo.getKind()) {
            case DeviceConst.KIND_WIN:
                return "Windows";
            case DeviceConst.KIND_MAC:
                return "MAC";
            case DeviceConst.KIND_LINUX:
                return "Linux";
            case DeviceConst.KIND_ANDROID:
            case DeviceConst.KIND_IOS:
                return "Mobile";
            case DeviceConst.KIND_DROPBOX:
            case DeviceConst.KIND_GOOGLEDRIVE:
            case DeviceConst.KIND_EVERNOTE:
            case DeviceConst.KIND_WEBMAIL:
            case DeviceConst.KIND_ONEDRIVE:
            case DeviceConst.KIND_GMAIL:
                return null;
            case DeviceConst.KIND_FILESERVER:
                return "File Server";
        }
        return null;
    }
}

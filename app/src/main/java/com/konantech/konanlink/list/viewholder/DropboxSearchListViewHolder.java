package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.konantech.konanlink.R;

public class DropboxSearchListViewHolder extends SearchListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public ImageView    mThumbnail;
    public TextView     mName;
    public TextView     mInfo;
    public TextView     mExif;

    public DropboxSearchListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public ViewGroup getLayout() {
        return mLayout;
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout    = (LinearLayout) convertView.findViewById(R.id.layout_file_search_dropbox);
        mThumbnail = (ImageView)    convertView.findViewById(R.id.img_file_search_dropbox_thumbnail);
        mIcon      = (ImageView)    convertView.findViewById(R.id.img_file_search_dropbox_icon);
        mName      = (TextView)     convertView.findViewById(R.id.text_file_search_dropbox_name);
        mInfo      = (TextView)     convertView.findViewById(R.id.text_file_search_dropbox_info);
        mExif      = (TextView)     convertView.findViewById(R.id.text_file_search_dropbox_exif);
    }
}

package com.konantech.konanlink.list.adapter;

import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.ListSetData;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.TextUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.dialog.ListSelectDialogFragment;
import com.konantech.konanlink.list.data.SearchListViewData;
import com.konantech.konanlink.list.viewholder.SearchListViewHolder;
import com.konantech.konanlink.part.DirectoryPartFragment;
import com.konantech.konanlink.part.PaymentReqPartFragment;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.io.File;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;

public abstract class SearchListAdapter<T extends SearchListViewData, S extends SearchListViewHolder, U extends ListSetData, V> extends ListViewAdapter<T, S, U, V> {
    private Animation      mAnimation;

    protected PartFragment mPageFragment;
    protected DeviceInfo   mDeviceInfo;
    protected TextView     mNoListText;

    private ImageView      mNoListImage;

    public SearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, U data) {
        super(parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNoListText   = (TextView)  mNoDataLayout.findViewById(R.id.text_listno);
        mNoListImage  = (ImageView) mNoDataLayout.findViewById(R.id.img_listno);
        mPageFragment = partFragment;
    }

    private void startLoading() {
        if (mAnimation == null) {
            mAnimation = new CircleAnimation(mNoListImage, AnimationConst.BIG_RADIOUS);
            mAnimation.setDuration(AnimationConst.BIG_DURATION);
            mAnimation.setRepeatCount(Animation.INFINITE);
        }
        mNoListImage.startAnimation(mAnimation);
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        mDeviceInfo = deviceInfo;
    }

    private void showPaymentDialog() {
        DialogUtils.showConverseCenterDialog(
                String.format(mContext.getString(R.string.s24_dialog_title_premium_upgrade)),
                String.format(mContext.getString(R.string.s19_dialog_text_premium_upgrade), 3),
                R.string.s99_dialog_link_upgrade_later,
                R.string.s99_dialog_btn_premium_upgrade,
                ((FragmentActivity) mContext).getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        if (isPositive) {
                            GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_POPUP);
                            mPageFragment.attachFragment(R.id.layout_main_main, PaymentReqPartFragment.class);
                        }
                    }
                });
    }

    @Override
    protected void onSetData(S listViewHolder, final T listViewData) {
        listViewHolder.getLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOpenMenu(listViewData);
            }
        });
    }

    @Override
    protected void onStartLoad(boolean first) {
        setMessage(R.drawable.img_search, mContext.getString(R.string.s99_text_loading));
        startLoading();
    }

    private void showOpenMenu(final T data) {
        DialogUtils.showContentsListDialog(
                data.getTitle(),
                data.getFolderPath(),
                String.format("%s | %s", FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(data.getModifyTime()), TextUtils.toNumInUnits(data.getSize())),
                data.getSummary(),
                StringUtils.defaultString(data.getThumbnail()),
                getMenuRes(data),
                getMenuValue(data),
                getMenuIcons(data),
                mDeviceInfo.getKind(),
                mDeviceInfo.getKey(),
                mPageFragment.getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        if (isPositive) {
                            switch ((String) dataMap.get(ListSelectDialogFragment.DATA_VALUE)) {
                                case "OPEN":
                                    GAManager.getInstance().sendEvent(GAConst.CATEGORY_PREVIEW, GAConst.EVENT_OPEN);
                                    startDownload(data, null, false);
                                    break;
                                case "SHARE":
                                    GAManager.getInstance().sendEvent(GAConst.CATEGORY_PREVIEW, GAConst.EVENT_SHARE);
                                    startDownload(data, null, true);
                                    break;
                                case "FOLDER":
                                    if (DataBank.getInstance().getAccountInfo().isExpired()) {
                                        showPaymentDialog();
                                        return;
                                    }
                                    GAManager.getInstance().sendEvent(GAConst.CATEGORY_PREVIEW, GAConst.EVENT_EXPLORER);
                                    mPageFragment.attachFragment(R.id.layout_main_main, DirectoryPartFragment.class, mDeviceInfo, data.getFolderPath());
                                    break;
                                case "SAVE":
                                    GAManager.getInstance().sendEvent(GAConst.CATEGORY_PREVIEW, GAConst.EVENT_SAVE);
                                    startDownload(data, FileUtils.getFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)), false);
                                    break;
                            }
                        }
                    }
                });
    }

    protected abstract String[] getMenuRes(T data);

    protected abstract String[] getMenuValue(T data);

    protected abstract int[] getMenuIcons(T data);

    protected abstract void startDownload(T searchFileListData, File downloadTarget, boolean isShare);

    protected void sendCount(TextView textView, int totalCount, boolean isNext) {
        textView.setText(MessageFormat.format(mContext.getString(isNext ? R.string.s17_text_search_result_over : R.string.s17_text_search_result), mDeviceInfo.getName() + (StringUtils.isNotBlank(mDeviceInfo.getAccount()) ? String.format("(%s)", mDeviceInfo.getAccount()) : ""), TextUtils.priceFormat(totalCount)));
    }

    protected void setMessage(int imgResId, String message) {
        mNoListImage.setVisibility(imgResId == 0 ? View.GONE : View.VISIBLE);
        mNoListImage.setImageResource(imgResId == 0 ? android.R.color.transparent : imgResId);
        mNoListText.setText(message);
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        mNoListImage.clearAnimation();
        if (itemCount == 0) {
            String error = mContext.getString(R.string.s18_text_no_result);
            switch (errorCode) {
                case ErrorConst.ERROR_CONNECT_CANCEL:
                    error = mContext.getString(R.string.s99_error_no_3rd_certification);
                    break;
                case ErrorConst.ERROR_CONNECT_NORMAL:
                    error = mContext.getString(R.string.s99_error_device_issue);
                    break;
                case ErrorConst.ERROR_CONNECT_CERT:
                    error = mContext.getString(R.string.s99_error_no_3rd_certification);
                    break;
                case ErrorConst.ERROR_CONNECT_LIMIT:
                    error = String.format(mContext.getString(R.string.s19_error_accounts), DataBank.getInstance().getDeviceLimitInfo().getLimit());
                    break;
                case ErrorConst.ERROR_CONNECT_ACCOUNT:
                    error = mContext.getString(R.string.s98_error_wrong_account);
                    break;
                case ErrorConst.ERROR_CONNECT_WAS:
                    error = mContext.getString(R.string.was_error_9999);
                    break;
                case ErrorConst.ERROR_SYSTEM:
                    error = mContext.getString(R.string.s99_error_device_issue);
                    break;
                case ErrorConst.ERROR_AUTH:
                    error = mContext.getString(R.string.s99_error_no_3rd_certification);
                    break;
                case ErrorConst.ERROR_CONNECT:
                    error = mContext.getString(R.string.s18_text_disconnected);
                    break;
                case ErrorConst.ERROR_PERMISSION:
                    error = "주소록 권한을 허용하세요.";
                    break;
            }
            setMessage(R.drawable.img_search_noresult, error);
        } else {
            setMessage(0, "");
        }
    }

    @Override
    protected void onCleanup() {
    }
}

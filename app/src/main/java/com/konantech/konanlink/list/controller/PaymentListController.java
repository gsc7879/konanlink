package com.konantech.konanlink.list.controller;

import android.content.Context;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.service.PaymentApiService;
import com.konantech.konanlink.list.data.PaymentListViewData;
import com.konantech.konanlink.list.viewholder.PaymentListViewHolder;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class PaymentListController extends ListViewController<PaymentListViewData, PaymentListViewHolder> {
    private String mDate;
    private String mTerm;
    private String mDescription;

    public PaymentListController(PaymentListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        Context context = listViewAdapter.getContext();
        mDate           = FastDateFormat.getInstance("yyyy.MM.dd", Locale.getDefault()).format(mViewData.getCreatedDate());
        mTerm           = String.format(context.getString(R.string.s28_text_payment_months), mViewData.getTerm());
        mDescription    = getDescription(context, mViewData.getPaymentType(), mViewData.getDescription());
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, PaymentListViewHolder listViewHolder) {
        listViewHolder.mDate.setText(mDate);
        listViewHolder.mTerm.setText(mTerm);
        listViewHolder.mDescription.setText(mDescription);
    }

    @Override
    protected void onCleanup() {
    }

    private String getDescription(Context context, int paymentType, String description) {
        switch (paymentType) {
            case PaymentApiService.TYPE_PAYPAL:
                return context.getString(R.string.s28_text_payment_paypal);
            case PaymentApiService.TYPE_GOOGLE:
                return context.getString(R.string.s28_text_payment_google);
            case PaymentApiService.TYPE_APPLE:
                return context.getString(R.string.s28_text_payment_apple);
            case PaymentApiService.TYPE_COUPON:
                return String.format(context.getString(R.string.s28_text_payment_coupon), description);
            case PaymentApiService.TYPE_GIFT_SAMSUNG:
                return context.getString(R.string.s28_text_payment_galaxygift);
            default:
                return "";
        }
    }
}

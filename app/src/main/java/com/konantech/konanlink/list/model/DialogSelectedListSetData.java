package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.model.device.DeviceInfo;

public class DialogSelectedListSetData extends ListSetData<DeviceInfo> {
    private String[] titles;
    private String[] values;
    private int selectedIndex;

    public String[] getTitles() {
        return titles;
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    @Override
    protected void onInitData() {}
}

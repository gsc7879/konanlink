package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

import java.util.List;

public class IndexingFolderListSetData extends ListSetData {
    private List<String> folderList;

    public List<String> getFolderList() {
        return folderList;
    }

    public void setFolderList(List<String> folderList) {
        this.folderList = folderList;
    }

    @Override
    protected void onInitData() {}
}

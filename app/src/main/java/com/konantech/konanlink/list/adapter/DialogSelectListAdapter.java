package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.view.View.OnClickListener;

import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.list.data.DialogSelectListViewData;
import com.konantech.konanlink.list.model.DialogCheckListData;
import com.konantech.konanlink.list.model.DialogSelectListSetData;
import com.konantech.konanlink.list.viewholder.DialogSelectListViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DialogSelectListAdapter extends ListViewAdapter<DialogSelectListViewData, DialogSelectListViewHolder, DialogSelectListSetData, List<DialogCheckListData>> {
    protected OnSelectedItemListener mSelectedItemListener;

    public interface OnSelectedItemListener {
        void onSelectedItem(DialogSelectListViewData listViewData);
    }

    public DialogSelectListAdapter(View rootView, OnSelectedItemListener selectedItemListener, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DialogSelectListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mSelectedItemListener = selectedItemListener;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        endLoadList(getCheckListData(mListSetData.getTitles(), mListSetData.getValues(), mListSetData.getIcons()));
    }

    @Override
    protected void onEndLoad(List<DialogCheckListData> list) {
        DialogCheckListData listData;
        for (int i = 0; i < list.size(); i++) {
            listData = list.get(i);
            addItem(new DialogSelectListViewData(i, listData.getTitle(), listData.getValue(), listData.getIcon()));
        }
    }

    private List<DialogCheckListData> getCheckListData(String[] titles, String[] values, int[] icons) {
        List<DialogCheckListData> checkListData = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            checkListData.add(new DialogCheckListData(icons == null ? null : icons[i], titles[i], values[i]));
        }
        return checkListData;
    }

    @Override
    protected DialogSelectListViewHolder createListViewHolder(View parent, int viewType) {
        return new DialogSelectListViewHolder(parent);
    }

    @Override
    protected void onSetData(DialogSelectListViewHolder listViewHolder, final DialogSelectListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedItemListener.onSelectedItem(listViewData);
            }
        });
    }

    @Override
    protected void onCleanup() {
    }
}

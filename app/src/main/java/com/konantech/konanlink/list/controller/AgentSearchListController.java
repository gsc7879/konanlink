package com.konantech.konanlink.list.controller;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.task.WorkTask;
import com.dignara.lib.utils.TextUtils;
import com.dignara.lib.utils.UnitUtils;
import com.dropbox.core.v2.DbxClientV2;
import com.google.api.services.drive.Drive;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.device.download.thumbnail.ReqDropBoxThumbnailTask;
import com.konantech.konanlink.device.download.thumbnail.ReqGoogleDriveThumbnailTask;
import com.konantech.konanlink.device.download.thumbnail.ReqLocalThumbnailTask;
import com.konantech.konanlink.device.download.thumbnail.ReqThumbnailTask;
import com.konantech.konanlink.list.data.AgentSearchListViewViewData;
import com.konantech.konanlink.list.viewholder.AgentSearchFileListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class AgentSearchListController extends SearchListController<AgentSearchListViewViewData, AgentSearchFileListViewHolder> {
    private static final int THUMBNAIL_SIZE = 300;

    private static Picasso mAgentPicasso;

    private Spanned          mTitle;
    private Spanned          mSummary;
    private Spanned          mExif;
    private String           mInfo;
    private int              mTypeResId;
    private ReqThumbnailTask mThumbnailTask;
    private boolean          mIsImageLoading;
    private boolean          mIsThumbnail;
    private String           mUrlPath;
    private int              mThumbnailPixel;

    public AgentSearchListController(AgentSearchListViewViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        String extension   = StringUtils.isBlank(mViewData.getExtension()) ? StringUtils.upperCase(mViewData.getType()) : StringUtils.upperCase(mViewData.getExtension());
        String fileType    = DataBank.getInstance().getExtensionType(extension);
        boolean isMyDevice = mViewData.getDeviceInfo().isMyDevice();
        mThumbnailPixel    = UnitUtils.convertDpToPixel(listViewAdapter.getContext(), 62);
        mTypeResId         = InfoUtils.getTypeIconResId(fileType, mViewData.isAttached());
        mThumbnailTask     = getThumbnailTask(mViewData.getDeviceInfo().getKind(), listViewAdapter.getContext(), mViewData.getDeviceInfo().getConnector(), mViewData.getUrl(), isMyDevice);
        mTitle             = Html.fromHtml(mViewData.getHighlightTitle());

        String date        = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(mViewData.getModifyTime());
        String size        = TextUtils.toNumInUnits(mViewData.getSize());
        String resolution  = mViewData.getResolution();
        mInfo              = StringUtils.isNoneBlank(resolution) ? String.format("%s | %s | %s", date, resolution, size) : String.format("%s | %s", date, size);

        if (mAgentPicasso == null) {
            mAgentPicasso = new Picasso.Builder(listViewAdapter.getContext())
                                       .downloader(new OkHttp3Downloader(getDownloader()))
                                       .build();
        }

        if (StringUtils.equalsIgnoreCase(fileType, "image")) {
            int kind     = mViewData.getDeviceInfo().getKind();
            mIsThumbnail = (kind != DeviceConst.KIND_EVERNOTE) && (kind != DeviceConst.KIND_WEBMAIL) && (kind != DeviceConst.KIND_IOS) && ((kind != DeviceConst.KIND_ANDROID) || isMyDevice);
            mUrlPath     = String.format("%s_%d", mViewData.getTitle(), mViewData.getModifyTime().getTime());
            mSummary     = Html.fromHtml(mViewData.isAttached() ? StringUtils.defaultString(mViewData.getEmailTitle()) : "");
            mExif        = Html.fromHtml(StringUtils.defaultString(mViewData.getLocation()));
        } else {
            mIsThumbnail = false;
            if (mViewData.isEmail()) {
                mSummary = Html.fromHtml(String.format("From : %s  To : %s", mViewData.getEmailFrom(), mViewData.getEmailTo()));
            } else {
                mSummary = Html.fromHtml(StringUtils.isBlank(mViewData.getSummary()) ? "" : mViewData.getSummary());
            }
        }
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, AgentSearchFileListViewHolder listViewHolder) {
        if (StringUtils.isBlank(mSummary)) {
            listViewHolder.mDescription.setVisibility(View.GONE);
        } else {
            listViewHolder.mDescription.setText(mSummary);
            listViewHolder.mDescription.setVisibility(View.VISIBLE);
        }

        if (StringUtils.isBlank(mExif)) {
            listViewHolder.mExif.setVisibility(View.GONE);
        } else {
            listViewHolder.mExif.setText(mExif);
            listViewHolder.mExif.setVisibility(View.VISIBLE);
        }

        mPicasso.load(mTypeResId).into(listViewHolder.mIcon);
        listViewHolder.mName.setText(mTitle);
        listViewHolder.mInfo.setText(mInfo);

        if (mIsThumbnail) {
            if (mThumbnailTask != null && !mIsImageLoading) {
                downloadThumbnail(mUrlPath, listViewAdapter, mViewData);
            }
            listViewHolder.mThumbnail.setVisibility(View.VISIBLE);


            String thumbnail = mViewData.getThumbnail();
            if (mThumbnailTask == null) {
                thumbnail = getDownloadUri(mViewData.getThumbnailUrl()).toString();
            }
            if (StringUtils.isBlank(thumbnail)) {
                mPicasso.load(mIsImageLoading ? R.drawable.loading : R.drawable.failed).into(listViewHolder.mThumbnail);
            } else {
                if (StringUtils.startsWith(thumbnail, "http")) {
                    mAgentPicasso.load(thumbnail)
                                 .error(android.R.color.transparent)
                                 .resize(mThumbnailPixel, mThumbnailPixel)
                                 .centerCrop()
                                 .into(listViewHolder.mThumbnail);
                } else {
                    mPicasso.with(listViewAdapter.getContext())
                            .load(FileUtils.getFile(thumbnail))
                            .error(android.R.color.transparent)
                            .resize(mThumbnailPixel, mThumbnailPixel)
                            .centerCrop()
                            .into(listViewHolder.mThumbnail);
                }
            }
        } else {
            listViewHolder.mThumbnail.setVisibility(View.GONE);
        }
    }


    private Uri getDownloadUri(String thumbNailUrl) {
        return new Uri.Builder().encodedPath(NetConst.getInstance().getLinkUrl())
                .appendEncodedPath(thumbNailUrl)
                .build();
    }

    private OkHttpClient getDownloader() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Authorization", DataBank.getInstance().getApiKey())
                        .build();
                return chain.proceed(newRequest);
            }
        });
        httpClient.followRedirects(false);
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }

    @Override
    protected void onCleanup() {
        if (mThumbnailTask != null) {
            mThumbnailTask.cancel(true);
        }
    }

    private ReqThumbnailTask getThumbnailTask(int kind, Context context, Object connector, String fileUrl, boolean isMyDevice) {
        if (connector == null && !isMyDevice) {
            return null;
        }
        switch (kind) {
            case DeviceConst.KIND_ANDROID:
                if (isMyDevice) {
                    return new ReqLocalThumbnailTask(fileUrl);
                }
            case DeviceConst.KIND_DROPBOX:
                return new ReqDropBoxThumbnailTask((DbxClientV2) connector, fileUrl);
            case DeviceConst.KIND_GOOGLEDRIVE:
                return new ReqGoogleDriveThumbnailTask((Drive) connector, fileUrl);
        }
        return null;
    }

    private void downloadThumbnail(String cacheName, final ListViewAdapter listViewAdapter, final AgentSearchListViewViewData listViewData) {
        if (mThumbnailTask == null) {
            return;
        }
        mThumbnailTask.setOnTaskListener(new WorkTask.OnTaskListener() {
            @Override
            public void onStartTask(int callBackId) {
                mIsImageLoading = true;
            }

            @Override
            public void onEndTask(int callBackId, Object object) {
                listViewData.setThumbnail((String) object);
                mIsImageLoading = false;
                mThumbnailTask  = null;
                listViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelTask(int callBackId, int cancelCode, Object data) {
                listViewData.setThumbnail(null);
                mIsImageLoading = false;
                mThumbnailTask  = null;
                listViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onTaskProgress(int callBackId, Object... params) {
            }
        });
        mThumbnailTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,  THUMBNAIL_SIZE, THUMBNAIL_SIZE, cacheName);
    }
}
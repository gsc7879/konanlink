package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class IndexingFolderListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public CheckBox     mCheckbox;

    public IndexingFolderListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout   = (LinearLayout) convertView.findViewById(R.id.layout_folder);
        mCheckbox = (CheckBox)     convertView.findViewById(R.id.check_folder_use);
    }
}

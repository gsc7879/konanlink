package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.ExtensionMenuListViewData;
import com.konantech.konanlink.list.viewholder.ExtensionMenuListViewHolder;

public class ExtensionMenuListController extends ListViewController<ExtensionMenuListViewData, ExtensionMenuListViewHolder> {
    private String  mTitle;
    private boolean mIsSelected;

    public ExtensionMenuListController(ExtensionMenuListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle      = mViewData.getTitle();
        mIsSelected = mViewData.isSelected();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, ExtensionMenuListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(mTitle);
        listViewHolder.mTitle.setSelected(mIsSelected);
    }

    @Override
    protected void onCleanup() {
    }
}
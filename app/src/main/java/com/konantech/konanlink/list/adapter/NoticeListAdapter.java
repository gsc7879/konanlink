package com.konantech.konanlink.list.adapter;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.list.ListSetData;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.pager.TotalNextListPager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AnnounceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.announce.AnnounceResult;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.NoticeListViewData;
import com.konantech.konanlink.list.viewholder.NoticeListViewHolder;
import com.konantech.konanlink.part.NoticeFragment;
import com.konantech.konanlink.part.WebFragment;

public class NoticeListAdapter extends ListViewAdapter<NoticeListViewData, NoticeListViewHolder, ListSetData, ResultList<AnnounceResult>> {

    private static final int LIMIT = 20;

    private NoticeFragment     mNoticeFragment;
    private TotalNextListPager mTotalNextListPager;

    @InjectView(id = R.id.text_listno)
    private TextView                mNoListText;

    public NoticeListAdapter(NoticeFragment noticeFragment, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId) {
        super(noticeFragment.getView(), listParentResId, listViewResId, adapterResId, noDataViewResId);
        mNoticeFragment     = noticeFragment;
        mTotalNextListPager = new TotalNextListPager(LIMIT);
        setNextListController(mTotalNextListPager);
    }

    @Override
    protected NoticeListViewHolder createListViewHolder(View parent, int viewType) {
        return new NoticeListViewHolder(parent);
    }

    @Override
    protected void onSetData(NoticeListViewHolder listViewHolder, final NoticeListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setAnnounceStatus(listViewData.getUpdatedDate().getTime());
                mNoticeFragment.attachFragment(R.id.layout_list_explorer_main, WebFragment.class, listViewData.getUrl(), listViewData.getTitle());
            }
        });
    }

    private void setAnnounceStatus(long readAnnounceTime) {
        if (DataBank.getInstance().getReadAnnounceTime() <= readAnnounceTime) {
            DataBank.getInstance().saveReadAnnounceTime(readAnnounceTime);
            setReadStatus(readAnnounceTime);
        }
    }

    private void setReadStatus(long readAnnounceTime) {
        for (NoticeListViewData noticeListViewData : mItemList) {
            noticeListViewData.setRead(readAnnounceTime);
        }
        notifyDataSetChanged();
    }

    @Override
    protected void onCleanup() {
    }

    @Override
    public void onStartLoad(boolean isFirst) {
        mNoListText.setText(R.string.s99_text_loading);
        reqAnnounceList();
    }

    private void reqAnnounceList() {
        new AnnounceApiManager().reqList(mContext, mNoticeFragment.getSubscriptions(), mTotalNextListPager.getOffset(), mTotalNextListPager.getLimit(), new Result.OnResultListener<ResultList<AnnounceResult>>() {
            @Override
            public void onSuccess(ResultList<AnnounceResult> data) {
                endLoadList(data);
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(mNoticeFragment.getView(), msg, Snackbar.LENGTH_SHORT).show();
                endLoadList(null);
            }
        });
    }


    @Override
    public void onEndLoad(ResultList<AnnounceResult> result) {
        long lastReadTime = DataBank.getInstance().getReadAnnounceTime();
        for (AnnounceResult announceResult : result.getList()) {
            addItem(new NoticeListViewData(announceResult.getIdx(), announceResult.getPlatform(), announceResult.getCategory(), announceResult.getTitle(), announceResult.getContents(), announceResult.getUrl(), announceResult.getCreatedTime(), announceResult.getUpdatedTime(), lastReadTime));
        }
        mTotalNextListPager.addListCount(result.getTotal(), result.getList().size());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        if (mListSetData.isSuccess()) {
            if (itemCount == 0) {
                mNoListText.setText(R.string.s37_text_no_notice);
            } else {
                mNoListText.setText("");
            }
        } else {
            mNoListText.setText(R.string.s01_dialog_error_cannot_access_server);
        }
    }
}

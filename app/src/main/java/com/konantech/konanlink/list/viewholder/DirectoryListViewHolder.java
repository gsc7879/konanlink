package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class DirectoryListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public TextView     mName;
    public TextView     mInfo;
    public ImageButton  mEtcBtn;

    public DirectoryListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_directory);
        mIcon   = (ImageView)    convertView.findViewById(R.id.img_directory_icon);
        mName   = (TextView)     convertView.findViewById(R.id.text_directory_name);
        mInfo   = (TextView)     convertView.findViewById(R.id.text_directory_info);
        mEtcBtn = (ImageButton)  convertView.findViewById(R.id.btn_directory_etc);
    }
}

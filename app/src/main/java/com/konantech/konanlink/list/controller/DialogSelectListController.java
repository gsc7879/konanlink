package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.DialogSelectListViewData;
import com.konantech.konanlink.list.viewholder.DialogSelectListViewHolder;

public class DialogSelectListController extends ListViewController<DialogSelectListViewData, DialogSelectListViewHolder> {
    private String  mTitle;
    private int     mIcon;

    public DialogSelectListController(DialogSelectListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle = mViewData.getTitle();
        mIcon  = mViewData.getIcon();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, DialogSelectListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(mTitle);
        listViewHolder.mIcon.setImageResource(mIcon);
    }

    @Override
    protected void onCleanup() {
    }
}
package com.konantech.konanlink.list.adapter;

import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.Click;
import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.dialog.DialogController;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.GAManager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.api.model.device.RemoveDeviceResult;
import com.konantech.konanlink.constant.AnimationConst;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.DeviceListViewData;
import com.konantech.konanlink.list.model.DeviceListSetData;
import com.konantech.konanlink.list.viewholder.DeviceListViewHolder;
import com.konantech.konanlink.part.AccountInfoPartFragment;
import com.konantech.konanlink.part.DeviceAddPartFragment;
import com.konantech.konanlink.part.DeviceListPartFragment;
import com.konantech.konanlink.part.DirectoryPartFragment;
import com.konantech.konanlink.part.UpdateInfoPartFragment;
import com.konantech.konanlink.utils.DialogUtils;
import com.konantech.konanlink.utils.animation.CircleAnimation;

import java.util.List;
import java.util.Map;

public class DeviceListAdapter extends ListViewAdapter<DeviceListViewData, DeviceListViewHolder, DeviceListSetData, List<DeviceListViewData>> {
    private DeviceListPartFragment mDeviceListPartFragment;
    private Animation              mAnimation;

    @InjectView(id = R.id.text_listno)
    private TextView mNoListText;

    @InjectView(id = R.id.img_listno)
    private ImageView mNoListImage;

    @InjectView(id = R.id.btn_listno)
    private TextView mNoListBtn;

    public DeviceListAdapter(DeviceListPartFragment drawerMenuFragment, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DeviceListSetData listData) {
        super(drawerMenuFragment.getView(), listParentResId, listViewResId, adapterResId, noDataViewResId, listData);
        mDeviceListPartFragment = drawerMenuFragment;
        mNoListBtn.setText(R.string.s25_link_add_device);
    }

    private void startLoading() {
        if (mAnimation == null) {
            mAnimation = new CircleAnimation(mNoListImage, AnimationConst.BIG_RADIOUS);
            mAnimation.setDuration(AnimationConst.BIG_DURATION);
            mAnimation.setRepeatCount(Animation.INFINITE);
        }
        mNoListImage.startAnimation(mAnimation);
    }

    @Override
    protected DeviceListViewHolder createListViewHolder(View parent, int viewType) {
        return new DeviceListViewHolder(parent);
    }

    public TextView getLoadingMsgTextView() {
        return mNoListText;
    }

    @Override
    protected void onSetData(DeviceListViewHolder listViewHolder, final DeviceListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String deviceKey = listViewData.getDeviceInfo().getKey();
                DeviceInfo deviceInfo = DataBank.getInstance().getDeviceInfo(deviceKey);
                if (DataBank.getInstance().getAccountInfo().isExpired()) {
                    showPaymentDialog();
                } else {
                    showDirectory(deviceInfo);
                }
            }
        });

        listViewHolder.mDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GAManager.getInstance().sendEvent(GAConst.CATEGORY_SERVICE, GAConst.EVENT_DELETE);
                showDeleteDialog(listViewData);
            }
        });
    }

    private void showDirectory(DeviceInfo deviceInfo) {
        switch (deviceInfo.getKind()) {
            case DeviceConst.KIND_WEBMAIL:
            case DeviceConst.KIND_EVERNOTE:
            case DeviceConst.KIND_GMAIL:
                Snackbar.make(mDeviceListPartFragment.getView(), R.string.s40_error_not_support, Snackbar.LENGTH_LONG).show();
                break;
            default:
                mDeviceListPartFragment.attachFragment(R.id.layout_list_device_list_main, DirectoryPartFragment.class, deviceInfo, "");
                break;
        }
    }

    private void showPaymentDialog() {
        DialogUtils.showConverseCenterDialog(
                mContext.getString(R.string.s24_dialog_title_premium_upgrade),
                String.format(mContext.getString(R.string.s19_dialog_text_premium_upgrade), 3),
                R.string.s99_dialog_link_upgrade_later,
                R.string.s99_dialog_btn_premium_upgrade,
                ((FragmentActivity) mContext).getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        if (isPositive) {
                            GAManager.getInstance().sendEvent(GAConst.CATEGORY_PAY, GAConst.EVENT_POPUP);
                            mDeviceListPartFragment.attachFragment(R.id.layout_list_device_list_main, AccountInfoPartFragment.class);
                        }
                    }
                });
    }

    private void showDeleteDialog(final DeviceListViewData listViewData) {
        DialogUtils.showConverseCenterDialog(
                mContext.getString(R.string.s44_dialog_title_delete),
                mContext.getString(R.string.s44_dialog_text_wanna_delete),
                R.string.s99_dialog_btn_cancel,
                R.string.s99_dialog_btn_okay,
                ((FragmentActivity) mContext).getFragmentManager(),
                new DialogController.OnDismissListener() {
                    @Override
                    public void onDismiss(int dialogId, boolean isPositive, boolean isCanceled, Map<Integer, Object> dataMap) {
                        if (isPositive) {
                            regRemoveDevice(listViewData);
                        }
                    }
                });
    }

    private void regRemoveDevice(final DeviceListViewData listViewData) {
        new DeviceApiManager().reqDeviceRemove(mContext, mDeviceListPartFragment.getSubscriptions(), listViewData.getDeviceInfo().getKey(), new Result.OnResultListener<RemoveDeviceResult>() {
            @Override
            public void onSuccess(RemoveDeviceResult result) {
                removeItem(listViewData);
                Snackbar.make(mListView, R.string.s44_toast_device_deleted, Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(mListView, msg, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onCleanup() {
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        setMessage(R.drawable.img_search, mContext.getString(R.string.s99_text_loading), false);
        mDeviceListPartFragment.attachFragment(R.id.layout_list_device_list_main, UpdateInfoPartFragment.class, UpdateInfoPartFragment.TYPE_DEVICE_INFO);
        startLoading();
    }

    @Override
    protected void onEndLoad(List<DeviceListViewData> list) {
        for (DeviceListViewData deviceViewInfo : list) {
            addItem(deviceViewInfo);
        }
    }

    @Click(R.id.btn_listno)
    public void addDevice() {
        mDeviceListPartFragment.attachFragment(R.id.layout_list_device_list_main, DeviceAddPartFragment.class);
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        if (itemCount == 0) {
            setMessage(R.drawable.img_nodevice, mContext.getString(R.string.s13_text_no_device), true);
        } else {
            setMessage(0, "", false);
        }
    }

    private void setMessage(int imgResId, String message, boolean isShowAddDevice) {
        mNoListText.setText(message);
        mNoListImage.setImageResource(imgResId == 0 ? android.R.color.transparent : imgResId);
        mNoListImage.setVisibility(imgResId == 0 ? View.GONE : View.VISIBLE);
        mNoListBtn.setVisibility(isShowAddDevice ? View.VISIBLE : View.GONE);
    }
}

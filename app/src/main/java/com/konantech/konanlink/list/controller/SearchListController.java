package com.konantech.konanlink.list.controller;

import com.dignara.lib.bank.ImageBank;
import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.list.data.SearchListViewData;
import com.squareup.picasso.Picasso;

public abstract class SearchListController<T extends SearchListViewData, S extends ListViewHolder> extends ListViewController<T, S> {
    protected static Picasso mPicasso;

    public SearchListController(T listViewData) {
        super(listViewData);
        if (mPicasso == null) {
            mPicasso = ImageBank.getInstance().getPicasso();
        }
    }

    @Override
    protected void onCleanup() {
    }
}
package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.DialogCheckListViewData;
import com.konantech.konanlink.list.viewholder.DialogCheckListViewHolder;

public class DialogCheckListController extends ListViewController<DialogCheckListViewData, DialogCheckListViewHolder> {
    private String mTitle;

    public DialogCheckListController(DialogCheckListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle = mViewData.getTitle();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, DialogCheckListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(mTitle);
        listViewHolder.mTitle.setChecked(mViewData.isChecked());
    }

    @Override
    protected void onCleanup() {
    }
}
package com.konantech.konanlink.list.adapter;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.pager.IsNextListPager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.AgentSearchListViewViewData;
import com.konantech.konanlink.list.model.AgentSearchListSetData;
import com.konantech.konanlink.list.viewholder.AgentSearchFileListViewHolder;
import com.konantech.konanlink.model.DownloadAttachData;
import com.konantech.konanlink.part.AgentSearchPartFragment;
import com.konantech.konanlink.part.DownloadPartFragment;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class AgentSearchListAdapter extends SearchListAdapter<AgentSearchListViewViewData, AgentSearchFileListViewHolder, AgentSearchListSetData, SearchResponse> {
    private static final int COUNT_PER_PAGE = 20;

    private IsNextListPager mNextListPager;
    private int mTotalCount;

    @InjectView(id = R.id.text_list_search_kms_device_name)
    private TextView mResultText;

    public AgentSearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, AgentSearchListSetData data) {
        super(partFragment, parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNextListPager = new IsNextListPager(COUNT_PER_PAGE);
        setNextListController(mNextListPager);
    }

    @Override
    protected AgentSearchFileListViewHolder createListViewHolder(View parent, int viewType) {
        return new AgentSearchFileListViewHolder(parent);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        super.onStartLoad(isFirst);
        if (isFirst) {
            mTotalCount = 0;
        }
        sendCount(mResultText, mTotalCount, false);
        mPageFragment.attachFragment(R.id.layout_list_search_main, AgentSearchPartFragment.class, mDeviceInfo, mNextListPager.getOffset(), mNextListPager.getLimit(), mListSetData, mNoListText);
    }

    protected void startDownload(AgentSearchListViewViewData searchFileListData, File downloadTarget, boolean isShare) {
        DownloadAttachData attachData = new DownloadAttachData();
        String type = searchFileListData.getType();
        String title = searchFileListData.getTitle();
        String extension = FilenameUtils.getExtension(title);
        attachData.setFileTitle(title);
        attachData.setFileExtension(extension);
        attachData.setFileType(StringUtils.equals(type, "mail") ? type : DataBank.getInstance().getExtensionType(extension));
        attachData.setFilePath(searchFileListData.getPath());
        attachData.setFolderPath(searchFileListData.getFolderPath());
        attachData.setDownloadURL(searchFileListData.getDownloadUrl());
        attachData.setFileSize(searchFileListData.getSize());
        attachData.setFileModifyTime(searchFileListData.getModifyTime().getTime());
        attachData.setAttached(searchFileListData.isAttached());
        attachData.setShare(isShare);
        attachData.setDeviceInfo(mDeviceInfo);
        attachData.setDownloadTarget(downloadTarget);

        mPageFragment.attachFragment(R.id.layout_main_main, DownloadPartFragment.class, attachData);
    }

    @Override
    protected void onCleanup() {
    }

    @Override
    public void onEndLoad(SearchResponse response) {
        if (getItemCount() == 0 && response.getTotalCount() > 100) {
            Snackbar.make(mPageFragment.getView(), R.string.s17_toast_use_mulifile_keywords, Snackbar.LENGTH_LONG).show();
        }

        for (SearchResponse.Item searchItem : response.getItems()) {
            addItem(new AgentSearchListViewViewData(searchItem, mDeviceInfo));
        }
        mTotalCount = response.getTotalCount();
        mNextListPager.setListCount(!response.isLastPage(), getItemCount());
        sendCount(mResultText, mTotalCount, false);
    }

    @Override
    protected String[] getMenuRes(AgentSearchListViewViewData searchListData) {
        switch (mDeviceInfo.getKind()) {
            case DeviceConst.KIND_WEBMAIL:
                return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file)};
            default:
                if (StringUtils.equals(searchListData.getType(), "mail")) {
                    return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s99_link_save_file)};
                } else {
                    return new String[]{mContext.getString(R.string.s19_link_open_file), mContext.getString(R.string.s99_link_share_file), mContext.getString(R.string.s19_link_file_explorer), mContext.getString(R.string.s99_link_save_file)};
                }
        }
    }

    @Override
    protected String[] getMenuValue(AgentSearchListViewViewData searchListData) {
        switch (mDeviceInfo.getKind()) {
            case DeviceConst.KIND_WEBMAIL:
                return new String[]{"OPEN", "SHARE"};
            default:
                if (StringUtils.equals(searchListData.getType(), "mail")) {
                    return new String[]{"OPEN", "SHARE", "SAVE"};
                } else {
                    return new String[]{"OPEN", "SHARE", "FOLDER", "SAVE"};
                }
        }
    }

    @Override
    protected int[] getMenuIcons(AgentSearchListViewViewData searchListData) {
        switch (mDeviceInfo.getKind()) {
            case DeviceConst.KIND_WEBMAIL:
                return new int[]{R.drawable.ic_down, R.drawable.ic_share_small};
            default:
                if (StringUtils.equals(searchListData.getType(), "mail")) {
                    return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_downtophone};
                } else {
                    return new int[]{R.drawable.ic_down, R.drawable.ic_share_small, R.drawable.ic_filefolder, R.drawable.ic_downtophone};
                }

        }
    }
}

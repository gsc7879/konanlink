package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.model.directory.DirectoryInfo;

public class DirectorySetData extends ListSetData<DirectoryInfo> {
    private DeviceInfo deviceInfo;
    private String     currentPath;
    private String     parentPath;
    private String     folder;

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getDeviceKey() {
        return deviceInfo.getKey();
    }

    public int getDeviceKind() {
        return deviceInfo.getKind();
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    @Override
    protected void onInitData() {
    }
}

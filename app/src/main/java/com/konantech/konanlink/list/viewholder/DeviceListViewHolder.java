package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class DeviceListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;

    public ImageView    mStatusIcon;

    public TextView     mName;
    public TextView     mType;
    public TextView     mStatus;

    public ImageButton  mDeleteBtn;

    public DeviceListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout     = (LinearLayout) convertView.findViewById(R.id.layout_device);
        mStatusIcon = (ImageView)    convertView.findViewById(R.id.img_device_icon);
        mName       = (TextView)     convertView.findViewById(R.id.text_device_title);
        mStatus     = (TextView)     convertView.findViewById(R.id.text_device_status);
        mType       = (TextView)     convertView.findViewById(R.id.text_device_type);
        mDeleteBtn  = (ImageButton)  convertView.findViewById(R.id.btn_device_delete);
    }
}

package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.pager.IsNextListPager;
import com.dignara.lib.utils.IntentUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.list.data.EvernoteSearchListViewViewData;
import com.konantech.konanlink.list.model.EvernoteSearchListSetData;
import com.konantech.konanlink.list.viewholder.EvernoteSearchListViewHolder;
import com.konantech.konanlink.model.search.EvernoteSearchInfo;
import com.konantech.konanlink.model.search.SearchResult;
import com.konantech.konanlink.part.EvernoteSearchPartFragment;

import java.io.File;

public class EvernoteSearchListAdapter extends SearchListAdapter<EvernoteSearchListViewViewData, EvernoteSearchListViewHolder, EvernoteSearchListSetData, SearchResult<EvernoteSearchInfo>> {
    private static final int COUNT_LIMIT = 50;

    private IsNextListPager mNextListPager;

    @InjectView(id = R.id.text_list_search_evernote_device_name)
    private TextView mResultText;

    public EvernoteSearchListAdapter(PartFragment partFragment, View parentView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, EvernoteSearchListSetData data) {
        super(partFragment, parentView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mNextListPager = new IsNextListPager(COUNT_LIMIT);
        setNextListController(mNextListPager);
    }

    @Override
    protected void startDownload(EvernoteSearchListViewViewData searchFileListData, File downloadTarget, boolean isShare) {
        IntentUtils.openingUrl(getContext(), searchFileListData.getPath());
    }

    @Override
    protected EvernoteSearchListViewHolder createListViewHolder(View parent, int viewType) {
        return new EvernoteSearchListViewHolder(parent);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        super.onStartLoad(isFirst);
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        mPageFragment.attachFragment(R.id.layout_list_search_main, EvernoteSearchPartFragment.class, mDeviceInfo, mNextListPager.getOffset(), mNextListPager.getLimit(), mListSetData, mNoListText);
    }

    @Override
    protected void onEndLoad(SearchResult<EvernoteSearchInfo> searchResult) {
        for (EvernoteSearchInfo searchInfo : searchResult.getResultList()) {
            addItem(new EvernoteSearchListViewViewData(searchInfo, mDeviceInfo));
        }
        mNextListPager.setListCount(searchResult.isNext(), getItemCount());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        sendCount(mResultText, mNextListPager.getOffset(), mNextListPager.isNext());
        super.onAddedItems(itemCount, errorCode);
    }

    @Override
    protected String[] getMenuRes(EvernoteSearchListViewViewData searchListData) {
        return new String[]{mContext.getString(R.string.s19_link_open_file)};
    }

    @Override
    protected String[] getMenuValue(EvernoteSearchListViewViewData searchListData) {
        return new String[]{"OPEN"};
    }

    @Override
    protected int[] getMenuIcons(EvernoteSearchListViewViewData data) {
        return new int[]{R.drawable.ic_down};
    }
}

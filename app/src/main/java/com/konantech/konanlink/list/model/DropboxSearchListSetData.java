package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

public class DropboxSearchListSetData extends ListSetData {
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    protected void onInitData() {}
}

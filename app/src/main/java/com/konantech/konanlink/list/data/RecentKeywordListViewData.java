package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.dao.RecentKeyword;
import com.konantech.konanlink.list.controller.RecentKeywordListController;

import org.apache.commons.lang3.StringUtils;

public class RecentKeywordListViewData extends ListViewData {
    private RecentKeyword recentKeyword;
    private String        title;

    public RecentKeywordListViewData(RecentKeyword recentKeyword, String inputKeyword) {
        this.recentKeyword = recentKeyword;
        this.title         = StringUtils.replace(recentKeyword.getKeyword(), inputKeyword, String.format("<font color='#ffab1b'>%s</font>", inputKeyword));
    }

    public long getIdx() {
        return recentKeyword.getId();
    }

    public String getKeyword() {
        return recentKeyword.getKeyword();
    }

    public String getTitle() {
        return title;
    }

    @Override
    protected ListViewController createViewController() {
        return new RecentKeywordListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
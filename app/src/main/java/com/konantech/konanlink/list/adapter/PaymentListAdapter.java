package com.konantech.konanlink.list.adapter;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.fragment.PartFragment;
import com.dignara.lib.list.ListSetData;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.pager.TotalNextListPager;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.PaymentApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.payment.PaidResult;
import com.konantech.konanlink.list.data.PaymentListViewData;
import com.konantech.konanlink.list.viewholder.PaymentListViewHolder;

public class PaymentListAdapter extends ListViewAdapter<PaymentListViewData, PaymentListViewHolder, ListSetData, ResultList<PaidResult>> {
    private static final int PAGE_LIMIT = 20;

    private PartFragment       mPartFragment;
    private TotalNextListPager mTotalNextListPager;

    @InjectView(id = R.id.text_listno)
    private TextView mNoListText;

    public PaymentListAdapter(PartFragment partFragment, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId) {
        super(partFragment.getView(), listParentResId, listViewResId, adapterResId, noDataViewResId);
        mPartFragment       = partFragment;
        mNoListText         = (TextView) mNoDataLayout.findViewById(R.id.text_listno);
        mTotalNextListPager = new TotalNextListPager(PAGE_LIMIT);
        setNextListController(mTotalNextListPager);
        ((ImageView) mNoDataLayout.findViewById(R.id.img_listno)).setImageResource(R.drawable.ic_wallet);
    }

    @Override
    protected PaymentListViewHolder createListViewHolder(View parent, int viewType) {
        return new PaymentListViewHolder(parent);
    }

    @Override
    protected void onSetData(PaymentListViewHolder listViewHolder, PaymentListViewData listViewData) {
    }

    @Override
    protected void onCleanup() {
    }

    @Override
    public void onStartLoad(boolean isFirst) {
        mNoListText.setText(R.string.s99_text_loading);
        reqPaidList();
    }

    private void reqPaidList() {
        new PaymentApiManager().reqList(mContext, mPartFragment.getSubscriptions(), mTotalNextListPager.getOffset(), mTotalNextListPager.getLimit(), new Result.OnResultListener<ResultList<PaidResult>>() {
            @Override
            public void onSuccess(ResultList<PaidResult> data) {
                endLoadList(data);
            }

            @Override
            public void onFailed(int code, String msg) {
                Snackbar.make(mPartFragment.getView(), msg, Snackbar.LENGTH_SHORT).show();
                endLoadList(null);
            }
        });
    }

    @Override
    public void onEndLoad(ResultList<PaidResult> result) {
        for (PaidResult paidInfo : result.getList()) {
            addItem(new PaymentListViewData(paidInfo.getTerm(), paidInfo.getPaymentType(), paidInfo.getDescription(), paidInfo.getCreatedDate()));
        }
        mTotalNextListPager.addListCount(result.getTotal(), result.getList().size());
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        if (mListSetData.isSuccess()) {
            if (itemCount == 0) {
                mNoListText.setText(R.string.s29_text_no_history);
            } else {
                mNoListText.setText("");
            }
        } else {
            mNoListText.setText(R.string.s26_error_initialize_history);
        }
    }
}

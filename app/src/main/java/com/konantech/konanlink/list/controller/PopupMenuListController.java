package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.PopupMenuListViewData;
import com.konantech.konanlink.list.viewholder.PopupMenuListViewHolder;

public class PopupMenuListController extends ListViewController<PopupMenuListViewData, PopupMenuListViewHolder> {
    private String title;
    private boolean isSelected;

    public PopupMenuListController(PopupMenuListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        title      = mViewData.getTitle();
        isSelected = mViewData.isSelected();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, PopupMenuListViewHolder listViewHolder) {
        listViewHolder.mTitle.setText(title);
        listViewHolder.mTitle.setSelected(isSelected);
    }

    @Override
    protected void onCleanup() {
    }
}
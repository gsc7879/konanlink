package com.konantech.konanlink.list.model;

public class DialogCheckListData {
    private int iconResId;
    private String title;
    private String value;
    private boolean isChecked;

    public DialogCheckListData(int iconResId, String title, String value) {
        this.iconResId = iconResId;
        this.title     = title;
        this.value     = value;
    }

    public DialogCheckListData(String title, String value, boolean isChecked) {
        this.title = title;
        this.value = value;
        this.isChecked = isChecked;
    }

    public int getIcon() {
        return iconResId;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public boolean isChecked() {
        return isChecked;
    }
}

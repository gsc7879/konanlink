package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.TextUtils;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.EvernoteSearchListViewViewData;
import com.konantech.konanlink.list.viewholder.EvernoteSearchListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Locale;

public class EvernoteSearchListController extends SearchListController<EvernoteSearchListViewViewData, EvernoteSearchListViewHolder> {
    private String mTitle;
    private String mInfo;
    private String mContent;
    private int    mTypeResId;

    public EvernoteSearchListController(EvernoteSearchListViewViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mTitle     = mViewData.getTitle();
        mInfo      = getInfo();
        mTypeResId = InfoUtils.getTypeIconResId(DataBank.getInstance().getExtensionType(FilenameUtils.getExtension(mViewData.getTitle())), false);
        mContent   = mViewData.getContent();
    }

    private String getInfo() {
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(mViewData.getModifyTime());
        String size = TextUtils.toNumInUnits(mViewData.getSize());
        return String.format("%s | %s", date, size);
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, EvernoteSearchListViewHolder viewHolder) {
        mPicasso.load(mTypeResId).into(viewHolder.mIcon);
        viewHolder.mName.setText(mTitle);
        viewHolder.mInfo.setText(mInfo);
        viewHolder.mContent.setText(mContent);
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.NoticeListController;

import java.util.Date;

public class NoticeListViewData extends ListViewData {
    private int    idx;
    private int    platform;
    private String category;
    private String title;
    private String contents;
    private String url;
    private Date   createdDate;
    private Date   updatedDate;
    private boolean isRead;


    public NoticeListViewData(int idx, int platform, String category, String title, String contents, String url, Date createdDate, Date updatedDate, long lastReadTime) {
        this.idx = idx;
        this.platform = platform;
        this.category = category;
        this.title = title;
        this.contents = contents;
        this.url = url;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        setRead(lastReadTime);
    }

    public int getIdx() {
        return idx;
    }

    public int getPlatform() {
        return platform;
    }

    public String getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public String getContents() {
        return contents;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public String getUrl() {
        return url;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(long lastReadTime) {
        isRead = updatedDate.getTime() <= lastReadTime;
    }

    @Override
    protected ListViewController createViewController() {
        return new NoticeListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
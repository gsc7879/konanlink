package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.view.View.OnClickListener;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.data.ExtensionMenuListViewData;
import com.konantech.konanlink.list.model.ExtensionMenuListData;
import com.konantech.konanlink.list.model.ExtensionMenuListSetData;
import com.konantech.konanlink.list.viewholder.ExtensionMenuListViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ExtensionMenuListAdapter extends ListViewAdapter<ExtensionMenuListViewData, ExtensionMenuListViewHolder, ExtensionMenuListSetData, List<ExtensionMenuListData>> {
    private OnSelectedItemListener mSelectedItemListener;

    public interface OnSelectedItemListener {
        void onSelectedItem(int callbackId, ListViewData listViewData);
    }

    public ExtensionMenuListAdapter(View rootView, OnSelectedItemListener selectedItemListener, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, ExtensionMenuListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mSelectedItemListener = selectedItemListener;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        List<ExtensionMenuListData> extensionMenuListDataList = new ArrayList<>();
        String[] menuArray = mListSetData.getMenuArray();
        int selectedIndex  = mListSetData.getSelectedIndex();
        for (int i = 0; i < menuArray.length; i++) {
            extensionMenuListDataList.add(new ExtensionMenuListData(menuArray[i], selectedIndex == i));
        }
        endLoadList(extensionMenuListDataList);
    }

    @Override
    protected void onEndLoad(List<ExtensionMenuListData> dataList) {
        for (ExtensionMenuListData data : dataList) {
            addItem(new ExtensionMenuListViewData(data.getTitle(), data.isSelected()));
        }
    }

    @Override
    protected ExtensionMenuListViewHolder createListViewHolder(View parent, int viewType) {
        return new ExtensionMenuListViewHolder(parent);
    }

    @Override
    protected void onSetData(ExtensionMenuListViewHolder listViewHolder, final ExtensionMenuListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItemListener.onSelectedItem(mListSetData.getCallbackId(), listViewData);
            }
        });
    }

    @Override
    protected void onCleanup() {
    }
}

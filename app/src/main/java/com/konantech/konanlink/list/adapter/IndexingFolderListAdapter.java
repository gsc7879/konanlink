package com.konantech.konanlink.list.adapter;

import android.view.View;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.FileConst;
import com.konantech.konanlink.list.data.IndexingFolderListViewData;
import com.konantech.konanlink.list.model.IndexingFolderListSetData;
import com.konantech.konanlink.list.viewholder.IndexingFolderListViewHolder;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class IndexingFolderListAdapter extends ListViewAdapter<IndexingFolderListViewData, IndexingFolderListViewHolder, IndexingFolderListSetData, File[]> {
    private File mStorage;

    @InjectView(id = R.id.text_listno)
    private TextView mNoListText;

    private final FileFilter mDirectoryFilter = new FileFilter() {
        public boolean accept(File file) {
            if (file.isDirectory() && !file.getName().startsWith(".") && !StringUtils.equals(file.getPath(), FileConst.FOLDER_HOME.getPath())) {
                return true;
            }
            return false;
        }
    };
    public IndexingFolderListAdapter(File storage, View rootView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, IndexingFolderListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
        mStorage = storage;
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        mNoListText.setText(R.string.s99_text_loading);
        endLoadList(mStorage.listFiles(mDirectoryFilter));
    }

    @Override
    protected void onEndLoad(File[] files) {
        List<String> indexingFolderList = mListSetData.getFolderList();
        for (File subFolder : files) {
            if (indexingFolderList.contains(subFolder.getPath())) {
                addItem(new IndexingFolderListViewData(subFolder, true), 0);
            } else {
                addItem(new IndexingFolderListViewData(subFolder, false));
            }
        }
    }

    @Override
    protected void onAddedItems(int itemCount, int errorCode) {
        if (itemCount == 0) {
            mNoListText.setText(R.string.s99_error_cannot_read);
        } else {
            mNoListText.setText("");
        }
    }

    @Override
    protected IndexingFolderListViewHolder createListViewHolder(View parent, int viewType) {
        return new IndexingFolderListViewHolder(parent);
    }

    @Override
    protected void onSetData(IndexingFolderListViewHolder listViewHolder, final IndexingFolderListViewData listViewData) {
        listViewHolder.mCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listViewData.setIndexing(!listViewData.isIndexing());
            }
        });
    }

    @Override
    protected void onCleanup() {
    }

    public List<String> getCheckedFolderList() {
        List<String> checkedFolderList = new ArrayList<>();
        IndexingFolderListViewData listData;
        for (ListViewData data : mItemList) {
            listData = (IndexingFolderListViewData) data;
            if (listData.isIndexing()) {
                checkedFolderList.add(listData.getFolder().getPath());
            }
        }
        return checkedFolderList;
    }
}

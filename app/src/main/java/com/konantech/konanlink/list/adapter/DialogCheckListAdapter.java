package com.konantech.konanlink.list.adapter;

import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.konantech.konanlink.list.data.DialogCheckListViewData;
import com.konantech.konanlink.list.model.DialogCheckListData;
import com.konantech.konanlink.list.model.DialogListSetData;
import com.konantech.konanlink.list.viewholder.DialogCheckListViewHolder;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DialogCheckListAdapter extends ListViewAdapter<DialogCheckListViewData, DialogCheckListViewHolder, DialogListSetData, List<DialogCheckListData>> {
    public DialogCheckListAdapter(View rootView, int listParentResId, int listViewResId, int adapterResId, int noDataViewResId, DialogListSetData data) {
        super(rootView, listParentResId, listViewResId, adapterResId, noDataViewResId, data);
    }

    @Override
    protected void onStartLoad(boolean isFirst) {
        endLoadList(getCheckListData(mListSetData.getTitles(), mListSetData.getValues(), mListSetData.getChecked()));
    }

    private List<DialogCheckListData> getCheckListData(String[] titles, String[] values, String[] checked) {
        List<DialogCheckListData> checkListData = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            checkListData.add(new DialogCheckListData(titles[i], values[i], ArrayUtils.contains(checked, values[i])));
        }
        return checkListData;
    }

    @Override
    protected void onEndLoad(List<DialogCheckListData> list) {
        DialogCheckListData listData;
        for (int i = 0; i < list.size(); i++) {
            listData = list.get(i);
            addItem(new DialogCheckListViewData(i, listData.getTitle(), listData.getValue(), listData.isChecked()));
        }
    }

    @Override
    protected DialogCheckListViewHolder createListViewHolder(View parent, int viewType) {
        return new DialogCheckListViewHolder(parent);
    }

    @Override
    protected void onSetData(DialogCheckListViewHolder listViewHolder, final DialogCheckListViewData listViewData) {
        listViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listViewData.setIsChecked(!listViewData.isChecked());
                if (StringUtils.equals(listViewData.getValue(), "all")) {
                    checkAll(listViewData.isChecked());
                } else {
                    checkAllCheck();
                }
                notifyDataSetChanged();
            }
        });
    }

    private void checkAll(boolean isCheck) {
        for (int i = 1; i < getItemCount(); i++) {
            getItem(i).setIsChecked(isCheck);
        }
    }

    private void checkAllCheck() {
        for (int i = 1; i < getItemCount(); i++) {
            if (!getItem(i).isChecked()) {
                getItem(0).setIsChecked(false);
                return;
            }
        }
        getItem(0).setIsChecked(true);
    }

    @Override
    protected void onCleanup() {
    }
}

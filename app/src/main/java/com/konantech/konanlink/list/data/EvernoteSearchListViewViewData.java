package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.EvernoteSearchListController;
import com.konantech.konanlink.model.search.EvernoteSearchInfo;

import java.util.Date;

public class EvernoteSearchListViewViewData extends SearchListViewData {
    private EvernoteSearchInfo searchInfo;

    public EvernoteSearchListViewViewData(EvernoteSearchInfo searchInfo, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.searchInfo = searchInfo;
    }

    public String getContent() {
        return searchInfo.getContent();
    }

    @Override
    protected ListViewController createViewController() {
        return new EvernoteSearchListController(this);
    }

    @Override
    public String getTitle() {
        return searchInfo.getName();
    }

    @Override
    public String getFolder() {
        return searchInfo.getFolderName();
    }

    @Override
    public Date getModifyTime() {
        return searchInfo.getModifiedTime();
    }

    @Override
    public long getSize() {
        return searchInfo.getSize();
    }

    @Override
    public String getSummary() {
        return searchInfo.getContent();
    }

    @Override
    public String getThumbnail() {
        return "";
    }

    @Override
    public String getPath() {
        return searchInfo.getPath();
    }

    @Override
    public String getFolderPath() {
        return searchInfo.getFolderPath();
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
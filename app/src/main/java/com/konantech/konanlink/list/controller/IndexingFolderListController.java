package com.konantech.konanlink.list.controller;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.list.data.IndexingFolderListViewData;
import com.konantech.konanlink.list.viewholder.IndexingFolderListViewHolder;

public class IndexingFolderListController extends ListViewController<IndexingFolderListViewData, IndexingFolderListViewHolder> {
    private String mFolderName;

    public IndexingFolderListController(IndexingFolderListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mFolderName = mViewData.getFolderName();
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, IndexingFolderListViewHolder listViewHolder) {
        listViewHolder.mCheckbox.setText(mFolderName);
        listViewHolder.mCheckbox.setChecked(mViewData.isIndexing());
    }

    @Override
    protected void onCleanup() {
    }
}

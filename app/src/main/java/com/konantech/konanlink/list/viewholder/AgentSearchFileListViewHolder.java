package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.konantech.konanlink.R;

public class AgentSearchFileListViewHolder extends SearchListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public ImageView    mThumbnail;
    public TextView     mName;
    public TextView     mDescription;
    public TextView     mInfo;
    public TextView     mExif;

    public AgentSearchFileListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public ViewGroup getLayout() {
        return mLayout;
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout      = (LinearLayout) convertView.findViewById(R.id.layout_file_search_kms);
        mThumbnail   = (ImageView)    convertView.findViewById(R.id.img_file_search_kms_thumbnail);
        mIcon        = (ImageView)    convertView.findViewById(R.id.img_file_search_kms_icon);
        mName        = (TextView)     convertView.findViewById(R.id.text_file_search_kms_name);
        mDescription = (TextView)     convertView.findViewById(R.id.text_file_search_kms_description);
        mInfo        = (TextView)     convertView.findViewById(R.id.text_file_search_kms_info);
        mExif        = (TextView)     convertView.findViewById(R.id.text_file_search_kms_info_exif);
    }
}

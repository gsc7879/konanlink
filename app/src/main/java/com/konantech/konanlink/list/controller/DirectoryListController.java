package com.konantech.konanlink.list.controller;

import android.os.AsyncTask;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.list.ListViewController;
import com.dignara.lib.utils.TextUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.list.data.DirectoryListViewData;
import com.konantech.konanlink.list.viewholder.DirectoryListViewHolder;
import com.konantech.konanlink.model.directory.DirectoryInfo;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DirectoryListController extends ListViewController<DirectoryListViewData, DirectoryListViewHolder> {
    private String  mName;
    private String  mInfo;
    private String  mExtension;
    private int     mIconResId;
    private boolean mIsFile;
    private AsyncTask<Void, Void, String> mExecute;


    public DirectoryListController(DirectoryListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        mExtension = mViewData.getExtension();
        mIsFile    = StringUtils.equals(mViewData.getType(), DirectoryInfo.TYPE_FILE);
        mIconResId = mIsFile ? InfoUtils.getTypeIconResId(DataBank.getInstance().getExtensionType(mExtension), false) : getIConResId(mViewData.getType());

        if (mViewData.getDeviceInfo().getKind() == DeviceConst.KIND_GMAIL && StringUtils.equals(mViewData.getType(), DirectoryInfo.TYPE_FILE)) {
            mExecute = checkAccount(listViewAdapter);
        } else {
            setInfo();
        }
    }

    private void setInfo() {
        mInfo = getFileInfo(mViewData.getModifyTime(), mViewData.getSize());
        mName = mViewData.getTitle();
    }

    private AsyncTask<Void, Void, String> checkAccount(final ListViewAdapter listViewAdapter) {
        AsyncTask<Void, Void, String> execute = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                getMailInfo((Gmail) mViewData.getDeviceInfo().getConnector());
                return null;
            }

            @Override
            protected void onPostExecute(String loginAccount) {
                setInfo();
                listViewAdapter.notifyDataSetChanged();
            }

            @Override
            protected void onCancelled(String o) {
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return execute;
    }

    private void getMailInfo(Gmail gmail) {
        try {
            Message message = gmail.users().messages().get("me", mViewData.getPath()).setFormat("metadata").execute();
            mViewData.setSize(message.getSizeEstimate());
            List<MessagePartHeader> headers = message.getPayload().getHeaders();
            for (MessagePartHeader header : headers) {
                switch (StringUtils.upperCase(header.getName())) {
                    case "FROM":
//                        mDescription = String.format("From : %s", header.getValue());
                        break;
                    case "SUBJECT":
                        mViewData.setTitle(header.getValue());
                        break;
                    case "DATE":
                        mViewData.setModifiedTime(new Date(header.getValue()));
                        break;
                    case "TO":
//                        mDescription += String.format("To : %s", header.getValue());
                        break;

                }
            }
        } catch (Exception e) {
        }
    }

    private String getFileInfo(Date modifyedDate, long size) {
        if (modifyedDate == null) {
            return "";
        }
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(modifyedDate);
        return String.format("%s | %s", date, TextUtils.toNumInUnits(size));
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, DirectoryListViewHolder listViewHolder) {
        listViewHolder.mName.setText(mName);
        listViewHolder.mIcon.setBackgroundResource(mIconResId);
        listViewHolder.mEtcBtn.setVisibility(mIsFile && StringUtils.isNotBlank(mExtension) ? View.VISIBLE : View.GONE);
        listViewHolder.mInfo.setVisibility(mIsFile ? View.VISIBLE : View.GONE);
        listViewHolder.mInfo.setText(mIsFile ? mInfo : "");
    }

    @Override
    protected void onCleanup() {
        if (mExecute != null) {
            mExecute.cancel(true);
            mExecute = null;
        }
    }

    private int getIConResId(String type) {
        if (StringUtils.equals(type, DirectoryInfo.TYPE_UP)) {
            return R.drawable.ic_upfolder;
        } else if (StringUtils.equals(type, DirectoryInfo.TYPE_DRIVE)) {
            return R.drawable.ic_drive;
        } else if (StringUtils.equals(type, DirectoryInfo.TYPE_DIRECTORY)) {
            return R.drawable.ic_folder;
        } else {
            return 0;
        }
    }
}
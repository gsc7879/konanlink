package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.view.ViewGroup;

import com.dignara.lib.list.ListViewHolder;

public abstract class SearchListViewHolder extends ListViewHolder {
    public SearchListViewHolder(View itemView) {
        super(itemView);
    }

    public abstract ViewGroup getLayout();
}

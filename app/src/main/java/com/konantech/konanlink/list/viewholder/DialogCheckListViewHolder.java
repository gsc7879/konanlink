package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class DialogCheckListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public CheckBox mTitle;

    public DialogCheckListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_dialog_check);
        mTitle  = (CheckBox) convertView.findViewById(R.id.text_dialog_check_title);
    }
}

package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.device.search.model.OneDriveFile;
import com.konantech.konanlink.list.controller.OneDriveSearchFileListController;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

public class OneDriveSearchListViewViewData extends SearchListViewData {
    private OneDriveFile.DriveFile driveFile;

    public OneDriveSearchListViewViewData(OneDriveFile.DriveFile driveFile, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.driveFile = driveFile;
    }

    public String getFileExtension() {
        return FilenameUtils.getExtension(driveFile.getName());
    }

    public String getLink() {
        return driveFile.getLink();
    }

    public String getResolution() {
        if (driveFile.getWidth() == 0 || driveFile.getHeight() == 0) {
            return null;
        }
        return String.format("%d x %d", driveFile.getWidth(), driveFile.getHeight());
    }

    public OneDriveFile.DriveFile.Location getLocation() {
        return driveFile.getLocation();
    }

    @Override
    protected ListViewController createViewController() {
        return new OneDriveSearchFileListController(this);
    }

    @Override
    public String getTitle() {
        return driveFile.getName();
    }

    @Override
    public String getFolder() {
        return driveFile.getParentId();
    }

    @Override
    public String getFolderPath() {
        return driveFile.getParentId();
    }

    @Override
    public Date getModifyTime() {
        return driveFile.getUpdateDate();
    }

    @Override
    public long getSize() {
        return driveFile.getSize();
    }

    @Override
    public String getSummary() {
        return driveFile.getDescription();
    }

    @Override
    public String getThumbnail() {
        List<OneDriveFile.DriveFile.ImageInfo> imageInfoList = driveFile.getImageInfoList();
        if (imageInfoList != null) {
            for (OneDriveFile.DriveFile.ImageInfo imageInfo : imageInfoList) {
                if (StringUtils.equals(imageInfo.getType(), "normal")) {
                    return imageInfo.getSource();
                }
            }
        }
        return driveFile.getPicture();
    }

    @Override
    public String getPath() {
        return driveFile.getId();
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.list.controller.DropboxSearchListController;
import com.konantech.konanlink.model.search.DropboxSearchInfo;

import java.util.Date;

public class DropboxSearchListViewViewData extends SearchListViewData {
    private DropboxSearchInfo searchInfo;

    public DropboxSearchListViewViewData(DropboxSearchInfo searchInfo, DeviceInfo deviceInfo) {
        super(deviceInfo);
        this.searchInfo = searchInfo;
    }

    public String getResolution() {
        return searchInfo.getResolution();
    }

    public String getAddress() {
        return searchInfo.getAddress();
    }

    public void setResolution(String resolution) {
        searchInfo.setResolution(resolution);
    }

    public void setAddress(String address) {
        searchInfo.setAddress(address);
    }

    @Override
    public String getTitle() {
        return searchInfo.getName();
    }

    @Override
    public String getFolder() {
        return searchInfo.getFolderName();
    }

    @Override
    public String getFolderPath() {
        return searchInfo.getFolderPath();
    }

    @Override
    public Date getModifyTime() {
        return searchInfo.getModifiedTime();
    }

    public long getSize() {
        return searchInfo.getSize();
    }

    @Override
    public String getSummary() {
        return "";
    }

    @Override
    public String getThumbnail() {
        return searchInfo.getThumbNail();
    }

    @Override
    public String getPath() {
        return searchInfo.getPath();
    }

    @Override
    protected ListViewController createViewController() {
        return new DropboxSearchListController(this);
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
    }
}
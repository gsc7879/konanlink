package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;
import com.konantech.konanlink.api.model.device.DeviceInfo;

public class DialogSelectListSetData extends ListSetData<DeviceInfo> {
    private String[] titles;
    private String[] values;
    private int[] icons;

    public String[] getTitles() {
        return titles;
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public int[] getIcons() {
        return icons;
    }

    public void setIcons(int[] icons) {
        this.icons = icons;
    }

    @Override
    protected void onInitData() {}
}

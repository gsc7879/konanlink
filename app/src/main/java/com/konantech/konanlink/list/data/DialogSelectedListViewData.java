package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.DialogSelectedListController;

public class DialogSelectedListViewData extends ListViewData {
    private int     idx;
    private String  title;
    private String  value;
    private boolean isSelected;

    public DialogSelectedListViewData(int idx, String title, String value, boolean isSelected) {
        this.idx       = idx;
        this.title     = title;
        this.value     = value;
        this.isSelected = isSelected;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    protected ListViewController createViewController() {
        return new DialogSelectedListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
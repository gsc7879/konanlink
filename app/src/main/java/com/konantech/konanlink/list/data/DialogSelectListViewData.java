package com.konantech.konanlink.list.data;

import com.dignara.lib.list.ListViewController;
import com.dignara.lib.list.ListViewData;
import com.konantech.konanlink.list.controller.DialogSelectListController;

public class DialogSelectListViewData extends ListViewData {
    private int      idx;
    private String   title;
    private String   value;
    private int iconResId;

    public DialogSelectListViewData(int idx, String title, String value, int iconResId) {
        this.idx   = idx;
        this.title = title;
        this.value = value;
        this.iconResId = iconResId;
    }

    public int getIdx() {
        return idx;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public int getIcon() {
        return iconResId;
    }

    @Override
    protected ListViewController createViewController() {
        return new DialogSelectListController(this);
    }

    @Override
    protected void onCleanup() {
    }
}
package com.konantech.konanlink.list.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dignara.lib.list.ListViewHolder;
import com.konantech.konanlink.R;

public class DialogSelectListViewHolder extends ListViewHolder {
    public LinearLayout mLayout;
    public ImageView    mIcon;
    public TextView     mTitle;

    public DialogSelectListViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setListViewHolder(View convertView) {
        mLayout = (LinearLayout) convertView.findViewById(R.id.layout_dialog_select);
        mIcon   = (ImageView)    convertView.findViewById(R.id.img_dialog_select_icon);
        mTitle  = (TextView)     convertView.findViewById(R.id.text_dialog_select_title);
    }
}

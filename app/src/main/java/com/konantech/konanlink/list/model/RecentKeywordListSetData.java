package com.konantech.konanlink.list.model;

import com.dignara.lib.list.ListSetData;

public class RecentKeywordListSetData extends ListSetData {
    private String recentKeyword;

    public String getRecentKeyword() {
        return recentKeyword;
    }

    public void setRecentKeyword(String recentKeyword) {
        this.recentKeyword = recentKeyword;
    }

    @Override
    protected void onInitData() {}
}

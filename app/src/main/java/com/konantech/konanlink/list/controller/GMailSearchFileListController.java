package com.konantech.konanlink.list.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.dignara.lib.list.ListViewAdapter;
import com.dignara.lib.utils.TextUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.konantech.konanlink.R;
import com.konantech.konanlink.list.data.GMailSearchListViewData;
import com.konantech.konanlink.list.viewholder.GMailSearchListViewHolder;
import com.konantech.konanlink.utils.InfoUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GMailSearchFileListController extends SearchListController<GMailSearchListViewData, GMailSearchListViewHolder> {
    private String mTitle;
    private String mInfo;
    private String mSnippet;
    private String mFrom;
    private String mTo;
    private int    mTypeResId;
    private AsyncTask<Void, Void, String> mExecute;

    public GMailSearchFileListController(GMailSearchListViewData listViewData) {
        super(listViewData);
    }

    @Override
    public void setInitViewData(ListViewAdapter listViewAdapter) {
        checkAccount(listViewAdapter);
        mTypeResId = InfoUtils.getTypeIconResId("mail", false);
    }

    private void setInfo() {
        mSnippet = mViewData.getSnippet();
        mTitle = mViewData.getTitle();
        mFrom = String.format("From : %s", mViewData.getFrom());
        mTo = String.format("To : %s", mViewData.getTo());
        mInfo = getInfo(mViewData.getModifyTime(), mViewData.getSize());
    }

    private String getInfo(Date modifyDate, long mailSize) {
        String date = FastDateFormat.getInstance("yyyy.MM.dd a hh:mm", Locale.getDefault()).format(modifyDate);
        if (mailSize > 0) {
            return String.format("%s | %s", date, TextUtils.toNumInUnits(mailSize));
        } else {
            return date;
        }
    }

    private void checkAccount(final ListViewAdapter listViewAdapter) {
        if (mExecute != null) {
            return;
        }
        mExecute  = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                getMailInfo((Gmail) mViewData.getDeviceInfo().getConnector(), listViewAdapter.getContext());
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                setInfo();
                listViewAdapter.notifyDataSetChanged();

            }

            @Override
            protected void onCancelled(String o) {
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void getMailInfo(Gmail gmail, Context context) {
        try {
            Message message = gmail.users().messages().get("me", mViewData.getPath()).setFormat("metadata").execute();
            List<MessagePartHeader> headers = message.getPayload().getHeaders();
            mViewData.setSnippet(message.getSnippet());
            mViewData.setSize(message.getSizeEstimate());
            mViewData.setFolderPath(message.getLabelIds().get(0));
            for (MessagePartHeader header : headers) {
                switch (StringUtils.upperCase(header.getName())) {
                    case "FROM":
                        mViewData.setFrom(header.getValue());
                        break;
                    case "SUBJECT":
                        mViewData.setTitle(StringUtils.defaultIfBlank(header.getValue(), context.getString(R.string.s17_text_no_subject)));
                        break;
                    case "DATE":
                        mViewData.setModifyTime(new Date(header.getValue()));
                        break;
                    case "TO":
                        mViewData.setTo(header.getValue());
                        break;
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void setView(ListViewAdapter listViewAdapter, GMailSearchListViewHolder viewHolder) {
        mPicasso.load(mTypeResId).into(viewHolder.mIcon);
        viewHolder.mName.setText(mTitle);
        viewHolder.mInfo.setText(mInfo);
        viewHolder.mFrom.setText(mFrom);
        viewHolder.mTo.setText(mTo);
        viewHolder.mLayout.setEnabled(StringUtils.isNotBlank(mInfo));
        if (StringUtils.isBlank(mSnippet)) {
            viewHolder.mDescription.setVisibility(View.GONE);
        } else {
            viewHolder.mDescription.setVisibility(View.VISIBLE);
            viewHolder.mDescription.setText(mSnippet);
        }
    }

    @Override
    protected void onCleanup() {
        super.onCleanup();
        if (mExecute != null) {
            mExecute.cancel(true);
            mExecute = null;
        }
    }
}
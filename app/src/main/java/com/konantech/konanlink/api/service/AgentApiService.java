package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.api.model.agent.request.MetaIndexRequest;
import com.konantech.konanlink.api.model.agent.response.DirectoryResponse;
import com.konantech.konanlink.api.model.agent.response.MetaIndexResponse;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

public interface AgentApiService {
    String URL_SEARCH = "/v1/device/%s/search";
    String URL_DIRECTORY_INFO = "/v1/device/%s/directory";
    String URL_MOBILE_META_INFO = "/v1/device/%s/update/meta";
    @GET
    Observable<SearchResponse> search (
            @Url                                    String url,
            @Header(ParamName.AUTHORIZATION)        String authorization,
            @Query(ParamName.DEVICE_KEY)            String deviceKey,
            @Query(ParamName.KEYWORD)               String keyword,
            @Query(ParamName.RANGE)                 String range,
            @Query(ParamName.SORT)                  String sort,
            @Query(ParamName.SUMMARY_SIZE)          String summarySize,
            @Query(ParamName.PAGE)                  String page,
            @Query(ParamName.SIZE)                  String size,
            @Query(ParamName.HIGHLIGHT_START_TAG)   String highlightStartTag,
            @Query(ParamName.HIGHLIGHT_END_TAG)     String highlightEndTag,
            @Query(ParamName.START_PERIOD_TIME)     String startPeriodTime,
            @Query(ParamName.END_PERIOD_TIME)       String endPeriodTime
     );

    @GET
    Observable<DirectoryResponse> directory (
            @Url                                String url,
            @Header(ParamName.AUTHORIZATION)    String authorization,
            @Query(ParamName.PATH)              String path
    );

    @GET
    @Streaming
    Call<ResponseBody> downloadFile(
            @Header(ParamName.AUTHORIZATION)    String authorization,
            @Url String url
    );


    @POST
    Observable<MetaIndexResponse> metaIndexRequest (
            @Url                                String url,
            @Header(ParamName.AUTHORIZATION)    String authorization,
            @Body                               MetaIndexRequest requestBody
    );

    @Multipart
    @POST("v1/response/{" + ParamName.REQUEST_ID + "}/download")
    Call<ResponseBody> downloadReqResponse (
            @Path(ParamName.REQUEST_ID)         String requestId,
            @Part                               MultipartBody.Part file
    );
}
package com.konantech.konanlink.api.manager;

import android.content.Context;
import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.announce.AnnounceResult;
import com.konantech.konanlink.api.service.AnnounceApiService;
import com.konantech.konanlink.utils.InfoUtils;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AnnounceApiManager extends ApiManager {
    public void reqList(final Context context, CompositeSubscription subscriptions, int offset, int limit, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(AnnounceApiService.class).list(getApiKey(), AnnounceApiService.PLATFORM, InfoUtils.getServerLocale(context), offset, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<ResultList<AnnounceResult>>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<AnnounceResult>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }
}

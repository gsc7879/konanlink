package com.konantech.konanlink.api.model.agent.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.constant.TransactionType;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * Created by kimmyeongseong on 2016. 11. 22..
 */

public class DownloadRequest extends AgentRequest {
    @JsonProperty(ParamName.PATH)
    private String path;

    @JsonProperty(ParamName.TYPE)
    private String type;

    @JsonProperty(ParamName.TITLE)
    private String title;

    public DownloadRequest() {
        this(TransactionType.DOWNLOAD, null);
    }

    protected DownloadRequest(String transaction, String path) {
        this(transaction, path, null);
    }

    protected DownloadRequest(String transaction, String path, String type) {
        super(transaction);
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonIgnore
    public File getFile() {
        return FileUtils.getFile(path);
    }

    @JsonIgnore
    public String getFilename() {
        return StringUtils.isBlank(title) ? FilenameUtils.getName(path) : String.format("%s.%s", title, FilenameUtils.getExtension(path));
    }
}

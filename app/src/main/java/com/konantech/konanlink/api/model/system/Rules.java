package com.konantech.konanlink.api.model.system;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.SystemApiService;
import com.konantech.konanlink.constant.NetConst;

import java.util.Date;

public class Rules {
    @JsonProperty(SystemApiService.RESPONSE_RULES_TYPE)
    private String type;

    @JsonProperty(SystemApiService.RESPONSE_UPDATE_TIME)
    private Date updateTime;

    public Rules() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUrl() {
        return String.format("%s/%s", NetConst.getInstance().getBaseUrl(), type);
    }
}

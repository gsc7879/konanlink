package com.konantech.konanlink.api.model;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.constant.ErrorConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.page.fragment.IntroPageFragment;
import com.konantech.konanlink.utils.ErrorUtil;
import com.konantech.konanlink.utils.IntentChangeUtils;

import org.apache.commons.lang3.StringUtils;

public class Result<T> {
    public interface OnResultListener<T> {
        void onSuccess(T data);

        void onFailed(int code, String msg);
    }

    public interface OnResult<T> {
        void onResult(T data);
    }

    @JsonProperty("code")
    private int code;

    @JsonProperty("status")
    private String status;

    @JsonProperty("data")
    private T data;

    public Result() {

    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setResult(Context context, OnResultListener resultListener) {
        setResult(context, resultListener, null);
    }

    public void setResult(Context context, OnResultListener resultListener, OnResult result) {
        if (StringUtils.equals(status, NetConst.STATUS_FAIL)) {
            switch (code) {
                case ErrorConst.ERR_1201_EMPTY_API_KEY:
                case ErrorConst.ERR_1202_NOT_EXIST_API_KEY:
                    IntentChangeUtils.goLoadingPage(context, IntroPageFragment.class);
                    return;
            }
            if (resultListener != null) {
                resultListener.onFailed(code, ErrorUtil.getErrorMsg(context, code));
            }
        } else {
            if (result != null) {
                result.onResult(data);
            }

            if (resultListener != null) {
                resultListener.onSuccess(data);
            }
        }
    }
}

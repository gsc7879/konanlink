package com.konantech.konanlink.api.model.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.AccountApiService;

import java.io.Serializable;
import java.util.Date;

public class AccountInfo implements Serializable {
    @JsonProperty(AccountApiService.RESPONSE_EMAIL)
    private String email;

    @JsonProperty(AccountApiService.RESPONSE_SUB_EMAIL)
    private String subEmail;

    @JsonProperty(AccountApiService.RESPONSE_LANG)
    private String lang;

    @JsonProperty(AccountApiService.RESPONSE_COUNTRY)
    private String country;

    @JsonProperty(AccountApiService.RESPONSE_CREATED_TIME)
    private Date createdDate;

    @JsonProperty(AccountApiService.RESPONSE_EXPIRE_DATE)
    private Date expireDate;

    @JsonProperty(AccountApiService.RESPONSE_UPDATED_TIME)
    private Date updatedDate;

    @JsonProperty(AccountApiService.RESPONSE_EXPIRE_REMAIN)
    private long expireRemainTime;

    @JsonProperty(AccountApiService.RESPONSE_USER_TYPE)
    private int userType;

    public AccountInfo() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubEmail() {
        return subEmail;
    }

    public void setSubEmail(String subEmail) {
        this.subEmail = subEmail;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public long getExpireRemainTime() {
        return expireRemainTime;
    }

    public void setExpireRemainTime(long expireRemainTime) {
        this.expireRemainTime = expireRemainTime;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public boolean isExpired() {
        return expireRemainTime < 0;
    }
}

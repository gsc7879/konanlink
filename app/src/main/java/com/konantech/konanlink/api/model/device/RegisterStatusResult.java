package com.konantech.konanlink.api.model.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.DeviceApiService;

public class RegisterStatusResult {
    @JsonProperty(DeviceApiService.RESPONSE_REGISTERED)
    boolean isRegistered;

    @JsonProperty(DeviceApiService.RESPONSE_GIFT)
    boolean isGiftAble;

    public RegisterStatusResult() {

    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    public boolean isGiftAble() {
        return isGiftAble;
    }

    public void setGiftAble(boolean giftAble) {
        isGiftAble = giftAble;
    }
}

package com.konantech.konanlink.api.manager;

import android.content.Context;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultMap;
import com.konantech.konanlink.api.model.system.Rules;
import com.konantech.konanlink.api.model.system.SystemInfo;
import com.konantech.konanlink.api.service.SystemApiService;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.utils.InfoUtils;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import java.util.List;

public class SystemApiManager extends ApiManager {
    public void reqSystemInfo(final Context context, CompositeSubscription subscriptions, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(SystemApiService.class).systemInfo(DeviceConst.KIND_ANDROID, InfoUtils.getServerLocale(context))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new rx.Observer<ApiResult<Result<SystemInfo>>>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    if (resultListener != null) {
                        resultListener.onFailed(0, null);
                    }
                }

                @Override
                public void onNext(ApiResult<Result<SystemInfo>> resultApiResult) {
                    resultApiResult.getResult().setResult(context, resultListener);
                }
            }));
    }

    public void reqRules(final Context context, CompositeSubscription subscriptions, String type, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(SystemApiService.class).rules(type, InfoUtils.getServerLocale(context))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<ApiResult<Result<Rules>>>() {
               @Override
               public void onCompleted() {
                   hideDialog();
               }

               @Override
               public void onError(Throwable e) {
                   hideDialog();
                   if (resultListener != null) {
                       resultListener.onFailed(0, null);
                   }
               }

               @Override
               public void onNext(ApiResult<Result<Rules>> resultApiResult) {
                   resultApiResult.getResult().setResult(context, resultListener);
               }
           }));
        showDialog(context, context.getString(R.string.s99_text_loading));
    }

    public void reqExtensionList(final Context context, CompositeSubscription subscriptions, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(SystemApiService.class).extensionList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new Observer<ApiResult<Result<ResultMap<String, List<String>>>>>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    if (resultListener != null) {
                        resultListener.onFailed(0, null);
                    }
                }

                @Override
                public void onNext(ApiResult<Result<ResultMap<String, List<String>>>> resultApiResult) {
                    resultApiResult.getResult().setResult(context, resultListener);
                }
            }));
    }
}

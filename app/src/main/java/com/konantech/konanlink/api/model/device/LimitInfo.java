package com.konantech.konanlink.api.model.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.DeviceApiService;

import java.io.Serializable;

public class LimitInfo implements Serializable {
    @JsonProperty(DeviceApiService.RESPONSE_LIMIT)
    private int limit;

    @JsonProperty(DeviceApiService.RESPONSE_LIMIT_REMAINS)
    private int remains;

    public LimitInfo() {

    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getRemains() {
        return remains;
    }

    public void setRemains(int remains) {
        this.remains = remains;
    }
}
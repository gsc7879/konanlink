package com.konantech.konanlink.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

public class ResultMap<K, V> {
    @JsonProperty("list")
    private HashMap<K, V> map;

    public ResultMap() {

    }

    public HashMap<K, V> getMap() {
        return map;
    }

    public void setMap(HashMap<K, V> map) {
        this.map = map;
    }
}

package com.konantech.konanlink.api.model.agent.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;

import java.util.ArrayList;
import java.util.List;

public class DirectoryResponse extends Response {

    @JsonProperty(ParamName.PARENT)
    private String parent;

    @JsonProperty(ParamName.FOLDERS)
    private List<Item> folders;

    @JsonProperty(ParamName.VOLUMES)
    private List<Item> volumes;

    @JsonProperty(ParamName.FILES)
    private List<Item> files;

    public DirectoryResponse() {
        volumes = new ArrayList<>();
        folders = new ArrayList<>();
        files = new ArrayList<>();
    }

    public List<Item> getFolders() {
        return folders;
    }

    public void setFolders(List<Item> folders) {
        this.folders = folders;
    }

    public List<Item> getVolumes() {
        return volumes;
    }

    public void setVolumes(List<Item> volumes) {
        this.volumes = volumes;
    }

    public List<Item> getFiles() {
        return files;
    }

    public void setFiles(List<Item> files) {
        this.files = files;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public static class Item {
        @JsonProperty(ParamName.NAME)
        private String name;

        @JsonProperty(ParamName.SIZE)
        private long size;

        @JsonProperty(ParamName.MODIFIED_TIME)
        private long modifiedDate;

        @JsonProperty(ParamName.EXTENSION)
        private String extension;

        @JsonProperty(ParamName.PATH)
        private String path;

        @JsonProperty(ParamName.DOWNLOAD_URL)
        private String downloadURL;

        @JsonProperty(ParamName.THUMBNAIL_URL)
        private String thumbnailURL;

        public Item() {

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public long getModifiedDate() {
            return modifiedDate;
        }

        public void setModifiedDate(long modifiedDate) {
            this.modifiedDate = modifiedDate;
        }

        public String getExtension() {
            return extension;
        }

        public void setExtension(String extension) {
            this.extension = extension;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getDownloadURL() {
            return downloadURL;
        }

        public void setDownloadURL(String downloadURL) {
            this.downloadURL = downloadURL;
        }

        public String getThumbnailURL() {
            return thumbnailURL;
        }

        public void setThumbnailURL(String thumbnailURL) {
            this.thumbnailURL = thumbnailURL;
        }
    }
}


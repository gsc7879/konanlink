package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.announce.AnnounceResult;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import rx.Observable;

public interface AnnounceApiService {
    String URL_LIST             = "v1/announce/list.json";

    String HEADER_AUTHORIZATION = "Authorization";

    String REQUEST_PLATFORM     = "platform";
    String REQUEST_LANGUAGE     = "lang";
    String REQUEST_OFFSET       = "offset";
    String REQUEST_LIMIT        = "limit";

    String RESPONSE_IDX         = "announce_idx";
    String RESPONSE_PLATFORM    = "platform";
    String RESPONSE_CATEGORY    = "category";
    String RESPONSE_CREATED     = "created_time";
    String RESPONSE_UPDATED     = "updated_time";
    String RESPONSE_TITLE       = "title";
    String RESPONSE_CONTENTS    = "content";
    String RESPONSE_URL         = "url";

    int PLATFORM                = 2;

    @GET(URL_LIST)
    Observable<ApiResult<Result<ResultList<AnnounceResult>>>> list(
            @Header(HEADER_AUTHORIZATION) String apiKey,
            @Query(REQUEST_PLATFORM)      int    platform,
            @Query(REQUEST_LANGUAGE)      String lang,
            @Query(REQUEST_OFFSET  )      int    offset,
            @Query(REQUEST_LIMIT   )      int    limit);
}

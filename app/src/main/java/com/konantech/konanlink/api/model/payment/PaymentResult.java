package com.konantech.konanlink.api.model.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.PaymentApiService;

import java.util.Date;

public class PaymentResult {
    @JsonProperty(PaymentApiService.RESPONSE_TYPE)
    private int paymentType;

    @JsonProperty(PaymentApiService.RESPONSE_TERM)
    private int term;

    @JsonProperty(PaymentApiService.RESPONSE_EXPIRE_DATE)
    private Date expireDate;

    @JsonProperty(PaymentApiService.RESPONSE_EXPIRE_REMAIN)
    private long expireRemainTime;

    @JsonProperty(PaymentApiService.RESPONSE_DESCRIPTION)
    private String description;


    public PaymentResult() {

    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public long getExpireRemainTime() {
        return expireRemainTime;
    }

    public void setExpireRemainTime(long expireRemainTime) {
        this.expireRemainTime = expireRemainTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

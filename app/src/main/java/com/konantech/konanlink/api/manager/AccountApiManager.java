package com.konantech.konanlink.api.manager;

import android.content.Context;
import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.account.JoinResult;
import com.konantech.konanlink.api.model.account.LoginResult;
import com.konantech.konanlink.api.model.account.SendEmailResult;
import com.konantech.konanlink.api.service.AccountApiService;
import com.konantech.konanlink.data.DataBank;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AccountApiManager extends ApiManager {
    public void reqUsersInfo(final Context context, CompositeSubscription subscriptions, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(AccountApiService.class).userInfo(getApiKey())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(new rx.Observer<ApiResult<Result<AccountInfo>>>() {
                @Override
                public void onCompleted() {
                }

                @Override
                public void onError(Throwable e) {
                    if (resultListener != null) {
                        resultListener.onFailed(0, null);
                    }
                }

                @Override
                public void onNext(ApiResult<Result<AccountInfo>> resultApiResult) {
                    resultApiResult.getResult().setResult(context, resultListener);
                }
            }));
    }

    public void reqLogin(final Context context, CompositeSubscription subscriptions, String email, String password, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(AccountApiService.class).login(email, password, DataBank.getInstance().getDeviceKey())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<LoginResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<LoginResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }

    public void reqJoin(final Context context, CompositeSubscription subscriptions, String email, String password, String country, String lang, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(AccountApiService.class).join(email, password, country, lang)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<JoinResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<JoinResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }

    public void reqSendEmail(final Context context, CompositeSubscription subscriptions, String email, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(AccountApiService.class).sendEmail(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<SendEmailResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<SendEmailResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }
}

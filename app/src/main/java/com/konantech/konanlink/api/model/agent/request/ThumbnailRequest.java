package com.konantech.konanlink.api.model.agent.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;

/**
 * Created by kimmyeongseong on 2016. 11. 22..
 */

public class ThumbnailRequest extends DownloadRequest {
    @JsonProperty(ParamName.WIDTH)
    private int width;

    @JsonProperty(ParamName.HEIGHT)
    private int height;

    public ThumbnailRequest() {

    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.device.*;
import retrofit2.http.*;
import rx.Observable;

public interface DeviceApiService {
    String URL_DEVICE_LIST            = "v1/device/list.json";
    String URL_DEVICE_INFO            = "v1/device/profile.json";
    String URL_DEVICE_REGISTER        = "v1/device/register.json";
    String URL_DEVICE_REMOVE          = "v1/device/remove.json";
    String URL_DEVICE_INDEXING_STATUS = "v1/device/update_indexing_status.json";
    String URL_DEVICE_UPDATE_NOTI_ID  = "v1/device/update_notification_id.json";
    String URL_DEVICE_REGISTER_STATUS = "v1/device/status_registered.json";
    String URL_DEVICE_UPDATE_HOST     = "v1/device/update_host.json";
    String URL_FILESERVER_LIST        = "v1/device/list_file_server.json";

    String HEADER_AUTHORIZATION       = "Authorization";

    String REQUEST_DEVICE_NAME        = "device_name";
    String REQUEST_DEVICE_KEY         = "device_key";
    String REQUEST_OAUTH_KEY          = "auth_key";
    String REQUEST_HOST_KEY           = "host_key";
    String REQUEST_ACCOUNT            = "account";
    String REQUEST_INDEXING_STATUS    = "indexing_status";
    String REQUEST_NOTIFICATION_ID    = "notification_id";
    String REQUEST_DEVICE_KIND        = "device_kind";
    String REQUEST_OFFSET             = "offset";
    String REQUEST_LIMIT              = "limit";
    String REQUEST_EMAIL              = "email";

    String RESPONSE_REGISTERED        = "registered";
    String RESPONSE_GIFT              = "gift_registered";
    String RESPONSE_HOST_KEY          = "host_key";
    String RESPONSE_DEVICE_KEY        = "device_key";
    String RESPONSE_DEVICE_NAME       = "device_name";
    String RESPONSE_ACCOUNT           = "account";
    String RESPONSE_DEVICE_KIND       = "device_kind";
    String RESPONSE_AUTH_KEY          = "auth_key";
    String RESPONSE_NOTIFICATION_ID   = "notification_id";
    String RESPONSE_DEVICE_NAT_IP     = "device_nat_ip";
    String RESPONSE_DEVICE_NAT_PORT   = "device_nat_port";
    String RESPONSE_DEVICE_LOCAL_IP   = "device_local_ip";
    String RESPONSE_DEVICE_LOCAL_PORT = "device_local_port";
    String RESPONSE_DEVICE_RELAY_IP   = "device_relay_ip";
    String RESPONSE_DEVICE_RELAY_PORT = "device_relay_port";
    String RESPONSE_INDEXING_STATUS   = "indexing_status";
    String RESPONSE_LIMIT_INFO        = "limit_info";
    String RESPONSE_LIMIT_REMAINS     = "remains";
    String RESPONSE_LIMIT             = "limit";

    @GET(URL_DEVICE_LIST)
    Observable<ApiResult<Result<DeviceResultList<DeviceInfo>>>> deviceList(
            @Header(HEADER_AUTHORIZATION) String apiKey,
            @Query(REQUEST_OFFSET)        int    offset,
            @Query(REQUEST_LIMIT)         int    limit);

    @GET(URL_DEVICE_INFO)
    Observable<ApiResult<Result<DeviceInfo>>> deviceInfo(
            @Header(HEADER_AUTHORIZATION) String apiKey,
            @Query(REQUEST_DEVICE_KEY)    String deviceKey);

    @FormUrlEncoded
    @POST(URL_DEVICE_REGISTER)
    Observable<ApiResult<Result<AddDeviceResult>>> addDevice(
            @Header(HEADER_AUTHORIZATION)   String apiKey,
            @Field(REQUEST_DEVICE_KEY)      String deviceKey,
            @Field(REQUEST_DEVICE_NAME)     String deviceName,
            @Field(REQUEST_DEVICE_KIND)     int    deviceKind,
            @Field(REQUEST_HOST_KEY)        String hostKey,
            @Field(REQUEST_ACCOUNT)         String account,
            @Field(REQUEST_OAUTH_KEY)       String oauthKey,
            @Field(REQUEST_INDEXING_STATUS) int    indexingStatus);

    @FormUrlEncoded
    @POST(URL_DEVICE_REMOVE)
    Observable<ApiResult<Result<RemoveDeviceResult>>> removeDevice(
            @Header(HEADER_AUTHORIZATION)   String apiKey,
            @Field(REQUEST_DEVICE_KEY)      String deviceKey);

    @FormUrlEncoded
    @POST(URL_DEVICE_INDEXING_STATUS)
    Observable<ApiResult<Result<ResultList<DeviceInfo>>>> updateIndexingStatus(
            @Header(HEADER_AUTHORIZATION)   String apiKey,
            @Field(REQUEST_DEVICE_KEY)      String deviceKey,
            @Field(REQUEST_INDEXING_STATUS) int    indexingStatus);

    @GET(URL_DEVICE_REGISTER_STATUS)
    Observable<ApiResult<Result<RegisterStatusResult>>> registerStatus(
            @Query(REQUEST_DEVICE_KEY)      String deviceKey);

    @FormUrlEncoded
    @POST(URL_DEVICE_UPDATE_HOST)
    Observable<ApiResult<Result<ResultList<DeviceInfo>>>> hostUpdate(
            @Header(HEADER_AUTHORIZATION)   String apiKey,
            @Field(REQUEST_DEVICE_KEY)      String deviceKey,
            @Field(REQUEST_HOST_KEY)        String hostKey,
            @Field(REQUEST_INDEXING_STATUS) int    indexingStatus);

    @FormUrlEncoded
    @POST(URL_DEVICE_UPDATE_NOTI_ID)
    Observable<ApiResult<Result<ResultList<DeviceInfo>>>> updateNotificationId(
            @Header(HEADER_AUTHORIZATION)   String apiKey,
            @Field(REQUEST_DEVICE_KEY)      String deviceKey,
            @Field(REQUEST_NOTIFICATION_ID) String notificationId);

    @GET(URL_FILESERVER_LIST)
    Observable<ApiResult<Result<ResultList<FileServerInfo>>>> fileServerList(
            @Query(REQUEST_EMAIL)           String email,
            @Query(REQUEST_OFFSET)          int    offset,
            @Query(REQUEST_LIMIT)           int    limit);
}

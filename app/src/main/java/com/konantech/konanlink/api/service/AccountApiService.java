package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.account.JoinResult;
import com.konantech.konanlink.api.model.account.LoginResult;
import com.konantech.konanlink.api.model.account.SendEmailResult;
import retrofit2.http.*;
import rx.Observable;

public interface AccountApiService {
    String URL_JOIN                = "v1/account/register.json";
    String URL_LOGIN               = "v1/account/login.json";
    String URL_RESET_PW            = "v1/account/reset_password.json";
    String URL_INFO                = "v1/account/profile.json";

    String HEADER_AUTHORIZATION    = "Authorization";

    String REQUEST_PASSWORD        = "password";
    String REQUEST_DEVICE_KEY      = "device_key";
    String REQUEST_COUNTRY         = "country";
    String REQUEST_LANGUAGE        = "lang";
    String REQUEST_EMAIL           = "email";

    String RESPONSE_KEY            = "api_key";
    String RESPONSE_EMAIL          = "email";
    String RESPONSE_SUB_EMAIL      = "sub_email";
    String RESPONSE_LANG           = "lang";
    String RESPONSE_ADMIN          = "isadmin";
    String RESPONSE_COUNTRY        = "country";
    String RESPONSE_GIFT           = "gift_registered";
    String RESPONSE_USER_TYPE      = "users_type";
    String RESPONSE_UPDATED_TIME   = "updated_time";
    String RESPONSE_CREATED_TIME   = "created_time";
    String RESPONSE_EXPIRE_REMAIN  = "expire_remain_time";
    String RESPONSE_EXPIRE_DATE    = "expire_date";

    @GET(URL_INFO)
    Observable<ApiResult<Result<AccountInfo>>> userInfo(
            @Header(HEADER_AUTHORIZATION) String apiKey);

    @FormUrlEncoded
    @POST(URL_LOGIN)
    Observable<ApiResult<Result<LoginResult>>> login(
            @Field(REQUEST_EMAIL)      String email,
            @Field(REQUEST_PASSWORD)   String password,
            @Field(REQUEST_DEVICE_KEY) String deviceKey);

    @FormUrlEncoded
    @POST(URL_JOIN)
    Observable<ApiResult<Result<JoinResult>>> join(
            @Field(REQUEST_EMAIL)      String email,
            @Field(REQUEST_PASSWORD)   String password,
            @Field(REQUEST_COUNTRY)    String country,
            @Field(REQUEST_LANGUAGE)   String lang);

    @FormUrlEncoded
    @POST(URL_RESET_PW)
    Observable<ApiResult<Result<SendEmailResult>>> sendEmail(
            @Field(REQUEST_EMAIL) String email);
}

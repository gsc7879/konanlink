package com.konantech.konanlink.api.model.payment;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.PaymentApiService;

import java.util.Date;

public class PaidResult {
    @JsonProperty(PaymentApiService.RESPONSE_TERM)
    private int term;

    @JsonProperty(PaymentApiService.RESPONSE_TYPE)
    private int paymentType;

    @JsonProperty(PaymentApiService.RESPONSE_DESCRIPTION)
    private String description;

    @JsonProperty(PaymentApiService.RESPONSE_CREATED_TIME)
    private Date createdDate;

    public PaidResult() {

    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}

package com.konantech.konanlink.api.manager;

import android.content.Context;

import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.api.model.agent.request.MetaIndexRequest;
import com.konantech.konanlink.api.model.agent.response.DirectoryResponse;
import com.konantech.konanlink.api.model.agent.response.DownloadRespone;
import com.konantech.konanlink.api.model.agent.response.MetaIndexResponse;
import com.konantech.konanlink.api.model.agent.response.SearchResponse;
import com.konantech.konanlink.api.service.AgentApiService;
import com.konantech.konanlink.constant.SearchConst;
import com.konantech.konanlink.utils.ErrorUtil;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.activation.MimetypesFileTypeMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

public class AgentApiManager extends ApiManager {
    public AgentApiManager(String response_url) {
        super(response_url);
    }


    public interface OnDownload {
        void onSuccess(File downloadFile);

        void onFailed(int code, String msg);

        void onProgress(long transSize, long totalSize);
    }

    public void reqDirectory(final Context context, CompositeSubscription subscriptions, String url, String path, final Result.OnResultListener<DirectoryResponse> resultListener) {
        subscriptions.add(retrofit.create(AgentApiService.class).directory(url, getAuthorization(), path)
                .subscribe(new rx.Observer<DirectoryResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        int erroCode = ErrorUtil.getErrorCode(e);
                        if (ErrorUtil.isCheckLoadingPage(context, erroCode)) {
                            return;
                        }
                        if (resultListener != null) {
                            resultListener.onFailed(erroCode, ErrorUtil.getErrorMsg(context, erroCode));
                        }
                    }

                    @Override
                    public void onNext(DirectoryResponse resultApiResult) {
                        if (resultListener != null) {
                            resultListener.onSuccess(resultApiResult);
                        }
                    }
                }));
    }

    public void reqSearch(final Context context, CompositeSubscription subscriptions, String url, String deviceKey, String keyWord, String range, String sort, String summarySize, String page, String size, String startPeriodTime, String endPeriodTime, final Result.OnResultListener<SearchResponse> resultListener) {
        subscriptions.add(retrofit.create(AgentApiService.class).search(url, getAuthorization(), deviceKey, keyWord, range, sort, summarySize, page, size, SearchConst.TAG_HIGHLIGHT_START, SearchConst.TAG_HIGHLIGHT_END, startPeriodTime, endPeriodTime)
                .subscribe(new rx.Observer<SearchResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        int erroCode = ErrorUtil.getErrorCode(e);
                        if (ErrorUtil.isCheckLoadingPage(context, erroCode)) {
                            return;
                        }
                        if (resultListener != null) {
                            resultListener.onFailed(erroCode, ErrorUtil.getErrorMsg(context, erroCode));
                        }
                    }

                    @Override
                    public void onNext(SearchResponse resultApiResult) {
                        if (resultListener != null) {
                            resultListener.onSuccess(resultApiResult);
                        }
                    }
                }));
    }

    public void reqDownload(final Context context, CompositeSubscription subscriptions, Scheduler scheduler, final String downloadUrl, final File destFile, final OnDownload downloadListener) {
        subscriptions.add(Observable.create(
                (Observable.OnSubscribe<DownloadRespone>) new Observable.OnSubscribe<DownloadRespone>() {
                    @Override
                    public void call(Subscriber<? super DownloadRespone> subscriber) {
                        try {
                            Request request = retrofit.create(AgentApiService.class).downloadFile(getAuthorization(), downloadUrl).request();
                            Response response = new OkHttpClient().newCall(request).execute();
                            if (response != null && response.isSuccessful()) {
                                ResponseBody body = (ResponseBody) response.body();
                                InputStream inputStream = null;
                                OutputStream outputStream = null;
                                try {
                                    long fileSize = body.contentLength();
                                    long downloadedSize = 0;
                                    byte[] fileReader = new byte[4096];
                                    inputStream = body.byteStream();
                                    outputStream = new FileOutputStream(destFile);
                                    int read;
                                    DownloadRespone download = new DownloadRespone(0, fileSize);
                                    subscriber.onNext(download);
                                    while ((read = inputStream.read(fileReader)) != -1) {
                                        outputStream.write(fileReader, 0, read);
                                        downloadedSize += read;
                                        download.setTransSize(downloadedSize);
                                        download.setTotalSize(fileSize);
                                        subscriber.onNext(download);

                                    }
                                    outputStream.flush();
                                    download.setTotalSize(fileSize);
                                    download.setTransSize(fileSize);
                                    subscriber.onNext(download);
                                    subscriber.onCompleted();
                                } catch (IOException e) {
                                    subscriber.onError(e);
                                } finally {
                                    IOUtils.closeQuietly(inputStream);
                                    IOUtils.closeQuietly(outputStream);
                                }
                            }
                        } catch (IOException e) {
                            subscriber.onError(e);
                        }
                    }
                })
                .onBackpressureDrop()
                .observeOn(scheduler)
                .subscribe(new Subscriber<DownloadRespone>() {
                    @Override
                    public void onCompleted() {
                        if (downloadListener != null) {
                            downloadListener.onSuccess(destFile);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        int erroCode = ErrorUtil.getErrorCode(e);
                        if (ErrorUtil.isCheckLoadingPage(context, erroCode)) {
                            return;
                        }
                        if (downloadListener != null) {
                            downloadListener.onFailed(erroCode, ErrorUtil.getErrorMsg(context, erroCode));
                        }
                    }

                    @Override
                    public void onNext(DownloadRespone download) {
                        if (downloadListener != null) {
                            downloadListener.onProgress(download.getTransSize(), download.getTotalSize());
                        }
                    }
                }));
    }

    public void reqFileIndex(final Context context, CompositeSubscription subscriptions, String url, MetaIndexRequest requestBody, final Result.OnResultListener<MetaIndexResponse> resultListener) {
        subscriptions.add(retrofit.create(AgentApiService.class).metaIndexRequest(url, getAuthorization(), requestBody)
                .subscribe(new rx.Observer<MetaIndexResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        int erroCode = ErrorUtil.getErrorCode(e);
                        if (ErrorUtil.isCheckLoadingPage(context, erroCode)) {
                            return;
                        }
                        if (resultListener != null) {
                            resultListener.onFailed(erroCode, ErrorUtil.getErrorMsg(context, erroCode));
                        }
                    }

                    @Override
                    public void onNext(MetaIndexResponse resultApiResult) {
                        if (resultListener != null) {
                            resultListener.onSuccess(resultApiResult);
                        }
                    }
                }));
    }

    public void reqUpload(final Context context, CompositeSubscription subscriptions, String requestId, final File file, final OnDownload resultListener) throws IOException {
        reqUpload(context, subscriptions, requestId, file.getName(), MediaType.parse(new MimetypesFileTypeMap().getContentType(file)), file.length(), new FileInputStream(file), resultListener);
    }

    public void reqUpload(final Context context, CompositeSubscription subscriptions, String requestId, String fileName, final MediaType contentType, final long contentLength, final InputStream inputStream, final OnDownload resultListener) {
        RequestBody requestBody = new RequestBody() {
            final int DEFAULT_BUFFER_SIZE = 65536;

            @Override
            public MediaType contentType() {
                return contentType;
            }

            @Override
            public long contentLength() {
                return contentLength;
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                long bytesUploaded = 0;
                try {
                    byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                    int bytesRead = 0;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        bytesUploaded += bytesRead;
                        sink.write(buffer, 0, bytesRead);
                        if (resultListener != null && (bytesUploaded % 5 == 0)) {
                            resultListener.onProgress(bytesUploaded, contentLength());
                        }
                    }

                    if (resultListener != null) {
                        resultListener.onProgress(contentLength(), contentLength());
                    }
                } finally {
                    inputStream.close();
                }
            }
        };

        retrofit.create(AgentApiService.class).downloadReqResponse(requestId, MultipartBody.Part.createFormData(ParamName.FILE, fileName, requestBody))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (resultListener != null) {
                                resultListener.onSuccess(null);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable e) {
                        int erroCode = ErrorUtil.getErrorCode(e);
                        if (ErrorUtil.isCheckLoadingPage(context, erroCode)) {
                            return;
                        }
                        if (resultListener != null) {
                            resultListener.onFailed(erroCode, ErrorUtil.getErrorMsg(context, erroCode));
                        }
                    }
                });
    }
}

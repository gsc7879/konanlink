package com.konantech.konanlink.api.model.announce;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.AnnounceApiService;

import java.util.Date;

public class AnnounceResult {
    @JsonProperty(AnnounceApiService.RESPONSE_IDX)
    private int idx;

    @JsonProperty(AnnounceApiService.RESPONSE_PLATFORM)
    private int platform;

    @JsonProperty(AnnounceApiService.RESPONSE_CATEGORY)
    private String category;

    @JsonProperty(AnnounceApiService.RESPONSE_TITLE)
    private String title;

    @JsonProperty(AnnounceApiService.RESPONSE_CONTENTS)
    private String contents;

    @JsonProperty(AnnounceApiService.RESPONSE_URL)
    private String url;

    @JsonProperty(AnnounceApiService.RESPONSE_CREATED)
    private Date createdTime;

    @JsonProperty(AnnounceApiService.RESPONSE_UPDATED)
    private Date updatedTime;

    public AnnounceResult() {

    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}

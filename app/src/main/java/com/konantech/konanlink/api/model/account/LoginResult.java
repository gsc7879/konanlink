package com.konantech.konanlink.api.model.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.AccountApiService;

public class LoginResult {
    @JsonProperty(AccountApiService.RESPONSE_KEY)
    private String apiKey;

    @JsonProperty(AccountApiService.RESPONSE_EMAIL)
    private String email;

    @JsonProperty(AccountApiService.RESPONSE_ADMIN)
    private boolean isAdmin;

    @JsonProperty(AccountApiService.RESPONSE_GIFT)
    private boolean isGiftRegistered;

    public LoginResult() {

    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isGiftRegistered() {
        return isGiftRegistered;
    }

    public void setGiftRegistered(boolean giftRegistered) {
        isGiftRegistered = giftRegistered;
    }
}

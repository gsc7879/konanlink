package com.konantech.konanlink.api.model.agent.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgentRequest {
    @JsonProperty(ParamName.REQUEST_ID)
    private String requestId;

    @JsonProperty(ParamName.TRANSACTION)
    private String transaction;

    @JsonProperty(ParamName.RESPONSE_URL)
    private String responseURL;

    public AgentRequest() {

    }

    @JsonCreator
    public AgentRequest(@JsonProperty(ParamName.TRANSACTION) String transaction) {
        this.requestId   = UUID.randomUUID().toString();
        this.transaction = transaction;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getResponseURL() {
        return responseURL;
    }

    public void setResponseURL(String responseURL) {
        this.responseURL = responseURL;
    }
}
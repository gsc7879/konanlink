package com.konantech.konanlink.api.model.agent.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.constant.TransactionType;

import java.util.ArrayList;
import java.util.List;

public class MetaIndexRequest extends AgentRequest {
    @JsonProperty(ParamName.DEVICE_KEY)
    private String deviceKey;

    @JsonProperty(ParamName.RECREATE)
    private boolean isRecreate;

    @JsonProperty(ParamName.ITEMS)
    private List<Item> items;

    public MetaIndexRequest() {
        super(TransactionType.MOBILE_META);
        items = new ArrayList<>();
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public boolean isRecreate() {
        return isRecreate;
    }

    public void setRecreate(boolean recreate) {
        isRecreate = recreate;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public static class Item {
        @JsonProperty(ParamName.STATUS)
        private String status;

        @JsonProperty(ParamName.TITLE)
        private String title;

        @JsonProperty(ParamName.PATH)
        private String path;

        @JsonProperty(ParamName.SIZE)
        private long size;

        @JsonProperty(ParamName.SEARCH_KEYWORD)
        private String searchKeyword;

        @JsonProperty(ParamName.CREATED_TIME)
        private long createdTime;

        @JsonProperty(ParamName.MODIFIED_TIME)
        private long modifiedTime;

        @JsonProperty(ParamName.LONGITUDE)
        private double longitude;

        @JsonProperty(ParamName.LATITUDE)
        private double latitude;

        @JsonProperty(ParamName.WIDTH)
        private int width;

        @JsonProperty(ParamName.HEIGHT)
        private int height;

        public Item () {

        }

        public Item(String status, String path, long size, String searchKeyword, long createdTime, long modifiedTime, double longitude, double latitude, int width, int height) {
            this.status = status;
            this.path = path;
            this.size = size;
            this.searchKeyword = searchKeyword;
            this.createdTime = createdTime;
            this.modifiedTime = modifiedTime;
            this.longitude = longitude;
            this.latitude = latitude;
            this.width = width;
            this.height = height;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        public String getSearchKeyword() {
            return searchKeyword;
        }

        public void setSearchKeyword(String searchKeyword) {
            this.searchKeyword = searchKeyword;
        }

        public long getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(long createdTime) {
            this.createdTime = createdTime;
        }

        public long getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(long modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
    }
}
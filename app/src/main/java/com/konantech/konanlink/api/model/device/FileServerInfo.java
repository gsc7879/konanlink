package com.konantech.konanlink.api.model.device;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FileServerInfo {
    @JsonProperty("fileServerId")
    private int fileServerId;

    @JsonProperty("deviceKey")
    private String deviceKey;

    @JsonProperty("deviceName")
    private String deviceName;

    public FileServerInfo() {

    }

    public int getFileServerId() {
        return fileServerId;
    }

    public void setFileServerId(int fileServerId) {
        this.fileServerId = fileServerId;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}

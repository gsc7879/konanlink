package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.*;
import com.konantech.konanlink.api.model.system.Rules;
import com.konantech.konanlink.api.model.system.SystemInfo;
import retrofit2.http.*;
import rx.Observable;

import java.util.List;

public interface SystemApiService {
    String URL_SYSTEM_INFO             = "v1/system/info_type.json";
    String URL_SYSTEM_RULES            = "v1/system/rules_recent.json";
    String URL_SYSTEM_EXTENSION_LIST   = "v1/system/extension_list.json";

    String RULES_TYPE_SERVICE          = "service";
    String RULES_TYPE_PRIVACY          = "privacy";
    String RULES_TYPE_PAYMENT          = "pricing";

    String REQUEST_RULES_TYPE          = "rules_type";
    String REQUEST_LANGUAGE            = "lang";
    String REQUEST_DEVICE_KIND         = "device_kind";

    String RESPONSE_UPDATE_URL         = "update_url";
    String RESPONSE_RULES_TYPE         = "rules_type";
    String RESPONSE_UPDATE_TIME        = "update_time";
    String RESPONSE_VERSION            = "version";
    String RESPONSE_MESSAGE            = "message";
    String RESPONSE_LAST_ANNOUNCE_TIME = "last_announce_time";
    String RESPONSE_MAINTENANCE        = "maintenance";

    @GET(URL_SYSTEM_INFO)
    Observable<ApiResult<Result<SystemInfo>>> systemInfo(
            @Query(REQUEST_DEVICE_KIND) int    deviceKind,
            @Query(REQUEST_LANGUAGE)    String lang);

    @GET(URL_SYSTEM_RULES)
    Observable<ApiResult<Result<Rules>>> rules(
            @Query(REQUEST_RULES_TYPE) String type,
            @Query(REQUEST_LANGUAGE)   String lang);

    @GET(URL_SYSTEM_EXTENSION_LIST)
    Observable<ApiResult<Result<ResultMap<String, List<String>>>>> extensionList();
}

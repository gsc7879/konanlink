package com.konantech.konanlink.api.model.facebook;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.api.model.agent.request.DownloadRequest;
import com.konantech.konanlink.constant.TransactionType;

public class FacebookDownloadRequest extends DownloadRequest {
    @JsonProperty(ParamName.SENDER_ID)
    private String senderId;

    @JsonProperty(ParamName.USER_ID)
    private String userId;

    @JsonProperty(ParamName.ACCESS_TOKEN)
    private String accessToken;

    public FacebookDownloadRequest() {
        super(TransactionType.FACEBOOK_DOWNLOAD, null);
    }

    public FacebookDownloadRequest(String userId, String senderId, String accessToken, String path) {
        super(TransactionType.FACEBOOK_DOWNLOAD, path);
        this.userId      = userId;
        this.senderId    = senderId;
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

}
package com.konantech.konanlink.api.model.device;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.service.DeviceApiService;

import java.util.Map;

public class DeviceResultList<T> extends ResultList<T> {
    @JsonProperty(DeviceApiService.RESPONSE_LIMIT_INFO)
    private Map<String, LimitInfo> map;

    public DeviceResultList() {

    }
    public Map<String, LimitInfo> getMap() {
        return map;
    }

    public void setMap(Map<String, LimitInfo> map) {
        this.map = map;
    }
}



package com.konantech.konanlink.api.manager;

import android.content.Context;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.device.*;
import com.konantech.konanlink.api.service.DeviceApiService;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.db.manager.AccessKeyDBManager;
import org.apache.commons.lang3.StringUtils;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DeviceApiManager extends ApiManager {
    public void reqAddDevice(final Context context, CompositeSubscription subscriptions, String deviceKey, String deviceName, int deviceKind, String hostKey, String account, String oauthKey, int indexingStatus, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).addDevice(getApiKey(), deviceKey, deviceName, deviceKind, hostKey, account, oauthKey, indexingStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<AddDeviceResult>>>() {
                    @Override
                    public void onCompleted() {
                        hideDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<AddDeviceResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
        showDialog(context, context.getString(R.string.s99_text_loading));
    }

    public void reqUpdateHost(final Context context, CompositeSubscription subscriptions, final String deviceKey, String hostKey, int indexingStatus, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).hostUpdate(getApiKey(), deviceKey, hostKey, indexingStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<ResultList<DeviceInfo>>>>() {
                    @Override
                    public void onCompleted() {
                        hideDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<DeviceInfo>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener, new Result.OnResult<ResultList<DeviceInfo>>() {
                            @Override
                            public void onResult(ResultList<DeviceInfo> deviceInfoList) {
                                for (DeviceInfo deviceInfo : deviceInfoList.getList()) {
                                    if (StringUtils.equals(deviceKey, deviceInfo.getKey())) {
                                        DataBank.getInstance().updateDeviceInfo(deviceInfo);
                                    }
                                }
                            }
                        });
                    }
                }));
        showDialog(context, context.getString(R.string.s99_text_loading));
    }

    public void reqDeviceRemove(final Context context, CompositeSubscription subscriptions, String removeDeviceKey, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).removeDevice(getApiKey(), removeDeviceKey)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<RemoveDeviceResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<RemoveDeviceResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }

    public void reqUpdateNotificationId(final Context context, CompositeSubscription subscriptions, final String deviceKey, String notificationId, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).updateNotificationId(getApiKey(), deviceKey, notificationId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<ResultList<DeviceInfo>>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<DeviceInfo>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener, new Result.OnResult<ResultList<DeviceInfo>>() {
                            @Override
                            public void onResult(ResultList<DeviceInfo> deviceInfoList) {
                                for (DeviceInfo deviceInfo : deviceInfoList.getList()) {
                                    if (StringUtils.equals(deviceKey, deviceInfo.getKey())) {
                                        DataBank.getInstance().updateDeviceInfo(deviceInfo);
                                    }
                                }
                            }
                        });
                    }
                }));
    }

    public void reqUpdateIndexingStatus(final Context context, CompositeSubscription subscriptions, final String deviceKey, int indexingStatus, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).updateIndexingStatus(getApiKey(), deviceKey, indexingStatus)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<ResultList<DeviceInfo>>>>() {
                    @Override
                    public void onCompleted() {
                        hideDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<DeviceInfo>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener, new Result.OnResult<ResultList<DeviceInfo>>() {
                            @Override
                            public void onResult(ResultList<DeviceInfo> deviceInfoList) {
                                for (DeviceInfo deviceInfo : deviceInfoList.getList()) {
                                    if (StringUtils.equals(deviceKey, deviceInfo.getKey())) {
                                        DataBank.getInstance().updateDeviceInfo(deviceInfo);
                                    }
                                }
                            }
                        });
                    }
                }));
        showDialog(context, context.getString(R.string.s32_toast_updating));
    }

    public void reqFileServerList(final Context context, CompositeSubscription subscriptions, String email, int offset, int limit, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).fileServerList(email, offset, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<ResultList<FileServerInfo>>>>() {
                    @Override
                    public void onCompleted() {
                        hideDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideDialog();
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<FileServerInfo>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
        showDialog(context, context.getString(R.string.s99_text_loading));
    }


    public void reqRegisterStatus(final Context context, CompositeSubscription subscriptions, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).registerStatus(DataBank.getInstance().getDeviceKey())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<RegisterStatusResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<RegisterStatusResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }

    public void reqDeviceInfo(final Context context, CompositeSubscription subscriptions, final String deviceKey, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).deviceInfo(getApiKey(), deviceKey)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<DeviceInfo>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<DeviceInfo>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener, new Result.OnResult<DeviceInfo>() {
                            @Override
                            public void onResult(DeviceInfo deviceInfo) {
                                DataBank.getInstance().updateDeviceInfo(deviceInfo);
                            }
                        });
                    }
                }));
    }

    public void reqDeviceList(final Context context, CompositeSubscription subscriptions, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(DeviceApiService.class).deviceList(getApiKey(), 0, 100)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ApiResult<Result<DeviceResultList<DeviceInfo>>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<DeviceResultList<DeviceInfo>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener, new Result.OnResult<DeviceResultList<DeviceInfo>>() {
                            @Override
                            public void onResult(DeviceResultList<DeviceInfo> resultList) {
                                AccessKeyDBManager accessKeyDBManager = new AccessKeyDBManager(context);
                                for (DeviceInfo deviceInfo : resultList.getList()) {
                                    deviceInfo.setAccessKey(accessKeyDBManager.getAccessKey(deviceInfo.getKey(), deviceInfo.getAccount()));
                                    if (StringUtils.isBlank(deviceInfo.getAccessKey())) {
                                        deviceInfo.setAccessKey(deviceInfo.getAuthKey());
                                    }
                                }

                                Map<String, LimitInfo> limitInfoMap = resultList.getMap();
                                DataBank.getInstance().setLimitInfo(limitInfoMap.get("etc"));
                                DataBank.getInstance().setDeviceInfoMap(resultList.getList());
                            }
                        });
                    }
                }));
    }
}

package com.konantech.konanlink.api.model.facebook;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;

import java.util.List;

public class FacebookAttachmentRequest {
    public static final String BUTTON_TYPE_WEB_URL             = "web_url";
    public static final String BUTTON_TYPE_POSTBACK            = "postback";
    public static final String ATTACHMENT_TYPE_TEMPLATE        = "template";

    public static final String PAYLAOD_TEMPLATE_TYPE_GENERIC   = "generic";
    public static final String PAYLAOD_TEMPLATE_TYPE_LIST      = "list";
    public static final String ATTACHMENT_TYPE_FILE            = "file";
    public static final String ATTACHMENT_TYPE_IMAGE           = "image";


    @JsonProperty(ParamName.RECIPIENT)
    protected Recipient recipient;

    @JsonProperty(ParamName.MESSAGE)
    protected Message message;

    public FacebookAttachmentRequest() {

    }

    public FacebookAttachmentRequest(String senderId, Message message) {
        this.recipient = new FacebookAttachmentRequest.Recipient(senderId);
        this.message = message;
    }

    public FacebookAttachmentRequest(String senderId, List<Element> elements, List<FacebookAttachmentRequest.Button> buttons) {

        this.recipient = new FacebookAttachmentRequest.Recipient(senderId);
        if (elements.size() == 1) {
            this.message = new FacebookAttachmentRequest.Message("photos not found.\nFor example, you could type “July Hawaii Sea”, “Beijing 2016”, or “December Boston Tree Cat”.");
        } else {
            this.message = new FacebookAttachmentRequest.Message(new FacebookAttachmentRequest.Attatchment(ATTACHMENT_TYPE_TEMPLATE, new Payload(elements, buttons)));
        }
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public static class Recipient {
        @JsonProperty(ParamName.ID)
        private String id;

        public Recipient() {
        }

        public Recipient(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Message {
        @JsonProperty(ParamName.TEXT)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String text;

        @JsonProperty(ParamName.ATTACHMENT)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private Attatchment attatchment;

        public Message() {
        }

        public Message(String text) {
            this.text = text;
        }

        public Message(Attatchment attatchment) {
            this.attatchment = attatchment;
        }

        public Attatchment getAttatchment() {
            return attatchment;
        }

        public void setAttatchment(Attatchment attatchment) {
            this.attatchment = attatchment;
        }
    }

    public static class Attatchment {
        @JsonProperty(ParamName.TYPE)
        private String type;

        @JsonProperty(ParamName.PAYLOAD)
        private Payload payload;

        public Attatchment() {

        }

        public Attatchment(String type, Payload payload) {
            this.type = type;
            this.payload = payload;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Payload getPayload() {
            return payload;
        }

        public void setPayload(Payload payload) {
            this.payload = payload;
        }


    }

    public static class Payload {
        @JsonProperty(ParamName.TEMPLATE_TYPE)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String templateType;

        @JsonProperty(ParamName.TOP_ELEMENT_STYLE)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String topElementStyle;

        @JsonProperty(ParamName.ELEMENTS)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<Element> elements;

        @JsonProperty(ParamName.URL)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String url;

        @JsonProperty(ParamName.IS_REUSABLE)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private Boolean isReusable;

        @JsonProperty(ParamName.BUTTONS)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<Button> buttons;

        public Payload() {}

        public Payload(String url) {
            this.url = url;
        }

        public Payload(String url, boolean isReusable) {
            this.url = url;
            this.isReusable = isReusable;
        }

        public Payload(List<Element> elements) {
            this(elements, null);
        }

        public Payload(List<Element> elements, List<Button> buttons) {
            if (elements.size() == 1) {
                this.templateType = PAYLAOD_TEMPLATE_TYPE_GENERIC;
            } else {
                this.templateType = PAYLAOD_TEMPLATE_TYPE_LIST;
            }
            this.elements = elements;
            this.buttons = buttons;
        }

        public String getTemplateType() {
            return templateType;
        }

        public void setTemplateType(String templateType) {
            this.templateType = templateType;
        }

        public String getTopElementStyle() {
            return topElementStyle;
        }

        public void setTopElementStyle(String topElementStyle) {
            this.topElementStyle = topElementStyle;
        }

        public List<Element> getElements() {
            return elements;
        }

        public void setElements(List<Element> elements) {
            this.elements = elements;
        }

        public List<Button> getButtons() {
            return buttons;
        }

        public void setButtons(List<Button> buttons) {
            this.buttons = buttons;
        }


    }

    public static class Element {
        @JsonProperty(ParamName.TITLE)
        private String title;

        @JsonProperty(ParamName.SUB_TITLE)
        private String subTitle;

        @JsonProperty(ParamName.IMAGE_URL)
        private String imageUrl;

        @JsonProperty(ParamName.BUTTONS)
        private List<Button> buttons;

        public Element() {
        }

        public Element(String title, String subTitle, String imageUrl, List<Button> buttons) {
            this.title = title;
            this.subTitle = subTitle;
            this.imageUrl = imageUrl;
            this.buttons = buttons;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }


        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public List<Button> getButtons() {
            return buttons;
        }

        public void setButtons(List<Button> buttons) {
            this.buttons = buttons;
        }


    }

    public static class Button {
        @JsonProperty(ParamName.TYPE)
        private String type;

        @JsonProperty(ParamName.TITLE)
        private String title;

        @JsonProperty(ParamName.URL)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String url;

        @JsonProperty(ParamName.PAYLOAD)
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private String payload;

        public Button() {
        }

        public Button(String type, String title, String url, String payload) {
            this.type = type;
            this.title = title;
            this.url = url;
            this.payload = payload;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPayload() {
            return payload;
        }

        public void setPayload(String payload) {
            this.payload = payload;
        }
    }

}
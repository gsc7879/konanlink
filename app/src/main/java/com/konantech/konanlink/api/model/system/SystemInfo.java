package com.konantech.konanlink.api.model.system;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.SystemApiService;

public class SystemInfo {
    @JsonProperty(SystemApiService.RESPONSE_VERSION)
    private int recentVersion;

    @JsonProperty(SystemApiService.RESPONSE_UPDATE_URL)
    private String updateUrl;

    @JsonProperty(SystemApiService.RESPONSE_MESSAGE)
    private String message;

    @JsonProperty(SystemApiService.RESPONSE_LAST_ANNOUNCE_TIME)
    private long lastAnnounceTime;

    @JsonProperty(SystemApiService.RESPONSE_MAINTENANCE)
    private boolean isMaintenance;

    public SystemInfo() {

    }

    public int getRecentVersion() {
        return recentVersion;
    }

    public void setRecentVersion(int recentVersion) {
        this.recentVersion = recentVersion;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getLastAnnounceTime() {
        return lastAnnounceTime;
    }

    public void setLastAnnounceTime(long lastAnnounceTime) {
        this.lastAnnounceTime = lastAnnounceTime;
    }

    public boolean isMaintenance() {
        return isMaintenance;
    }

    public void setIsMaintenance(boolean isMaintenance) {
        this.isMaintenance = isMaintenance;
    }
}

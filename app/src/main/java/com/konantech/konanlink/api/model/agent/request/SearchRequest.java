package com.konantech.konanlink.api.model.agent.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.constant.TransactionType;


public class SearchRequest extends AgentRequest {
    @JsonProperty(ParamName.DEVICE_KEY)
    private String deviceKey;

    @JsonProperty(ParamName.DEVICE_NAME)
    private String deviceName;

    @JsonProperty(ParamName.KEYWORD)
    private String keyword;

    @JsonProperty(ParamName.RANGE)
    private String range;

    @JsonProperty(ParamName.SORT)
    private String sort;

    @JsonProperty(ParamName.HIGHLIGHT_START_TAG)
    private String highlightStartTag;

    @JsonProperty(ParamName.HIGHLIGHT_END_TAG)
    private String highlightEndTag;

    @JsonProperty(ParamName.START_PERIOD_TIME)
    private long   startPeriodTime;

    @JsonProperty(ParamName.END_PERIOD_TIME)
    private long   endPeriodTime;

    @JsonProperty(ParamName.SUMMARY_SIZE)
    private int    summarySize;

    @JsonProperty(ParamName.PAGE)
    private int    page;

    @JsonProperty(ParamName.SIZE)
    private int    size;

    public SearchRequest() {
        super(TransactionType.SEARCH);
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getHighlightStartTag() {
        return highlightStartTag;
    }

    public void setHighlightStartTag(String highlightStartTag) {
        this.highlightStartTag = highlightStartTag;
    }

    public String getHighlightEndTag() {
        return highlightEndTag;
    }

    public void setHighlightEndTag(String highlightEndTag) {
        this.highlightEndTag = highlightEndTag;
    }

    public int getSummarySize() {
        return summarySize;
    }

    public void setSummarySize(int summarySize) {
        this.summarySize = summarySize;
    }

    public long getStartPeriodTime() {
        return startPeriodTime;
    }

    public void setStartPeriodTime(long startPeriodTime) {
        this.startPeriodTime = startPeriodTime;
    }

    public long getEndPeriodTime() {
        return endPeriodTime;
    }

    public void setEndPeriodTime(long endPeriodTime) {
        this.endPeriodTime = endPeriodTime;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
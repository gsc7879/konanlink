package com.konantech.konanlink.api.manager;

import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.facebook.FacebookAttachmentRequest;
import com.konantech.konanlink.api.model.facebook.FacebookDownloadRequest;
import com.konantech.konanlink.api.service.FaceBookApiService;
import com.squareup.okhttp.ResponseBody;

import rx.subscriptions.CompositeSubscription;

public class FaceBookApiManager extends ApiManager {

    public FaceBookApiManager() {
        super();
    }

    public void reqPhotoSearchList(CompositeSubscription subscriptions, String url, FacebookAttachmentRequest request, String token, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(FaceBookApiService.class).searchResult(url, token, request)
                .subscribe(new rx.Observer<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(ResponseBody resultApiResult) {
                    }
                }));
    }

    public void reqPhotoMessageUpload(CompositeSubscription subscriptions, String url, FacebookAttachmentRequest request, String token, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(FaceBookApiService.class).messageDownload(url, token, request)
                .subscribe(new rx.Observer<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(ResponseBody resultApiResult) {
                    }
                }));
    }
}

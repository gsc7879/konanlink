package com.konantech.konanlink.api.manager;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

import com.dignara.lib.dialog.DialogController;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.utils.DialogUtils;

import org.apache.commons.lang3.StringUtils;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.protobuf.ProtoConverterFactory;

public class ApiManager {
    private String apiKey;

    protected Retrofit retrofit;

    protected DialogController mDialogController;

    protected ApiManager() {
        this(NetConst.getInstance().getBaseUrl());
    }

    protected ApiManager(String baseUrl) {
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ProtoConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(createObjectMapper()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return objectMapper;
    }

    private OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.followRedirects(false);
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }

    protected void showDialog(Context context, String loadingString) {
        hideDialog();
        if (loadingString != null) {
            mDialogController = DialogUtils.showLoadingDialog(loadingString, ((FragmentActivity) context).getFragmentManager());
        }
    }

    protected void hideDialog() {
        if (mDialogController != null) {
            mDialogController.dismiss();
            mDialogController = null;
        }
    }

    protected String getApiKey() {
        if (StringUtils.isBlank(apiKey)) {
            String apiKey = DataBank.getInstance().getApiKey();
            if (StringUtils.isBlank(apiKey)) {
                return null;
            } else {
                this.apiKey = String.format("ApiKey api_key=%s", apiKey);
            }
        }
        return apiKey;
    }

    protected String getAuthorization() {
        if (StringUtils.isBlank(apiKey)) {
            String apiKey = DataBank.getInstance().getApiKey();
            if (StringUtils.isBlank(apiKey)) {
                return null;
            } else {
                this.apiKey = apiKey;
            }
        }
        return apiKey;
    }
}

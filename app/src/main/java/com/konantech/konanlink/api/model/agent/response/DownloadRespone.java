package com.konantech.konanlink.api.model.agent.response;

/**
 * Created by kimmyeongseong on 2016. 10. 25..
 */

public class DownloadRespone {
    private long transSize;
    private long totalSize;

    public DownloadRespone() {
    }

    public DownloadRespone(long transSize, long totalSize) {
        this.transSize = transSize;
        this.totalSize = totalSize;
    }

    public long getTransSize() {
        return transSize;
    }

    public void setTransSize(long transSize) {
        this.transSize = transSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
    }
}

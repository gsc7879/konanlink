package com.konantech.konanlink.api.model.facebook;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;
import com.konantech.konanlink.api.model.agent.request.SearchRequest;

import java.util.Date;

public class FacebookSearchRequest extends SearchRequest {
    @JsonProperty(ParamName.SENDER_ID)
    private String senderId;

    @JsonProperty(ParamName.USER_ID)
    private String userId;

    @JsonProperty(ParamName.ACCESS_TOKEN)
    private String accessToken;

    @JsonProperty(ParamName.TIMESTAMP)
    private Date timestamp;

    @JsonProperty(ParamName.MESSAGE_ID)
    private String messageId;

    @JsonProperty(ParamName.SEQ)
    private int seq;

    public FacebookSearchRequest() {

    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
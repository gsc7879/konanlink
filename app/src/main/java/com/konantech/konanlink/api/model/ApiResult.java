package com.konantech.konanlink.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResult<E> {
    @JsonProperty("result")
    private E result;

    public E getResult() {
        return result;
    }

    public void setResult(E result) {
        this.result = result;
    }

    public ApiResult() {
    }

}

package com.konantech.konanlink.api.manager;

import android.content.Context;
import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.payment.PaidResult;
import com.konantech.konanlink.api.model.payment.PaymentResult;
import com.konantech.konanlink.api.service.PaymentApiService;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class PaymentApiManager extends ApiManager {
    public void reqList(final Context context, CompositeSubscription subscriptions, int offset, int limit, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(PaymentApiService.class).list(getApiKey(), offset, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<ResultList<PaidResult>>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<ResultList<PaidResult>>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }

    public void reqRegister(final Context context, CompositeSubscription subscriptions, int type, String key, int term, long time, String description, final Result.OnResultListener resultListener) {
        subscriptions.add(retrofit.create(PaymentApiService.class).register(getApiKey(), type, key, term, time, description)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new rx.Observer<ApiResult<Result<PaymentResult>>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (resultListener != null) {
                            resultListener.onFailed(0, null);
                        }
                    }

                    @Override
                    public void onNext(ApiResult<Result<PaymentResult>> resultApiResult) {
                        resultApiResult.getResult().setResult(context, resultListener);
                    }
                }));
    }
}

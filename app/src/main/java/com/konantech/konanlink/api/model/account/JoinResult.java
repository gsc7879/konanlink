package com.konantech.konanlink.api.model.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.AccountApiService;

public class JoinResult {
    @JsonProperty(AccountApiService.RESPONSE_EMAIL)
    private String email;

    public JoinResult() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

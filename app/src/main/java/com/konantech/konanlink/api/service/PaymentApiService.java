package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.ApiResult;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.payment.PaidResult;
import com.konantech.konanlink.api.model.payment.PaymentResult;
import retrofit2.http.*;
import rx.Observable;

public interface PaymentApiService {
    String URL_LIST               = "v1/payment/list.json";
    String URL_REGISTER           = "v1/payment/register.json";

    String REQUEST_TYPE           = "payment_type";
    String REQUEST_KEY            = "payment_key";
    String REQUEST_TERM           = "term";
    String REQUEST_TIME           = "payment_time";
    String REQUEST_DESCRIPTION    = "description";
    String HEADER_AUTHORIZATION   = "Authorization";
    String REQUEST_OFFSET         = "offset";
    String REQUEST_LIMIT          = "limit";

    String RESPONSE_TOTAL         = "total";
    String RESPONSE_LIMIT         = "limit";
    String RESPONSE_OFFSET        = "offset";
    String RESPONSE_EXPIRE_REMAIN = "expire_remain_time";
    String RESPONSE_EXPIRE_DATE   = "expire_date";
    String RESPONSE_TYPE          = "payment_type";
    String RESPONSE_TERM          = "term";
    String RESPONSE_DESCRIPTION   = "description";
    String RESPONSE_CREATED_TIME  = "created_time";

    int TYPE_PAYPAL               = 0;
    int TYPE_GOOGLE               = 1;
    int TYPE_APPLE                = 2;
    int TYPE_COUPON               = 3;
    int TYPE_GIFT_SAMSUNG         = 5;

    @GET(URL_LIST)
    Observable<ApiResult<Result<ResultList<PaidResult>>>> list(
            @Header(HEADER_AUTHORIZATION) String apiKey,
            @Query(REQUEST_OFFSET)        int offset,
            @Query(REQUEST_LIMIT)         int limit);

    @FormUrlEncoded
    @POST(URL_REGISTER)
    Observable<ApiResult<Result<PaymentResult>>> register(
            @Header(HEADER_AUTHORIZATION) String apiKey,
            @Field(REQUEST_TYPE)          int    type,
            @Field(REQUEST_KEY)           String key,
            @Field(REQUEST_TERM)          int    term,
            @Field(REQUEST_TIME)          long   time,
            @Field(REQUEST_DESCRIPTION)   String description);
}

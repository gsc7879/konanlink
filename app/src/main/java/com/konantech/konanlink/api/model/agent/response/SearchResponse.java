package com.konantech.konanlink.api.model.agent.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.model.agent.ParamName;

public class SearchResponse extends Response {
	@JsonProperty(ParamName.KEYWORD)
	private String keyword;

	@JsonProperty(ParamName.PAGE)
	private int page;

	@JsonProperty(ParamName.SIZE)
	private int size;

	@JsonProperty(ParamName.TOTAL_COUNT)
	private int totalCount;

	@JsonProperty(ParamName.TOTAL_PAGES)
	private int totalPages;

	@JsonProperty(ParamName.FIRST)
	private boolean isFirstPage;

	@JsonProperty(ParamName.LAST)
	private boolean isLastPage;

	@JsonProperty(ParamName.ITEMS)
	private Item[] items;

	public SearchResponse() {
		items = new Item[0];
	}


	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isFirstPage() {
		return isFirstPage;
	}

	public void setFirstPage(boolean firstPage) {
		isFirstPage = firstPage;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean lastPage) {
		isLastPage = lastPage;
	}

	public static class Item {
		@JsonProperty(ParamName.TITLE)
		private String title;

		@JsonProperty(ParamName.HIGHLIGHTED_TITLE)
		private String highLightedTitle;

		@JsonProperty(ParamName.FOLDER)
		private String folder;
		
		@JsonProperty(ParamName.DOWNLOAD_PATH)
		private String downloadPath;
		
		@JsonProperty(ParamName.MODIFIED_TIME)
		private String modifyTime;
		
		@JsonProperty(ParamName.SUMMARY)
		private String summary;
		
		@JsonProperty(ParamName.TYPE)
		private String type;
		
		@JsonProperty(ParamName.EXTENSION)
		private String extension;
		
		@JsonProperty(ParamName.ATTACHED)
		private boolean attached;

		@JsonProperty(ParamName.SIZE)
		private int size;

		@JsonProperty(ParamName.FROM)
		private String from;

		@JsonProperty(ParamName.TO)
		private String to;

		@JsonProperty(ParamName.LOCATION)
		private String location;

		@JsonProperty(ParamName.RESOLUTION)
		private String resolution;


		@JsonProperty(ParamName.DOWNLOAD_URL)
		private String downloadURL;

		@JsonProperty(ParamName.THUMBNAIL_URL)
		private String thumbNailUrl;

		public Item() {

		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getFolder() {
			return folder;
		}

		public void setFolder(String folder) {
			this.folder = folder;
		}
	
		public String getDownloadPath() {
			return downloadPath;
		}

		public void setDownloadPath(String downloadPath) {
			this.downloadPath = downloadPath;
		}

		public String getModifyTime() {
			return modifyTime;
		}
	
		public void setModifyTime(String modifyTime) {
			this.modifyTime = modifyTime;
		}
	
		public String getSummary() {
			return summary;
		}
	
		public void setSummary(String summary) {
			this.summary = summary;
		}
	
		public String getType() {
			return type;
		}
	
		public void setType(String type) {
			this.type = type;
		}
	
		public String getExtension() {
			return extension;
		}
	
		public void setExtension(String extension) {
			this.extension = extension;
		}
	
		public boolean getAttached() {
			return attached;
		}
	
		public void setAttached(boolean attached) {
			this.attached = attached;
		}
	
		public String getHighLightedTitle() {
			return highLightedTitle;
		}
	
		public void setHighLightedTitle(String highLightedTitle) {
			this.highLightedTitle = highLightedTitle;
		}
	
		public int getSize() {
			return size;
		}
	
		public void setSize(int size) {
			this.size = size;
		}

		public boolean isAttached() {
			return attached;
		}

		public String getFrom() {
			return from;
		}

		public void setFrom(String from) {
			this.from = from;
		}

		public String getTo() {
			return to;
		}

		public void setTo(String to) {
			this.to = to;
		}

		public String getLocation() {
			return location;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public String getResolution() {
			return resolution;
		}

		public void setResolution(String resolution) {
			this.resolution = resolution;
		}

		public String getDownloadURL() {
			return downloadURL;
		}

		public void setDownloadURL(String downloadURL) {
			this.downloadURL = downloadURL;
		}

		public String getThumbNailUrl() {
			return thumbNailUrl;
		}

		public void setThumbNailUrl(String thumbNailUrl) {
			this.thumbNailUrl = thumbNailUrl;
		}
	}
}
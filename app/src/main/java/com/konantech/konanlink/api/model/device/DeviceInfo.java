package com.konantech.konanlink.api.model.device;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.konantech.konanlink.api.service.DeviceApiService;
import com.konantech.konanlink.constant.DeviceConst;
import com.konantech.konanlink.constant.IndexingConst;
import com.konantech.konanlink.data.DataBank;

import org.apache.commons.lang3.StringUtils;

public class DeviceInfo implements Parcelable {
    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_KIND)
    private int kind;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_NAT_PORT)
    private int natPort;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_LOCAL_PORT)
    private int localPort;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_RELAY_PORT)
    private int relayPort;

    @JsonProperty(DeviceApiService.RESPONSE_INDEXING_STATUS)
    private int indexingStatus;

    @JsonProperty(DeviceApiService.RESPONSE_ACCOUNT)
    private String account;

    @JsonProperty(DeviceApiService.RESPONSE_HOST_KEY)
    private String hostKey;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_KEY)
    private String key;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_NAME)
    private String name;

    @JsonProperty(DeviceApiService.RESPONSE_AUTH_KEY)
    private String authKey;

    @JsonProperty(DeviceApiService.RESPONSE_NOTIFICATION_ID)
    private String notificationId;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_NAT_IP)
    private String natIP;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_LOCAL_IP)
    private String localIP;

    @JsonProperty(DeviceApiService.RESPONSE_DEVICE_RELAY_IP)
    private String relayIP;

    private String accessKey;

    private int connectType;

    private Object connector;

    private DeviceInfo hostInfo;

    public DeviceInfo() {

    }

    public DeviceInfo(Parcel src) {
        readFromParcel(src);
    }


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getKind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    public int getNatPort() {
        return natPort;
    }

    public void setNatPort(int natPort) {
        this.natPort = natPort;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    public int getIndexingStatus() {
        return indexingStatus;
    }

    public void setIndexingStatus(int indexingStatus) {
        this.indexingStatus = indexingStatus;
    }

    public void setHostKey(String hostKey) {
        this.hostKey = hostKey;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getNatIP() {
        return natIP;
    }

    public void setNatIP(String natIP) {
        this.natIP = natIP;
    }

    public String getLocalIP() {
        return localIP;
    }

    public void setLocalIP(String localIP) {
        this.localIP = localIP;
    }

    public int getRelayPort() {
        return relayPort;
    }

    public void setRelayPort(int relayPort) {
        this.relayPort = relayPort;
    }

    public String getRelayIP() {
        return relayIP;
    }

    public void setRelayIP(String relayIP) {
        this.relayIP = relayIP;
    }

    public void setConnectType(int connectType) {
        this.connectType = connectType;
    }

    public void setAccessKey(String authKey) {
        this.accessKey = authKey;
    }

    public int getCategory() {
        return kind / 10;
    }

    public void setConnector(Object connector) {
        this.connector = connector;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public int getConnectType() {
        return connectType;
    }

    public boolean isAgent() {
        return getCategory() == DeviceConst.CATEGORY_PC && StringUtils.equals(hostKey, key);
    }

    public Object getConnector() {
        return connector;
    }

    public boolean isSearchHost() {
        return isAgent() ||
                kind == DeviceConst.KIND_DROPBOX ||
                kind == DeviceConst.KIND_GOOGLEDRIVE ||
                kind == DeviceConst.KIND_ONEDRIVE ||
                kind == DeviceConst.KIND_GMAIL ||
                kind == DeviceConst.KIND_EVERNOTE;
    }

    public boolean isSearchAble() {
        return isIndexing() ||
                kind == DeviceConst.KIND_DROPBOX ||
                kind == DeviceConst.KIND_GOOGLEDRIVE ||
                kind == DeviceConst.KIND_ONEDRIVE ||
                kind == DeviceConst.KIND_GMAIL ||
                kind == DeviceConst.KIND_EVERNOTE;
    }

    public boolean isIndexing() {
        return indexingStatus == IndexingConst.STATUS_REGULAR && StringUtils.isNotBlank(hostKey);
    }

    public boolean isMyDevice() {
        return StringUtils.equals(key, DataBank.getInstance().getDeviceKey());
    }

    public DeviceInfo getHostInfo() {
        if (hostInfo == null) {
            hostInfo = StringUtils.isBlank(hostKey) ? this : DataBank.getInstance().getDeviceInfo(hostKey);
        }
        return hostInfo;
    }

    public void initHostInfo() {
        hostInfo = null;
    }

    public void resetConnector(int connectType) {
        connector        = null;
        this.connectType = connectType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(kind);
        dest.writeInt(natPort);
        dest.writeInt(localPort);
        dest.writeInt(relayPort);
        dest.writeInt(indexingStatus);
        dest.writeString(account);
        dest.writeString(hostKey);
        dest.writeString(key);
        dest.writeString(name);
        dest.writeString(authKey);
        dest.writeString(notificationId);
        dest.writeString(natIP);
        dest.writeString(localIP);
        dest.writeString(relayIP);
        dest.writeString(accessKey);
    }

    private void readFromParcel(Parcel src){
        this.kind            = src.readInt();
        this.natPort         = src.readInt();
        this.localPort       = src.readInt();
        this.relayPort       = src.readInt();
        this.indexingStatus  = src.readInt();
        this.account         = src.readString();
        this.hostKey         = src.readString();
        this.key             = src.readString();
        this.name            = src.readString();
        this.authKey         = src.readString();
        this.notificationId  = src.readString();
        this.natIP           = src.readString();
        this.localIP         = src.readString();
        this.relayIP         = src.readString();
        this.accessKey       = src.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public DeviceInfo createFromParcel(Parcel in) {
            return new DeviceInfo(in);
        }

        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };
}

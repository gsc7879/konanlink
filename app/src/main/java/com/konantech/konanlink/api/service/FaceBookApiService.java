package com.konantech.konanlink.api.service;

import com.konantech.konanlink.api.model.facebook.FacebookAttachmentRequest;
import com.konantech.konanlink.api.model.facebook.FacebookDownloadRequest;
import com.squareup.okhttp.ResponseBody;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

public interface FaceBookApiService {
    String ACCESS_TOKEN = "access_token";

    @POST
    Observable<ResponseBody> searchResult(
            @Url                 String url,
            @Query(ACCESS_TOKEN) String accessToken,
            @Body FacebookAttachmentRequest request
    );

    @POST
    Observable<ResponseBody> messageDownload(
            @Url                 String url,
            @Query(ACCESS_TOKEN) String accessToken,
            @Body FacebookAttachmentRequest request
    );
}
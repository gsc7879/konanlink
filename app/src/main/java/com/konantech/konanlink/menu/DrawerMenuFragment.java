package com.konantech.konanlink.menu;

import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dignara.lib.annotation.InjectView;
import com.dignara.lib.annotation.MenuFragmentInfo;
import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.fragment.MenuFragment;
import com.dignara.lib.utils.GAManager;
import com.dignara.lib.utils.IntentUtils;
import com.dignara.lib.utils.KeyboardUtils;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.GAConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.part.AccountInfoPartFragment;
import com.konantech.konanlink.part.AppInfoPartFragment;
import com.konantech.konanlink.part.DeviceAddPartFragment;
import com.konantech.konanlink.part.DeviceListPartFragment;
import com.konantech.konanlink.part.IndexingPartFragment;
import com.konantech.konanlink.part.NoticeFragment;
import com.konantech.konanlink.part.PasswordSettingsFragment;

@MenuFragmentInfo(layout = R.layout.menu_drawer)
public class DrawerMenuFragment extends MenuFragment {
    private static final int MENU_DEVICE_LIST  = 0;
    private static final int MENU_DEVICE_ADD   = 1;
    private static final int MENU_INDEXING     = 2;
    private static final int MENU_ACCOUNT_INFO = 3;
    private static final int MENU_PASSWORD     = 4;
    private static final int MENU_NOTICE       = 5;
    private static final int MENU_INFO         = 6;
    private static final int MENU_RATING       = 7;
    private static final int MENU_SHARE        = 8;

    @InjectView(id = R.id.layout_main_base)
    private DrawerLayout mDrawerLayout;

    @InjectView(id = R.id.menu_layout)
    private View mDrawer;

    @InjectView(id = R.id.img_menu_indexing)
    private SwitchCompat mIncludeIndexing;

    @InjectView(id = R.id.img_menu_notice_new)
    private ImageView mNoticeNew;

    @InjectView(id = R.id.text_popup_hint)
    private  TextView mPopupHint;

    @Override
    public void onInitView(View view) {
        addButton();
        if (!DataBank.getInstance().isHideDrawerMenu()) {
            onShowMenu();
            DataBank.getInstance().setHideDrawerMenu(true);
        }
    }

    private void setIndexingStatus() {
        boolean isIncludeIndexing = DataBank.getInstance().getDeviceInfo().isIndexing();
        mIncludeIndexing.setChecked(isIncludeIndexing);
    }

    private void setNoticeNew() {
        boolean isNewAnnounce = DataBank.getInstance().isHideNewNoti();
        mNoticeNew.setVisibility(isNewAnnounce ? View.GONE : View.VISIBLE);
    }

    private void setHintPopUp() {
        boolean isHintPopup = DataBank.getInstance().isAddedSearchAbleDevice();
        mPopupHint.setVisibility(isHintPopup ?  View.GONE : View.VISIBLE);
    }

    private void addButton() {
        addButton(MENU_DEVICE_LIST  , R.id.btn_menu_device_list);
        addButton(MENU_DEVICE_ADD   , R.id.btn_menu_device_add);
        addButton(MENU_INDEXING     , R.id.btn_menu_indexing);
        addButton(MENU_ACCOUNT_INFO , R.id.btn_menu_account_info);
        addButton(MENU_PASSWORD     , R.id.btn_menu_password);
        addButton(MENU_NOTICE       , R.id.btn_menu_notice);
        addButton(MENU_INFO         , R.id.btn_menu_info);
        addButton(MENU_RATING       , R.id.btn_menu_rating);
        addButton(MENU_SHARE        , R.id.btn_menu_share);
    }

    @Override
    protected boolean onClickMenuItem(int id, View menuButton) {
        switch (id) {
            case MENU_DEVICE_LIST:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(DeviceListPartFragment.class);
                        closeDrawer();
                    }
                });
                break;
            case MENU_DEVICE_ADD:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(DeviceAddPartFragment.class);
                        GAManager.getInstance().sendEvent(GAConst.CATEGORY_ADD, GAConst.EVENT_SERVICE);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_INDEXING:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(IndexingPartFragment.class);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_ACCOUNT_INFO:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(AccountInfoPartFragment.class);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_PASSWORD:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(PasswordSettingsFragment.class);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_NOTICE:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(NoticeFragment.class);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_INFO:
                AppManager.getInstance().detachAllSubPart();
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppManager.getInstance().attachFragment(AppInfoPartFragment.class);
                        closeDrawer();
                    }
                });
                return true;
            case MENU_RATING:
                GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_FEEDBACK);
                IntentUtils.openingMarket(getActivity(), getActivity().getPackageName());
                closeDrawer();
                return true;
            case MENU_SHARE:
                GAManager.getInstance().sendEvent(GAConst.CATEGORY_MENU, GAConst.EVENT_RECOMMEND);
                IntentUtils.sharing(getActivity(), getString(R.string.s25_text_about_konanlink));
                closeDrawer();
                return true;
        }
        return false;
    }

    @Override
    protected void onShowMenu() {
        KeyboardUtils.hideSoftInput(getActivity());
        setIndexingStatus();
        setNoticeNew();
        setHintPopUp();
        mDrawerLayout.openDrawer(mDrawer);
    }

    @Override
    protected void onHideMenu() {

    }

    @Override
    protected void onChangeMenu(int menuIndex) {
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mDrawer);
    }

    @Override
    protected boolean onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawer)) {
            closeDrawer();
            return true;
        }
        return false;
    }
}

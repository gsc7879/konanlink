package com.konantech.konanlink.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.ResultList;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.data.DataBank;

import java.io.IOException;

import rx.subscriptions.CompositeSubscription;

public class GCMRegistrationService extends IntentService {
    private static final String TAG = "GCMRegistrationService";
    private static final Object LOCK = new Object();

    private CompositeSubscription subscriptions;

    public GCMRegistrationService() {
        super(TAG);
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            synchronized (LOCK) {
                String senderId = getString(R.string.gcm_defaultSenderId);
                String token    = InstanceID.getInstance(this).getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                reqUpdatePushId(getApplicationContext(), token);
            }
        } catch (IOException e) {
        }
    }

    public void reqUpdatePushId(final Context context, String regId) {
        new DeviceApiManager().reqUpdateNotificationId(context, subscriptions, DataBank.getInstance().getDeviceKey(), regId, new Result.OnResultListener<ResultList<DeviceInfo>>() {
            @Override
            public void onSuccess(ResultList<DeviceInfo> data) {
            }

            @Override
            public void onFailed(int code, String msg) {
            }
        });
    }

    @Override
    public void onDestroy() {
        if (subscriptions != null && subscriptions.isUnsubscribed()) {
            subscriptions.clear();
        }
        super.onDestroy();
    }
}

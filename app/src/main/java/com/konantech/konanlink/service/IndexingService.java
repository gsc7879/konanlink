package com.konantech.konanlink.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class IndexingService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IndexingManager.getInstance().startTimer(this);
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        IndexingManager.getInstance().stopTimer();
        super.onDestroy();
    }
}

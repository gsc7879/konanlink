package com.konantech.konanlink.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.utils.ImageUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.GcmListenerService;
import com.konantech.konanlink.KonanLinkLoading;
import com.konantech.konanlink.R;
import com.konantech.konanlink.api.manager.AccountApiManager;
import com.konantech.konanlink.api.manager.AgentApiManager;
import com.konantech.konanlink.api.manager.DeviceApiManager;
import com.konantech.konanlink.api.manager.FaceBookApiManager;
import com.konantech.konanlink.api.manager.SystemApiManager;
import com.konantech.konanlink.api.model.Result;
import com.konantech.konanlink.api.model.account.AccountInfo;
import com.konantech.konanlink.api.model.agent.request.DownloadRequest;
import com.konantech.konanlink.api.model.agent.request.ThumbnailRequest;
import com.konantech.konanlink.api.model.device.DeviceInfo;
import com.konantech.konanlink.api.model.device.DeviceResultList;
import com.konantech.konanlink.api.model.facebook.FacebookAttachmentRequest;
import com.konantech.konanlink.api.model.facebook.FacebookDownloadRequest;
import com.konantech.konanlink.api.model.facebook.FacebookSearchRequest;
import com.konantech.konanlink.api.model.system.SystemInfo;
import com.konantech.konanlink.constant.DataIdConst;
import com.konantech.konanlink.constant.NetConst;
import com.konantech.konanlink.constant.NotificationConst;
import com.konantech.konanlink.constant.TypeConst;
import com.konantech.konanlink.dao.PhotoMetaInfo;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.db.PhotoMetaDBManager;
import com.konantech.konanlink.page.fragment.MainPageFragment;
import com.konantech.konanlink.part.DeviceListPartFragment;
import com.konantech.konanlink.part.IndexingPartFragment;
import com.konantech.konanlink.part.PaymentReqPartFragment;
import com.konantech.konanlink.part.SearchListPartFragment;
import com.konantech.konanlink.utils.BadgeUtil;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import rx.subscriptions.CompositeSubscription;

public class GCMListenerService extends GcmListenerService {
    private CompositeSubscription       mSubscriptions;
    private NotificationManager         mStatusNotiManager;
    private NotificationCompat.Builder  mStatusNotiBuilder;
    private NotificationManager         mNotifyManager;

    public int getUploadPercentage() {
        return mPercentage;
    }

    public void setUploadPercentage(int percentage) {
        this.mPercentage = percentage;
    }

    private int                         mPercentage;

    public GCMListenerService() {
        this.mSubscriptions = new CompositeSubscription();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String type = data.getString(NotificationConst.PARAM_TYPE);
        if  (!StringUtils.isBlank(type)) {
            newService(type, data);
        } else {
            oldService(data.getString(NotificationConst.PARAM_MSG));
        }
    }

    private void oldService(String msg) {

        if (StringUtils.isBlank(msg)) {
            return;
        }
        try {
            JSONObject jsonObject = new JSONObject(msg);
            String type = jsonObject.getString(NotificationConst.PARAM_NOTI_TYPE);
            switch (type) {
                case TypeConst.NOTI_CHANGE_DEVICE_INFO:
                    requestDeviceInfo();
                    break;
                case TypeConst.NOTI_DELETED_DEVICE:
                    deletedDevice();
                    break;
                case TypeConst.NOTI_CHANGE_USER_INFO:
                    requestUserInfo();
                    break;
                case TypeConst.NOTI_INFO_INDEXING:
                    showNotification(jsonObject.getString(NotificationConst.PARAM_MESSAGE));
                    break;
                case TypeConst.NOTI_NEW_ANNOUNCE:
                    requestCheckSystem(getApplicationContext());
                    break;

            }
        } catch (JSONException ignored) {
        } catch (Exception e) {
        }

    }

    private void newService(String type, Bundle data) {
        String message = data.getString(NotificationConst.PARAM_MESSAGE);
        if (StringUtils.isBlank(message)) {
            return;
        }
        try {
            switch (type) {
                case TypeConst.NOTI_DOWNLOAD:
                    mStatusNotiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mStatusNotiBuilder = new NotificationCompat.Builder(this);
                    requestUploadResponse(getApplicationContext(), message);
                    break;
                case TypeConst.NOTI_THUMBNAIL:
                    requestThumbNail(getApplicationContext(), message);
                    break;
                case TypeConst.NOTI_FACEBOOK_SEARCH:
                    requestFaceBookSearchList(getApplicationContext(), message);
                    break;
                case TypeConst.NOTI_FACEBOOK_DOWNLOAD:
                    requestFaceBookMessageDownload(message);
                    break;
            }
        } catch (Exception e) {

        }
    }

    private void requestCheckSystem(final Context context) {
        new SystemApiManager().reqSystemInfo(context, new CompositeSubscription(), new Result.OnResultListener<SystemInfo>() {
            @Override
            public void onSuccess(SystemInfo systemInfo) {

                DataBank.getInstance().setLastAnnounceDate(context, systemInfo.getLastAnnounceTime());
                DataBank.getInstance().setHideNewNoti(context, false);

                if (AppManager.getInstance().getPageManager() == null) {
                    BadgeUtil.updateBadgeCount(context, 1);
                } else {
                    AppManager.getInstance().sendData(MainPageFragment.class, DataIdConst.CHANGED_BADGE_STATUS);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
            }
        });
    }

    private void showNotification(String msg) {
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(NotificationConst.ID_INFO_REPORTING, getNotification(msg));
    }

    private Notification getNotification(String msg) {
        NotificationCompat.BigTextStyle style = new NotificationCompat.BigTextStyle();
        style.bigText(msg);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker(msg)
                .setSmallIcon(R.drawable.notification_konan)
                .setVibrate(TypeConst.PATTERN_VIBRATION)
                .setContentTitle(getString(R.string.s97_text_konanlink))
                .setContentText(msg)
                .setStyle(style)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(getAppIntent());
        return builder.build();
    }

    private void progressNotification(int transId, int iconResId, String ticker, String msg, boolean sound, int max, int progress) {
        mStatusNotiBuilder
                .setTicker(ticker)
                .setContentTitle(getApplicationContext().getString(R.string.s97_text_konanlink))
                .setContentText(msg)
                .setSmallIcon(iconResId)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(sound ?  RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION) : null)
                .setVibrate(sound ? TypeConst.PATTERN_VIBRATION : null)
                .setProgress(max, progress, false);
        mStatusNotiManager.notify(transId, mStatusNotiBuilder.build());
    }

    private PendingIntent getAppIntent() {
        Intent notificationIntent = new Intent(this, KonanLinkLoading.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
    }

    private void deletedDevice() {
        DataBank.getInstance().initPreferences(getApplicationContext());
        BadgeUtil.updateBadgeCount(getApplicationContext(), 0);
        System.exit(0);
    }

    private void requestDeviceInfo() {
        new DeviceApiManager().reqDeviceList(getBaseContext(), mSubscriptions, new Result.OnResultListener<DeviceResultList<DeviceInfo>>() {

            @Override
            public void onSuccess(DeviceResultList<DeviceInfo> data) {
                if (AppManager.getInstance().getPageManager() != null) {
                    AppManager.getInstance().sendData(new Class[]{SearchListPartFragment.class, DeviceListPartFragment.class, MainPageFragment.class, IndexingPartFragment.class}, DataIdConst.CHANGED_DEVICE_INFO);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
            }
        });
    }

    private void requestUserInfo() {
        new AccountApiManager().reqUsersInfo(getBaseContext(), mSubscriptions, new Result.OnResultListener<AccountInfo>() {
            @Override
            public void onSuccess(AccountInfo accountInfo) {
                if (AppManager.getInstance().getPageManager() != null) {
                    AppManager.getInstance().sendData(PaymentReqPartFragment.class, DataIdConst.CHANGED_USERS_INFO);
                }
            }

            @Override
            public void onFailed(int code, String msg) {
            }
        });
    }

    private void requestUploadResponse(final Context context, String msg) {
        try {
            DownloadRequest downloadRequest = new ObjectMapper().readValue(msg, DownloadRequest.class);
            String requestID        = downloadRequest.getRequestId();
            String path             = downloadRequest.getPath();
            String responseURL      = downloadRequest.getResponseURL();

            final File file = new File(path);
            if (file.exists() ) {
                String message = String.format(context.getString(R.string.s97_text_transmit_ready));
                progressNotification(NotificationConst.ID_DOWNLOAD_SERVER, R.drawable.notification_status_1, message, message, true, 100, 0);
                setUploadPercentage(0);
                new AgentApiManager(responseURL).reqUpload(getBaseContext(), mSubscriptions, requestID, file, new AgentApiManager.OnDownload() {
                    @Override
                    public void onSuccess(File downloadFile) {
                        String message = String.format(context.getString(R.string.s97_text_transmit_successfully), file.getName());
                        progressNotification(NotificationConst.ID_DOWNLOAD_SERVER, R.drawable.notification_status_2, message, message, false, 100, 100);
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                        String message = String.format(context.getString(R.string.s97_text_transmit_failed), file.getName());
                        progressNotification(NotificationConst.ID_DOWNLOAD_SERVER, R.drawable.notification_status_5, message, message, false, 100, getUploadPercentage());
                    }

                    @Override
                    public void onProgress(long transSize, long totalSize) {
                        String message = String.format(context.getString(R.string.s97_text_transmitting), file.getName());
                        setUploadPercentage( (int) (transSize * 100/ totalSize));
                        progressNotification(NotificationConst.ID_DOWNLOAD_SERVER, R.drawable.notification_status_3, message, message, false, 100,getUploadPercentage());
                    }
                });
            } else {
                // TODO: 파일이 존재 하지 않음 예외처리.
            }


        } catch (Exception e) {
        }
    }

    private void requestThumbNail(Context context, String msg) {
        try {
            ThumbnailRequest thumbnailRequest = new ObjectMapper().readValue(msg, ThumbnailRequest.class);
            String requestID        = thumbnailRequest.getRequestId();
            String path             = thumbnailRequest.getPath();
            String responseURL      = thumbnailRequest.getResponseURL();
            int width               = thumbnailRequest.getWidth();
            int height              = thumbnailRequest.getHeight();

            File file = new File(path);
            if (file.exists() ) {
                byte[] bytes = bitmapToByteArray(Bitmap.createScaledBitmap(ImageUtils.getBitmap(getApplicationContext(), Uri.fromFile(file), 1024), width, height, false));
                new AgentApiManager(responseURL).reqUpload(context, mSubscriptions, requestID, file.getName(), MediaType.parse(new MimetypesFileTypeMap().getContentType(file)), bytes.length, new ByteArrayInputStream(bytes), new AgentApiManager.OnDownload() {
                    @Override
                    public void onSuccess(File downloadFile) {
                    }

                    @Override
                    public void onFailed(int code, String msg) {
                    }

                    @Override
                    public void onProgress(long transSize, long totalSize) {
                    }
                });
            } else {
                // TODO: 파일이 존재 하지 않음 예외처리.
            }


        } catch (Exception e) {
        }
    }

    private void requestFaceBookSearchList(Context context, String msg) {
        try {
            FacebookSearchRequest facebookSearchRequest = new ObjectMapper().readValue(msg, FacebookSearchRequest.class);
            String userID           = facebookSearchRequest.getUserId();
            String senderID         = facebookSearchRequest.getSenderId();
            String responseURL      = facebookSearchRequest.getResponseURL();
            String accessToken      = facebookSearchRequest.getAccessToken();
            String keyword          = facebookSearchRequest.getKeyword();
            String deviceName       = facebookSearchRequest.getDeviceName();
            int page                = facebookSearchRequest.getPage();
            int size                = facebookSearchRequest.getSize();

            PhotoMetaDBManager.SearchResult searchResult = PhotoManager.getInstance().searchPhotoList(context, keyword, page * size, size);
            List<FacebookAttachmentRequest.Element> elements = searchPhotoElementList(searchResult, deviceName, userID, senderID, accessToken);
            List<FacebookAttachmentRequest.Button> moreButtons = new ArrayList<>();
            if (searchResult.isMore()) {
                facebookSearchRequest.setPage(page + 1);
                moreButtons.add(new FacebookAttachmentRequest.Button(FacebookAttachmentRequest.BUTTON_TYPE_POSTBACK, "view more", null, new ObjectMapper().writeValueAsString(facebookSearchRequest)));
            }

            FacebookAttachmentRequest attachmentRequest = new FacebookAttachmentRequest(senderID, elements, moreButtons.size() != 0 ? moreButtons : null);
            new FaceBookApiManager().reqPhotoSearchList(mSubscriptions, responseURL, attachmentRequest, accessToken, new Result.OnResultListener<ResponseBody>() {
                @Override
                public void onSuccess(ResponseBody data) {
                }

                @Override
                public void onFailed(int code, String msg) {
                }
            });
        } catch (Exception e) {
        }
    }

    private List<FacebookAttachmentRequest.Element> searchPhotoElementList(PhotoMetaDBManager.SearchResult searchResult, String deviceName, String userID, String senderID, String accessToken) {
        List<FacebookAttachmentRequest.Element> elements = new ArrayList<>();
        try {
            FacebookAttachmentRequest.Element firstElement = new FacebookAttachmentRequest.Element();
            firstElement.setTitle(deviceName != null ? deviceName : "");
            firstElement.setSubTitle(String.format("%d / %d", searchResult.getCurrPage() + 1, searchResult.getTotalPage()));
            firstElement.setImageUrl(String.format("%s/assets/service/main/imgs/bg-facebook.jpg",  NetConst.getInstance().getLinkUrl()));
            elements.add(firstElement);

            for (PhotoMetaInfo metaInfo : searchResult.getViewList()) {
                FacebookAttachmentRequest.Element element = new FacebookAttachmentRequest.Element();
                element.setTitle(metaInfo.getFileName());
                element.setSubTitle(metaInfo.getMetaInfo());
                element.setImageUrl(String.format("%s/v1/facebook/thumbnail/%s?path=%s&width=300&height=300",  NetConst.getInstance().getLinkUrl(), userID, URLEncoder.encode(metaInfo.getFileAbsPath(), "UTF-8")));

                // buttons
                List<FacebookAttachmentRequest.Button> buttons = new ArrayList<>();
                FacebookAttachmentRequest.Button button = new FacebookAttachmentRequest.Button(FacebookAttachmentRequest.BUTTON_TYPE_POSTBACK, "download", null, new ObjectMapper().writeValueAsString(new FacebookDownloadRequest(userID, senderID, accessToken, metaInfo.getFileAbsPath())));
                buttons.add(button);
                element.setButtons(buttons);
                elements.add(element);
            }
        } catch (UnsupportedEncodingException e) {
        } catch (JsonProcessingException e1) {
        }
        return elements;
    }

    private void requestFaceBookMessageDownload(String msg) {
        try {
            FacebookDownloadRequest facebookDownloadRequest = new ObjectMapper().readValue(msg, FacebookDownloadRequest.class);
            String userID       = facebookDownloadRequest.getUserId();
            String senderID     = facebookDownloadRequest.getSenderId();
            String accessToken  = facebookDownloadRequest.getAccessToken();
            String responseURL  = facebookDownloadRequest.getResponseURL();
            String itemUrl      = String.format("%s/v1/facebook/download/%s?path=%s", NetConst.getInstance().getLinkUrl(), userID, facebookDownloadRequest.getPath());

            FacebookAttachmentRequest attachmentRequest = new FacebookAttachmentRequest(senderID, new FacebookAttachmentRequest.Message(new FacebookAttachmentRequest.Attatchment(FacebookAttachmentRequest.ATTACHMENT_TYPE_IMAGE, new FacebookAttachmentRequest.Payload(itemUrl, true))));
            new FaceBookApiManager().reqPhotoMessageUpload(mSubscriptions, responseURL, attachmentRequest, accessToken, new Result.OnResultListener<ResponseBody>() {
                @Override
                public void onSuccess(ResponseBody data) {
                }

                @Override
                public void onFailed(int code, String msg) {
                }
            });
        } catch (Exception e) {
        }
    }

    public byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
        bitmap.compress( Bitmap.CompressFormat.JPEG, 100, stream) ;
        byte[] byteArray = stream.toByteArray() ;
        return byteArray ;
    }

    @Override
    public void onDestroy() {
        if (mSubscriptions != null && mSubscriptions.isUnsubscribed()) {
            mSubscriptions.clear();
        }
        super.onDestroy();
    }
}

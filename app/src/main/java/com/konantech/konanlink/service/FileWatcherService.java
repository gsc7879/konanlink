package com.konantech.konanlink.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;

import com.konantech.konanlink.utils.PhotoUtil;

import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import java.io.File;


public class FileWatcherService extends Service {
    private static final String path =  Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
    private static final String TAG = "FileWatcherService";
    private FileAlterationMonitor mMonitor;

    @Override
    public void onCreate(){
//        Toast.makeText(this,"Service가 생성되었습니다.", Toast.LENGTH_LONG).show();

        FileFileFilter fileFilter = new FileFileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isDirectory() == true) {
                    return !file.getName().startsWith(".");
                }
                else {
                    return PhotoUtil.isImageFile(file);
                }
            }
        };

        mMonitor = new FileAlterationMonitor(10000*6*2);
        FileAlterationObserver observer = new FileAlterationObserver(path, fileFilter);
        FileAlterationListener listener = new FileAlterationListenerAdaptor() {
            @Override
            public void onFileCreate(final File file) {
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        PhotoManager.getInstance().insertFile(getApplicationContext(), file);
                        return "";
                    }

                    @Override
                    protected void onPostExecute(String folderPath) {
                    }

                    @Override
                    protected void onCancelled(String o) {
                    }
                }.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);

            }

            @Override
            public void onFileDelete(final File file) {
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        PhotoManager.getInstance().deleteFile(getApplicationContext(), file);
                        return "";
                    }

                    @Override
                    protected void onPostExecute(String folderPath) {
                    }

                    @Override
                    protected void onCancelled(String o) {
                    }
                }.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);


            }

            @Override
            public void onFileChange(final File file) {
                new AsyncTask<Void, Void, String>() {
                    @Override
                    protected String doInBackground(Void... params) {
                        return "";
                    }

                    @Override
                    protected void onPostExecute(String folderPath) {
                    }

                    @Override
                    protected void onCancelled(String o) {
                    }
                }.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);


            }
        };
        observer.addListener(listener);
        mMonitor.addObserver(observer);

    }

    @Override
    public void onDestroy(){
//        Toast.makeText(this, "Service가 중지되었습니다.", Toast.LENGTH_LONG).show();
        try {
            mMonitor.stop();
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags,int startId){
//        Toast.makeText(this,"Service가 시작되었습니다.", Toast.LENGTH_LONG).show();
        try {
            mMonitor.start();
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return START_REDELIVER_INTENT;
    }
}

package com.konantech.konanlink.service;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.konantech.konanlink.PasswordDialog;
import com.konantech.konanlink.data.DataBank;

public class PassCodeBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Intent.ACTION_SCREEN_ON:
                if (DataBank.getInstance().isUsePassword()) {
                    Intent i = new Intent(context.getApplicationContext(), PasswordDialog.class);
                    PendingIntent pi = PendingIntent.getActivity(context.getApplicationContext(), 0, i, PendingIntent.FLAG_ONE_SHOT);
                    try {
                        pi.send();
                    } catch (Exception e) {
                    }
                }
                break;
        }
    }
}
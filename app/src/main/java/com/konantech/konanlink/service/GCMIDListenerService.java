package com.konantech.konanlink.service;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class GCMIDListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, GCMRegistrationService.class);
        startService(intent);
    }
}
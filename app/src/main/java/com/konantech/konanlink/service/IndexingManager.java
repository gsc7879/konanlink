package com.konantech.konanlink.service;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.view.View;

import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.R;
import com.konantech.konanlink.constant.NotificationConst;
import com.konantech.konanlink.constant.TypeConst;
import com.konantech.konanlink.data.DataBank;
import com.konantech.konanlink.task.info.ReqSendMetaTask;

import java.util.Calendar;

import rx.subscriptions.CompositeSubscription;

public class IndexingManager {
    private static IndexingManager uniqueIndexingManager;

    private static final int DEFAULT_INTERVAL_HOUR = 24;
    private static final int REQUEST_CODE          = 234324243;

    private static PendingIntent pendingIntent;
    private static AlarmManager  alarmManager;

    private static ReqSendMetaTask mReqSendMetaTask;

    private static int count;

    public synchronized static IndexingManager getInstance() {
        if (uniqueIndexingManager == null) {
            uniqueIndexingManager = new IndexingManager();
        }
        return uniqueIndexingManager;
    }

    public void stopTimer() {
        if (alarmManager != null && pendingIntent != null) {
            alarmManager.cancel(pendingIntent);
        }
    }

    public void startTimer(Context context) {
        stopTimer();
        if (alarmManager == null) {
            alarmManager = (AlarmManager) context.getSystemService(Activity.ALARM_SERVICE);
        }

        if (pendingIntent == null) {
            pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), REQUEST_CODE, new Intent(context, IndexingScheduleReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 1);
        alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), DEFAULT_INTERVAL_HOUR * 360000L, pendingIntent);
    }

    public void updateIndex(Context context, CompositeSubscription subscription, View view, boolean isRecreate, String hostKey) {
        if (DataBank.getInstance().getDeviceInfo().isIndexing()) {
            requestAgentClient(context, subscription, view, isRecreate, hostKey);
        } else {
            if (!DataBank.getInstance().existApiKey() && count++ % 4 == 0) {
                showNotification(context, R.string.s97_notification_join_now);
            }
        }
    }

    public boolean isIndexing() {
        return mReqSendMetaTask != null;
    }

    private void sendMeta(final Context context, CompositeSubscription subscription, final View view, boolean isRecreate, String hostKey) {
        if (mReqSendMetaTask != null && view != null) {
            showMessage(context, view, R.string.s32_toast_updating, Snackbar.LENGTH_LONG);
            return;
        }

        mReqSendMetaTask = new ReqSendMetaTask(0, new WorkTask.OnTaskListener() {
            @Override
            public void onStartTask(int callBackId) {
                if (view != null) {
                    showMessage(context, view, R.string.s32_toast_updating, Snackbar.LENGTH_LONG);
                }
            }

            @Override
            public void onEndTask(int callBackId, Object result) {
                int[] resultCount     = (int[]) result;
                int modifiedFileCount = resultCount[ReqSendMetaTask.MODIFIED_FILE_COUNT];
                int deletedFileCount  = resultCount[ReqSendMetaTask.DELETED_FILE_COUNT];

                if (deletedFileCount > 0 || modifiedFileCount > 0) {
                    showMessage(context, view, R.string.s32_toast_update_successfully, Snackbar.LENGTH_LONG);
                }
                mReqSendMetaTask = null;
            }

            @Override
            public void onCancelTask(int callBackId, int cancelCode, Object data) {
                if (view != null) {
                    showMessage(context, view, R.string.s32_error_update_failed, Snackbar.LENGTH_LONG);
                }
                mReqSendMetaTask = null;
            }

            @Override
            public void onTaskProgress(int callBackId, Object... params) {
            }
        });
        mReqSendMetaTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, context, subscription, hostKey, isRecreate);
    }

    private void requestAgentClient(final Context context, CompositeSubscription subscription, final View view, final boolean isRecreate, String hostKey) {
        sendMeta(context, subscription, view, isRecreate, hostKey);
    }

    private void showMessage(Context context, View view, int msgResId, int duration) {
        if (view == null) {
            showNotification(context, msgResId);
        } else {
            Snackbar.make(view, msgResId, duration).show();
        }
    }

    private void showNotification(Context context, int msgResId) {
        NotificationManager mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyManager.notify(NotificationConst.ID_INFO_INDEXING, getNotification(context, context.getString(msgResId)));
    }

    private Notification getNotification(Context context, String msg) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setTicker(msg)
                .setSmallIcon(R.drawable.notification_konan)
                .setVibrate(TypeConst.PATTERN_VIBRATION)
                .setContentTitle(context.getString(R.string.s97_text_konanlink))
                .setContentText(msg)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        return builder.build();
    }

}

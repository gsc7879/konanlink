package com.konantech.konanlink.service;

import android.content.Context;
import android.os.AsyncTask;

import com.dignara.lib.fragment.AppManager;
import com.dignara.lib.task.WorkTask;
import com.konantech.konanlink.dao.PhotoMetaInfo;
import com.konantech.konanlink.db.PhotoMetaDBManager;
import com.konantech.konanlink.task.info.ReqSendPhotoMetaTask;

import java.io.File;
import java.util.List;

public class PhotoManager {
    private static ReqSendPhotoMetaTask mReqSendMetaTask;

    private static PhotoManager uniquePhotoManager;
    public synchronized static PhotoManager getInstance() {
        if (uniquePhotoManager == null) {
            uniquePhotoManager = new PhotoManager();
        }
        return uniquePhotoManager;
    }

    public  void insertFile(Context context, File file) {
        new ReqSendPhotoMetaTask(context, 0).insertOrReplacePhotoMetaInfo(context, file);
    }

    public  void deleteFile(Context context, File file) {
        new ReqSendPhotoMetaTask(context, 0).deletePhotoMetaInfo(file);
    }

    public PhotoMetaDBManager.SearchResult searchPhotoList(Context context, String keywords, int offset, int count) {
        return new ReqSendPhotoMetaTask(context, 0).searchPhotoMetaInfoList(keywords, offset, count);
    }

    public boolean isFileExist(Context context, File file) {
        return new ReqSendPhotoMetaTask(context, 0).isFileExist(file);

    }

    public void collectPhotoMetaInfo(Context context, boolean isRecreate) {
        sendMeta(context, isRecreate);
    }

    private void sendMeta(final Context context, boolean isRecreate) {
        if (mReqSendMetaTask != null && AppManager.getInstance().getPageManager() != null) {
            return;
        }

        mReqSendMetaTask = new ReqSendPhotoMetaTask(0, new WorkTask.OnTaskListener() {
            @Override
            public void onStartTask(int callBackId) {
            }

            @Override
            public void onEndTask(int callBackId, Object result) {
                mReqSendMetaTask = null;
            }

            @Override
            public void onCancelTask(int callBackId, int cancelCode, Object data) {
                mReqSendMetaTask = null;
            }

            @Override
            public void onTaskProgress(int callBackId, Object... params) {
            }
        });

        try {
            mReqSendMetaTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, context, isRecreate);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}

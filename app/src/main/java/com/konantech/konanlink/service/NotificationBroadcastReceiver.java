package com.konantech.konanlink.service;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    public static final String CANCEL = "CANCEL_ACTION";
    public static final String DELETE = "DELETE_ACTION";

    public static final String NOTIFICATION_ID = "id";
    public static final String FILEPATH        = "path";

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case CANCEL:
                cancelNotification(context, intent);
                break;
            case DELETE:
                File file = FileUtils.getFile(intent.getExtras().getString(FILEPATH));
                try {
                    FileUtils.forceDelete(file);
                    cancelNotification(context, intent);
                } catch (IOException ignored) {
                }
                break;
        }
    }

    private void cancelNotification(Context context, Intent intent) {
        NotificationManager manager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        manager.cancel(intent.getIntExtra(NOTIFICATION_ID, -1));
    }
}
